<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hotel_name' => 'required|max:255',
            'hotel_address' => 'required',
            'hotel_city' => 'required|max:255',
            'hotel_rate' => 'required',
            'hotel_status' => 'required',
            'chse_score' => 'max:100'
        ];
    }
}
