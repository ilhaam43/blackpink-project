<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TripItineraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'travel_package_id' => 'unique:App\Models\TripItinerary,travel_package_id|required|exists:App\Models\TravelPackage,id',
            'highlights' => 'required|max:255',
            'pdf_file' => 'mimes:pdf',
            'day1' => 'required',
            'day2' => 'required'
        ];
    }
}
