<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TransportationEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:255|unique:Transportations,title,'. request()->route()->parameters['transportation'],
            'type'      => 'required',
            'capacity'  => 'numeric',
            'region'    => 'max:255',
            'note'      => 'max:255',
            'image'     => 'image',
        ];
    }
}
