<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TravelPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255|unique:App\Models\TravelPackage,title',
            'region' => 'required|max:255',
            'about' => 'required',
            'duration' => 'required|max:255',
            'flight' => 'max:255',
            'accomodation' => 'max:255',
            'land_transport' => 'max:255',
            'meal' => 'max:255',
            'travel_docs' => 'max:255',
            'special' => 'boolean',
            'category_id' => 'exists:App\Models\DestinationCategory,id',
            'is_available' => 'boolean'
        ];
    }
}
