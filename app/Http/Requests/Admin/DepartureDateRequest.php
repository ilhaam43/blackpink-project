<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class DepartureDateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'travel_package_code' => 'required|exists:App\Models\TravelPackage,code',
            'departure' => 'required',
            'arrival' => 'required'
        ];
    }

    // public function messages(){
    //     return [
    //         'travel_package_code.required' => 'Code is required',
    //         'travel_package_code.unique' => 'Please enter a valid code',
    //         'departure.required' => 'Date is required'
    //     ];
    // }
}