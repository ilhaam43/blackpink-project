<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HotelRoomPriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_occupant' => 'required',
            'room_price' => 'required',
            'hotel_id' => 'required|exists:App\Models\Hotel,id',
            'room_id' => 'required|exists:App\Models\HotelRoom,id'
        ];
    }
}
