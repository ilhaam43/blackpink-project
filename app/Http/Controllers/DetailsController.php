<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\TravelPackage;
use App\Models\Hotel;

class DetailsController extends Controller
{
    public function index($slug){
        $item = TravelPackage::with(['travelGalleries', 'departureDates', 'tripItinerary', 'inclusive'])
            ->where('slug', $slug)
            ->firstOrFail();
        $now= \Carbon\Carbon::now();
        
        if($item->tripItinerary){
            $days = [
                $item->tripItinerary->day1,
                $item->tripItinerary->day2,
                $item->tripItinerary->day3,
                $item->tripItinerary->day4,
                $item->tripItinerary->day5,
                $item->tripItinerary->day6,
                $item->tripItinerary->day7,
                $item->tripItinerary->day8,
                $item->tripItinerary->day9,
                $item->tripItinerary->day10,
                $item->tripItinerary->day11,
                $item->tripItinerary->day12,
                $item->tripItinerary->day13,
                $item->tripItinerary->day14,
                $item->tripItinerary->day15,
                $item->tripItinerary->day16,
                $item->tripItinerary->day17,
                $item->tripItinerary->day18,
                $item->tripItinerary->day19,
                $item->tripItinerary->day20,
                $item->tripItinerary->day21,
                $item->tripItinerary->day22,
                $item->tripItinerary->day23,
                $item->tripItinerary->day24,
                $item->tripItinerary->day25,
                $item->tripItinerary->day26,
                $item->tripItinerary->day27,
                $item->tripItinerary->day28,
                $item->tripItinerary->day29,
                $item->tripItinerary->day30
            ];

            if($item->inclusive){
            $incs = [
                $item->inclusive->incl1,
                $item->inclusive->incl2,
                $item->inclusive->incl3,
                $item->inclusive->incl4,
                $item->inclusive->incl5,
                $item->inclusive->incl6,
                $item->inclusive->incl7,
                $item->inclusive->incl8,
                $item->inclusive->incl9,
                $item->inclusive->incl10,
                $item->inclusive->incl11,
                $item->inclusive->incl12,
                $item->inclusive->incl13,
                $item->inclusive->incl14,
                $item->inclusive->incl15,
                $item->inclusive->incl16,
                $item->inclusive->incl17,
                $item->inclusive->incl18,
                $item->inclusive->incl19,
                $item->inclusive->incl20,
                $item->inclusive->uincl1,
                $item->inclusive->uincl2,
                $item->inclusive->uincl3,
                $item->inclusive->uincl4,
                $item->inclusive->uincl5,
                $item->inclusive->uincl6,
                $item->inclusive->uincl7,
                $item->inclusive->uincl8,
                $item->inclusive->uincl9,
                $item->inclusive->uincl10,
                $item->inclusive->uincl11,
                $item->inclusive->uincl12,
                $item->inclusive->uincl13,
                $item->inclusive->uincl14,
                $item->inclusive->uincl15,
                $item->inclusive->uincl16,
                $item->inclusive->uincl17,
                $item->inclusive->uincl18,
                $item->inclusive->uincl19,
                $item->inclusive->uincl20
            ];
        }
        }else{
            $days = [];
            $incs = [];
        }

        return view('pages.details.travel-packages', [
            'item' => $item,
            'days' => $days,
            'incs' => $incs ?? "",
            'now' => $now
        ]);
    }

    public function quarantineHotels($slug){
        $item = Hotel::with('hotelPrices')->where('slug', $slug)->firstOrFail();

        $rooms= $item->hotelPrices;

        $rooms = $rooms->groupBy('room_id');
        
        return view('pages.details.quarantine-hotels', [
            'item'=> $item,
            'rooms' => $rooms
        ]);
    }

    public function careers($slug){
        $item = Career::where('slug', $slug)->first();
        return view('pages.details.career', [
            'item' => $item
        ]);
    }
}