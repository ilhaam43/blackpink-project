<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Section::all()->sortBy('sort_number');
        return view('pages.admin.arrange-sections.index', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCheckbox(Request $request, $id)
    {
        $section = Section::where('id', $id)->update([
            'is_show' => $request->is_show
        ]);

        return response()->json($section);
    }
}
