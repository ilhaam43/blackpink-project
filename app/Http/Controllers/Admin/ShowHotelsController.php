<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\ShowHotels;
use App\Models\Hotel;

class ShowHotelsController extends Controller
{
    public function index()
    {
        $items = ShowHotels::with('hotel')->get();
        return view('pages.admin.show-quarantine-hotel.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        $quarantineHotels = Hotel::where('hotel_status', 1)->get();
        return view('pages.admin.show-quarantine-hotel.create', [
            'quarantineHotels' => $quarantineHotels
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $addquarantineHotels = ShowHotels::create($data);

        return redirect()->route('show-quarantine-hotel.index')->with('success', 'Quarantine hotel created successfully');
    }

    public function edit($id)
    {
        $data = ShowHotels::where('id', $id)->first();
        $quarantineHotels = Hotel::where('hotel_status', 1)->get();
        return view('pages.admin.show-quarantine-hotel.edit', [
            'quarantineHotels' => $quarantineHotels,
            'id' => $data->id
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $updateQuarantineHotel = ShowHotels::findorfail($id)->update($data);

        return redirect()->route('show-quarantine-hotel.index')->with('success', 'Quarantine hotel updated successfully');
    }

    public function destroy($id)
    {
        $deleteData = ShowHotels::destroy($id);
        return redirect()->route('show-quarantine-hotel.index')->with('success', 'Quarantine hotel deleted successfully');
    }
}
