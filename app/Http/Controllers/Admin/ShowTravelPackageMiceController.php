<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShowTravelPackageMice;
use App\Models\TravelPackage;
use Illuminate\Http\Request;

class ShowTravelPackageMiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ShowTravelPackageMice::with('travelPackage')->get();
        return view('pages.admin.show-travel-package-mice.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $travelPackages = TravelPackage::where('is_tours', 0)->where('is_available', 1)->where('is_mice', 1)->get();
        return view('pages.admin.show-travel-package-mice.create', [
            'travelPackages' => $travelPackages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        ShowTravelPackageMice::create($data);

        return redirect()->route('show-travel-package-mice.index')->with('success', 'Show travel package MICE created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ShowTravelPackageMice::where('id', $id)->first();
        $travelId = $data->travel_package_id;
        $travelPackages = TravelPackage::where('is_tours', 0)->where('is_available', 1)->where('is_mice', 1)->get();
        return view('pages.admin.show-travel-package-mice.edit', [
            'travelPackages' => $travelPackages,
            'travelId' => $travelId,
            'id' => $data->id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        ShowTravelPackageMice::findorfail($id)->update($data);

        return redirect()->route('show-travel-package-mice.index')->with('success', 'Travel package updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShowTravelPackageMice::destroy($id);
        return redirect()->route('show-travel-package-mice.index')->with('success', 'Travel package deleted successfully');
    }
}
