<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TourOffer;
use App\Http\Requests\Admin\TourRequest;

class TourOfferController extends Controller
{
    public function index(){
        $items = TourOffer::all();
        return view('pages.admin.tours.index',[
            'items' => $items
        ]);
    }

    public function create()
    {
        return view('pages.admin.tours.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TourRequest $request)
    {
        $data = $request->all();
        if($request->file('tour_image') != ''){
            $data['tour_image'] = $request->file('tour_image')->store(
                'assets/tour-offer', 'public'
            );
        }
        
        TourOffer::create($data);
        return redirect()->route('tour-offers.index');
    }

    public function edit($id)
    {
        $item = TourOffer::findOrFail($id);
        return view('pages.admin.tours.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TourRequest $request, $id)
    {
        $data = $request->all();
        $tour = TourOffer::findorfail($id);

        if ($request->file('tour_image') == '') {
            $data['tour_image']=$tour->tour_image;
        } else {
            $data['tour_image'] = $request->file('tour_image')->store(
                'assets/tour-offer', 'public'
            );
        }
        $tour->update($data);
        return redirect()->route('tour-offers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TourOffer::destroy($id);
        return redirect()->route('tour-offers.index');
    }
}
