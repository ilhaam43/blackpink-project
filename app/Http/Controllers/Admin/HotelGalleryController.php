<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\HotelGalleryRequest;
use App\Models\HotelGallery;
use App\Models\Hotel;
use Illuminate\Http\Request;
use File;

class HotelGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = HotelGallery::all();
        return view('pages.admin.quarantine-hotel-gallery.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hotels = Hotel::all();
        return view('pages.admin.quarantine-hotel-gallery.create', [
            'hotels' => $hotels
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelGalleryRequest $request)
    {
        $data = $request->all();
        $file = $request->file('image');

        if ($file == '') {
            $request->image = '';
        }else{
            $imageName = $request->file('image')->getClientOriginalName();
            $request->image->move(public_path('storage/assets/quarantine-hotel/'), $imageName);
            $data['image'] = 'assets/quarantine-hotel/' . $imageName;
        }

        HotelGallery::create($data);

        return redirect()->route('quarantine-hotel-gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = HotelGallery::find($id);
        $hotels = Hotel::all();
        return view('pages.admin.quarantine-hotel-gallery.edit', [
            'gallery' => $gallery,
            'hotels' => $hotels
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelGalleryRequest $request, $id)
    {
        $data = $request->all();
        $gallery = HotelGallery::findorfail($id);
        $file = $request->file('image');

        if ($file == '') {
            $request->image = $gallery->image;
        } else {
            $deletePath = public_path('storage/'.$gallery->image);
            if(File::exists($deletePath)) {
                $deleteImage = File::delete($deletePath);
            }
            $imageName = $request->file('image')->getClientOriginalName();
            $request->image->move(public_path('storage/assets/quarantine-hotel/'), $imageName);
            $data['image'] = 'assets/quarantine-hotel/' . $imageName;
        }
        
        $gallery->update($data);

        return redirect()->route('quarantine-hotel-gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotelGallery = HotelGallery::findOrFail($id);
        $deletePath = public_path('storage/'.$hotelGallery->image);

        if(File::exists($deletePath)) {
            $deleteImage = File::delete($deletePath);
        }

        $deleteData = HotelGallery::destroy($id);
        return redirect()->route('quarantine-hotel-gallery.index');
    }
}
