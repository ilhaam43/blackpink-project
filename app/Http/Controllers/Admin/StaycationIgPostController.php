<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StaycationIgpost;
use App\Http\Requests\Admin\StaycationIgPostRequest;
use App\Http\Requests\Admin\StaycationIgPostEditRequest;

class StaycationIgPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = StaycationIgpost::all();
        return view('pages.admin.staycation-ig-post.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.staycation-ig-post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaycationIgPostRequest $request)
    {
        $data = $request->all();
        StaycationIgpost::create($data);
        return redirect()->route('staycation-ig-post.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = StaycationIgpost::findOrFail($id);
        return view('pages.admin.staycation-ig-post.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StaycationIgPostRequest $request, $id)
    {
        $data = $request->all();
        StaycationIgpost::findorfail($id)->update($data);
        return redirect()->route('staycation-ig-post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StaycationIgpost::findorfail($id)->delete();
        return redirect()->route('staycation-ig-post.index');
    }
}
