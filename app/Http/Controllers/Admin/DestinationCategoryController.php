<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DestinationCategory;
use App\Http\Requests\Admin\DestinationCategoryRequest;
use File;


class DestinationCategoryController extends Controller
{
    public function index()
    {
        $items = DestinationCategory::all();
        return view('pages.admin.destination-category.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        return view('pages.admin.destination-category.create');
    }

    public function edit($id)
    {
        $item = DestinationCategory::findOrFail($id);
        return view('pages.admin.destination-category.edit', [
            'item' => $item
        ]);
    }

    public function store(DestinationCategoryRequest $request)
    {
        $data = $request->all();
        $file = $request->file('category_image');

        if ($file == '') {
            $request->category_image = '';
        }else{
            $imageName = $request->file('category_image')->getClientOriginalName();
            $uploadImage = $request->category_image->move(public_path('storage/assets/destination-category/'), $imageName);
            $data['category_image'] = 'assets/destination-category/' . $imageName;
        }

        $create = DestinationCategory::create($data);
        return redirect()->route('destination-category.index');
    }

    public function update(DestinationCategoryRequest $request, $id)
    {
        $data = $request->all();
        $categories = DestinationCategory::findorfail($id);
        if ($request->file('category_image') == '') {
            $data['category_image']= $categories->category_image;
        } else {
            $deletePath = public_path('storage/'.$categories->image);
            if(File::exists($deletePath)) {
                $deleteImage = File::delete($deletePath);
            }
            $imageName = $request->file('category_image')->getClientOriginalName();
            $uploadImage = $request->category_image->move(public_path('storage/assets/destination-category/'), $imageName);
            $data['category_image'] = 'assets/destination-category/' . $imageName;
        }
        $updateData = $categories->update($data);
        return redirect()->route('destination-category.index');
    }

    public function destroy($id)
    {
        $destinationCategory = DestinationCategory::findOrFail($id);
        $deletePath = public_path('storage/'.$destinationCategory->category_image);

        if(File::exists($deletePath)) {
            $deleteImage = File::delete($deletePath);
        }
        $deleteData = DestinationCategory::destroy($id);
        return redirect()->route('destination-category.index');
    }
}