<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\ShowTourEvents;
use App\Models\TravelPackage;

class ShowTourEventsController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $items = ShowTourEvents::with('TravelPackage')->get();
        return view('pages.admin.show-tour-events.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        $tourEvents = TravelPackage::where('is_tours', 1)->where('is_available', 1)->get();
        return view('pages.admin.show-tour-events.create', [
            'tourEvents' => $tourEvents
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $addShowTourEvents = ShowTourEvents::create($data);

        return redirect()->route('show-tour-events.index')->with('success', 'Travel package created successfully');
    }

    public function edit($id)
    {
        $data = ShowTourEvents::where('id', $id)->first();
        $tourEventsId = $data->travel_package_id;
        $tourEvents = TravelPackage::where('is_tours', 1)->where('is_available', 1)->get();
        return view('pages.admin.show-tour-events.edit', [
            'tourEvents' => $tourEvents,
            'tourEventsId' => $tourEventsId,
            'id' => $data->id
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $updateShowTourEvents = ShowTourEvents::findorfail($id)->update($data);

        return redirect()->route('show-tour-events.index')->with('success', 'Travel package updated successfully');
    }

    public function destroy($id)
    {
        $deleteData = ShowTourEvents::destroy($id);
        return redirect()->route('show-tour-events.index')->with('success', 'Travel package deleted successfully');
    }
}
