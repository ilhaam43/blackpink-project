<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BookingTour;
use App\Models\TravelPackage;
use App\Models\Passenger;
use App\Models\PassengerPic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\BookingTourStatus;
use PDF;

class BookingTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = BookingTour::orderBy('id', 'DESC')->get();
        $allStatus = BookingTourStatus::all();
        $waitStatus = BookingTourStatus::where('id', '6')->first();
        $requestVendorStatus = BookingTourStatus::where('id', '7')->first();
        $processStatus = BookingTourStatus::where('id', '8')->first();
        $cancelStatus = BookingTourStatus::where('id', '11')->first();
        $holdStatus = BookingTourStatus::where('id', '10')->first();
        return view('pages.admin.booking-tour.index', [
            'items' => $items,
            'allStatus' => $allStatus,
            'waitStatus' => $waitStatus,
            'requestVendorStatus' => $requestVendorStatus,
            'cancelStatus' => $cancelStatus,
            'processStatus' => $processStatus,
            'holdStatus' => $holdStatus
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = BookingTour::find($id);
        return view('pages.admin.booking-tour.details', [
            'item' => $item
        ]);
    }

    public function exportPDF($id) {
        $bookingData = BookingTour::findorfail($id);
        if($bookingData->status_id == 1){
            $bookingData->update([
                'status_id' => 2
            ]);
        }
        // retreive all records from db
        $data = [
            'item' => BookingTour::find($id)
        ];
        $pdf = PDF::loadView('pages.admin.booking-tour.pdf', $data);

        // download PDF file with download method
        return $pdf->stream('Booking'. BookingTour::find($id)->invoice_code.'.pdf');
    }

    public function create(){
        $tourPackage = TravelPackage::all();
        return view('pages.admin.booking-tour.create', [
            'tourPackage' => $tourPackage
        ]);
    }

    public function store(Request $request){
        $item = TravelPackage::all();
        $latest = BookingTour::latest()->first();
        $date = Carbon::now('Asia/Jakarta')->format('ym');
        $travelCode = TravelPackage::where('code', $request->tour_package)->first();
        $typeData = $travelCode->type_data_code;
        if (! $latest) {
            $booking = 'DA'.$typeData.$date.'001';
        } else{
            $expNum = substr($latest->invoice_code, '-3');
            $booking= 'DA'.$typeData.$date.sprintf("%03d",$expNum+1);
        }
        
        $booking = BookingTour::create([
            'user_id' => Auth::user()->id,
            'travel_package_code' => $request->tour_package,
            'invoice_code' => $booking,
            'status_id' => '1',
            'info_daysoff' => $request->info_daysoff
        ]);

        $passengerPic = PassengerPic::create([
            'booking_tour_id' => $booking->id,
            'title'=> $request->pic_title,
            'gender' => $request->pic_gender,
            'first_name' => $request->pic_first_name,
            'last_name' => $request->pic_last_name,
            'email' => $request->pic_email,
            'address' => $request->pic_address,
            'phone_number' => $request->pic_phone,
            'date_birth' => $request->pic_dob,
        ]);
    
        foreach ($request->passenger_title as $key => $value) {
            Passenger::create([
                'booking_tour_id' => $booking->id,
                'passenger_pic_id' => $passengerPic->id,
                'title' => $request->passenger_title[$key],
                'gender' => $request->passenger_gender[$key],
                'first_name' => $request->passenger_first_name[$key],
                'last_name' => $request->passenger_last_name[$key],
                'email' => $request->passenger_email[$key],
                'date_birth' => $request->passenger_dob[$key],
                'identity_number' => $request->passenger_identity[$key],
                'passport_number' => $request->passenger_passport[$key],
                'address' => $request->passenger_address[$key],
                'phone_number' => $request->passenger_phone[$key],
                'social_media' => $request->passenger_socmed[$key],
                'price' => str_replace(',', '', $request->passenger_price[$key]),
                'occupation' => $request->passenger_occupation[$key],
                'food_allergy' => $request->food_allergy[$key],
                'medical_history' => $request->medical_history[$key],
                'booking_details' => $request->booking_details[$key],
                'booking_notes' => $request->booking_notes[$key],
                'life_insurance' => $request->life_insurance[$key],
                'buy_insurance' => (isset($request->buy_insurance[$key])) == '1' ? '1' : '0',
                'travel_insurance' => (isset($request->travel_insurance[$key])) == '1' ? '1' : '0',
                'clothing_size' => $request->clothing_size[$key]
            ]);
        }

        return redirect()->route('booking.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        BookingTour::findorfail($id)->update([
            'status_id' => $request->status
        ]);
        return redirect()->route('booking.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
