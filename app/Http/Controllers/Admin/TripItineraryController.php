<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TravelPackage;
use App\Models\TripItinerary;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TripItineraryRequest;
use App\Http\Requests\Admin\TripItineraryEditRequest;
use File;

class TripItineraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = TripItinerary::all();
        return view('pages.admin.trip-itineraries.index',[
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = TravelPackage::all();
        return view('pages.admin.trip-itineraries.create', [
            'items' => $items
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TripItineraryRequest $request)
    {
        $data = $request->all();
        $pdfName = $request->file('pdf_file')->getClientOriginalName();

        if($request->file('pdf_file')){
            $uploadPdf = $request->pdf_file->move(public_path('storage/assets/itinerary-pdf'), $pdfName);
            $data['pdf_file'] = 'assets/itinerary-pdf/' . $pdfName;
        }
        $createData = TripItinerary::create($data);
        return redirect()->route('trip-itineraries.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = TripItinerary::findorfail($id);
        return view('pages.admin.trip-itineraries.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function update(TripItineraryEditRequest $request, $id)
    {
        $data = $request->all();
        $tripItinerary = TripItinerary::findOrFail($id);

        if($request->file('pdf_file')){
            $deletePath = public_path('storage/'.$tripItinerary->pdf_file);
            if(File::exists($deletePath)) {
                $deleteOldPdf = File::delete($deletePath);
            }
            $pdfName = $request->file('pdf_file')->getClientOriginalName();
            $uploadPdf = $request->pdf_file->move(public_path('storage/assets/itinerary-pdf'), $pdfName);
            $data['pdf_file'] = 'assets/itinerary-pdf/' . $pdfName;
        }

        $updateData = TripItinerary::findorfail($id)->update($data);
        return redirect()->route('trip-itineraries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tripItinerary = TripItinerary::findorfail($id);
        $deletePath = public_path('storage/'.$tripItinerary->pdf_file);
        if(File::exists($deletePath)) {
            $deleteOldPdf = File::delete($deletePath);
        }
        $deleteData = $tripItinerary->delete();
        return redirect()->route('trip-itineraries.index');
    }
}