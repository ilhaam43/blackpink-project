<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VisaDataMaster;
use File;

class VisaDataController extends Controller
{
    public function index()
    {
        $visa = VisaDataMaster::all();

        return view('pages.admin.visa-data.index', [
            'visa' => $visa
        ]);
    }

    public function create()
    {
        return view('pages.admin.visa-data.create');
    }

    public function store(Request $request)
    {
        try{
            $data = $request->all();

            if($request->file('visa_pdf')){
                $pdfName = $request->file('visa_pdf')->getClientOriginalName();
                $uploadPdf = $request->visa_pdf->move(public_path('storage/assets/visa-pdf'), $pdfName);
                $data['pdf_url'] = 'assets/visa-pdf/' . $pdfName;
                $data['country'] = $request->country;
                $data['description'] = $request->description;
                $createData = VisaDataMaster::create($data);
                return redirect()->route('visa-data.index')->with('success', 'Visa data success to add');
            }
            else{
                $data['country'] = $request->country;
                $data['description'] = $request->description;
                VisaDataMaster::create($data);
                return redirect()->route('visa-data.index')->with('success', 'Visa data success to add');
            }

        }catch(\Throwable $th){
            return redirect()->route('visa-data.index')->with('error', 'Visa data failed to add');
        } 
    }

    public function edit($id)
    {
        $visa = VisaDataMaster::findOrFail($id);
        return view('pages.admin.visa-data.edit', [
            'visa' => $visa
        ]);
    }

    public function update(Request $request, $id)
    {
        $visa = VisaDataMaster::findOrFail($id);

        try{
            $data = $request->all();
            if($request->file('visa_pdf')){
                $pdfName = $request->file('visa_pdf')->getClientOriginalName();
                $uploadPdf = $request->visa_pdf->move(public_path('storage/assets/visa-pdf'), $pdfName);
                $data['pdf_url'] = 'assets/visa-pdf/' . $pdfName;
                $data['country'] = $request->country;
                $data['description'] = $request->description;
                $updateData = $visa->update($data);
                return redirect()->route('visa-data.index')->with('success', 'Visa data success to update');
            }

            $data['country'] = $request->country;
            $updateData = $visa->update($data);

            return redirect()->route('visa-data.index')->with('success', 'Visa data success to update');

        }catch(\Throwable $th){
            return redirect()->route('visa-data.index')->with('error', 'Visa data failed to update');
        } 
    }

    public function destroy($id)
    {
        $visa = VisaDataMaster::findOrFail($id);
        $deletePath = public_path('storage/'.$visa->pdf_url);
        if(File::exists($deletePath)) {
            $deleteOldPdf = File::delete($deletePath);
        }
        $deleteVisa = VisaDataMaster::destroy($id);
        return redirect()->route('visa-data.index')->with('success', 'Visa data success to delete');
    }
}
