<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TransportationEditRequest;
use App\Http\Requests\Admin\TransportationRequest;
use App\Models\Transportation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use File;

class TransportationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transportation = Transportation::all()->sortByDesc('updated_at');
        return view('pages.admin.transportation.index', compact('transportation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.transportation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransportationRequest $request)
    {
        $data = $request->all();
        $file = $request->file('image');

        if ($file == '') {
            $request->image = '';
        }else{
            $imageName = $request->file('image')->getClientOriginalName();
            $request->image->move(public_path('storage/assets/transportation/'), $imageName);
            $data['image'] = 'assets/transportation/' . $imageName;
        }
        
        $data['title'] = $request->title;
        $data['slug'] = Str::slug($request->title);
        $data['type'] = $request->type;
        $data['capacity'] = $request->capacity;
        $data['price'] = str_replace(',', '', $request->price);
        $data['note'] = $request->note;
        $data['region'] = $request->region;
        $data['is_available'] = $request->is_available;

        Transportation::create($data);

        return redirect()->route('transportation.index')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Transportation::find($id);
        return view('pages.admin.transportation.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransportationEditRequest $request, $id)
    {
        $data = $request->all();
        $file = $request->file('image');
        $transportation = Transportation::find($id);

        if ($file == '') {
            $request->image = $transportation->image;
        }else{
            $deletePath = public_path('storage/'.$transportation->image);
            if(File::exists($deletePath)) {
                File::delete($deletePath);
            }
            $imageName = $request->file('image')->getClientOriginalName();
            $request->image->move(public_path('storage/assets/transportation/'), $imageName);
            $data['image'] = 'assets/transportation/' . $imageName;
        }
        
        $data['title'] = $request->title;
        $data['slug'] = Str::slug($request->title);
        $data['type'] = $request->type;
        $data['capacity'] = $request->capacity;
        $data['price'] = str_replace(',', '', $request->price);
        $data['note'] = $request->note;
        $data['region'] = $request->region;
        $data['is_available'] = $request->is_available;

        $transportation->update($data);

        return redirect()->route('transportation.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transportation = Transportation::find($id);
        $deletePath = public_path('storage/'. $transportation->image);
        if(File::exists($deletePath)){
            File::delete($deletePath);
        }

        $transportation->delete();
        return redirect()->route('transportation.index')->with('success', 'Data Deleted');
    }
}
