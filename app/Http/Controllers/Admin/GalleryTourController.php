<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GalleryTour;
use Illuminate\Http\Request;
use File;

class GalleryTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = GalleryTour::all();
        return view('pages.admin.gallery-tour.index', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.gallery-tour.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $file = $request->file('image');

        if ($file == '') {
            $request->image = '';
        }else{
            $imageName = $request->file('image')->getClientOriginalName();
            $request->image->move(public_path('storage/assets/gallery-tour/'), $imageName);
            $data['image'] = 'assets/gallery-tour/' . $imageName;
        }

        GalleryTour::create($data);

        return redirect()->route('gallery-tour.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = GalleryTour::find($id);
        return view('pages.admin.gallery-tour.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $gallery = GalleryTour::findorfail($id);
        $file = $request->file('image');

        if ($file == '') {
            $request->image = $gallery->image;
        } else {
            $deletePath = public_path('storage/'.$gallery->image);
            if(File::exists($deletePath)) {
                File::delete($deletePath);
            }
            $imageName = $request->file('image')->getClientOriginalName();
            $request->image->move(public_path('storage/assets/gallery-tour/'), $imageName);
            $data['image'] = 'assets/gallery-tour/' . $imageName;
        }
        
        $gallery->update($data);

        return redirect()->route('gallery-tour.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = GalleryTour::findorfail($id);
        $deletePath = public_path('storage/'.$gallery->image);
        if(File::exists($deletePath)) {
            File::delete($deletePath);
        }
        $gallery->delete();
        return redirect()->route('gallery-tour.index');
    }
}
