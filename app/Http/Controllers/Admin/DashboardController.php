<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TravelPackage;
use App\Models\Hotel;
use App\Models\BookingTour;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function index(){
        $currentTime = Carbon::now();
        $items = BookingTour::where('status_id', '=', '6')->whereDate('created_at', '<', $currentTime->subDays(13))->get();
        $travelPackages = count(TravelPackage::all());
        $hotels = count(Hotel::all());
        return view('pages.admin.dashboard', [
            'items' => $items,
            'travelPackages' => $travelPackages,
            'hotels' => $hotels
        ]);
    }
}

?>