<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\Inclusive;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\DepartureDate;
use App\Models\TravelPackage;
use App\Models\TripItinerary;
use App\Models\DestinationCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TravelPackageRequest;
use App\Http\Requests\Admin\TravelPackageEditRequest;
use App\Models\DestinationDataMaster;
use App\Models\TypeDataMaster;
use App\Models\VendorDataMaster;
use Illuminate\Support\Carbon;

class TravelPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $items = TravelPackage::orderBy('id', 'desc')->get();
        return view('pages.admin.travel-package.index', [
            'items' => $items
        ]);
    }

    public function indexMice()
    {
        $items = TravelPackage::where('is_mice', 1)->orderBy('id', 'desc')->get();
        return view('pages.admin.travel-package-mice.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DestinationCategory::all();
        $vendors = VendorDataMaster::all();
        $types = TypeDataMaster::all();
        $destinations = DestinationDataMaster::all();
        return view('pages.admin.travel-package.create', [
            'categories' => $categories,
            'vendors' => $vendors,
            'types' => $types,
            'destinations' => $destinations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TravelPackageRequest $request)
    {
        $data = $request->all();
        $date = Carbon::now('Asia/Jakarta')->format('Ym');
        $latest = TravelPackage::latest()->first();
        $country = DestinationDataMaster::where('code', $request->destination_code)->first();
        $destinationCode = substr($request->destination_code, 0, 3);
        $lastDestination = TravelPackage::where('destination_data_code', 'LIKE', '%'.$destinationCode.'%')->latest()->first();
        if($request->type_code == 'I'){
            if (! $latest) {
                $dest = null;
                $code = $request->vendor_code.$request->type_code.'-xxxxxx-'.$date.'0001';
            } else{
                $dest = null;
                $expNum = substr($latest->code, -3);
                $code= $request->vendor_code.$request->type_code.'-xxxxxx-'.$date.sprintf("%04d",$expNum+1);
            }
        }else{
            if (! $latest && ! $lastDestination) {
                $dest =$request->destination_code.'001';
                $code = $request->vendor_code.$request->type_code.'-'.$dest.'-'.$date.'0001';
            }elseif(! $lastDestination){
                $dest =$request->destination_code.'001';
                $expNum = substr($latest->code, -4);
                $code = $request->vendor_code.$request->type_code.'-'.$dest.'-'.$date.sprintf("%04d",$expNum+1);
            }
            else{
                $num = substr($lastDestination->destination_data_code, -3);
                $dest = $request->destination_code.sprintf("%03d",$num+1);
                $expNum = substr($latest->code, -4);
                $code = $request->vendor_code.$request->type_code.'-'.$dest.'-'.$date.sprintf("%04d",$expNum+1);
            }
        }
        
        $data['slug'] = Str::slug($request->title);
        $data['code'] = $code;
        $data['price'] = str_replace(',', '', $request->price);
        $data['vendor_data_code'] = $request->vendor_code;
        $data['country'] = $country->name ?? '';
        $data['destination_data_code'] = $dest;
        $data['type_data_code'] = $request->type_code;
        TravelPackage::create($data);
        return redirect()->route('travel-package.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = TravelPackage::findOrFail($id);
        $categories = DestinationCategory::all();
        $vendors = VendorDataMaster::all();
        $destinations = DestinationDataMaster::all();
        $types = TypeDataMaster::all();
        return view('pages.admin.travel-package.edit', [
            'item' => $item,
            'categories' => $categories,
            'vendors' => $vendors,
            'destinations' => $destinations,
            'types' => $types
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TravelPackageEditRequest $request, $id)
    {
        $currentData = TravelPackage::find($id);

        if(!$request->destination_code)
        {   
            $request->destination_code = substr($currentData->destination_data_code, 0, 3);
        }

        $data = $request->all();
        $date = Carbon::now('Asia/Jakarta')->format('Ym');
        $latest = TravelPackage::latest()->first();
        $country = DestinationDataMaster::where('code', $request->destination_code)->first();
        $destinationCode = substr($request->destination_code, 0, 3);
        $lastDestination = TravelPackage::where('destination_data_code', 'LIKE', '%'.$destinationCode.'%')->latest()->first();
        
        if (($request->vendor_code == $currentData->vendor_data_code)
            && ($request->destination_code == substr($currentData->destination_data_code, 0, 3))
            && ($request->type_code == $currentData->type_data_code)) {
                $dest = $currentData->destination_data_code;
                $code = $currentData->code;
        }else{
            if($request->type_code == 'I'){
                if (! $latest) {
                    $dest = null;
                    $code = $request->vendor_code.$request->type_code.'-xxxxxx-'.$date.'0001';
                } else{
                    $dest = null;
                    $expNum = substr($currentData->code, -4);
                    $code= $request->vendor_code.$request->type_code.'-xxxxxx-'.$date.sprintf("%04d",$expNum);
                }
            }else{
                if (! $latest && ! $lastDestination) {
                    $dest = $request->destination_code.'001';
                    $code = $request->vendor_code.$request->type_code.'-'.$dest.'-'.$date.'0001';
                }elseif(! $lastDestination){
                    $dest = $request->destination_code.'001';
                    $expNum = substr($currentData->code, -4);
                    $code = $request->vendor_code.$request->type_code.'-'.$dest.'-'.$date.sprintf("%04d",$expNum);
                }
                else{
                    $num = substr($lastDestination->destination_data_code, -3);
                    $dest = $request->destination_code.sprintf("%03d",$num+1);
                    $expNum = substr($currentData->code, -4);
                    $code = $request->vendor_code.$request->type_code.'-'.$dest.'-'.$date.sprintf("%04d",$expNum);
                }
            }
        }
        if(!($request->has('special'))){
            $data['special'] = 0;
        } 
        $data['code'] = $code;
        $data['price'] = str_replace(',', '', $request->price);
        $data['vendor_data_code'] = $request->vendor_code;
        $data['country'] = $country->name ?? '';
        $data['destination_data_code'] = $dest;
        $data['type_data_code'] = $request->type_code;
        $data['slug'] = Str::slug($request->title);
        TravelPackage::findorfail($id)->update($data);
        return redirect()->route('travel-package.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TravelPackage::destroy($id);
        return redirect()->route('travel-package.index');
    }
}