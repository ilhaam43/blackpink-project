<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\DepartureDate;
use App\Models\TravelPackage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DepartureDateRequest;

class DepartureDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DepartureDate::all();
        // dd($items);
        return view('pages.admin.departure-date.index',[
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $travelPackages = TravelPackage::all();
        return view('pages.admin.departure-date.create',[
            'travelPackages' => $travelPackages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartureDateRequest $request)
    {
        $data = $request->all();
        DepartureDate::create($data);
        return redirect()->route('departure-date.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = DepartureDate::findorfail($id);
        $travelPackages = TravelPackage::all();
        return view('pages.admin.departure-date.edit', [
            'item' => $item,
            'travelPackages' => $travelPackages
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartureDateRequest $request, $id)
    {
        $data = $request->all();
        DepartureDate::findorfail($id)->update($data);
        return redirect()->route('departure-date.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DepartureDate::findorfail($id)->delete();
        return redirect()->route('departure-date.index');
    }
}