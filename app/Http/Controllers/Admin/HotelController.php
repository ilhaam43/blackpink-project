<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Hotel;
use App\Http\Requests\Admin\HotelRequest;
use App\Http\Requests\Admin\HotelEditRequest;

class HotelController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Hotel::all();
        return view('pages.admin.quarantine-hotel.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.quarantine-hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRequest $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->hotel_name);
        Hotel::create($data);
        return redirect()->route('quarantine-hotel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Hotel::findOrFail($id);
        return view('pages.admin.quarantine-hotel.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelEditRequest $request, $id)
    {
        $data = $request->all();
        $hotel = Hotel::findorfail($id);
        $data['slug'] = Str::slug($request->hotel_name);
        $hotel->update($data);
        return redirect()->route('quarantine-hotel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Hotel::destroy($id);
        return redirect()->route('quarantine-hotel.index');
    }
}
