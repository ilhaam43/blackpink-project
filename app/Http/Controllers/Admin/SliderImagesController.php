<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\SliderImages;
use File;

class SliderImagesController extends Controller
{
    public function __construct(){

    }
    
    public function index()
    {
        $sliderImage = SliderImages::all();

        return view('pages.admin.slider-image.index', [
            'sliderImage' => $sliderImage
        ]);
    }

    public function create(){
        return view('pages.admin.slider-image.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $file = $request->file('slider_image');

        if ($file == '') {
            $data['image_url'] = '';
        }else{
            $imageName = $request->file('slider_image')->getClientOriginalName();
            $request->slider_image->move(public_path('storage/assets/slider-image/'), $imageName);
            $data['image_url'] = 'assets/slider-image/' . $imageName;
        }

        SliderImages::create($data);

        return redirect()->route('slider-image.index');
    }

    public function edit($id)
    {
        $sliderImage = SliderImages::findOrFail($id);

        return view('pages.admin.slider-image.edit', [
            'sliderImage' => $sliderImage
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $sliderImage = SliderImages::findOrFail($id);

        $file = $request->file('slider_image');

        if ($file == '') {
            $data['slider_image'] = $sliderImage->image_url;
            return redirect()->route('flyer-image.index');
        } else {
            $deletePath = public_path('storage/'.$sliderImage->image_url);
            if(File::exists($deletePath)) {
                $deleteImage = File::delete($deletePath);
            }
            $imageName = $request->file('slider_image')->getClientOriginalName();
            $uploadImage = $request->slider_image->move(public_path('storage/assets/slider-image/'), $imageName);
            $data['image_url'] = 'assets/slider-image/' . $imageName;
        }
        
        $update = $sliderImage->update($data);

        return redirect()->route('slider-image.index')->with('success', 'Slider Image Update Success');
    }

    public function destroy($id){
        $slider = SliderImages::findOrFail($id);
        $deletePath = public_path('storage/'.$slider->image_url);

        if(File::exists($deletePath)) {
            File::delete($deletePath);
        }

        SliderImages::destroy($id);
        return redirect()->route('slider-image.index');
    }
}
