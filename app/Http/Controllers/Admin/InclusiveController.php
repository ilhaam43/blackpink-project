<?php

namespace App\Http\Controllers\Admin;

use App\Models\Inclusive;
use Illuminate\Http\Request;
use App\Models\TravelPackage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\InclusiveRequest;
use App\Http\Requests\Admin\InclusiveEditRequest;

class InclusiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Inclusive::all();
        return view('pages.admin.inclusives.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = TravelPackage::all();
        return view('pages.admin.inclusives.create', [
            'items' => $items
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InclusiveRequest $request)
    {
        $data = $request->all();
        Inclusive::create($data);
        return redirect()->route('inclusives.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $travelPackages = TravelPackage::all();
        $item = Inclusive::findOrFail($id);
        return view('pages.admin.inclusives.edit', [
            'item' => $item,
            'travelPackages' => $travelPackages
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        Inclusive::findOrFail($id)->update($data);
        return redirect()->route('inclusives.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Inclusive::findOrFail($id)->delete();
        return redirect()->route('inclusives.index');
    }
}
