<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\HotelRoomRequest;
use App\Models\HotelRoom;
use Illuminate\Http\Request;

class HotelRoomTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = HotelRoom::all();
        return view('pages.admin.quarantine-hotel-room.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.quarantine-hotel-room.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRoomRequest $request)
    {
        $data = $request->all();
        HotelRoom::create($data);
        return redirect()->route('quarantine-hotel-room.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = HotelRoom::find($id);
        return view('pages.admin.quarantine-hotel-room.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelRoomRequest $request, $id)
    {
        $data = $request->all();
        HotelRoom::findorfail($id)->update($data);
        return redirect()->route('quarantine-hotel-room.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HotelRoom::destroy($id);
        return redirect()->route('quarantine-hotel-room.index');
    }
}
