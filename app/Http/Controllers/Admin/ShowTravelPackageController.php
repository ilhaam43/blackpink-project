<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\ShowTravelPackage;
use App\Models\TravelPackage;

class ShowTravelPackageController extends Controller
{
    public function index()
    {
        $items = ShowTravelPackage::with('TravelPackage')->get();
        return view('pages.admin.show-travel-package.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        $tourEvents = TravelPackage::where('is_tours', 0)->where('is_available', 1)->where('is_mice', 0)->get();
        return view('pages.admin.show-travel-package.create', [
            'tourEvents' => $tourEvents
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $addShowTourEvents = ShowTravelPackage::create($data);

        return redirect()->route('show-travel-package.index')->with('success', 'Travel package created successfully');
    }

    public function edit($id)
    {
        $data = ShowTravelPackage::where('id', $id)->first();
        $tourEventsId = $data->travel_package_id;
        $tourEvents = TravelPackage::where('is_tours', 0)->where('is_available', 1)->where('is_mice', 0)->get();
        return view('pages.admin.show-travel-package.edit', [
            'tourEvents' => $tourEvents,
            'tourEventsId' => $tourEventsId,
            'id' => $data->id
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $updateShowTourEvents = ShowTravelPackage::findorfail($id)->update($data);

        return redirect()->route('show-travel-package.index')->with('success', 'Travel package updated successfully');
    }

    public function destroy($id)
    {
        $deleteData = ShowTravelPackage::destroy($id);
        return redirect()->route('show-travel-package.index')->with('success', 'Travel package deleted successfully');
    }
}
