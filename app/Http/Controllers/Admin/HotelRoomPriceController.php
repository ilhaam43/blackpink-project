<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HotelRoomPrice;
use App\Models\Hotel;
use App\Models\HotelRoom;
use App\Http\Requests\Admin\HotelRoomPriceRequest;

class HotelRoomPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::whereHas('hotelPrices')->get();
        $hotelrooms = HotelRoom::all();
        $items = HotelRoomPrice::all();
        return view('pages.admin.quarantine-hotel-price.index', [
            'items' => $items,
            'hotels' => $hotels,
            'hotelrooms' => $hotelrooms
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hotels = Hotel::all();
        $hotelrooms = HotelRoom::all();
        return view('pages.admin.quarantine-hotel-price.create', [
            'hotels' => $hotels,
            'hotelrooms' => $hotelrooms
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRoomPriceRequest $request)
    {
        $data = $request->all();
        $data['room_price']= str_replace(',', '', $request->room_price);
        HotelRoomPrice::create($data);
        return redirect()->route('quarantine-hotel-price.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = HotelRoomPrice::findOrFail($id);
        $hotels = Hotel::all();
        $hotelrooms = HotelRoom::all();
        return view('pages.admin.quarantine-hotel-price.edit', [
            'hotels' => $hotels,
            'hotelrooms' => $hotelrooms,
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelRoomPriceRequest $request, $id)
    {
        $data = $request->all();
        $data['room_price']= str_replace(',', '', $request->room_price);
        HotelRoomPrice::findorfail($id)->update($data);
        return redirect()->route('quarantine-hotel-price.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HotelRoomPrice::destroy($id);
        return redirect()->route('quarantine-hotel-price.index');
    }
}
