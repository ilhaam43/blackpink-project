<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GalleryRequest;
use App\Models\Gallery;
use App\Models\TravelPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use File;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Gallery::all()->sortBy('travel_package_code');
        return view('pages.admin.gallery.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $travelPackages = TravelPackage::all();
        return view('pages.admin.gallery.create', [
            'travelPackages' => $travelPackages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
        $data = $request->all();
        $file = $request->file('image');

        if ($file == '') {
            $data['image'] = '';
        }else{
            $imageName = $request->file('image')->getClientOriginalName();
            $uploadImage = $request->image->move(public_path('storage/assets/gallery/'), $imageName);
            $data['image'] = 'assets/gallery/' . $imageName;
        }

        $insert = Gallery::create($data);

        return redirect()->route('gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        $travelPackages = TravelPackage::all();
        return view('pages.admin.gallery.edit', [
            'gallery' => $gallery,
            'travelPackages' => $travelPackages
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryRequest $request, $id)
    {
        $data = $request->all();
        $gallery = Gallery::findorfail($id);
        $file = $request->file('image');

        if ($file == '') {
            $data['image'] = $gallery->image;
        } else {
            $deletePath = public_path('storage/'.$gallery->image);
            if(File::exists($deletePath)) {
                $deleteImage = File::delete($deletePath);
            }
            $imageName = $request->file('image')->getClientOriginalName();
            $uploadImage = $request->image->move(public_path('storage/assets/gallery/'), $imageName);
            $data['image'] = 'assets/gallery/' . $imageName;
        }
        
        $update = $gallery->update($data);

        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findorfail($id);
        $deletePath = public_path('storage/'.$gallery->image);
        if(File::exists($deletePath)) {
            $deleteImage = File::delete($deletePath);
        }
        $deleteData = $gallery->delete();
        return redirect()->route('gallery.index');
    }
}