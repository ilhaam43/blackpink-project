<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\FlyerImage;
use File;

class FlyerImageController extends Controller
{
    public function __construct(){

    }
    
    public function index()
    {
        $flyerImage = FlyerImage::all();

        return view('pages.admin.flyer-image.index', [
            'flyerImage' => $flyerImage
        ]);
    }

    public function store()
    {

    }

    public function edit($id)
    {
        $flyerImage = FlyerImage::findOrFail($id);

        return view('pages.admin.flyer-image.edit', [
            'flyerImage' => $flyerImage
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $flyerImage = FlyerImage::findOrFail($id);

        $file = $request->file('flyer_image');

        if ($file == '') {
            $data['flyer_image'] = $flyerImage->flyer_image;
            return redirect()->route('flyer-image.index');
        } else {
            $deletePath = public_path('storage/'.$flyerImage->flyer_image);
            if(File::exists($deletePath)) {
                $deleteImage = File::delete($deletePath);
            }
            $imageName = $request->file('flyer_image')->getClientOriginalName();
            $uploadImage = $request->flyer_image->move(public_path('storage/assets/flyer-image/'), $imageName);
            $data['image_url'] = 'assets/flyer-image/' . $imageName;
        }
        
        $update = $flyerImage->update($data);

        return redirect()->route('flyer-image.index')->with('success', 'Flyer Image Update Success');
    }
}
