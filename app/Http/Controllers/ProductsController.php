<?php

namespace App\Http\Controllers;

use App\Models\StaycationIgpost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\TravelPackage;
use App\Models\Hotel;

class ProductsController extends Controller
{
    public function travelPackages(){
        $travelPackages = TravelPackage::where('special', false)
                                        ->where('is_available', true)
                                        ->where('is_tours', 0)
                                        ->where('is_mice', 0)
                                        ->with(['travelGalleries', 'departureDates'])
                                        ->get();
        $seasonals = TravelPackage::where('special', true)
                                    ->with(['travelGalleries', 'departureDates'])
                                    ->get();
        return view('pages.products.travel-packages', [
            'travelPackages' => $travelPackages,
            'seasonals' => $seasonals
        ]);
    }

    public function travelMices(){
        $travelMice = TravelPackage::where('is_available', true)
                                    ->where('is_mice', 1)
                                    ->with(['travelGalleries', 'departureDates'])
                                    ->get();
        return view('pages.products.travel-packages-mice', [
            'travelMices' => $travelMice,
        ]);
    }

    public function staycations(){
        $staycationIGPosts = StaycationIgpost::all();
        return view('pages.products.staycations', [
            'staycationIGPosts' => $staycationIGPosts
        ]);
    }

    public function quarantineHotels(){
        $quarantineHotels = Hotel::where('hotel_status', true)->with('hotelPrices')->get();
        return view('pages.products.quarantine-hotels', [
            'quarantineHotels' => $quarantineHotels
        ]);
    }

    public function qhSearch(Request $request){
        if($request->ajax()){
            $output = '';
            $cities = array('Central Jakarta', 'North Jakarta', 'South Jakarta', 'East Jakarta', 'West Jakarta', 'Tangerang', 'Bekasi', 'Bali');
            if(empty($request['search_hotel']) && empty($request['hotel_city'])){
                $hotels = Hotel::where('hotel_status', true)->get();
            }else if(empty($request['hotel_city']) && $request['search_hotel']){
                $hotels= Hotel::where('hotel_name','LIKE','%'.$request->search_hotel."%")->where('hotel_status', true)->get();
            }else if($request['hotel_city'] && empty($request['search_hotel'])){
                if(count($request['hotel_city']) > 0 && in_array('Lainnya', $request['hotel_city'])){
                    $diff = (array_diff($cities, $request['hotel_city']));
                    $hotels= Hotel::whereNotIn('hotel_city', $diff)->where('hotel_status', true)->get();
                }else{
                    $hotels= Hotel::whereIn('hotel_city', $request->hotel_city)->where('hotel_status', true)->get();
                }
            }else if($request['hotel_city'] && $request['search_hotel']){
                if(count($request['hotel_city']) > 0 && in_array('Lainnya', $request['hotel_city'])){
                    $diff = (array_diff($cities, $request['hotel_city']));
                    $hotels= Hotel::where('hotel_name','LIKE','%'.$request->search_hotel."%")->whereNotIn('hotel_city', $diff)->where('hotel_status', true)->get();
                }else{
                    $hotels= Hotel::where('hotel_name','LIKE','%'.$request->search_hotel."%")->whereIn('hotel_city', $request->hotel_city)->where('hotel_status', true)->get();
                }
            }
        
            if(count($hotels) > 0){
                foreach($hotels as $key => $hotel){
                    if(count($hotel->hotelGalleries) > 0){
                    $output .='
                        <div class="card card-body mt-3">
                            <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                <div class="col-sm-4"> 
                                    <div class="card pulse-card infinite shadow h-100">
                                        <img class="card-img-top hotel-thumbnail" src="'.url('storage/'.$hotel->hotelGalleries->shuffle()->first()->image).'" loading ="lazy">
                                    </div>
                                </div>';
                    }else{
                        $output .='
                        <div class="card card-body mt-3">
                            <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                <div class="col-sm-4"> 
                                    <div class="card pulse-card infinite shadow h-100">
                                        <img class="card-img-top hotel-thumbnail" src="/" loading ="lazy">
                                    </div>
                                </div>';
                    }
                    $output .='
                                <div class="media-body">
                                    <h6 class="card-title"> <a href="'.route('QuarantineHotelsDetails', $hotel->slug).'" data-abc="true">'.$hotel->hotel_name.'</a> </h6>
                                    <p class="text-muted"><i class="fa fa-map-marker"></i> '.$hotel->hotel_address.', '.$hotel->hotel_city.'</p>
                                    <div class="col-sm-4 chse-box">
                                        <p>CHSE</p>
                                        <img src="'.url('frontend/images/quarantinehotel-details/CHSE-Ceritificate.png').'" style="width: 100%;">
                                        <p>'.$hotel->chse_score.'</p>
                                    </div>
                                </div>
                                <div class="mt-3 mt-lg-0 ml-lg-3 text-center">
                                    <p>Start From</p>
                                    <h4 class="card-price">';
                                        if ($hotel->hotelPrices->count()){
                    $output .='
                                                IDR'.$hotel->hotelPrices->sortBy('room_price')->whereNotIn('room_id', [10])->first()->room_price/1000000 .' jt/Person';
                                        }else{
                    $output .='                       
                                            Please contact us for price!';
                                        }
                    $output .='
                                    </h4>
                                    <div class="sold_stars ml-auto">'; 
                                        for($i=0; $i < 5; $i++){
                                            if($i<$hotel->hotel_rate){
                    $output .='
                                                <i class="fa fa-star"></i>';
                                            }
                                        }
                    $output .='
                                    </div><br>
                                    <a href="'.route('QuarantineHotelsDetails', $hotel->slug).'" class="btn btn-lg btn-outline-primary btn-outline-black" style="color: black;  margin-top:30px;">Details</a>
                                </div>
                            </div>
                        </div>';
                } 
            } else{
                $output .='
                    <div class="col-sm-8">
                        <div class="card card-body mt-3">
                            <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                <p>No Data Available</p>
                            </div>
                        </div>
                    </div>';
            }
            
            return Response ($output);
        }
    }

    // public function qhSorting(Request $request){
    //     if($request->ajax()){
            
    //         $output = '';

    //         if($request['sorting_price'] == "sort-asc"){
    //             $hotels = DB::table('hotels')->join('hotel_room_prices as prices', 'hotels.id', '=', 'prices.hotel_id')
    //                         ->select('hotels.*', 'prices.*')
    //                         ->where('prices.room_occupant', '=', 'Single Occupancy')->where('prices.room_id', '=', '1') 
    //                         ->orderBy('prices.room_price', 'ASC')->get(); 
                
    //         }
    //         if($request['sorting_price'] == "sort-desc"){
    //             $hotels = DB::table('hotels')->join('hotel_room_prices as prices', 'hotels.id', '=', 'prices.hotel_id')
    //                         ->select('hotels.*', 'prices.*')
    //                         ->where('prices.room_occupant', '=', 'Single Occupancy')->where('prices.room_id', '=', '1') 
    //                         ->orderBy('prices.room_price', 'DESC')->get();
    //         }
        
    //         if($hotels){
    //             foreach($hotels as $key => $hotel){
    //                 $output .='
    //                     <div class="card card-body mt-3">
    //                         <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
    //                             <div class="col-sm-4"> 
    //                                 <div class="card pulse-card infinite shadow h-100">
    //                                     <img class="card-img-top hotel-thumbnail" src="'.url('storage/'.$hotel->hotel_image).'" loading ="lazy">
    //                                 </div>
    //                             </div>
    //                             <div class="media-body">
    //                                 <h6 class="card-title"> <a href="'.route('QuarantineHotelsDetails', $hotel->slug).'" data-abc="true">'.$hotel->hotel_name.'</a> </h6>
    //                                 <p class="text-muted"><i class="fa fa-map-marker"></i> '.$hotel->hotel_address.', '.$hotel->hotel_city.'</p>
    //                                 <div class="col-sm-4 chse-box">
    //                                     <p>CHSE</p>
    //                                     <img src="'.url('frontend/images/quarantinehotel-details/CHSE-Ceritificate.png').'" style="width: 100%;">
    //                                 </div>
    //                             </div>
    //                             <div class="mt-3 mt-lg-0 ml-lg-3 text-center">
    //                                 <p>Start From</p>   
    //                                 <h4 class="card-price">';
    //                                     if ($hotel->hotelPrices->count()){
    //                 $output .='
    //                                             IDR'.$hotel->hotelPrices->sortBy('room_price')->where('room_occupant', '!=', 'Additional Person')->first()->room_price/1000000 .' jt/Person';
    //                                     }else{
    //                 $output .='                       
    //                                         Please contact us for price!';
    //                                     }
    //                 $output .='
    //                                 </h4>
    //                                 <div class="sold_stars ml-auto">'; 
    //                                     for($i=0; $i < 5; $i++){
    //                                         if($i<$hotel->hotel_rate){
    //                 $output .='
    //                                             <i class="fa fa-star"></i>';
    //                                         }
    //                                     }
    //                 $output .='
    //                                 </div><br>
    //                                 <a href="'.route('QuarantineHotelsDetails', $hotel->slug).'" class="btn btn-lg btn-outline-primary btn-outline-black" style="color: black;  margin-top:30px;">Details</a>
    //                             </div>
    //                         </div>
    //                     </div>';
    //             } 
    //         } 
    //         return Response ($output);
    //     }
    // }
}