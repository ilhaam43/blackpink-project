<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DestinationCategory;
use App\Models\TravelPackage;
use App\Models\TypeDataMaster;
use Illuminate\Support\Facades\DB;

class DestinationController extends Controller
{
    public function EuropeRegion()
    {
        $categories = DestinationCategory::where('category_region', 'Europe')->get();
        return view('pages.destinations.europe', [
            'categories' => $categories
        ]);
    }

    public function AmericaRegion()
    {
        $categories = DestinationCategory::where('category_region', 'America')->get();
        return view('pages.destinations.america', [
            'categories' => $categories
        ]);
    }

    public function AustraliaRegion()
    {
        $categories = DestinationCategory::where('category_region', 'Australia')->get();
        return view ('pages.destinations.australia', [
            'categories' => $categories
        ]);
    }

    public function AfricaRegion()
    {
        $categories = DestinationCategory::where('category_region', 'Africa')->get();
        return view('pages.destinations.africa', [
            'categories' => $categories
        ]);
    }

    public function AsiaRegion()
    {
        $categories = DestinationCategory::where('category_region', 'Asia')->get();
        return view('pages.destinations.asia', [
            'categories' => $categories
        ]);
    }

    public function DomesticRegion()
    {
        $categories = DestinationCategory::where('category_region', 'Indonesia')->get();
        return view('pages.destinations.domestic', [
            'categories' => $categories
        ]);
    }

    public function MiddleEastRegion()
    {
        $categories = DestinationCategory::where('category_region', 'Middle East')->get();
        return view('pages.destinations.middle-east', [
            'categories' => $categories
        ]);
    }

    public function DestinationCategory($category)
    {
        $fitTour = TravelPackage::where('category_id', $category)->where('type_data_code', 'F')->where('is_available', true)->where('is_tours', 0)->get();
        $groupTour = TravelPackage::where('category_id', $category)->where('type_data_code', 'G')->where('is_available', true)->where('is_tours', 0)->get();
        $incentiveTour = TravelPackage::where('category_id', $category)->where('type_data_code', 'I')->where('is_available', true)->where('is_tours', 0)->get();
        $privateTour = TravelPackage::where('category_id', $category)->where('type_data_code', 'P')->where('is_available', true)->where('is_tours', 0)->get();
        $attractionTour = TravelPackage::where('category_id', $category)->where('type_data_code', 'A')->where('is_available', true)->where('is_tours', 0)->get();
        
        $categoryName = DestinationCategory::where('id', $category)->firstorfail();
        
        return view('pages.destinations.detail-categories', [
            'category' => $category,
            'categoryName' => $categoryName,
            'fitTour' => $fitTour,
            'groupTour' => $groupTour,
            'incentiveTour' => $incentiveTour,
            'privateTour' => $privateTour,
            'attractionTour' => $attractionTour
        ]);
    }
}
