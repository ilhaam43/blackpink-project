<?php

namespace App\Http\Controllers\superadmin;

use App\Http\Controllers\Controller;
use App\Models\CommitionRequest;
use Illuminate\Http\Request;

class CommitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CommitionRequest::all();
        return view('pages.superadmin.commitionRequest', [
            'items' => $items
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve1(Request $request, $id)
    {
        CommitionRequest::findorfail($id)->update([
            'status_id' => '2'
        ]);
        return redirect()->route('commition.index');
    }

    public function approve2(Request $request, $id)
    {
        CommitionRequest::findorfail($id)->update([
            'status_id' => '3'
        ]);
        return redirect()->route('commition.index');
    }

    public function approve3(Request $request, $id)
    {
        CommitionRequest::findorfail($id)->update([
            'status_id' => '4'
        ]);
        return redirect()->route('commition.index');
    }
}
