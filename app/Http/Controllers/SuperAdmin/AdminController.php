<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Superadmin\AdminRequest;
use App\Http\Requests\Superadmin\AdminUpdateRequest;

class AdminController extends Controller
{
    public function index()
    {
        $admin = User::where('roles', 'ADMIN')->get();

        return view('pages.superadmin.admin.index', [
            'admin' => $admin
        ]);
    }

    public function create()
    {
        return view('pages.superadmin.admin.create');
    }

    public function store(AdminRequest $request)
    {
        if($request['password'] !== $request['confirm_password'])
        {
            return redirect()->route('admin.index')->with('error', 'Admin failed to add cause password and confirm password not same');
        }

        $createAdmin = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'roles' => 'ADMIN',
            'status' => 'Active'
        ]);

        return redirect()->route('admin.index')->with('success', 'Admin success to create');
    }

    public function edit($id)
    {
        $admin = User::where('id', $id)->first();

        return view('pages.superadmin.admin.edit', [
            'admin' => $admin
        ]);
    }

    public function update(AdminUpdateRequest $request, $id)
    {
        if($request['password'] !== $request['confirm_password'])
        {
            return redirect()->route('admin.index')->with('error', 'Admin failed to add cause password and confirm password not same');
        }

        if($request['password'] == null || $request['confirm_password'] == null)
        {
            $update = User::find($id)->update($request->except(['password','confirm_password']));
            return redirect()->route('admin.index')->with('success', 'Admin success to update');
        }

        $request['password'] = Hash::make($request['password']);

        $update = User::find($id)->update($request->except(['confirm_password']));

        return redirect()->route('admin.index')->with('success', 'Admin success to update');
    }

    public function destroy($id)
    {
        $destroyAdmin = User::destroy($id);

        return redirect()->route('admin.index')->with('success', 'Admin success to delete');
    }
}

?>