<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\BookingTour;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $currentTime = Carbon::now();
        $items = BookingTour::where('status_id', '=', '6')->whereDate('created_at', '<', $currentTime->subDays(13))->get();
        $admin = count(User::where('roles', 'ADMIN')->get());
        $superadmin = count(User::where('roles', 'SUPERADMIN')->get());
        $sales = User::where('roles', 'SALES')->count();
        return view('pages.superadmin.dashboard', [
            'items' => $items,
            'admin' => $admin,
            'superadmin' => $superadmin, 
            'sales' => $sales
        ]);
    }
}

?>