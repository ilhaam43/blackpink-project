<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Superadmin\SuperAdminRequest;
use App\Http\Requests\Superadmin\SuperAdminUpdateRequest;

class SuperAdminController extends Controller
{
    public function index()
    {
        $superadmin = User::where('roles', 'SUPERADMIN')->get();

        return view('pages.superadmin.superadmin.index', [
            'superadmin' => $superadmin
        ]);
    }

    public function create()
    {
        return view('pages.superadmin.superadmin.create');
    }

    public function store(SuperAdminRequest $request)
    {
        if($request['password'] !== $request['confirm_password'])
        {
            return redirect()->route('superadmin.index')->with('error', 'Superadmin failed to add cause password and confirm password not same');
        }

        $createSales = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'roles' => 'SUPERADMIN',
            'status' => 'Active'
        ]);

        return redirect()->route('superadmin.index')->with('success', 'Superadmin success to create');
    }

    public function edit($id)
    {
        $superadmin = User::where('id', $id)->first();

        return view('pages.superadmin.superadmin.edit', [
            'superadmin' => $superadmin
        ]);
    }

    public function update(SuperAdminUpdateRequest $request, $id)
    {
        if($request['password'] !== $request['confirm_password'])
        {
            return redirect()->route('superadmin.index')->with('error', 'Superadmin failed to add cause password and confirm password not same');
        }

        if($request['password'] == null || $request['confirm_password'] == null)
        {
            $update = User::find($id)->update($request->except(['password','confirm_password']));
            return redirect()->route('superadmin.index')->with('success', 'Superadmin success to update');
        }

        $request['password'] = Hash::make($request['password']);

        $update = User::find($id)->update($request->except(['confirm_password']));

        return redirect()->route('superadmin.index')->with('success', 'Superadmin success to update');
    }

    public function destroy($id)
    {
        $destroySuperAdmin = User::destroy($id);

        return redirect()->route('superadmin.index')->with('success', 'Superadmin success to delete');
    }
}

?>