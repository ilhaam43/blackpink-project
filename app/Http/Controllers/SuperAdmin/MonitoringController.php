<?php

namespace App\Http\Controllers\superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BookingTour;
use App\Models\BookingTourStatus;
use App\Exports\BookingToursExport;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class MonitoringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = BookingTour::orderBy('id', 'DESC')->get();
        $allStatus = BookingTourStatus::all();
        $updateStatus = BookingTourStatus::whereIn('id', [6,7,8,9,10,11,12])->get();
        return view('pages.superadmin.monitoring.index', [
            'items' => $items,
            'allStatus' => $allStatus,
            'updateStatus' => $updateStatus
        ]);
    }

    public function confirmation()
    {
        $allStatus = BookingTourStatus::all();
        $approves = BookingTour::where('status_id', '=', '3')->get();
        $invoiceSent = BookingTour::where('status_id', '=', '4')->get();
        $payments = BookingTour::where('status_id', '=', '8')->get();
        $completes = BookingTour::where('status_id', '=', '9')->get();
        $cancelled = BookingTour::where('status_id', '=', '10')->get(); //change to onhold status
        $statusRefund = BookingTourStatus::whereIn('id', [10,13,14,15])->get();
        $statusInProcess = BookingTourStatus::whereIn('id', [8,9,10])->get();
        return view('pages.superadmin.monitoring.confirmation', [
            'allStatus' => $allStatus,
            'approves' => $approves,
            'invoiceSent' => $invoiceSent,
            'payments' => $payments,
            'completes' => $completes,
            'cancelled' => $cancelled,
            'statusRefund' => $statusRefund,
            'statusInProcess' => $statusInProcess
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = BookingTour::find($id);
        return view('pages.superadmin.monitoring.details', [
            'item' => $item
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = BookingTour::findOrFail($id);
        return view('pages.superadmin.monitoring.edit', [
            'item' => $item
        ]);
    }
    
    public function exportInvoice($id){
        $bookingData = BookingTour::findorfail($id);
        if($bookingData->status_id == 1 || $bookingData->status_id == 2){
            $bookingData->update([
                'status_id' => 3
            ]);
        }
        $data = [
            'item' =>BookingTour::find($id)
        ];
        $pdf = PDF::loadView('pages.superadmin.monitoring.invoice', $data);
        return $pdf->stream('INV'. BookingTour::find($id)->invoice_code.'.pdf');
    }

    public function exportExcel(Request $request){
        $bookingData = BookingTour::findorfail($request->id);
        if($bookingData->status_id == 1 || $bookingData->status_id == 2){
            $bookingData->update([
                'status_id' => 3
            ]);
        }
        return Excel::download(new BookingToursExport($request->id), 'Booking #'. BookingTour::find($request->id)->invoice_code.'.xlsx');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        BookingTour::findorfail($id)->update([
            'down_payment' => str_replace(',', '', $request->down_payment)
        ]);
        return redirect()->route('monitor.index');
    }

    public function refund(Request $request, $id)
    {
        BookingTour::findorfail($id)->update([
            'status_id' => $request->status
        ]);
        return redirect()->route('monitor.confirmation');
    }

    public function approve($id)
    {
        BookingTour::findorfail($id)->update([
            'status_id' => 4
        ]);
        return redirect()->route('monitor.confirmation');
    }

    public function invoiceSent($id){
        BookingTour::find($id)->update([
            'status_id' => 6
        ]);
        return redirect()->route('monitor.confirmation');
    }

    public function paymentDone($id){
        BookingTour::findorfail($id)->update([
            'status_id' => 9
        ]);
        return redirect()->route('monitor.confirmation');
    }

    public function completed($id){
        BookingTour::findorfail($id)->update([
            'status_id' => 12
        ]);
        return redirect()->route('monitor.confirmation');
    }

    public function updateStatus(Request $request, $id)
    {
        $update = BookingTour::findorfail($id)->update([
            'status_id' => $request->status
        ]);
        return redirect()->route('monitor.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
