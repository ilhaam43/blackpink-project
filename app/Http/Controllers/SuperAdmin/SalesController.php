<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Superadmin\SalesRequest;
use App\Http\Requests\Superadmin\SalesUpdateRequest;

class SalesController extends Controller
{
    public function index()
    {
        $sales = User::where('roles', 'SALES')->get();

        return view('pages.superadmin.sales.index', [
            'sales' => $sales
        ]);
    }

    public function create()
    {
        return view('pages.superadmin.sales.create');
    }

    public function store(SalesRequest $request)
    {
        if($request['password'] !== $request['confirm_password'])
        {
            return redirect()->route('sales.index')->with('error', 'Sales failed to add cause password and confirm password not same');
        }

        $createSales = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'roles' => 'SALES',
            'status' => 'Active'
        ]);

        return redirect()->route('sales.index')->with('success', 'Sales success to create');
    }

    public function edit($id)
    {
        $sales = User::where('id', $id)->first();

        return view('pages.superadmin.sales.edit', [
            'sales' => $sales
        ]);
    }

    public function update(SalesUpdateRequest $request, $id)
    {
        if($request['password'] !== $request['confirm_password'])
        {
            return redirect()->route('sales.index')->with('error', 'Sales failed to add cause password and confirm password not same');
        }

        if($request['password'] == null || $request['confirm_password'] == null)
        {
            $update = User::find($id)->update($request->except(['password','confirm_password']));
            return redirect()->route('sales.index')->with('success', 'Sales success to update');
        }

        $request['password'] = Hash::make($request['password']);

        $update = User::find($id)->update($request->except(['confirm_password']));

        return redirect()->route('sales.index')->with('success', 'Sales success to update');
    }

    public function destroy($id)
    {
        $destroySales = User::destroy($id);

        return redirect()->route('sales.index')->with('success', 'Sales success to delete');
    }
}

?>