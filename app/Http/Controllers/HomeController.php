<?php

namespace App\Http\Controllers;

use App\Models\Career;
use Illuminate\Http\Request;
use App\Models\ShowTravelPackage;
use App\Models\ShowTourEvents;
use App\Models\TravelPackage;
use App\Models\StaycationIgpost;
use App\Models\ShowHotels;
use App\Models\Hotel;
use App\Models\DestinationCategory;
use App\Models\FlyerImage;
use App\Models\GalleryTour;
use App\Models\SliderImages;
use App\Models\Transportation;
use App\Models\VisaDataMaster;
use App\Models\Section;
use App\Models\ShowTravelPackageMice;

class HomeController extends Controller
{
    public function index()
    {

        $showTravelPackage = ShowTravelPackage::all();
        $showTravelMice = ShowTravelPackageMice::all();
        $travelPackagesId = [];
        $travelMiceId = [];

        foreach($showTravelPackage as $showTravelPackages){
            array_push($travelPackagesId, $showTravelPackages['travel_package_id']);
        }

        foreach($showTravelMice as $travel){
            array_push($travelMiceId, $travel['travel_package_id']);
        }

        $travelPackages = TravelPackage::whereIn('id', $travelPackagesId)
                                        ->where('is_available', true)
                                        ->where('is_mice', 0)
                                        ->with(['travelGalleries', 'departureDates'])
                                        ->take(6)->get();
        
        $travelMice = TravelPackage::whereIn('id', $travelMiceId)
                                    ->where('is_available', true)
                                    ->where('is_mice', true)
                                    ->with(['travelGalleries', 'departureDates'])
                                    ->take(6)->get();
        
        $seasonals = TravelPackage::where('special', true)
                                    ->with(['travelGalleries', 'departureDates'])
                                    ->take(6)->get();

        $showHotel = ShowHotels::all();
        $hotelId = [];

        foreach($showHotel as $showHotels){
            array_push($hotelId, $showHotels['hotel_id']);
        }

        $showTourEvents = TravelPackage::has('showTourEvents')->get();

        $quarantineHotels = Hotel::whereIn('id', $hotelId)
                                    ->where('hotel_status', true)
                                    ->with(['hotelPrices', 'hotelGalleries'])
                                    ->take(6)->get();

        $staycationIGPosts = StaycationIgpost::take(6)->get();
        $regions = DestinationCategory::get();

        $flyerImage1 = FlyerImage::where('id', 1)->first();
        $flyerImage2 = FlyerImage::where('id', 2)->first();
        $flyerImage3 = FlyerImage::where('id', 3)->first();

        $sliderImage = SliderImages::all();

        return view('pages.home', [
            'travelPackages'    => $travelPackages,
            'travelMice'        => $travelMice,
            'seasonals'         => $seasonals,
            'staycationIGPosts' => $staycationIGPosts,
            'quarantineHotels'  => $quarantineHotels,
            'regions'           => $regions,
            'flyerImage1'       => $flyerImage1,
            'flyerImage2'       => $flyerImage2,
            'flyerImage3'       => $flyerImage3,
            'sliderImage'       => $sliderImage,
            'showTourEvents'    => $showTourEvents
        ]);
    }

    public function about()
    {
        return view('pages.about-us');
    }
    public function contact()
    {
        return view('pages.contact-us');
    }
    public function products()
    {
        $travelPackages = TravelPackage::all()->count();
        $staycationIgposts = StaycationIgpost::all()->count();
        return view('pages.products.index', [
            'travelPackages' => $travelPackages,
            'staycationIgposts' => $staycationIgposts
        ]);
    }

    public function mice()
    {
        return view('pages.mice');
    }

    public function visaAndPassport()
    {
        $visa = VisaDataMaster::all();

        return view('pages.visa-and-passport', [
            'visa' => $visa
        ]);
    }

    public function visaAndPassportSearch(Request $request)
    {
        if($request->ajax()){
            $output = '';
            $visa = VisaDataMaster::where('id', $request['id'])->first();

            if($visa){
                if($visa->pdf_url)
                $output .= 
                '</br><h4 class="justified-content text-center"> Documents & Visa </h4>
                <p class="justified-content text-center"> Description: </p>
                <p class="justified-content"> '.$visa->description.' </p>
                <i class="fa fa-file" class="justified-content"> <a href="/storage/'.$visa->pdf_url.'" >Visa information PDF</a></i>';
                else if($visa->pdf_url == NULL){
                    $output .= 
                    '</br><h4 class="justified-content text-center"> Documents & Visa</h4>
                    <p class="justified-content text-center"> Description: </p>
                    <p class="justified-content"> '.$visa->description.' </p>';
                }
            }
            return response($output);
        }
    }

    public function toursOffer(){
        $tours = TravelPackage::where('is_available', 1)->where('is_tours', 1)->get();
        return view('pages.tours', [
            'tours' => $tours
        ]);
    }

    public function faq(){
        return view('pages.faq');
    }

    public function termsandconds(){
        return view('pages.terms-and-conditions');
    }

    public function galleries(){
        $gallery = GalleryTour::all();
        return view('pages.galleries', compact('gallery'));
    }

    public function attractions(){
        return view('pages.attractions');
    }

    public function transportation()
    {
        $transportation = Transportation::where('is_available', true)->get();
        return view('pages.transportation', compact('transportation'));
    }

    public function careers(){
        $careers = Career::where('is_available', '1')->get();
        return view('pages.careers', [
            'careers' => $careers
        ]);
    }

    public function arrange()
    {
        $section = Section::all()->sortBy('sort_number');

        return response()->json($section);
    }
}