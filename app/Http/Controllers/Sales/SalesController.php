<?php

namespace App\Http\Controllers\sales;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sales\CommitionRequest as SalesCommitionRequest;
use Illuminate\Http\Request;
use App\Models\BookingTour;
use App\Models\CommitionRequest;
use Illuminate\Support\Facades\Auth;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Auth::user();
        $completed = BookingTour::where('status_id', '=', '12')->where('user_id', $sales->id)->doesntHave('CommitionRequest')->get();
        
        $comm = CommitionRequest::where('user_id', $sales->id)->with('BookingTours')->get();
        
        return view('pages.sales.request', [
            'comm' => $comm,
            'completed' => $completed
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesCommitionRequest $request)
    {
        CommitionRequest::create([
            'user_id' => Auth::user()->id,
            'booking_tour_id' => $request['booking_tour_id'],
            'status_id' => $request['status_id']
        ]);
        return redirect()->route('sales.sales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
