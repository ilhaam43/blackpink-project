<?php

namespace App\Http\Controllers\Sales;

use App\Http\Controllers\Controller;
use App\Models\BookingTour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $auth = Auth::user();
        $booking = count(BookingTour::where('user_id', $auth->id)->get());
        
        return view('pages.sales.dashboard', [
            'booking' => $booking
        ]);
    }
}

?>