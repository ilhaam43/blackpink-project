<?php

namespace App\Exports;

use App\Models\BookingTour;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BookingToursExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $id;
    

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        return view('pages.admin.booking-tour.excel', [
            'item' => BookingTour::find($this->id),
        ]);
    }  
}
