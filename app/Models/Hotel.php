<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\HotelRoomPrice;
use App\Models\HotelGallery;

class Hotel extends Model
{
    use HasFactory;

    protected $fillable = [
        'hotel_name', 'hotel_rate', 'hotel_address', 'hotel_city', 
        'hotel_status', 'chse_score', 'slug', 'description'
    ];

    public function hotelPrices()
    {
        return $this->hasMany(HotelRoomPrice::class, 'hotel_id', 'id');
    }

    public function hotelGalleries(){
        return $this->hasMany(HotelGallery::class, 'hotel_id', 'id');
    }

    public function showHotel()
    {
        return $this->hasOne(ShowHotels::class);
    }
}
