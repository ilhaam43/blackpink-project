<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Hotel;

class HotelGallery extends Model
{
    use HasFactory;

    protected $table = 'hotel_galleries';

    protected $fillable = [
        'hotel_id', 'image'
    ];

    public function hotels(){
        return $this->belongsTo(Hotel::class, 'hotel_id', 'id');
    }
}
