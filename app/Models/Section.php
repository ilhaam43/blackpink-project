<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    protected $table = 'arrange_sections';

    protected $fillable = [
        'section_id', 'section_name', 'sort_number', 'is_show'
    ];
}
