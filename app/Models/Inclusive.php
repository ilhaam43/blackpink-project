<?php

namespace App\Models;

use App\Models\TravelPackage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Inclusive extends Model
{
    use HasFactory;

    protected $fillable = [
        'travel_package_id', 'incl1', 'incl2', 'incl3', 'incl4', 'incl5', 'incl6', 'incl7', 
        'incl8', 'incl9', 'incl10', 'incl11', 'incl12', 'incl13', 'incl14', 
        'incl15', 'incl16', 'incl17', 'incl18', 'incl19', 'incl20',
        'uincl1', 'uincl2', 'uincl3', 'uincl4', 'uincl5', 'uincl6', 'uincl7', 
        'uincl8', 'uincl9', 'uincl10', 'uincl11', 'uincl12', 'uincl13', 'uincl14', 
        'uincl15', 'uincl16', 'uincl17', 'uincl18', 'uincl19', 'uincl20'
    ];

    protected $hidden = [];

    public function travelPackage(){
        return $this->belongsTo(TravelPackage::class);
    }
}
