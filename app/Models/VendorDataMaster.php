<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorDataMaster extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'code'
    ];

    public function travelPackages(){
        return $this->hasMany(TravelPackage::class, 'vendor_data_code', 'code');
    }
}
