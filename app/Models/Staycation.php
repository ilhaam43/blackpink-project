<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staycation extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'location', 'slug', 'duration',
        'special_promo', 'stars', 'price_flat', 'price_per_night', 
        'booking_until', 'about', 'map_source', 'fact_1', 'fact_2',
        'fact_3', 'fact_4', 'fact_5'
    ];

    protected $hidden = [];

    public function staycationGallery(){
        return $this->hasOne(StaycationGallery::class);
    }
}
