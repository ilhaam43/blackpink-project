<?php

namespace App\Models;

use App\Models\TravelPackage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DepartureDate extends Model
{
    use HasFactory;

    protected $fillable = [
        'departure', 'arrival', 'travel_package_code', 'price'
    ];
    
    protected $hidden = [];

    public function travelPackage(){
        return $this->belongsTo(TravelPackage::class, 'travel_package_code', 'code');
    }
}
