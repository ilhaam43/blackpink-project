<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisaDataMaster extends Model
{
    use HasFactory;

    protected $table = 'visa_data_masters';

    protected $guarded = ['
        id
    '];
}
