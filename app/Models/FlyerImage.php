<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlyerImage extends Model
{
    use HasFactory;

    protected $table = 'flyer_image';

    protected $guarded = ['
        id
    '];
}
