<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingTour extends Model
{
    use HasFactory;

    protected $fillable= [
        'user_id', 'travel_package_code', 'invoice_code', 'status_id', 'info_daysoff', 'down_payment'
    ];

    public function users(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function tourPackages(){
        return $this->belongsTo(TravelPackage::class, 'travel_package_code', 'code');
    }

    public function bookingStatus(){
        return $this->belongsTo(BookingTourStatus::class, 'status_id', 'id');
    }

    public function passengerContactPersons(){
        return $this->hasMany(PassengerPic::class, 'booking_tour_id', 'id');
    }

    public function passengerLists(){
        return $this->hasMany(Passenger::class, 'booking_tour_id', 'id');
    }

    public function commitionRequest(){
        return $this->hasMany(CommitionRequest::class, 'booking_tour_id', 'id');
    }
}
