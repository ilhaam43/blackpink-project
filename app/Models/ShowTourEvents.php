<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowTourEvents extends Model
{
    use HasFactory;

    protected $table = 'show_tour_events';

    protected $guarded = [
        'id'
    ];

    public function travelPackage(){
        return $this->belongsTo(TravelPackage::class);
    }
}
