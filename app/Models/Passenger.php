<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    use HasFactory;

    protected $fillable= [
        'booking_tour_id', 'passenger_pic_id', 'title', 'gender',
        'first_name', 'last_name', 'email', 'date_birth', 'identity_number',
        'passport_number', 'address', 'phone_number', 'social_media', 'occupation',
        'food_allergy', 'medical_history', 'life_insurance', 'buy_insurance', 'travel_insurance',
        'clothing_size', 'price', 'booking_notes', 'booking_details'
    ];

    public function bookingTours(){
        return $this->belongsTo(BookingTour::class, 'booking_tour_id', 'id');
    }

    public function passengerPIC(){
        return $this->belongsTo(PassengerPic::class, 'passenger_pic_id', 'id');
    }
}