<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PassengerPic extends Model
{
    use HasFactory;

    protected $fillable= [
        'booking_tour_id', 'title', 'gender', 'first_name', 'last_name', 'email', 
        'address', 'phone_number', 'date_birth',
    ];

    public function bookingTours(){
        return $this->belongsTo(BookingTour::class, 'booking_tour_id', 'id');
    }

    public function passengers(){
        return $this->hasMany(Passenger::class, 'passenger_pic_id', 'id');
    }
}
