<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowTravelPackageMice extends Model
{
    use HasFactory;

    protected $table = 'show_travel_package_mices';

    protected $guarded = [
        'id'
    ];

    public function travelPackage(){
        return $this->belongsTo(TravelPackage::class);
    }
}
