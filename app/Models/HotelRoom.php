<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\HotelRoomPrice;

class HotelRoom extends Model
{
    use HasFactory;

    protected $fillable = [
       'room_name'
    ];

    public function roomPrices(){
        return $this->hasMany(HotelRoomPrice::class, 'room_id', 'id');
    }
}
