<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staycation extends Model
{
    use HasFactory;

    protected $fillable = [
        'staycation_id', 'image_1', 'image_2', 'image_3', 
        'image_4', 'image_5', 'hotel_partner_logo'
    ];

    protected $hidden = [];

    public function staycation(){
        return $this->belongsTo(Staycation::class);
    }
}
