<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommitionRequestStatus extends Model
{
    use HasFactory;

    protected $fillable= [
        'name'
    ];

    public function commitionStatus(){
        return $this->hasMany(CommitionRequestStatus::class, 'status_id', 'id');
    }
}
