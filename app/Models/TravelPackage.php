<?php

namespace App\Models;

use App\Models\Gallery;
use App\Models\Inclusive;
use App\Models\DepartureDate;
use App\Models\DestinationCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TravelPackage extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'code', 'slug', 'region',
        'about', 'duration', 'flight', 'accomodation', 'land_transport', 'meal', 'travel_docs', 'special', 
        'category_id', 'vendor_data_code', 'destination_data_code', 'type_data_code', 'country', 'is_available', 'is_tours', 'price', 'is_mice'
    ];

    // protected $hidden = [];

    public function departureDates(){
        return $this->hasMany(DepartureDate::class, 'travel_package_code', 'code');
    }

    public function travelGalleries(){
        return $this->hasMany(Gallery::class, 'travel_package_code', 'code');
    }

    public function tripItinerary(){
        return $this->hasOne(TripItinerary::class);
    }

    public function showTravelPackage()
    {
        return $this->hasOne(ShowTravelPackage::class);
    }

    public function showTravelPackageMice()
    {
        return $this->hasOne(ShowTravelPackageMice::class);
    }

    public function showTourEvents()
    {
        return $this->hasOne(ShowTourEvents::class);
    }

    public function inclusive(){
        return $this->hasOne(Inclusive::class);
    }

    public function destinationCategories(){
        return $this->belongsTo(DestinationCategory::class, 'category_id', 'id');
    }

    public function vendorData(){
        return $this->belongsTo(VendorDataMaster::class, 'vendor_data_code', 'code');
    }

    public function typeData(){
        return $this->belongsTo(TypeDataMaster::class, 'type_data_code', 'code');
    }

    public function bookingTour(){
        return $this->hasMany(BookingTour::class, 'travel_package_code', 'code');
    }
}
