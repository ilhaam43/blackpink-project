<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommitionRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'booking_tour_id', 'status_id'
    ];

    public function users(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function bookingTours(){
        return $this->belongsTo(BookingTour::class, 'booking_tour_id', 'id');
    }

    public function commitionStatus(){
        return $this->belongsTo(CommitionRequestStatus::class, 'status_id', 'id');
    }
}
