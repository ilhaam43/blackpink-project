<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Hotel;
use App\Models\HotelRoom;

class HotelRoomPrice extends Model
{
    use HasFactory;

    protected $fillable = [
        'room_occupant', 'room_price', 'room_facilities', 'hotel_id', 'room_id'
    ];

    public function hotels()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id', 'id');
    }

    public function rooms(){
        return $this->belongsTo(HotelRoom::class, 'room_id', 'id');
    }
}
