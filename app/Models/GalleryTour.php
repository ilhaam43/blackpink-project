<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryTour extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'image'
    ];
}
