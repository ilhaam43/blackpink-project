<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShowHotels extends Model
{
    use HasFactory;

    protected $table = 'show_hotels';

    protected $guarded = [
        'id'
    ];

    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }
}
