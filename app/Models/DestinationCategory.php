<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TravelPackage;

class DestinationCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_name', 'category_region', 'category_details', 'category_image'
    ];

    public function travelPackages(){
        return $this->hasMany(TravelPackage::class, 'category_id', 'id');
    }
}
