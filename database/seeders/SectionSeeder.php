<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $section = [
            ['section_id' => 'tourEvents', 'section_name' => 'Tour & Events', 'sort_number' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'hotelKarantina', 'section_name' => 'Quarantine Hotel', 'sort_number' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'paketPerjalanan', 'section_name' => 'Paket Perjalanan', 'sort_number' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'staycations', 'section_name' => 'Staycation', 'sort_number' => '4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'coming-soon', 'section_name' => 'Coming Soon', 'sort_number' => '5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'why-us', 'section_name' => 'Why Us', 'sort_number' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'mice', 'section_name' => 'Mice', 'sort_number' => '7', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['section_id' => 'youtube', 'section_name' => 'Youtube', 'sort_number' => '8', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        DB::table('arrange_sections')->insert($section);
    }
}
