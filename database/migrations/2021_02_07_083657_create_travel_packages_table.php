<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_packages', function (Blueprint $table) {
            $table->id();
            $table->string('title')->unique();
            $table->string('code')->unique();
            $table->string('slug')->unique()->nullable();
            $table->string('region');
            $table->string('country')->nullable();
            $table->longText('about');
            $table->string('duration');
            $table->string('flight')->nullable();
            $table->string('accomodation')->nullable();
            $table->string('land_transport')->nullable();
            $table->string('meal')->nullable();
            $table->string('travel_docs')->nullable();
            $table->tinyInteger('special');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('destination_categories')->onDelete('set null');
            $table->string('vendor_data_code');
            $table->foreign('vendor_data_code')->references('code')->on('vendor_data_masters')->onDelete('cascade')->onUpdate('cascade');
            $table->string('destination_data_code');
            $table->string('type_data_code');
            $table->foreign('type_data_code')->references('code')->on('type_data_masters')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_available')->default('1');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_packages');
    }
}
