<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaycationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staycations', function (Blueprint $table) {
            $table->id();
            $table->string('title')->unique();
            $table->string('location');
            $table->string('duration');
            $table->string('special_promo');
            $table->integer('stars');
            $table->integer('price_flat');
            $table->integer('price_per_night');
            $table->date('booking_until');
            $table->longText('about');
            $table->longText('map_source');
            $table->string('fact_1');
            $table->string('fact_2');
            $table->string('fact_3');
            $table->string('fact_4');
            $table->string('fact_5'); 
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staycations');
    }
}
