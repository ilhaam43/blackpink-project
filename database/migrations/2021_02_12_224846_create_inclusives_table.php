<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInclusivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inclusives', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('travel_package_id');
            $table->foreign('travel_package_id')->references('id')->on('travel_packages')->onDelete('cascade');
            $table->string('incl1')->nullable();
            $table->string('incl2')->nullable();
            $table->string('incl3')->nullable();
            $table->string('incl4')->nullable();
            $table->string('incl5')->nullable();
            $table->string('incl6')->nullable();
            $table->string('incl7')->nullable();
            $table->string('incl8')->nullable();
            $table->string('incl9')->nullable();
            $table->string('incl10')->nullable();
            $table->string('incl11')->nullable();
            $table->string('incl12')->nullable();
            $table->string('incl13')->nullable();
            $table->string('incl14')->nullable();
            $table->string('incl15')->nullable();
            $table->string('incl16')->nullable();
            $table->string('incl17')->nullable();
            $table->string('incl18')->nullable();
            $table->string('incl19')->nullable();
            $table->string('incl20')->nullable();
            $table->string('uincl1')->nullable();
            $table->string('uincl2')->nullable();
            $table->string('uincl3')->nullable();
            $table->string('uincl4')->nullable();
            $table->string('uincl5')->nullable();
            $table->string('uincl6')->nullable();
            $table->string('uincl7')->nullable();
            $table->string('uincl8')->nullable();
            $table->string('uincl9')->nullable();
            $table->string('uincl10')->nullable();
            $table->string('uincl11')->nullable();
            $table->string('uincl12')->nullable();
            $table->string('uincl13')->nullable();
            $table->string('uincl14')->nullable();
            $table->string('uincl15')->nullable();
            $table->string('uincl16')->nullable();
            $table->string('uincl17')->nullable();
            $table->string('uincl18')->nullable();
            $table->string('uincl19')->nullable();
            $table->string('uincl20')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inclusives');
    }
}
