<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_itineraries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('travel_package_id');
            $table->foreign('travel_package_id')->references('id')->on('travel_packages')->onDelete('cascade');
            $table->text('day1');
            $table->text('day2');
            $table->text('day3')->nullable();
            $table->text('day4')->nullable();
            $table->text('day5')->nullable();
            $table->text('day6')->nullable();
            $table->text('day7')->nullable();
            $table->text('day8')->nullable();
            $table->text('day9')->nullable();
            $table->text('day10')->nullable();
            $table->text('day11')->nullable();
            $table->text('day12')->nullable();
            $table->text('day13')->nullable();
            $table->text('day14')->nullable();
            $table->text('day15')->nullable();
            $table->text('day16')->nullable();
            $table->text('day17')->nullable();
            $table->text('day18')->nullable();
            $table->text('day19')->nullable();
            $table->text('day20')->nullable();
            $table->text('day21')->nullable();
            $table->text('day22')->nullable();
            $table->text('day23')->nullable();
            $table->text('day24')->nullable();
            $table->text('day25')->nullable();
            $table->text('day26')->nullable();
            $table->text('day27')->nullable();
            $table->text('day28')->nullable();
            $table->text('day29')->nullable();
            $table->text('day30')->nullable();
            $table->string('highlights');
            $table->text('pdf_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_itineraries');
    }
}
