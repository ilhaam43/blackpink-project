<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_tour_id');
            $table->foreign('booking_tour_id')->references('id')->on('booking_tours')->onDelete('cascade');
            $table->unsignedBigInteger('passenger_pic_id');
            $table->foreign('passenger_pic_id')->references('id')->on('passenger_pics')->onDelete('cascade');
            $table->string('title', '8');
            $table->string('gender', '8');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->date('date_birth');
            $table->string('identity_number');
            $table->string('passport_number');
            $table->text('address');
            $table->decimal('price', $precision = 12, $scale = 2);
            $table->string('phone_number')->nullable();
            $table->string('social_media')->nullable();
            $table->string('occupation')->nullable();
            $table->string('food_allergy')->nullable();
            $table->string('medical_history')->nullable();
            $table->string('life_insurance')->nullable();
            $table->boolean('buy_insurance');
            $table->string('clothing_size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengers');
    }
}
