<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassengerPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passenger_pics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_tour_id');
            $table->foreign('booking_tour_id')->references('id')->on('booking_tours')->onDelete('cascade');
            $table->string('title', '8');
            $table->string('gender', '8');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->text('address');
            $table->string('phone_number', '15');
            $table->date('date_birth');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passenger_pic');
    }
}
