<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_tours', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('booking_tour_statuses')->onDelete('cascade');
            $table->string('travel_package_code');
            $table->foreign('travel_package_code')->references('code')->on('travel_packages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('invoice_code', '10')->unique();
            $table->text('booking_notes')->nullable();
            $table->string('info_daysoff');
            $table->text('booking_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_tour');
    }
}
