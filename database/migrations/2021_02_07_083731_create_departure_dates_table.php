<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartureDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departure_dates', function (Blueprint $table) {
            $table->id();
            $table->date('departure');
            $table->string('travel_package_code');
            $table->foreign('travel_package_code')->references('code')->on('travel_packages')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('price');
            $table->date('arrival');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departure_dates');
    }
}
