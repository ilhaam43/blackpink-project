<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('App\Http\Controllers')
    ->group(function(){
        Route::get('/', 'HomeController@index')->name('Home');
        Route::get('/about', 'HomeController@about')->name('About');
        Route::get('/contact', 'HomeController@contact')->name('Contact');
        // Route::get('/blog', 'BlogControllerNoAuth@blog')->name('Blog');
        Route::get('/mice', 'HomeController@mice')->name('Mice');
        Route::get('/visa-and-passport', 'HomeController@visaAndPassport')->name('VisaAndPassport');
        Route::get('/visa-and-passport/search', 'HomeController@visaAndPassportSearch')->name('VisaAndPassportSearch');
        Route::get('/faq', 'HomeController@faq')->name('Faq');
        Route::get('/tours', 'HomeController@toursOffer')->name('Tours');
        Route::get('/terms-and-conditions', 'HomeController@termsandconds')->name('TermsAndConditions');
        Route::get('/galleries', 'HomeController@galleries')->name('Gallery');
        Route::get('/attractions', 'HomeController@attractions')->name('Attractions');
        Route::get('/transportation', 'HomeController@transportation')->name('Transportation');
        Route::get('/careers', 'HomeController@careers')->name('Careers');
        Route::get('/section', 'HomeController@arrange')->name('Arrange');
    });

Route::prefix('products')
->namespace('App\Http\Controllers')
->group(function(){
    Route::get('/', 'HomeController@products')->name('Products');
    Route::get('/travel-packages', 'ProductsController@travelPackages')->name('TravelPackagesPage');
    Route::get('/travel-packages-mice', 'ProductsController@travelMices')->name('TravelMicePage');
    Route::get('/staycations', 'ProductsController@staycations')->name('StaycationsPage');
    Route::get('/quarantine-hotels', 'ProductsController@quarantineHotels')->name('QuarantineHotelsPage');
    Route::get('/quarantine-hotels/search', 'ProductsController@qhSearch')->name('QuarantineHotelSearch');
    Route::get('/quarantine-hotels/sorting', 'ProductsController@qhSorting')->name('QuarantineHotelSorting');
});

Route::prefix('details')
->namespace('App\Http\Controllers')
->group(function(){
    Route::get('/travel-packages/{slug}', 'DetailsController@index')->name('TravelPackagesDetails');
    Route::get('/quarantine-hotels/{slug}', 'DetailsController@quarantineHotels')->name('QuarantineHotelsDetails');
    Route::get('/careers/{slug}', 'DetailsController@careers')->name('CareersDetails');
});

Route::prefix('destinations')
->namespace('App\Http\Controllers')
->group(function(){
    Route::get('/Europe', 'DestinationController@EuropeRegion')->name('EuropeRegion');
    Route::get('/Asia', 'DestinationController@AsiaRegion')->name('AsiaRegion');
    Route::get('/Indonesia', 'DestinationController@DomesticRegion')->name('DomesticRegion');
    Route::get('/America', 'DestinationController@AmericaRegion')->name('AmericaRegion');
    Route::get('/Australia', 'DestinationController@AustraliaRegion')->name('AustraliaRegion');
    Route::get('/Africa', 'DestinationController@AfricaRegion')->name('AfricaRegion');
    Route::get('/MiddleEast', 'DestinationController@MiddleEastRegion')->name('MiddleEastRegion');
    Route::get('/{categoryDestination}', 'DestinationController@DestinationCategory')->name('DestinationCategory');
});

Auth::routes();
Route::prefix('admin')
    ->namespace('App\Http\Controllers\Admin')
    ->middleware(['auth', 'admin'])
    ->group(function(){
        Route::get('/', 'DashboardController@index')
            ->name('dashboard');
        Route::resource('travel-package', 'TravelPackageController');
        Route::get('travel-package-mice', 'TravelPackageController@indexMice')->name('travelMice');
        Route::resource('show-travel-package', 'ShowTravelPackageController');
        Route::resource('show-travel-package-mice', 'ShowTravelPackageMiceController');
        Route::resource('show-tour-events', 'ShowTourEventsController');
        Route::resource('destination-category', 'DestinationCategoryController');
        Route::resource('gallery', 'GalleryController');
        Route::resource('departure-date', 'DepartureDateController');
        Route::resource('trip-itineraries', 'TripItineraryController');
        Route::resource('inclusives', 'InclusiveController');
        Route::resource('staycation-ig-post', 'StaycationIgPostController');
        Route::resource('transportation', 'TransportationController');
        Route::resource('quarantine-hotel', 'HotelController');
        Route::resource('show-quarantine-hotel', 'ShowHotelsController');
        Route::resource('quarantine-hotel-room', 'HotelRoomTypeController');
        Route::resource('quarantine-hotel-price', 'HotelRoomPriceController');
        Route::resource('quarantine-hotel-gallery', 'HotelGalleryController');
        Route::resource('booking', 'BookingTourController');
        Route::get('booking/exportPDF/{id}', 'BookingTourController@exportPDF')->name('exportPDF');
        Route::resource('tour-offers', 'TourOfferController');
        Route::resource('sections', 'SectionController');
        Route::put('section/{section}', 'SectionController@updateCheckbox')->name('updateCheckbox');
        Route::resource('blog', 'BlogController');
        Route::resource('gallery-tour', 'GalleryTourController');
        Route::resource('flyer-image', 'FlyerImageController');
        Route::resource('slider-image', 'SliderImagesController');
        Route::resource('career', 'CareerController');
        Route::resource('visa-data', 'VisaDataController');
        Route::resource('vendor-data', 'VendorDataController');
        Route::resource('destination-data', 'DestinationDataController');
        Route::resource('type-data', 'TypeDataController');
    });
Route::prefix('superadmin')
    ->namespace('App\Http\Controllers\SuperAdmin')
    ->middleware(['auth', 'superadmin'])
    ->group(function(){
        Route::get('/', 'DashboardController@index')->name('superadmin.dashboard');
        Route::resource('admin', 'AdminController');
        Route::resource('sales', 'SalesController');
        Route::put('comition/approve1/{id}', 'CommitionController@approve1')->name('commition.approved1');
        Route::put('comition/approve2/{id}', 'CommitionController@approve2')->name('commition.approved2');
        Route::put('comition/approve3/{id}', 'CommitionController@approve3')->name('commition.approved3');
        Route::resource('commition', 'CommitionController');
        Route::resource('superadmin', 'SuperAdminController');
        Route::get('monitor/exportExcel/{id}', 'MonitoringController@exportExcel')->name('exportExcel');
        Route::get('monitor/exportInvoice/{id}', 'MonitoringController@exportInvoice')->name('exportInvoice');
        Route::get('monitor/confirmation', 'MonitoringController@confirmation')->name('monitor.confirmation');
        Route::put('monitor/approve/{id}', 'MonitoringController@approve')->name('monitor.approve');
        Route::put('monitor/invoiceSent/{id}', 'MonitoringController@invoiceSent')->name('monitor.invoiceSent');
        Route::put('monitor/paymentDone/{id}', 'MonitoringController@paymentDone')->name('monitor.paymentDone');
        Route::put('monitor/completed/{id}', 'MonitoringController@completed')->name('monitor.completed');
        Route::put('monitor/refund/{id}', 'MonitoringController@refund')->name('monitor.refund');
        Route::put('monitor/status/{id}', 'MonitoringController@updateStatus')->name('monitor.update.status');
        Route::resource('monitor', 'MonitoringController');
    });
Route::prefix('sales')
    ->namespace('App\Http\Controllers\Sales')
    ->middleware(['auth', 'sales'])
    ->group(function(){
        Route::get('/', 'DashboardController@index')->name('sales.dashboard');
        Route::as('sales.')->group(function(){
            Route::resource('sales', 'SalesController');
        });
        Route::resource('bookingtour', 'BookingTourController');
    });
Route::prefix('auth')
    ->group(function(){
        Route::get('/verify', function(){
            return view('auth.verify');
        })->name('verify');
    });

// Auth::routes(['verify' => true]);

// Route::get('/email/verify', function(){
//     return view('auth.verify-email');
// })->middleware('auth')->name('verification.notice');

// Route::get('/email/verify/{id}/{hash}', function(EmailVerificationRequest $request){
//     $request->fulfill();
//     return redirect('/');
// })->middleware(['auth', 'signed'])->name('verification.verify');

// Route::post('/email/verification-notification', function(Request $request){
//     $request->user()->sendEmailVerificationNotification();
//     return back()->with('message', 'Verification Link Sent!');
// })->middleware(['auth', 'throttle:6,1'])->name('verification.send');