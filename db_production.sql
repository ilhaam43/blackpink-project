-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 11, 2021 at 12:56 AM
-- Server version: 10.3.30-MariaDB-cll-lve
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u9436384_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_tours`
--

CREATE TABLE `booking_tours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking_notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_daysoff` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booking_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `down_payment` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `booking_tour_statuses`
--

CREATE TABLE `booking_tour_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking_tour_statuses`
--

INSERT INTO `booking_tour_statuses` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Submitted', 'Form Already Filled and Submitted by Tour Operator/Sales', '2021-10-04 12:50:14', '2021-10-04 12:50:15'),
(2, 'Form Downloaded', 'Form Downloaded by Tour Operator', '2021-10-04 12:50:15', '2021-10-04 12:50:15'),
(3, 'INV Downloaded', 'Invoice Downloaded By Finance', '2021-10-04 12:50:51', '2021-10-04 12:50:51'),
(4, 'Form & Invoice Approved ', 'Form Checked by Tour Operator and Invoice Checked By Finance', '2021-10-20 04:19:58', '2021-10-20 04:19:58'),
(5, 'Invoice Sent', 'Invoice Sent To Customer by Customer Service or Sales', '2021-10-20 04:31:13', '2021-10-20 04:31:13'),
(6, 'Waiting For Payment', 'Waiting Payment From Customer', '2021-10-20 04:31:13', '2021-10-20 04:31:13'),
(7, 'Request to Vendor', 'Booking Form Sent to Vendor', '2021-10-20 04:31:13', '2021-10-20 04:31:13'),
(8, 'In Process', 'Booking is Being Processed By Vendor', '2021-10-20 04:31:13', '2021-10-20 04:31:13'),
(9, 'Payment Done', 'Customer Completed the Payment, Info to Finance At WA Group', '2021-10-20 04:31:13', '2021-10-20 04:31:13'),
(10, 'On Hold', 'Menunggu Quota Peserta Sesuai yang Ditentukan di Masing-masing Paket', '2021-10-20 04:31:13', '2021-10-20 04:31:13'),
(11, 'Cancelled', 'Tour Tidak Berjalan Karena Kondisi/Peraturan Pemerintah yang Berubah / Client Jatuh Sakit / Covid-19 (Dengan Syarat Belum Dibayar Customer)', '2021-10-20 04:33:53', '2021-10-20 04:33:53'),
(12, 'Completed', 'Successfully Processed by Vendor (Tour Ongoing)', '2021-10-20 04:33:53', '2021-10-20 04:33:53'),
(13, 'Partially Refund', 'Quota Tidak Mencukupi, Force Majeure ', '2021-10-20 06:32:21', '2021-10-20 06:32:21'),
(14, 'Open Voucher', 'Reschedule Tiket Pesawat', '2021-10-20 06:32:21', '2021-10-20 06:32:21'),
(15, 'Full Refund', 'Quota Tidak Mencukupi, Force Majeure (Incl. DP)', '2021-10-20 06:32:21', '2021-10-20 06:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_available` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `image`, `description`, `slug`, `is_available`, `created_at`, `updated_at`) VALUES
(1, 'Voluntary Internship - Tour Operator', 'assets/career/Apply_now_(4).png', '<p><strong>Open Recruitment</strong></p>\r\n\r\n<p><strong>Voluntary Internship (Magang Wajib)</strong></p>\r\n\r\n<p><strong>Email us your CV at:</strong></p>\r\n\r\n<p><a href=\"mailto:management@daysofftour.com\">management@daysofftour.com</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>G E N E R A L&nbsp; Q U A L I F I C A T I O N S</p>\r\n\r\n<ul>\r\n	<li>Woman, 19 &ndash; 27 Years Old</li>\r\n	<li>Academic: SMK/D1/SI in all Majority (preferable in Tourism Faculty)</li>\r\n	<li>Has an experience/basic knowledge in Travel Industry</li>\r\n	<li>Own private working device (laptop)</li>\r\n	<li>Willing to be located in East Jakarta, Rawamangun Office</li>\r\n	<li>Fully Vaccinated</li>\r\n	<li>Willing to be contacted on weekend</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>S K I L L S</p>\r\n\r\n<ul>\r\n	<li>Microsoft Office (Word, Excel, Powerpoint)</li>\r\n	<li>Flight System (Amadeus) *preferably</li>\r\n	<li>Speak Fluently in English or other foreign language</li>\r\n	<li>Mathematic Basic Skill</li>\r\n	<li>Design Thinking</li>\r\n	<li>Sales Skill</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>J O B D E S K</p>\r\n\r\n<ul>\r\n	<li>Managing and updating Tour Product from Land Tour Operator or Vendors</li>\r\n	<li>Update and manage all booking and reservation via system</li>\r\n	<li>Create Itinerary, Tour Schedule and Quotation</li>\r\n	<li>Preparing presentations for product summary</li>\r\n	<li>Data Entry in Excel</li>\r\n	<li>Become a representative for B2B and PIC to all Vendors</li>\r\n	<li>Taking part to all management&rsquo;s project, travel fair, exhibitions, etc</li>\r\n	<li>Other administration task</li>\r\n</ul>', 'voluntary-internship-tour-operator', 1, '2021-11-01 21:24:25', '2021-11-02 21:25:02');

-- --------------------------------------------------------

--
-- Table structure for table `commition_requests`
--

CREATE TABLE `commition_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `booking_tour_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `commition_request_statuses`
--

CREATE TABLE `commition_request_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commition_request_statuses`
--

INSERT INTO `commition_request_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Request Submitted', '2021-10-21 11:30:22', '2021-10-21 11:30:22'),
(2, 'Approved By Kevin', '2021-10-21 12:22:43', '2021-10-21 12:22:43'),
(3, 'Approved By Tasya', '2021-10-21 12:22:43', '2021-10-21 12:22:43'),
(4, 'Approved By Liliani', '2021-10-21 12:22:43', '2021-10-21 12:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `departure_dates`
--

CREATE TABLE `departure_dates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departure` date NOT NULL,
  `travel_package_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `arrival` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `destination_categories`
--

CREATE TABLE `destination_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `destination_categories`
--

INSERT INTO `destination_categories` (`id`, `category_name`, `category_region`, `category_details`, `category_image`, `created_at`, `updated_at`) VALUES
(1, 'West Europe', 'Europe', 'GERMANY - SWITZERLAND - FRANCE LUXEMBOURG - BRUSSELS - NETHERLAND', 'assets/destination-category/xxliZqaiNRhF5ix4E4FScj9mu2xkFv1pMv3tmkZe.jpg', '2021-09-12 10:43:54', '2021-09-12 19:36:36'),
(2, 'East Europe', 'Europe', 'GERMANY - CZECH REPUBLIC - SLOVENIA SLOVAKIA - HUNGARY - AUSTRIA KROATIA - ITALY', 'assets/destination-category/U1vpCerWNWFubq5z78ANRj5p5e6lyf8D0e552Dmp.jpg', '2021-09-12 10:45:08', '2021-09-12 19:37:33'),
(3, 'UK & Scotland', 'Europe', 'LONDON - MANCHESTER - LIVERPOOL LAKE DISTRICT - EDINBURGH - YORK', 'assets/destination-category/LGGiiWWdaubKNl0ecqgiiNLC8HLcNu2k4felMP1e.jpg', '2021-09-12 10:49:35', '2021-09-12 19:37:57'),
(4, 'Domestic', 'Asia', 'Domestik Indonesia', 'assets/destination-category/fAinWaZzwmg7t3pZ8CuGp3Eze4OYVfJ3qwjqViFf.png', '2021-09-12 23:37:24', '2021-09-12 23:37:24'),
(5, 'East Coast', 'America', 'East Coast USA', 'assets/destination-category/CUQ86WCxS4mfjAdsRNVKE4aLgqu4acQLt25nQSAw.jpg', '2021-09-12 23:53:40', '2021-09-12 23:53:40'),
(6, 'West Coast', 'America', 'West Coast USA', 'assets/destination-category/8TxAl6vJjg9CCVmfPdiPUSiYtgApaFDimphIDY4J.jpg', '2021-09-12 23:54:33', '2021-09-12 23:54:33'),
(7, 'West Coast + Honolulu', 'America', 'West Coast + Honolulu', 'assets/destination-category/5h0OANJZznMILovL6cFvLQM9T7XawyJMbF1wxYmW.jpg', '2021-09-12 23:55:29', '2021-09-28 07:14:06'),
(8, 'South Europe', 'Europe', 'SPANYOL', 'assets/destination-category/U1vpCerWNWFubq5z78ANRj5p5e6lyf8D0e552Dmp.jpg', '2021-09-12 10:45:08', '2021-09-12 19:37:33'),
(9, 'North Europe', 'Europe', 'FINLAND', 'assets/destination-category/aurora.jpeg', '2021-10-17 21:16:25', '2021-10-17 21:43:55'),
(10, 'Turkey', 'Middle East', 'TURKEY', 'assets/destination-category/Cappadocia-Uchisar-Castle.jpeg', '2021-10-17 21:59:38', '2021-10-17 21:59:38'),
(11, 'United Arab Emirates', 'Middle East', 'DUBAI', 'assets/destination-category/dubai city.jpeg', '2021-10-18 20:08:05', '2021-10-18 20:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `destination_data_masters`
--

CREATE TABLE `destination_data_masters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `destination_data_masters`
--

INSERT INTO `destination_data_masters` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Indonesia', 'IDN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(2, 'Malaysia\r\n', 'MYS', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(3, 'Singapore', 'SGP', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(4, 'Myanmar\r\n', 'MMR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(5, 'Laos\r\n', 'LAO', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(6, 'Thailand\r\n', 'THA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(7, 'Vietnam\r\n', 'VNM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(8, 'Kamboja\r\n', 'KHM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(9, 'Brunei Darussalam\r\n', 'BRN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(10, 'Filipina\r\n', 'PHL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(11, 'Timor Leste\r\n', 'TLS', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(12, 'Jepang\r\n', 'JPN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(13, 'Korea Utara\r\n', 'PRK', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(14, 'Korea Selatan\r\n', 'KOR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(15, 'China\r\n', 'CHI', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(16, 'Taiwan\r\n', 'TWN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(17, 'Pakistan\r\n', 'PAK', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(18, 'Bangladesh\r\n', 'BGD', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(19, 'India\r\n', 'IND', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(20, 'Nepal\r\n', 'NPL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(21, 'Bhutan\r\n', 'BTN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(22, 'Sri Langka\r\n', 'LKA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(23, 'Maladewa\r\n', 'MLD', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(24, 'Lebanon\r\n', 'LBN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(25, 'Arab Saudi\r\n', 'SAU', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(26, 'Bahrain\r\n', 'BHR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(27, 'Afganistan\r\n', 'AFG', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(28, 'Yordania\r\n', 'JOR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(29, 'Yaman\r\n', 'YEM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(30, 'Oman\r\n', 'OMN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(31, 'Qatar\r\n', 'QAT', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(32, 'Irak\r\n', 'IRQ', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(33, 'Suriah\r\n', 'SYR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(34, 'Iran\r\n', 'IRN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(35, 'Turki\r\n', 'TUR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(36, 'Israel\r\n', 'ISR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(37, 'Uni Emirat Arab\r\n', 'ARE', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(38, 'Siprus\r\n', 'CYP', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(39, 'Kazakhstan\r\n', 'KAZ', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(40, 'Azerbaijan\r\n', 'AZE', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(41, 'Kyrgyzstan\r\n', 'KGZ', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(42, 'Mongolia\r\n', 'MNG', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(43, 'Uzbekistan\r\n', 'UZB', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(44, 'Tajikistan\r\n', 'TJK', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(45, 'Armenia\r\n', 'ARM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(46, 'Turkmenistan\r\n', 'TKM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(47, 'Denmark\r\n', 'DNK', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(48, 'Norwegia\r\n', 'NOR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(49, 'Swedia\r\n', 'SWE', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(50, 'Finlandia\r\n', 'FIN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(51, 'Islandia\r\n', 'ISL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(52, 'Rusia\r\n', 'RUS', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(53, 'Belarus\r\n', 'BLR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(54, 'Estonia\r\n', 'EST', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(55, 'Latvia\r\n', 'LVA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(56, 'Ukrani\r\n', 'UKR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(57, 'Lithuania\r\n', 'LTU', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(58, 'Meldova\r\n', 'MDA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(59, 'Austria\r\n', 'AUT', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(60, 'Belanda\r\n', 'NLD', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(61, 'Inggris\r\n', 'ENG', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(62, 'Irlandia\r\n', 'IRL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(63, 'Perancis\r\n', 'FRA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(64, 'Belgium\r\n', 'BEL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(65, 'Jerman', 'DEU', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(66, 'Swiss\r\n', 'CHE', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(67, 'Luxemburg', 'LUX', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(68, 'Monako\r\n', 'MCO', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(69, 'Liechtenstein\r\n', 'LIE', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(70, 'Yunani\r\n', 'GRC', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(71, 'Italia\r\n', 'ITA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(72, 'Portugal\r\n', 'PRT', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(73, 'Spanyol\r\n', 'ESP', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(74, 'Vantikan\r\n', 'VAT', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(75, 'San Marino\r\n', 'SMR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(76, 'Andorra\r\n', 'AND', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(77, 'Albania\r\n', 'ALB', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(78, 'Bulgaria\r\n', 'BGR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(79, 'Bosnia and Herzegovina\r\n', 'BIH', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(80, 'Rep Ceko\r\n', 'CZE', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(81, 'Hongaria\r\n', 'HUN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(82, 'Kroasia\r\n', 'HRV', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(83, 'Makedonia Utara\r\n', 'MKD', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(84, 'Rumania\r\n', 'ROU', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(85, 'Polandia\r\n', 'POL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(86, 'Slovakia\r\n', 'SVK', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(87, 'Slovenia\r\n', 'SVN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(88, 'Serbia\r\n', 'SRB', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(89, 'Amerika Serikat\r\n', 'USA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(90, 'Canada\r\n', 'CAN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(91, 'Mexico\r\n', 'MEX', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(92, 'Guadeloupe', 'GLP\r\n', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(93, 'Trinidad and Tobago\r\n', 'TTO', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(94, 'Venezuela\r\n', 'VEN', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(95, 'Colombia', 'COL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(96, 'Guyana\r\n', 'GUY', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(97, 'Suriname\r\n', 'SUR', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(98, 'Brazil\r\n', 'BRA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(99, 'Equador', 'ECU', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(100, 'Peru\r\n', 'PER', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(101, 'Bolivia', 'BOL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(102, 'Chili\r\n', 'CHL', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(103, 'Paraguay\r\n', 'PRY', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(104, 'Uruguay\r\n', 'URY', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(105, 'Argentina\r\n', 'ARG', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(106, 'Bahama Island\r\n', 'BHS', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(107, 'Republic Of Cuba\r\n', 'CUB', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(108, 'Haiti\r\n', 'HTI', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(109, 'Republik Of Dominica\r\n', 'DMA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(110, 'Puerto Rico\r\n', 'PRI', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(111, 'Saint Kitts and Nevis\r\n', 'KNA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(112, 'Martinique\r\n', 'MTQ', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(113, 'Antigua and Barbuda\r\n', 'ATG', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(114, 'Barbados\r\n', 'BRB', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(115, 'Jamaica\r\n', 'JAM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(116, 'Saint Lucia\r\n', 'LCA', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(117, 'Saint Vincent and Grenadine\r\n', 'VCT', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(118, 'Belize\r\n', 'BLZ', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(119, 'Guatemala\r\n', 'GTM', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(120, 'El Savandor\r\n', 'SLV', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(121, 'Honduras\r\n', 'HND', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(122, 'Nikaragua\r\n', 'NIC', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(123, 'Kosta Rika\r\n', 'CRI', '2021-10-04 11:47:53', '2021-10-04 11:47:53'),
(124, 'Panama\r\n', 'PAN', '2021-10-04 11:47:53', '2021-10-04 11:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flyer_image`
--

CREATE TABLE `flyer_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flyer_image`
--

INSERT INTO `flyer_image` (`id`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 'assets/flyer-image/3fd67600-609b-4cc1-a3b5-cf1ad62808f9.JPG', '2021-10-06 09:21:56', '2021-11-05 04:19:50'),
(2, 'assets/flyer-image/f7afcade-713b-4c01-a98d-fd54b4d1c073.JPG', '2021-10-06 09:21:56', '2021-11-05 04:21:19'),
(3, 'assets/flyer-image/f69b787f-0617-4a0e-a67d-a9e9da9d478f.JPG', '2021-10-06 09:21:56', '2021-11-05 04:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `travel_package_code`, `image`, `created_at`, `updated_at`) VALUES
(65, 'ATF-FRA001-2021100001', 'assets/gallery/640-the-eiffel-tower-and-river-seine-at-twilight-in-paris.jpg', '2021-10-13 04:56:39', '2021-10-13 04:56:39'),
(66, 'ATF-CHE001-2021100002', 'assets/gallery/640-old-town-of-bern-capital-of-switzerland (1).jpg', '2021-10-13 04:57:58', '2021-10-13 04:57:58'),
(68, 'ATF-FRA001-2021100001', 'assets/gallery/640-eiffel-tower-at-sunrise-from-trocadero-fountains-in-paris.jpg', '2021-10-13 23:29:09', '2021-10-13 23:29:09'),
(69, 'ATF-ESP001-2021100003', 'assets/gallery/640-malaga-spain.jpeg', '2021-10-13 23:57:12', '2021-10-13 23:57:12'),
(70, 'ATF-NLD001-2021100004', 'assets/gallery/Amsterdam.jpeg', '2021-10-14 00:20:08', '2021-10-14 00:20:08'),
(71, 'ATF-USA001-2021100005', 'assets/gallery/los angelees.jpeg', '2021-10-14 00:35:16', '2021-10-14 00:37:16'),
(72, 'ATF-USA002-2021100006', 'assets/gallery/LA 5.jpg', '2021-10-14 01:06:23', '2021-10-14 01:07:38'),
(73, 'ATF-USA003-2021100007', 'assets/gallery/los angeles 2.jpeg', '2021-10-14 01:24:22', '2021-10-14 01:24:22'),
(74, 'ATF-USA004-2021100008', 'assets/gallery/los angelees.jpeg', '2021-10-14 01:56:29', '2021-10-14 02:02:16'),
(76, 'ATF-USA005-2021100009', 'assets/gallery/sunrise.jpeg', '2021-10-14 02:44:37', '2021-10-14 02:44:37'),
(77, 'ATF-USA006-2021100010', 'assets/gallery/new york 2.jpeg', '2021-10-14 20:58:35', '2021-10-14 20:59:58'),
(78, 'ATF-USA007-2021100011', 'assets/gallery/times square.jpeg', '2021-10-14 21:12:48', '2021-10-14 21:12:48'),
(79, 'ATF-USA008-2021100012', 'assets/gallery/new york liberty 2.jpeg', '2021-10-14 21:34:00', '2021-10-14 21:38:39'),
(80, 'ATF-USA009-2021100013', 'assets/gallery/brooklyn bridge.jpeg', '2021-10-14 21:47:51', '2021-10-14 21:48:44'),
(81, 'ATP-USA010-2021100014', 'assets/gallery/las vegas.jpeg', '2021-10-14 22:06:27', '2021-10-14 22:10:25'),
(82, 'ATP-USA011-2021100015', 'assets/gallery/las vegas 2.jpeg', '2021-10-14 22:20:00', '2021-10-14 22:20:00'),
(83, 'ATG-USA012-2021100016', 'assets/gallery/niagara falls2 .jpeg', '2021-10-14 22:54:14', '2021-10-14 22:54:14'),
(84, 'ATG-USA013-2021100017', 'assets/gallery/wall street.jpeg', '2021-10-14 23:11:58', '2021-10-14 23:11:58'),
(85, 'ATG-USA014-2021100018', 'assets/gallery/new york liberty.jpeg', '2021-10-15 02:18:50', '2021-10-15 02:18:50'),
(86, 'ATG-CHE002-2021100019', 'assets/gallery/gunung schilthorn 007.jpeg', '2021-10-17 20:24:53', '2021-10-17 20:24:53'),
(87, 'ATG-ESP002-2021100020', 'assets/gallery/alhambra 1.jpeg', '2021-10-17 21:09:39', '2021-10-17 21:09:39'),
(88, 'ATG-FIN001-2021100021', 'assets/gallery/glass igloo finland.jpeg', '2021-10-17 21:39:56', '2021-10-17 21:41:47'),
(89, 'ATG-FIN002-2021100022', 'assets/gallery/kakslauttanen.jpeg', '2021-10-17 21:49:02', '2021-10-17 21:49:02'),
(90, 'ATG-TUR001-2021100023', 'assets/gallery/hot air balloon.jpeg', '2021-10-17 22:11:42', '2021-10-17 22:41:07'),
(91, 'ATG-TUR002-2021100024', 'assets/gallery/pigeon valley 2.jpeg', '2021-10-17 22:40:04', '2021-10-17 22:40:04'),
(92, 'ATG-TUR003-2021100025', 'assets/gallery/baloon air.jpeg', '2021-10-18 03:08:18', '2021-10-18 03:13:21'),
(93, 'ATP-USA015-2021100026', 'assets/gallery/times square.jpeg', '2021-10-18 19:43:27', '2021-10-18 19:51:27'),
(94, 'ATF-ARE001-2021100027', 'assets/gallery/burj khalifa.jpeg', '2021-10-18 20:15:59', '2021-10-18 20:15:59'),
(101, 'PRP-IDN006-2021110033', 'assets/gallery/WSBK.png', '2021-11-03 20:49:48', '2021-11-03 20:50:39'),
(102, 'PRP-IDN007-2021110034', 'assets/gallery/WSBK.png', '2021-11-03 21:16:04', '2021-11-03 21:16:04'),
(103, 'PRG-IDN008-2021110035', 'assets/gallery/WSBK.png', '2021-11-03 21:33:45', '2021-11-03 21:33:45'),
(104, 'PRG-IDN009-2021110036', 'assets/gallery/WSBK.png', '2021-11-03 21:33:59', '2021-11-03 21:33:59'),
(105, 'AGF-IDN001-2021110028', 'assets/gallery/Belitung 1.jpg', '2021-11-03 23:12:28', '2021-11-03 23:12:28'),
(106, 'AGF-IDN002-2021110029', 'assets/gallery/Belitung 2.jpg', '2021-11-03 23:12:48', '2021-11-03 23:12:48'),
(107, 'MRF-IDN003-2021110030', 'assets/gallery/Derawan 3D2N.jpg', '2021-11-03 23:13:25', '2021-11-03 23:13:25'),
(108, 'MRF-IDN004-2021110031', 'assets/gallery/Derawan 4D3N.jpg', '2021-11-03 23:13:46', '2021-11-03 23:13:46'),
(109, 'MRF-IDN005-2021110032', 'assets/gallery/Derawan 5D4N.jpg', '2021-11-03 23:14:00', '2021-11-03 23:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_rate` int(11) NOT NULL,
  `hotel_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_status` tinyint(1) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chse_score` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `hotel_name`, `hotel_rate`, `hotel_address`, `hotel_city`, `hotel_status`, `description`, `chse_score`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Holiday Inn Express Pluit Citygate', 3, 'Emporium Pluit mall 10th floor, Jl. Pluit Selatan Raya', 'North Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'holiday-inn-express-pluit-citygate', '2021-09-01 09:55:01', '2021-10-01 07:38:13'),
(2, 'Novotel Mangga Dua', 4, 'Jalan Gunung Sahari Raya No 1', 'North Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '99', 'novotel-mangga-dua', '2021-09-01 04:09:13', '2021-11-09 21:39:56'),
(3, 'Aloft Wahid Hasyim SCBD', 4, 'JI. KH. Wahld Hasyfm No. 92; Menteng', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '94', 'aloft-wahid-hasyim-scbd', '2021-09-03 02:19:39', '2021-09-09 03:14:40'),
(4, 'Arya Duta', 5, 'Jln Prajurit KkO Usman & Harun W48', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '94', 'arya-duta', '2021-09-07 20:04:04', '2021-09-09 03:15:45'),
(5, 'Aston Kemayoran', 3, 'JI. HBR Motik Blok B-10.Not, Kompleks Kemayoran', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '98', 'aston-kemayoran', '2021-09-07 20:04:42', '2021-09-09 03:16:04'),
(6, 'Best Western Kemayoran', 4, 'JI. Benyamin Suaeb No.5, RT.13/RW.6, Kb. Kosong, Kec. kemayoran', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '99', 'best-western-kemayoran', '2021-09-07 20:05:23', '2021-09-09 03:20:03'),
(7, 'Blue Sky Petamburan', 3, 'Jl. KS Tubun No. 19, Petamburan, Tanah Abang', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '93', 'blue-sky-petamburan', '2021-09-07 20:15:08', '2021-11-09 21:40:52'),
(8, 'Grand Mercure Harmoni', 5, 'JI. Hayam Wuruk No. 36-37', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'grand-mercure-harmoni', '2021-09-07 20:15:48', '2021-09-09 03:21:08'),
(9, 'Grand Mercure Kemayoran', 5, 'Jl. Benyamin Sueb Kav. &6, Kawasan Mail Glodok Kemayoran', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'grand-mercure-kemayoran', '2021-09-07 20:16:30', '2021-11-09 21:41:36'),
(10, 'Grand Sahid Jaya Jakarta', 5, 'Jl. Jenderal Sudirman Kav. 86', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'grand-sahid-jaya-jakarta', '2021-09-08 04:27:30', '2021-11-09 21:41:50'),
(11, 'Holiday Inn Jiexpo', 3, 'Jl. Arena Pekan Raya Pintu 6, Kemayoran', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', NULL, 'holiday-inn-jiexpo', '2021-09-08 04:28:21', '2021-09-08 04:28:21'),
(12, 'Holiday Inn Wahid Hasyim', 3, 'Jl. K.H Wahid Hasyim No. 123', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'holiday-inn-wahid-hasyim', '2021-09-08 04:29:39', '2021-11-09 21:45:06'),
(13, 'Hotel Borobudur', 5, 'JI. Lapangan Banteng, No. 1A', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'hotel-borobudur', '2021-09-08 04:42:53', '2021-11-09 21:45:21'),
(14, 'Hotel Mulia', 5, 'Jalan Asla Afrika, Senayan', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'hotel-mulia', '2021-09-08 04:43:32', '2021-11-09 21:45:34'),
(15, 'Kempinski Jakarta', 5, 'Jl. MH Thamrin No. 1', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'kempinski-jakarta', '2021-09-08 04:44:07', '2021-11-09 21:45:55'),
(16, 'Kempinski Jakarta (with Limousine Car)', 5, 'Jl. MH Thamrin No. 1', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'kempinski-jakarta-with-limousine-car', '2021-09-08 04:45:42', '2021-09-09 03:24:30'),
(17, 'Le Meridien', 5, 'Jl. Jend Sudirman No. 10 Kav 18-20', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'le-meridien', '2021-09-08 04:46:16', '2021-09-09 03:24:50'),
(18, 'Luminor Mangga Besar', 3, 'Hotel Luminor Kota JI. Mangga Besar Raya No 73', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '96', 'luminor-mangga-besar', '2021-09-08 05:01:37', '2021-11-09 21:46:15'),
(19, 'Novotel Cikini', 4, 'Jalan Clkinl Raya No. 107-109, Menteng', 'Central Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', NULL, 'novotel-cikini', '2021-09-08 05:05:45', '2021-09-08 05:05:45'),
(20, 'Orchardz Jayakarta', 3, 'Jl. Pangeran Jayakarta No.44 Mangga Dua, Sawah Besar', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '93', 'orchardz-jayakarta', '2021-09-08 05:06:45', '2021-11-09 21:46:30'),
(21, 'Shangri-La', 5, 'JI. Jendral Sudirman Kav.1, Tanah Abang', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'shangri-la', '2021-09-08 05:07:32', '2021-11-09 21:56:46'),
(22, 'The Sultan Hotel', 5, 'JI. Gatot Subroto', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '98', 'the-sultan-hotel', '2021-09-08 05:08:00', '2021-11-09 21:57:03'),
(23, 'YELLO Harmoni Jakarta', 3, 'Jl. Hayam Wuruk No. 6  Harmoni', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'yello-harmoni-jakarta', '2021-09-08 23:26:52', '2021-11-09 21:57:20'),
(24, 'Harris Suite Puri', 4, 'JI. Lingkar Luar Puri Mansion Estate Kembangan', 'West Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 7 nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'harris-suite-puri', '2021-09-08 23:27:51', '2021-09-09 03:27:37'),
(25, 'Mandarin Oriental Jakarta', 5, 'Jl. MH. Thamrin', 'West Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mandarin-oriental-jakarta', '2021-09-08 23:29:32', '2021-11-09 21:57:34'),
(26, 'Mercure Jakarta Batavia', 4, 'Jl. Kali Besar Tim. No.44 46, Roa Malaka', 'West Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mercure-jakarta-batavia', '2021-09-08 23:31:59', '2021-11-09 21:57:48'),
(27, 'Mercure Jakarta Kota (Deluxe Room)', 4, 'JI. Hayam Wuruk No 123', 'West Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'mercure-jakarta-kota-deluxe-room', '2021-09-08 23:33:30', '2021-09-09 03:28:42'),
(28, 'Mercure Jakarta Kota (Superior Room)', 4, 'JI. Hayam Wuruk No 123', 'West Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mercure-jakarta-kota-superior-room', '2021-09-08 23:33:57', '2021-11-09 21:58:39'),
(29, 'Alila SCBD', 5, 'SCBD Lot 11 Jl. Jend Sudirman', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '96', 'alila-scbd', '2021-09-08 23:34:50', '2021-11-09 21:58:52'),
(30, 'Gran Melia', 5, 'JI. HR. Resuna Said Kav. X-0 Kuningan Timur - Setlabudi', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'gran-melia', '2021-09-08 23:37:00', '2021-11-09 22:00:32'),
(31, 'JS Luwansa', 4, 'JI. H.R Rasuna Said kav. C-22', 'South Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'js-luwansa', '2021-09-09 03:44:55', '2021-09-09 03:44:55'),
(32, 'Mercure Gatot Subroto', 4, 'Jl. Gatot Subroto No.Kav. 1, Kuningan Barat', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mercure-gatot-subroto', '2021-09-09 03:47:09', '2021-11-09 22:03:37'),
(33, 'Mercure Simatupang', 4, 'JI RA Kartini No.18, Lebak bulus', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mercure-simatupang', '2021-09-09 03:48:30', '2021-11-09 22:03:58'),
(34, 'Raffles Jakarta', 5, 'Ciputra World 1 JI. Prof. Dr. Satrio Kav. 3-5', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'raffles-jakarta', '2021-09-09 03:49:33', '2021-11-09 22:04:41'),
(35, 'Ritz Carlton Jakarta', 5, 'JI. DR. Ide Anak Agung Gde Agung Kav. E.1.1’No. 1', 'South Jakarta', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'ritz-carlton-jakarta', '2021-09-09 03:50:45', '2021-09-09 03:50:45'),
(36, 'Sheraton Grand Gandaria JKT', 5, 'Jl. Sultan Iskandar Muda', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'sheraton-grand-gandaria-jkt', '2021-09-09 03:51:56', '2021-11-09 22:05:08'),
(37, 'The 101 Jakarta Sedayu Dharmawangsa', 4, 'The darmawangsa square , JL  Darmawangsa IX No. 14', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', NULL, 'the-101-jakarta-sedayu-dharmawangsa', '2021-09-09 03:52:54', '2021-11-09 22:05:30'),
(38, 'The Mayflower - Marriot Apartement', 4, 'Sudirman Plaza, Indofood Tower. JI. Jend. Sudlrman Kav. 76-79', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'the-mayflower-marriot-apartement', '2021-09-09 03:54:34', '2021-11-09 22:05:46'),
(39, 'Wyndham Casablanca Jakarta', 5, 'JI. Casablanca No.Kav. 18, Menteng Dalam', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'wyndham-casablanca-jakarta', '2021-09-09 03:55:34', '2021-09-09 03:55:34'),
(40, 'Swiss-Bellinn TB Simatupang', 4, 'Jl. R.A. Kartini No.32,Lb. Bulus, Kec. Cilandak', 'South Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'swiss-bellinn-tb-simatupang', '2021-09-09 03:57:04', '2021-09-09 03:57:04'),
(41, 'Bandara International Hotel', 5, 'Soekarno Hatta Airport Complex, JI. Tol Prof Dr Ir Sedyatmo Km2', 'Tangerang', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', NULL, 'bandara-international-hotel', '2021-09-09 03:58:30', '2021-09-09 03:58:30'),
(42, 'Fave Bandara', 3, 'Jl. Husein Sastranegara, Jurumudi Benda RT 4 / RW 1', 'Tangerang', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '84', 'fave-bandara', '2021-09-09 03:59:44', '2021-09-09 03:59:44'),
(43, 'FM7 Resort Bandara', 4, 'JI. Perencis No. 67', 'Tangerang', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '99', 'fm7-resort-bandara', '2021-09-09 04:00:26', '2021-11-10 05:20:23'),
(45, 'Jakarta Airport by Topotels', 3, 'Terminal E2 Bandara Soekarno Hatta', 'Tangerang', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'jakarta-airport-by-topotels', '2021-09-09 04:02:19', '2021-09-09 04:02:19'),
(46, 'Mercure Alam Sutra Serpong', 4, 'JI. Alam Sutera Bodlevard Kav 23', 'Tangerang', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mercure-alam-sutra-serpong', '2021-09-09 04:03:14', '2021-11-10 05:20:59'),
(47, 'Novotel Tangerang', 4, 'Tangcity Superblock, JI. Jend Sudlrman No.1', 'Tangerang', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'novotel-tangerang', '2021-09-09 04:05:28', '2021-09-09 04:05:28'),
(48, 'Royal Palm Hotel & Conference Cengkareng', 3, 'JI. Outer Ring Road Mutiara Taman Palem Blok C1 Cengkareng', 'Tangerang', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', NULL, 'royal-palm-hotel-conference-cengkareng', '2021-09-09 04:06:17', '2021-11-10 05:21:15'),
(49, 'Swissbell Airport Cengkareng', 4, 'Jl. Husein Sastranegara No.kav. 1 Kota Tangerang, Banten', 'Tangerang', 0, '<p>Includes :</p>\r\n\r\n<p>- Accomodation for 4&nbsp;nights</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport - 5 pieces of laundry per day per person (non-accumulative) - Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>', '100', 'swissbell-airport-cengkareng', '2021-09-09 04:07:32', '2021-09-09 04:07:32'),
(50, 'Sahid Jaya Lippo Cikarang', 5, 'Jl. Moh. H. Thamrin Kav. 103 Lippo Clkarang Cikarang Selatan', 'Bekasi', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'sahid-jaya-lippo-cikarang', '2021-09-09 04:08:23', '2021-11-10 05:21:27'),
(51, 'Bali Dynasty', 4, 'Jl. Kartika Plaza, Tuban, Kuta, Kabupaten Badung', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'bali-dynasty', '2021-11-03 01:10:23', '2021-11-10 05:21:38'),
(52, 'Nusa Dua Beach Hotel & Spa', 4, 'Kawasan Pariwisata Nusa Dua Lot. North 4, Jl. Nusa Dua, Benoa, Kuta Selatan, Kab. Badung', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'nusa-dua-beach-hotel-spa', '2021-11-03 01:55:44', '2021-11-10 05:21:55'),
(53, 'Melia Bali', 4, 'Kawasan Wisata ITDC Lot 1, Jl. Nusa Dua, Benoa, Kec. Kuta Selatan, Kabupaten Badung', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'melia-bali', '2021-11-03 02:13:24', '2021-11-10 05:22:07'),
(54, 'Aston Kuta Hotel & Residence', 4, 'Jl. Wana Segara No.2-5, Kuta, Kabupaten Badung', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'aston-kuta-hotel-residence', '2021-11-03 02:26:26', '2021-11-10 05:22:21'),
(55, 'Swiss Belhotel Tuban', 4, 'Jl. Kubu Anyar No.31, Tuban, Kuta, Kabupaten Badung', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'swiss-belhotel-tuban', '2021-11-03 02:35:18', '2021-11-10 05:22:36'),
(56, 'Prime Plaza Suites Sanur', 4, 'Jl. Bypass Ngurah Rai No.83, Sanur, Denpasar Selatan, Kota Denpasar', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'prime-plaza-suites-sanur', '2021-11-03 02:46:43', '2021-11-10 05:23:03'),
(57, 'Mulia Resort', 4, 'Jl. Raya Nusa Dua Selatan Jl. Nusa Dua, Kawasan Sawangan, Kec. Kuta Sel., Kabupaten Badung', 'Bali', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'mulia-resort', '2021-11-03 02:51:11', '2021-11-10 05:23:29'),
(58, 'Fairmont Jakarta', 5, 'Jl. Asia Afrika No.8, Gelora, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10270', 'Central Jakarta', 1, '<p>Includes :</p>\r\n\r\n<p>- Daily meal provided by in Room Dinning (Breakfast, lunch, dinner)</p>\r\n\r\n<p>- Airport pickup from Airport</p>\r\n\r\n<p>- 5 pieces of laundry per day per person (non-accumulative)</p>\r\n\r\n<p>- Internet Connection (Wi-Fi)</p>\r\n\r\n<p>- 21% Government Tax and Service Charge</p>\r\n\r\n<p>- 2 Times PCR Test per person</p>\r\n\r\n<p>- Clearance Letter from SATGAS COVID-19</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>T&amp;C Apply :</p>\r\n\r\n<p>- 24 Hours room service is available (no outside food/beverage allowed refer to the new protocol of Covid-19)</p>\r\n\r\n<p>- Guest is not allowed to go outside the room during quarantine</p>\r\n\r\n<p>- 3 days 2 nights is valid for full dose vaccinated guest only (Vaccination Certificate Required)</p>', '100', 'fairmont-jakarta', '2021-11-10 05:39:34', '2021-11-10 05:40:16');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_galleries`
--

CREATE TABLE `hotel_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_galleries`
--

INSERT INTO `hotel_galleries` (`id`, `hotel_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'assets/quarantine-hotel/mvMpxvfcSS8EcYWyJVAZ2N0NFGJ2nRwZhJbYtMgj.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(2, 2, 'assets/quarantine-hotel/lpUvD82UuGk276T9Yj5izlVvdrOyJimQmEUvS9lw.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(3, 3, 'assets/quarantine-hotel/DNWdQUHqgpHqbor6LA2KTAjfruVHgsoFpMx1fwHO.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(4, 4, 'assets/quarantine-hotel/3VYToPOdx9Ej7tHlln2esSvb48HvZvyQGyFUuTjA.webp', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(5, 5, 'assets/quarantine-hotel/bdw2jCU1eILop8iEaouvsZSFQnSdUd1guFg26dQq.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(6, 6, 'assets/quarantine-hotel/VdzYDFOZLg52TXg3gb541fAYhjrpWNYTGEq7zUYN.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(7, 7, 'assets/quarantine-hotel/Ly01iUnNWiu24aIef3DwHjq7j8Qvbah3qV6BVHM4.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(8, 8, 'assets/quarantine-hotel/Phbn2Jvteb2PF5o4Zm4iieN1ubCtImWwEZI4tofz.png', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(9, 9, 'assets/quarantine-hotel/tRWfgsYOxbUkVwd0or7TOVLBiAJh2R0zfvheE2se.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(10, 10, 'assets/quarantine-hotel/rdWNFD6GhT5j1iONgdN97FmjQIgid5HzmPOPDTnX.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(11, 11, 'assets/quarantine-hotel/ocXSCcNew3bXUATRlj5GNcvt831MjsDnHKDuADYx.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(12, 12, 'assets/quarantine-hotel/HYJn0NRQ7mbmochA4mGBz5XpCXX6l8RtZwYbCFqQ.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(13, 13, 'assets/quarantine-hotel/KPsJ2SXJlgYJhnbJ1ODjbIz5BhJAepXIp4N70pRj.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(14, 14, 'assets/quarantine-hotel/7B3jAX4zr0DwVwlMOxTt0QwRjIJ1ElihMC7h3HUT.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(15, 15, 'assets/quarantine-hotel/c9gCdkL3Vy0QkM395Bsrr6C0kbIDu1gOVTVz8aLj.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(16, 16, 'assets/quarantine-hotel/3bynOr9FVu0qAiN8TDQJgasD0OTWlhQw1ena5r9m.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(17, 17, 'assets/quarantine-hotel/lZDq6qm1Ca9JwGeyViyRJgy8O3zgCzE28tRefwqf.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(18, 18, 'assets/quarantine-hotel/VGTMyPG1cQUxqeqCLynNIYRpdk3KwlwHL7wltQJl.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(19, 19, 'assets/quarantine-hotel/KE1dIwQEhEfwULWUE7bGlc3a3GbSqRvrNcofSWIS.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(20, 20, 'assets/quarantine-hotel/py1AQASZy8gmvt3A3zhSF4Nvt8PZOHLP28A72vaw.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(21, 21, 'assets/quarantine-hotel/EaYBj4juWmuLjxt1HdHf4deuf5SEockSZxcoYpY0.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(22, 22, 'assets/quarantine-hotel/zEJOBEJGCjuIAZ3JBuPpWAwNxOWIFFamosFLJsrr.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(23, 23, 'assets/quarantine-hotel/BKEt9lMEtS5FYQSEmy8vytJTfdbyRsEoPeUBpknY.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(24, 24, 'assets/quarantine-hotel/MtzQZVIgsUh8TXc8TGN3Chjsc9YqNBc2Sa12hNAf.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(25, 25, 'assets/quarantine-hotel/Fl0dVLxuqsgSmd8WQFuscWEkjxY40MG36UbYKlK4.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(26, 26, 'assets/quarantine-hotel/pq9Bqk5pvPv73F5dh3YB8xVj6asgrO2wfh0kyFzC.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(27, 27, 'assets/quarantine-hotel/TFbViH1T20tewDeEuplQiKIWIIZi4oaZ2HjLwsNO.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(28, 28, 'assets/quarantine-hotel/5gS88bwXVC36Hhbt2QcGc29vv9dMqSXEV0ixiYOX.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(29, 29, 'assets/quarantine-hotel/pABENp5VeGWgeGyGahb5Kgq9qZIJUvvaYFZ6VKFE.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(30, 30, 'assets/quarantine-hotel/LDGsqojbxB5IRfKlh0EwQNmmO9R7eWuxwp8KksQr.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(31, 31, 'assets/quarantine-hotel/bwFCjNFvRzeup7azHO8c110ZcgenWi4S6FCokrr1.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(32, 32, 'assets/quarantine-hotel/veU2XrIE4fpHCIIhF09vPVQBcPeJxpuOwbutch8k.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(33, 33, 'assets/quarantine-hotel/0eufOkqVTIh92W2UTqr2ZUElZOsz8FtFccm6B7ZN.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(34, 34, 'assets/quarantine-hotel/WyFzql7CzWSLxOfu2Dg904L9l9BiwKJNq5hR3RF5.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(35, 35, 'assets/quarantine-hotel/OhNk3b9RO7LRvvDVLHK5ykwjBdKvJ552ZPX6543u.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(36, 36, 'assets/quarantine-hotel/bOMckwfW3IYLWIgYrfGF1CoKKp8S18Xnhgutjcfz.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(37, 37, 'assets/quarantine-hotel/L8LEHeC8hvTdSTrr4plnX3i2eVzn5p71jhWOVRnx.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(38, 38, 'assets/quarantine-hotel/lqiQM4Nrz3tq0dBw14o9YL9kj2gcQeOx1NT4tkVE.webp', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(39, 39, 'assets/quarantine-hotel/ohx8vs8Vz1gjnCrOBSbJ3S8n2rjFCW03lMGnwA9I.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(40, 40, 'assets/quarantine-hotel/mhUDkwjjDEgNEUBfj2TGOwLCZSdp7qul2AaUGeFC.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(41, 41, 'assets/quarantine-hotel/hxplaJP4F9Cx4a3LOCJAczUFK64EJuRJNIlMk5Sf.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(42, 42, 'assets/quarantine-hotel/ZFTwnajR02bzg4iHMhLM60xdviJzZ9oLtN8L2yeW.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(43, 43, 'assets/quarantine-hotel/iT37chqljIch6v8X34vJMSVJimjdB0Sxxvd7xUPg.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(45, 45, 'assets/quarantine-hotel/lAxxVUEzVfe7F1NlkdUlPR5VPauoj4iebdqoQcrE.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(46, 46, 'assets/quarantine-hotel/Ipj2XKeNpP3vIPI5EwXKh5NHSRHC4bwPLixd0yQk.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(47, 47, 'assets/quarantine-hotel/QeOgm0Tn9ebsoNTrsbRRTdHKuePJsKQIE1dodssO.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(48, 48, 'assets/quarantine-hotel/ifa3YEUGoToWe0EepYIL4gSSsodBAhD3c5bwwqfU.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(49, 49, 'assets/quarantine-hotel/F6rbg1RyDKEDaa6WrzDe9pqpjwrlWGWTuvhdjgWD.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(50, 50, 'assets/quarantine-hotel/cc1zgpi19meh78RiY04Q8fJUXdjR8neYitLUY2GV.jpg', '2021-09-09 04:00:26', '2021-09-09 04:00:26'),
(51, 51, 'assets/quarantine-hotel/bali-dynasty-resort-hotel.jpeg', '2021-11-03 01:35:30', '2021-11-03 01:51:40'),
(52, 52, 'assets/quarantine-hotel/Nusa Dua.jpeg', '2021-11-03 02:03:26', '2021-11-03 02:03:26'),
(53, 53, 'assets/quarantine-hotel/melia bali.jpeg', '2021-11-03 02:19:14', '2021-11-03 02:19:14'),
(54, 54, 'assets/quarantine-hotel/Astonkuta_hotel_exterior02.jpeg', '2021-11-03 02:31:49', '2021-11-03 02:31:49'),
(55, 55, 'assets/quarantine-hotel/swiss.jpeg', '2021-11-03 02:37:21', '2021-11-03 02:41:47'),
(56, 56, 'assets/quarantine-hotel/prime.jpeg', '2021-11-03 02:48:03', '2021-11-03 02:48:03'),
(57, 57, 'assets/quarantine-hotel/mulia-resort.jpeg', '2021-11-03 02:54:25', '2021-11-03 03:06:52');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms`
--

CREATE TABLE `hotel_rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_rooms`
--

INSERT INTO `hotel_rooms` (`id`, `room_name`, `created_at`, `updated_at`) VALUES
(3, 'Suite Room', '2021-09-06 07:06:59', '2021-09-06 07:06:59'),
(4, 'Premier Room', '2021-11-03 01:12:33', '2021-11-03 02:20:10'),
(5, 'Deluxe with Pool View', '2021-11-03 02:37:54', '2021-11-03 02:37:54'),
(6, 'Grandeur Room', '2021-11-03 02:54:37', '2021-11-03 02:54:37'),
(7, 'Guest Room Twin Bed', '2021-11-09 19:30:11', '2021-11-09 19:30:11'),
(8, 'Guest Room King Bed', '2021-11-09 19:30:29', '2021-11-09 19:30:29'),
(9, 'Larger Guest Room', '2021-11-09 19:30:50', '2021-11-09 19:30:50'),
(10, 'Additional Person', '2021-11-09 19:36:32', '2021-11-09 19:36:32'),
(11, 'Superior Room', '2021-11-09 20:21:50', '2021-11-09 20:21:50'),
(12, 'Deluxe Room', '2021-11-09 20:22:16', '2021-11-09 20:22:16'),
(13, 'Urban Suite', '2021-11-09 20:22:46', '2021-11-09 20:22:46'),
(14, 'Oriental Suite', '2021-11-09 20:23:03', '2021-11-09 20:23:03'),
(15, 'Deluxe Suite Premier Floor', '2021-11-10 05:32:44', '2021-11-10 05:32:44'),
(16, 'Fairmont Room (49sqm)', '2021-11-10 05:40:49', '2021-11-10 05:41:36'),
(17, 'Fairmont Suite (113sqm)', '2021-11-10 05:41:21', '2021-11-10 05:41:21'),
(18, 'Grand Deluxe', '2021-11-10 05:46:47', '2021-11-10 05:46:47'),
(19, 'Super Deluxe Room', '2021-11-10 05:49:41', '2021-11-10 05:49:41'),
(20, 'Executive Suite Room', '2021-11-10 05:50:02', '2021-11-10 05:50:02'),
(21, 'Standart Room', '2021-11-10 05:55:08', '2021-11-10 05:55:08'),
(22, 'Privillage Room', '2021-11-10 06:04:28', '2021-11-10 06:04:28'),
(23, 'Deluxe Room + Bathtub', '2021-11-10 06:11:02', '2021-11-10 06:11:02'),
(24, 'Junior Suite Room', '2021-11-10 06:11:26', '2021-11-10 06:11:26'),
(25, 'Apartement Two Bedroom', '2021-11-10 06:23:20', '2021-11-10 06:23:20'),
(26, 'Apartement Three Bedroom', '2021-11-10 06:23:38', '2021-11-10 06:23:38'),
(27, 'Business Room', '2021-11-10 06:26:56', '2021-11-10 06:26:56'),
(28, 'Splendoor Room', '2021-11-10 06:39:46', '2021-11-10 06:39:46'),
(29, 'Royal Suite Room', '2021-11-10 06:40:02', '2021-11-10 06:40:02'),
(30, 'Family Suite Room', '2021-11-10 06:45:23', '2021-11-10 06:45:23'),
(31, 'Raffles Room (60sqm)', '2021-11-10 06:52:57', '2021-11-10 06:52:57'),
(32, 'Artist Suite (97sqm)', '2021-11-10 06:53:14', '2021-11-10 06:53:14'),
(33, 'Gallery Suite (135sqm)', '2021-11-10 06:53:39', '2021-11-10 06:53:39'),
(34, 'Royal Suite (198sqm)', '2021-11-10 06:53:55', '2021-11-10 06:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_room_prices`
--

CREATE TABLE `hotel_room_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_occupant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_price` decimal(12,2) NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `room_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_room_prices`
--

INSERT INTO `hotel_room_prices` (`id`, `room_occupant`, `room_price`, `hotel_id`, `room_id`, `created_at`, `updated_at`) VALUES
(9, '1', 3900000.00, 2, 12, '2021-09-07 19:53:35', '2021-11-10 05:33:18'),
(10, '2', 5100000.00, 2, 12, '2021-09-07 19:53:55', '2021-11-10 05:33:39'),
(11, '1', 5500000.00, 2, 15, '2021-09-07 19:54:28', '2021-11-10 05:34:03'),
(31, '2', 7200000.00, 2, 15, '2021-09-07 20:19:50', '2021-11-10 05:34:59'),
(32, '2', 28880000.00, 8, 3, '2021-09-07 20:20:15', '2021-09-07 20:20:15'),
(39, '1', 10400000.00, 9, 3, '2021-09-07 20:23:27', '2021-10-17 23:57:58'),
(40, '2', 14000000.00, 9, 3, '2021-09-07 20:23:46', '2021-10-17 23:58:20'),
(41, 'Additional', 6150000.00, 9, 3, '2021-09-07 20:24:02', '2021-10-18 01:11:54'),
(45, '1', 7465000.00, 10, 3, '2021-09-08 04:31:23', '2021-10-18 01:17:36'),
(46, '2', 11405000.00, 10, 3, '2021-09-08 04:31:56', '2021-10-18 01:18:11'),
(80, '1', 10190000.00, 19, 3, '2021-09-08 05:17:00', '2021-09-08 05:17:00'),
(81, '2', 13080000.00, 19, 3, '2021-09-08 05:17:27', '2021-09-08 05:17:27'),
(82, 'Additional', 4400000.00, 19, 3, '2021-09-08 05:18:07', '2021-09-08 05:18:07'),
(91, '1', 18790000.00, 21, 3, '2021-09-08 05:22:25', '2021-10-18 01:41:46'),
(92, '2', 21830000.00, 21, 3, '2021-09-08 05:22:56', '2021-10-18 01:42:05'),
(93, 'Additional', 4150000.00, 21, 3, '2021-09-08 05:23:36', '2021-10-18 01:42:27'),
(98, '1', 5800000.00, 22, 18, '2021-09-08 05:27:03', '2021-11-10 06:20:35'),
(105, '1', 4200000.00, 23, 3, '2021-09-09 04:12:06', '2021-11-10 06:43:28'),
(106, '2', 6200000.00, 23, 3, '2021-09-09 04:12:28', '2021-11-10 06:43:50'),
(111, '1', 18700000.00, 24, 3, '2021-09-09 04:17:20', '2021-09-09 04:17:20'),
(112, '2', 22300000.00, 24, 3, '2021-09-09 04:17:41', '2021-09-09 04:17:41'),
(117, '1', 6500000.00, 25, 11, '2021-09-09 04:39:54', '2021-11-09 20:24:25'),
(118, '2', 7700000.00, 25, 11, '2021-09-09 04:40:14', '2021-11-09 20:25:04'),
(142, '1', 12500000.00, 31, 3, '2021-09-09 04:51:43', '2021-09-09 04:54:18'),
(143, '2', 18830000.00, 31, 3, '2021-09-09 04:51:44', '2021-09-09 04:54:38'),
(150, '1', 3500000.00, 33, 11, '2021-09-09 04:57:26', '2021-11-10 06:32:21'),
(160, '1', 5900000.00, 36, 7, '2021-09-09 05:03:04', '2021-11-09 19:32:24'),
(161, '2', 8500000.00, 36, 7, '2021-09-09 05:03:05', '2021-11-09 19:33:51'),
(163, '1', 6900000.00, 36, 8, '2021-09-09 05:03:07', '2021-11-09 20:10:54'),
(164, '2', 9500000.00, 36, 8, '2021-09-09 05:03:08', '2021-11-09 20:11:28'),
(166, '1', 8900000.00, 36, 9, '2021-09-09 05:03:09', '2021-11-09 20:13:16'),
(167, '2', 11500000.00, 36, 9, '2021-09-09 05:03:10', '2021-11-09 20:13:49'),
(173, '1', 4400000.00, 37, 3, '2021-09-09 05:07:33', '2021-11-10 06:29:09'),
(174, '2', 6400000.00, 37, 3, '2021-09-09 05:07:34', '2021-11-10 06:29:23'),
(192, '1', 10900000.00, 41, 3, '2021-09-09 05:18:58', '2021-09-09 05:21:01'),
(193, '2', 15400000.00, 41, 3, '2021-09-09 05:18:59', '2021-09-09 05:21:19'),
(201, '1', 3300000.00, 43, 11, '2021-09-09 05:23:18', '2021-11-10 06:12:21'),
(202, '2', 4700000.00, 43, 11, '2021-09-09 05:23:19', '2021-11-10 06:12:42'),
(211, '1', 7700000.00, 45, 3, '2021-09-09 05:28:02', '2021-09-09 05:30:32'),
(212, '2', 11600000.00, 45, 3, '2021-09-09 05:28:03', '2021-09-09 05:30:49'),
(213, 'Additional', 4050000.00, 45, 3, '2021-09-09 05:28:04', '2021-09-09 05:31:06'),
(231, '2', 10000000.00, 35, 3, '2021-09-23 03:19:54', '2021-09-23 03:19:54'),
(235, '1', 6700000.00, 15, 12, '2021-10-18 21:34:59', '2021-11-10 05:44:28'),
(236, '2', 8300000.00, 15, 12, '2021-10-18 21:35:21', '2021-11-10 05:45:41'),
(241, '1', 9210000.00, 53, 4, '2021-11-03 02:20:39', '2021-11-03 02:20:39'),
(242, '2', 14460000.00, 53, 4, '2021-11-03 02:21:51', '2021-11-03 02:21:51'),
(245, '1', 8800000.00, 55, 5, '2021-11-03 02:38:26', '2021-11-03 02:38:26'),
(246, '2', 14400000.00, 55, 5, '2021-11-03 02:38:44', '2021-11-03 02:38:44'),
(247, '1', 8000000.00, 56, 3, '2021-11-03 02:48:32', '2021-11-03 02:48:32'),
(248, '2', 14500000.00, 56, 3, '2021-11-03 02:48:51', '2021-11-03 02:48:51'),
(249, '1', 12650000.00, 57, 6, '2021-11-03 02:55:07', '2021-11-03 02:55:07'),
(250, '2', 20100000.00, 57, 6, '2021-11-03 02:55:28', '2021-11-03 02:55:28'),
(251, '1', 11900000.00, 36, 3, '2021-11-09 20:18:53', '2021-11-09 20:18:53'),
(252, '2', 14500000.00, 36, 3, '2021-11-09 20:19:54', '2021-11-09 20:19:54'),
(253, '1', 2600000.00, 36, 10, '2021-11-09 20:20:53', '2021-11-09 20:20:53'),
(254, '1', 7350000.00, 25, 12, '2021-11-09 21:06:40', '2021-11-09 21:06:40'),
(255, '2', 8500000.00, 25, 12, '2021-11-09 21:07:16', '2021-11-09 21:07:16'),
(256, '1', 11000000.00, 25, 13, '2021-11-09 21:07:50', '2021-11-09 21:07:50'),
(257, '2', 12800000.00, 25, 13, '2021-11-09 21:08:12', '2021-11-09 21:08:12'),
(258, '1', 19500000.00, 25, 14, '2021-11-09 21:08:49', '2021-11-09 21:08:49'),
(259, '2', 22500000.00, 25, 14, '2021-11-09 21:09:11', '2021-11-09 21:09:11'),
(260, '1', 1500000.00, 2, 10, '2021-11-10 05:35:44', '2021-11-10 05:35:44'),
(261, '1', 1700000.00, 2, 10, '2021-11-10 05:36:18', '2021-11-10 05:36:18'),
(262, '1', 9700000.00, 58, 16, '2021-11-10 05:42:08', '2021-11-10 05:42:08'),
(263, '1', 9700000.00, 58, 16, '2021-11-10 05:42:27', '2021-11-10 05:42:27'),
(264, '2', 11840700.00, 58, 16, '2021-11-10 05:43:02', '2021-11-10 05:43:02'),
(265, '1', 13700000.00, 58, 17, '2021-11-10 05:43:25', '2021-11-10 05:43:25'),
(266, '2', 15800000.00, 58, 17, '2021-11-10 05:43:45', '2021-11-10 05:43:45'),
(267, '1', 7900000.00, 15, 18, '2021-11-10 05:47:22', '2021-11-10 05:47:22'),
(268, '2', 9500000.00, 15, 18, '2021-11-10 05:47:43', '2021-11-10 05:47:43'),
(269, '1', 2900000.00, 7, 12, '2021-11-10 05:50:53', '2021-11-10 05:50:53'),
(270, '2', 4300000.00, 7, 12, '2021-11-10 05:51:19', '2021-11-10 05:51:19'),
(271, '1', 3000000.00, 7, 19, '2021-11-10 05:51:51', '2021-11-10 05:51:51'),
(272, '2', 3200000.00, 7, 19, '2021-11-10 05:52:39', '2021-11-10 05:52:39'),
(273, '1', 5400000.00, 7, 20, '2021-11-10 05:53:18', '2021-11-10 05:53:18'),
(274, '2', 6900000.00, 7, 20, '2021-11-10 05:54:06', '2021-11-10 05:54:06'),
(275, '1', 9450000.00, 29, 21, '2021-11-10 05:56:21', '2021-11-10 05:56:21'),
(276, '2', 13300000.00, 29, 21, '2021-11-10 05:56:59', '2021-11-10 05:56:59'),
(277, '1', 3600000.00, 46, 11, '2021-11-10 06:03:10', '2021-11-10 06:03:10'),
(278, '2', 6350000.00, 46, 11, '2021-11-10 06:04:08', '2021-11-10 06:04:08'),
(279, '1', 4200000.00, 46, 22, '2021-11-10 06:05:38', '2021-11-10 06:05:38'),
(280, '2', 6950000.00, 46, 22, '2021-11-10 06:06:57', '2021-11-10 06:06:57'),
(281, '1', 3000000.00, 20, 21, '2021-11-10 06:09:15', '2021-11-10 06:09:15'),
(282, '2', 4000000.00, 20, 21, '2021-11-10 06:09:33', '2021-11-10 06:09:33'),
(283, '1', 3600000.00, 43, 12, '2021-11-10 06:13:24', '2021-11-10 06:13:24'),
(284, '2', 5000000.00, 43, 12, '2021-11-10 06:13:55', '2021-11-10 06:13:55'),
(285, '1', 4200000.00, 43, 23, '2021-11-10 06:14:24', '2021-11-10 06:14:24'),
(286, '2', 5600000.00, 43, 23, '2021-11-10 06:14:53', '2021-11-10 06:14:53'),
(287, '1', 5000000.00, 43, 24, '2021-11-10 06:17:57', '2021-11-10 06:17:57'),
(288, '2', 6600000.00, 43, 24, '2021-11-10 06:18:27', '2021-11-10 06:18:27'),
(289, '1', 8000000.00, 22, 18, '2021-11-10 06:22:03', '2021-11-10 06:22:03'),
(290, '1', 8900000.00, 22, 24, '2021-11-10 06:22:30', '2021-11-10 06:22:30'),
(291, '2', 10900000.00, 22, 24, '2021-11-10 06:22:49', '2021-11-10 06:22:49'),
(292, '4', 19800000.00, 22, 25, '2021-11-10 06:24:15', '2021-11-10 06:26:12'),
(293, '6', 23800000.00, 22, 26, '2021-11-10 06:25:47', '2021-11-10 06:25:47'),
(294, '1', 3700000.00, 37, 27, '2021-11-10 06:27:39', '2021-11-10 06:27:39'),
(295, '2', 5700000.00, 22, 27, '2021-11-10 06:29:58', '2021-11-10 06:29:58'),
(296, '1', 3300000.00, 37, 12, '2021-11-10 06:30:37', '2021-11-10 06:30:37'),
(297, '2', 5300000.00, 37, 12, '2021-11-10 06:31:07', '2021-11-10 06:31:07'),
(298, '1', 5400000.00, 33, 3, '2021-11-10 06:33:08', '2021-11-10 06:33:08'),
(299, '2', 7300000.00, 33, 3, '2021-11-10 06:34:24', '2021-11-10 06:34:24'),
(300, '1', 5400000.00, 30, 21, '2021-11-10 06:36:44', '2021-11-10 06:36:44'),
(301, '2', 9400000.00, 30, 21, '2021-11-10 06:37:17', '2021-11-10 06:37:17'),
(302, '1', 2600000.00, 30, 10, '2021-11-10 06:37:49', '2021-11-10 06:37:49'),
(303, '1', 10300000.00, 14, 24, '2021-11-10 06:38:44', '2021-11-10 06:38:44'),
(304, '2', 12600000.00, 14, 24, '2021-11-10 06:39:10', '2021-11-10 06:39:10'),
(305, '1', 5700000.00, 14, 28, '2021-11-10 06:40:38', '2021-11-10 06:40:38'),
(306, '2', 7800000.00, 14, 28, '2021-11-10 06:40:59', '2021-11-10 06:40:59'),
(307, '1', 15300000.00, 14, 29, '2021-11-10 06:41:52', '2021-11-10 06:41:52'),
(308, '2', 16600000.00, 14, 29, '2021-11-10 06:42:23', '2021-11-10 06:42:23'),
(309, '1', 2900000.00, 23, 21, '2021-11-10 06:44:20', '2021-11-10 06:44:20'),
(310, '2', 4900000.00, 23, 21, '2021-11-10 06:44:48', '2021-11-10 06:44:48'),
(311, '3', 9950000.00, 23, 30, '2021-11-10 06:46:05', '2021-11-10 06:46:05'),
(312, '1', 9990000.00, 34, 31, '2021-11-10 06:54:49', '2021-11-10 06:54:49'),
(313, '2', 13340000.00, 34, 31, '2021-11-10 06:55:11', '2021-11-10 06:55:11'),
(314, '1', 11900000.00, 34, 32, '2021-11-10 06:55:41', '2021-11-10 06:55:41'),
(315, '2', 15250000.00, 34, 32, '2021-11-10 06:56:07', '2021-11-10 06:56:07'),
(316, '1', 18990000.00, 34, 33, '2021-11-10 06:56:35', '2021-11-10 06:56:35'),
(317, '2', 22340000.00, 34, 33, '2021-11-10 06:57:17', '2021-11-10 06:57:17'),
(318, '1', 24890000.00, 34, 34, '2021-11-10 06:58:44', '2021-11-10 06:58:44'),
(319, '2', 28240000.00, 34, 34, '2021-11-10 06:59:40', '2021-11-10 06:59:40'),
(320, '1', 4000000.00, 28, 11, '2021-11-10 07:02:26', '2021-11-10 07:02:26'),
(321, '2', 6300000.00, 28, 11, '2021-11-10 07:06:03', '2021-11-10 07:06:03');

-- --------------------------------------------------------

--
-- Table structure for table `inclusives`
--

CREATE TABLE `inclusives` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_id` bigint(20) UNSIGNED NOT NULL,
  `incl1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl13` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl14` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl15` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl16` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl17` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl18` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl19` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incl20` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl13` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl14` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl15` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl16` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl17` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl18` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl19` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uincl20` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inclusives`
--

INSERT INTO `inclusives` (`id`, `travel_package_id`, `incl1`, `incl2`, `incl3`, `incl4`, `incl5`, `incl6`, `incl7`, `incl8`, `incl9`, `incl10`, `incl11`, `incl12`, `incl13`, `incl14`, `incl15`, `incl16`, `incl17`, `incl18`, `incl19`, `incl20`, `uincl1`, `uincl2`, `uincl3`, `uincl4`, `uincl5`, `uincl6`, `uincl7`, `uincl8`, `uincl9`, `uincl10`, `uincl11`, `uincl12`, `uincl13`, `uincl14`, `uincl15`, `uincl16`, `uincl17`, `uincl18`, `uincl19`, `uincl20`, `created_at`, `updated_at`) VALUES
(24, 47, 'Tiket Pesawat PP', 'Hotel berbintang 4 (Twin Rooms)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa Perancis', 'Biaya makan dan minum selama Tur berjalan', 'PCR Test Jakarta/Paris (jika diperlukan)', 'Tour opsional dan pengeluaran pribadi', 'Excess baggage/kelebihan bagasi (max. 30 kg, cabin max. 7 kg), dan biaya bea cukai (bila ada)', 'Medical expense (in case of COVID-19)', 'Asuransi jiwa/kesehatan (tidak wajib)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-13 23:38:23', '2021-10-13 23:38:23'),
(25, 48, 'Tiket Pesawat CGK/ZRH/CGK', 'Hotel berbintang 5 (Twin Rooms)', 'Acara Tour Lucerne & Mount Titlis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa Swiss', 'Tipping untuk Guide', 'PCR Test Jakarta/Zurich (jika diperlukan)', 'Biaya makan dan minum selama Tur berjalan', 'Tipping untuk Tour Leader & Local Driver', 'Tour opsional dan pengeluaran pribadi', 'Excess baggage/kelebihan bagasi (max. 30 kg, cabin max. 7 kg), dan biaya bea cukai (bila ada)', 'Medical expense (in case of COVID-19)', 'Asuransi jiwa/kesehatan (tidak wajib)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-13 23:47:05', '2021-10-13 23:47:05'),
(27, 50, 'Tiket Pesawat PP', 'Hotel berbintang 3 (Twin Rooms)', 'Toledo Tour', 'Transfer Airport – Hotel - Airport', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa Spain', 'Biaya makan dan minum selama Tur berjalan', 'Tipping untuk Guide, Tour Leader & Local Driver', 'PCR Test Jakarta/Madrid (jika diperlukan)', 'Tour opsional dan pengeluaran pribadi', 'Excess baggage/kelebihan bagasi (max. 30 kg, cabin max. 7 kg), dan biaya bea cukai (bila ada)', 'Medical expense (in case of COVID-19)', 'Asuransi jiwa/kesehatan (tidak wajib)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-13 23:56:55', '2021-10-13 23:56:55'),
(28, 51, 'Tiket Pesawat Turkish Airlines PP', 'Hotel berbintang 3 (Twin Rooms)', 'Tour ZAANSE SCHANS, VOLENDAM &  MARKEM', 'Transfer Airport – Hotel - Airport', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa', 'Biaya makan dan minum selama Tur berjalan', 'Tipping untuk Guide, Tour Leader & Local Driver', 'PCR Test Jakarta/Amsterdam (jika diperlukan)', 'Tour opsional dan pengeluaran pribadi', 'Excess baggage/kelebihan bagasi (max. 30 kg, cabin max. 7 kg), dan biaya bea cukai (bila ada)', 'Medical expense (in case of COVID-19)', 'Asuransi jiwa/kesehatan (tidak wajib)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 00:18:34', '2021-10-14 00:18:34'),
(29, 52, 'Mobil Pribadi sesuai acara di Itinerary (Free Program tidak ada standby mobil)', 'Akomodasi Hotel Selama 25 Malam (tidak ada Extra Bed)', 'Tiket pesawat PP : Economy Class by All Nippon Airways (NH) under K class. Untuk yang Min 4 orang based on promo ANA GV4 (K Class)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Domestic Flight SFO - HNL', 'Biaya Visa USA', 'Biaya Makan dan Minum selama Tur Berjalan', 'Biaya PCR Keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR di San Francisco ( jika diperlukan)', 'Biaya PCR di Los Angeles (jika diperlukan)', 'Biaya Pengeluaran Pribadi.', 'Biaya Tur Tambahan (Optional Tour) jika ada.', 'Biaya Travel Insurance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 00:34:09', '2021-10-14 00:34:09'),
(30, 53, 'Mobil Pribadi Sepanjang Perjalanan', 'Akomodasi Hotel Selama 3 Malam', 'Biaya PCR di Los Angeles', 'Tiket pesawat PP : Economy Class by All Nippon Always (NH) (GV4), JL,TK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement : IDR 3,500,000,-', 'Biaya Makan dan Minum lama Tur Berjalan', 'Biaya PCR Keberangkatan dari Jakarta (jika diperlukan)', 'Biaya Pengeluaran Pribadi.', 'Biaya Tur Tambahan (Optional Tour) jika ada.', 'Biaya Tipping Driver $12/ Orang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 01:05:48', '2021-10-14 01:05:48'),
(31, 54, 'Mobil Pribadi Sepanjang Perjalanan', 'Akomodasi Hotel Selama 3 Malam', 'Biaya PCR di Los Angeles', 'Tiket pesawat PP : Economy Class by All Nippon Always (NH) (GV4), JL,TK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement : IDR 3,500,000,-', 'Biaya Makan dan Minum lama Tur Berjalan', 'Biaya PCR Keberangkatan dari Jakarta (jika diperlukan)', 'Biaya Pengeluaran Pribadi.', 'Biaya Tur Tambahan (Optional Tour) jika ada.', 'Biaya Tipping Driver $12/ Orang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 01:23:51', '2021-10-14 01:23:51'),
(32, 55, 'Mobil Pribadi Sepanjang Perjalanan sesuai acara di Itinerary (Free Program tidak ada standby Mobil)', 'Akomodasi Hotel Selama 24 Malam', 'Tiket pesawat PP : Economy Class by All Nippon Airways (NH) (GV4)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Biaya Makan dan Minum selama Tur Berjalan', 'Biaya PCR Keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR di USA (Jika diperlukan & tidak bisa mendapatkan PCR Test free)', 'Biaya Pengeluaran Pribadi.', 'Biaya Tur Tambahan (Optional Tour) jika ada.', 'Biaya Tipping Driver $12/ Orang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 01:55:53', '2021-10-14 01:55:53'),
(34, 57, 'Mobil Pribadi Sepanjang Perjalanan sesuai acara di Ittinerary (Free Program tidak ada standby Mobil)', 'Akomodasi Hotel Selama 24 Malam', 'Tiket pesawat PP : Economy Class by Singapore Airlines', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Biaya Makan dan Minum selama Tur Berjalan', 'Biaya PCR Keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR di USA (Jika diperlukan & tidak bisa mendapatkan PCR Test free)', 'Biaya Pengeluaran Pribadi.', 'Biaya Tur Tambahan (Optional Tour) jika ada.', 'Biaya Tipping Driver $12/ Orang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 02:44:08', '2021-10-14 02:44:08'),
(35, 58, 'Akomodasi Hotel untuk 5 Malam (1 kamar)', 'Mobil Pribadi sesuai acara di Itinerary (Free Program tidak ada standby Mobil)', 'Tiket Pesawat :\r\nEconomy Class by EY/TK/EK/QR', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Biaya Makan & Minum selama Tur Berjalan', 'Biaya tambahan apabila Check in Hotel di hari Jumat dan Sabtu.', 'Biaya PCR keberangkatan dari Jakarta(jika diperlukan)', 'Biaya PCR kepulangan dari USA (jika diperlukan) (tersedia di Times Square Area)', 'Biaya Pengeluaran pribadi', 'Biaya tur tambahan (Optional Tour) Jika ada.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 20:57:15', '2021-10-14 20:57:15'),
(36, 59, 'Akomodasi Hotel untuk 5 Malam (1 kamar)', 'Mobil Pribadi sesuai acara di Itinerary (Free Program tidak ada standby Mobil)', 'Tiket Pesawat :\r\nEconomy Class by (EY) under U class promo', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Biaya Makan & Minum selama Tur Berjalan', 'Biaya PCR keberangkatan dari Jakarta(jika diperlukan)', 'Biaya PCR kepulangan dari USA (jika diperlukan) (tersedia di Times Square Area)', 'Biaya Pengeluaran pribadi dan biaya tur tambahan (Optional Tour) Jika ada.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 21:12:17', '2021-10-14 21:12:17'),
(37, 60, 'Akomodasi Hotel untuk 24 Malam (1 kamar) tidak ada extra bed', 'Mobil Pribadi sesuai acara di Itinerary transfer In & Out only (Free Program tidak ada standby Mobil)', 'Tiket Pesawat PP :\r\nEconomy Class by  EY (U Class)', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Biaya Makan & Minum selama Tur Berjalan', 'Biaya PCR keberangkatan dari Jakarta(jika diperlukan)', 'Biaya PCR kepulangan di New York ( estimasi $150-$200/orang) (jika tidak bisa dapat yang free PCR Test)', 'Biaya Travel Insurance', 'Biaya Pengeluaran pribadi dan biaya tur tambahan (Optional Tour) Jika ada.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 21:29:20', '2021-10-14 21:29:20'),
(38, 61, 'Akomodasi Hotel untuk 3 Malam (1 kamar)', 'Mobil Pribadi sesuai acara di Itinerary (Free Program tidak ada standby Mobil)', 'Tiket Pesawat :\r\nEconomy Class by  QR / EK', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Biaya Makan & Minum selama Tur Berjalan', 'Biaya tambahan apabila Check In hotel di hari Jumat dan Sabtu', 'Biaya PCR keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR kepulangan dari USA (jika diperlukan) (tersedia di Times Square Area)', 'Biaya Pengeluaran pribadi dan biaya tur tambahan (Optional Tour) Jika ada.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 21:46:37', '2021-10-14 21:46:37'),
(39, 62, 'Mobil Pribadi sepanjang perjalanan dengan asuransi', 'Sarapan pagi dari hotel', 'Tour Leader dari Indonesia', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement : IDR 9,900,000,-', 'Tiket Pesawat Internasional', 'Biaya Weekend Surcharge jika menginap hari Jumat dan Sabtu yang berlaku di Las Vegas', 'Biaya Makan & Minum pada makan Siang dan Malam selama Tur Berjalan', 'Biaya PCR keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR kepulangan dari USA (jika diperlukan)', 'Tipping yang diwajibkan untuk driver USD 50/Mobil/Hari. (Pembayaran Tips dilakukan di Negara tujuan berdasarkan mata uang setempat).', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 22:05:00', '2021-10-14 22:05:00'),
(40, 63, 'Mobil Pribadi sepanjang perjalanan dengan asuransi', 'Tour Leader dari Indonesia', 'Airport tax JKT + Internasional & fuel surcharge', 'Tiket Pesawat PP:\r\nEconomy Class by Singapore Airlines (SQ)\r\nEconomy Class by All Nippon Airways (NH)(GV4)', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya Visa USA', 'Single Supplement : IDR 9,900,000,-', 'Biaya Weekend Surcharge jika menginap hari Jumat dan Sabtu yang berlaku di Las Vegas', 'Biaya PCR keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR kepulangan dari USA(jika diperlukan)', 'Tipping yang diwajibkan untuk driver USD50/Mobil/hari. (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 22:19:40', '2021-10-14 22:19:40'),
(41, 64, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance **Syarat dan Ketentuan\r\nBerlaku*', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement: IDR 12,000,000 per orang.', 'Tipping yang diwajibkan untuk Tour Leader IDR\r\n45,000/orang/hari, driver IDR 45,000 /orang/hari. (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat)', 'Visa USA', 'PCR Test Indonesia sebelum keberangkatan (jika\r\ndiperlukan)', 'PCR Test USA sebelum kepulangan (jika diperlukan)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 22:53:20', '2021-10-14 22:53:20'),
(42, 65, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi', 'Mineral water 2 botol/orang/hari', 'Tipping Tour Leader', 'Tipping Driver', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance', 'PPN1%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Visa USA', 'PCR Test Indonesia sebelum keberangkatan (jika diperlukan)', 'PCR Test USA sebelum kepulangan (jika diperlukan)', 'Biaya makan dan minum selama perjalanan', 'Biaya pengeluaran pribadi, laundry, porter.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-14 23:11:12', '2021-10-14 23:11:12'),
(43, 66, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance **Syarat dan Ketentuan Berlaku**', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement: IDR 11,000,000 per orang.', 'Tipping yang diwajibkan untuk Tour Leader IDR\r\n45,000/orang/hari, driver IDR 45,000 /orang/hari. (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat)', 'Visa USA', 'PCR Test Indonesia sebelum keberangkatan (jika diperlukan)', 'PCR Test USA sebelum kepulangan (jika diperlukan)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-15 02:17:18', '2021-10-15 02:17:18'),
(44, 67, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement: IDR 10,500,000 per orang.', 'Tipping yang diwajibkan untuk Tour Leader EUR 3/orang/hari, Local Guide EUR 2/orang/hari & Driver EUR 2/orang/hari (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat).', 'Biaya Visa', 'Biaya PCR jakarta (72 jam sebelum keberangkatan) dan PCR di Swiss sebelum kembali ke jakarta (Estimasi CHF 195-250).', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-17 20:24:28', '2021-10-17 20:24:28'),
(45, 68, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi 5* atau setaraf', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance *syarat & kondisi berlaku (KEDUTAAN SPANYOL Tidak boleh ada nama tambahan di halaman belakang paspor (see page 4) dalam paspor dan dibutuhkan Asuransi perjalanan (Travel Insurance) minimal 1 bulan berlaku pada saat keberangkatan.)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement: IDR 8,500,000 per orang.', 'Tipping yang diwajibkan untuk Tour Leader IDR 50.000/orang/hari, Local Guide IDR 35.000/orang/hari & Driver IDR 35.000/orang/hari (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat).', 'Biaya Visa Spain.', 'Biaya PCR test di Jakarta (72 jam sebelum keberangkatan)', 'Biaya PCR test di Spanyol sebelum menuju Jakarta (Estimasi Eur 80-100)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-17 21:08:49', '2021-10-17 21:08:49'),
(46, 69, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi 4* atau setaraf', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement: IDR 16,500,000 per orang.', 'Tipping yang diwajibkan untuk Tour Leader EUR 03/orang/hari, local guide EUR 02/orang/hari & driver EUR 02/orang/hari (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat).', 'Biaya Visa', 'Biaya PCR test sebelum keberangkatan', 'Biaya PCR test di Helsinki sebelum kepulangan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-17 21:38:40', '2021-10-17 21:38:40'),
(47, 70, 'AIRPORT TAX JAKARTA & INTERNATIONAL /+FUEL SURCHARGE', 'Tiket Pesawat Internasional', 'Akomodasi 4* atau setaraf', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Travel Insurance', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement: IDR 16,500,000 per orang.', 'Tipping yang diwajibkan untuk Tour Leader EUR 03/orang/hari, local guide EUR 02/orang/hari & driver EUR 02/orang/hari (Pembayaran Tips dilakukan di Negara Tujuan berdasarkan mata uang setempat).', 'Biaya Visa', 'Biaya PCR test sebelum keberangkatan', 'Biaya PCR test di Helsinki sebelum kepulangan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-17 21:48:31', '2021-10-17 21:48:31'),
(48, 71, 'Tiket penerbangan International, Airport Tax dan Fuel Surcharge (based on availability seat)', 'Akomodasi bintang 5* atau setaraf', 'Bus Wisata Free Wifi', 'Mineral water 2 botol/orang/hari', '1X Makan siang/malam di Chinese restaurant – Istanbul', 'Biaya Semua admission dan entrance ticket sesuai dengan program perjalanan', 'Biaya English/Indonesian Speaking Guide', 'Travel kit masker & hand sanitizer', 'Biaya Tipping Hotel dan Restaurant', 'Biaya Bosphorus Cruise Public', 'Biaya Parkir dan Tol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya pengeluaran pribadi seperti laundry, mini bar, biaya tour tambahan (Optional Tour).', 'Biaya PCR Test sebelum keberangkatan  (diwajibkan)', 'Biaya PCR Turkey (untuk kembali ke Indonesia) 300 TKY/ Orang', 'Visa Turkey IDR 600.000', 'Single Supplement : IDR 4,250,000 per orang', 'Tipping yang diwajibkan untuk Tour Leader IDR 30,000/orang/hari, Driver IDR 30,000/orang/hari, Local Guide IDR 45,000/orang/hari', 'Biaya tambahan seperti weekend surcharge, festival ataupun high season period', 'Excess baggage/kelebihan bagasi', 'Asuransi perjalanan', 'Acara tur luar program', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-17 22:10:19', '2021-10-17 22:10:19'),
(49, 72, 'Tiket penerbangan International, Airport Tax dan Fuel Surcharge (based on availability seat)', 'Akomodasi bintang 5* atau setaraf', 'Bus Wisata Free Wifi', 'Mineral water 2 botol/orang/hari', '1X Makan siang/malam di Chinese restaurant – Istanbul', '1X Makan siang/malam di Indonesian restaurant – Istanbul', 'Biaya Semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', 'Asuransi perjalanan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement : IDR 4,350,000 per orang', 'Tipping yang diwajibkan untuk Tour Leader IDR 30,000/orang/hari, Driver IDR 30,000/orang/hari, Local Guide IDR 45,000/orang/hari', 'Biaya tambahan seperti weekend surcharge, festival ataupun high season period', '​​Biaya pengeluaran pribadi seperti laundry, mini bar, biaya tour tambahan (Optional Tour).', 'PCR Test sebelum keberangkatan  (diwajibkan)', 'Biaya PCR Turkey (untuk kembali ke Indonesia) TKY 300 per Orang (sekitar IDR 600.000)', 'Visa Turkey IDR 700.000', 'Excess baggage/kelebihan bagasi', 'Acara tur luar program', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-17 22:39:25', '2021-10-17 22:39:25'),
(50, 73, 'Tiket penerbangan International, Airport Tax dan Fuel Surcharge', 'Akomodasi bintang 5* atau setaraf', 'Bus Wisata Free Wifi', 'Mineral water 2 botol/orang/hari', '1X Makan siang/malam di Chinese restaurant – Istanbul', '1X Makan siang/malam di Indonesian restaurant – Istanbul', 'Asuransi perjalanan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Biaya pengeluaran pribadi seperti laundry, mini bar, biaya tour tambahan (Optional Tour).', 'Swab PCR Test sebelum keberangkatan  (diwajibkan)', 'Biaya PCR Turkey (untuk kembali ke Indonesia) TKY 300 per Orang\r\n​', 'Visa Turkey IDR 700.000', 'Single Supplement : IDR 4,350,000 per orang', 'Tipping yang diwajibkan untuk Tour Leader IDR 30,000/orang/hari, Driver IDR 30,000/orang/hari, Local Guide IDR 45,000/orang/hari', 'Excess baggage/kelebihan bagasi', 'Asuransi perjalanan', 'Acara tur luar program', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-18 02:45:55', '2021-10-18 02:45:55'),
(51, 74, 'Akomodasi Hotel untuk 5 Malam (3 Malam di Times Square Area - 1 kamar untuk 2 orang)', 'Transportasi : Van AC 15 Seaters', 'Tiket Pesawat :\r\nEconomy Class by Emirates (EK) QR Airways (QR)', 'Tour Leader dari Indonesia', 'Travel kit masker & hand sanitizer', 'Air mineral selama trip', 'Government tax', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Single Supplement : IDR 12,000,000,-', 'Biaya Visa USA', 'Biaya Weekend Surcharge jika menginap hari Jumat dan Sabtu yang berlaku di New York Square', 'Biaya Makan & Minum pada makan Siang dan Malam selama Tur Berjalan', 'Biaya PCR keberangkatan dari Jakarta (jika diperlukan)', 'Biaya PCR kepulangan dari USA (jika diperlukan)', 'Tipping yang diwajibkan untuk Tour Leader USD 50/day & driver USD 100 Hari. (Pembayaran Tips dilakukan di Negara tujuan berdasarkan mata uang setempat).', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-18 19:39:47', '2021-10-18 19:39:47'),
(52, 75, 'Akomodasi bintang 3/4 atau setaraf berdasarkan 1 kamar untuk 2 orang', 'Tour dan transfer akan menggunakan Minivan 7-12 seaters AC (bagasi diperbolehkan 01 koper besar + 01 hand carry per orang)', 'Semua admission dan entrance ticket sesuai dengan program perjalanan', 'English/Indonesian Speaking Guide', 'Makan sesuai dengan jadwal acara', 'Mineral water 2 botol/orang/hari', 'Biaya semua admission dan entrance ticket sesuai dengan program perjalanan', 'Travel kit masker & hand sanitizer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket penerbangan International, Airport Tax dan Fuel Surcharge (based on availability seat)', 'Biaya pengeluaran pribadi seperti laundry, mini bar, biaya tour tambahan (Optional Tour), jasa porter.', 'Asuransi Perjalanan', 'Visa Dubai IDR 1,980,000', 'PCR Test Dubai (untuk kembali ke Indonesia) IDR 1.050.000/ orang', 'Tipping pemandu wisata dan Driver USD 10/ orang/ hari. Total Tipping yang wajib dibayarkan : USD 10 x 04 hari = USD 40/ orang.', 'Biaya tambahan seperti weekend surcharge, festival, ataupun high season period', 'Biaya tambahan untuk penjemputan di atas jam 22.00 – 07.00', 'Harga dapat berubah sewaktu – waktu tanpa pemberitahuan terlebih dahulu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-18 20:15:40', '2021-10-18 20:15:40'),
(53, 76, 'Hotel Pilihan Anda (Twin/Triple)', 'Transportasi Full AC (Private)', 'Driver + BBM', 'Tour guide minimal 15 orang', 'Tour leader minimal 11 orang', 'Tipping driver + guide', '5x Makan Pagi, 5x Makan Siang, 5x Makan Malam, dan 1x Snack', 'Kuliner Mie Belitung & Es Jeruk Kunci', 'Air mineral 1 botol per hari (600ml)', 'Tiket masuk, parkir, dan donasi ke semua obyek wisata', 'Mobil angkot ke Gurok Beraye PP', 'Sewa ban untuk main air di Batu Mentas', 'Island Hopping (Tur Pulau)', 'Boat tradisional ke pulau', 'Alat snorkeling (snorkel & mask)', 'Life jacket (pelampung)', 'Dokumentasi GoPro/Dslr/Drone *sesuai ketersediaan saat tour', 'Free masker & hand sanitizer', 'PPN 1%', NULL, 'Tiket pesawat Jakarta - Belitung - Jakarta', 'Tipping Tour Leader', 'Airport tax dan kelebihan bagasi', 'Rapid test/PCR test', 'Tiket masuk Museum Kata Andrea Hirata (IDR 50.000/pax)', 'Tiket wahana di Wisata Batu Mentas', 'Pemandu khusus river tubing di Batu Mentas (IDR 50.000/pax untuk 20 orang)', 'Pengeluaran yang bersifat pribadi seperti laundry, telpon, layanan kamar, dll', 'Asuransi Perjalanan', 'Optional Tour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-02 20:10:09', '2021-11-02 20:10:09'),
(54, 77, 'Hotel Pilihan Anda (Twin/Triple)', 'Transportasi Full AC (Private)', 'Driver + BBM', 'Tour guide minimal 15 orang', 'Tour leader minimal 11 orang', 'Tipping driver + guide', '5x Makan Pagi dan 1x Snack', 'Kuliner Mie Belitung & Es Jeruk Kunci', 'Air mineral 1 botol per hari (600ml)', 'Tiket masuk, parkir, dan donasi ke semua obyek wisata', 'Mobil angkot ke Gurok Beraye PP', 'Sewa ban untuk main air di Batu Mentas', 'Island Hopping (Tur Pulau)', 'Boat tradisional ke pulau', 'Alat snorkeling (snorkel & mask)', 'Life jacket (pelampung)', 'Dokumentasi GoPro/Dslr/Drone *sesuai ketersediaan saat tour', 'Free masker & hand sanitizer', 'PPN 1%', NULL, 'Tiket pesawat Jakarta - Belitung - Jakarta', 'Tipping Tour Leader', 'Makan siang dan makan malam', 'Airport tax dan kelebihan bagasi', 'Rapid test/PCR test', 'Tiket masuk Museum Kata Andrea Hirata (IDR 50.000/pax)', 'Tiket wahana di Wisata Batu Mentas', 'Pemandu khusus river tubing di Batu Mentas (IDR 50.000/pax untuk 20 orang)', 'Pengeluaran yang bersifat pribadi seperti laundry, telpon, layanan kamar, dll', 'Asuransi Perjalanan', 'Optional Tour', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-02 20:39:47', '2021-11-02 20:39:47'),
(55, 78, 'Room Pratasaba Resort (twin/triple sharing)', 'Penjemputan Bandara – Pelabuhan (PP)', 'Private boat selama tour', 'Meals sesuai program', 'Rental sepeda di Derawan', 'Snack 1x , Coffee Break 1x, Air Mineral 1 botol per hari', 'Tiket Wisata', 'Life Jacket & Alat Snorkeling', 'Local Guide', 'Underwater Documentation', 'Coral Adoption', 'Free masker & hand sanitizer', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket pesawat', 'Tips guide dan motoris', 'Pengeluaran pribadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-02 21:05:40', '2021-11-02 21:05:40'),
(56, 79, 'Room Pratasaba Resort (twin/triple sharing)', 'Penjemputan Bandara – Pelabuhan (PP)', 'Private boat selama tour', 'Meals sesuai program', 'Rental sepeda di Derawan', 'Snack 1x , Coffee Break 1x, Air Mineral 1 botol / hari', 'Tiket Wisata', 'Life Jacket & Alat Snorkeling', 'Local Guide', 'Underwater Documentation', 'Coral Adoption', 'Free masker & hand sanitizer', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket pesawat', 'Tips guide dan motoris', 'Pengeluaran pribadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-02 21:17:04', '2021-11-02 21:17:04'),
(57, 80, 'Room Pratasaba Resort (twin/triple sharing)', 'Penjemputan Bandara – Pelabuhan (PP)', 'Private boat selama tour', 'Meals sesuai program', 'Rental sepeda di Derawan', 'Snack 1x , Coffee Break 1x, Air Mineral 1 botol per hari', 'Tiket Wisata', 'Life Jacket & Alat Snorkeling', 'Local Guide', 'Underwater Documentation', 'Coral Adoption', 'Free masker & hand sanitizer', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket pesawat', 'Tips guide dan motoris', 'Pengeluaran pribadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-02 21:46:01', '2021-11-02 21:46:01'),
(58, 81, 'Hotel 2 malam termasuk makan pagi (Standard room)', 'Tiket WSBK 2021 (Standard GrandStand 02) -> termasuk pajak tiket 15 %', 'Penjemputan (Airport-Hotel-Airport)', 'Transportasi Full AC (Private)', 'Professional tour guide berbahasa Indonesia/Inggris (min. 10 pax)', 'Tipping untuk tour guide', 'Perjalanan sesuai itinerary', 'Makan dan minum sesuai itinerary', 'Biaya parkir', 'Soft drink + air mineral botol (600ml) selama race dan tour', 'Handuk selama race dan tour', 'Snack selama main race dan tour', 'Gratis 6 item merchandise Mandalika Circuit (Hanya untuk 100 Tamu Pertama)', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket Pesawat', 'Upgrade tiket WSBK', 'Airport porter', 'Pengeluaran pribadi (biaya laundry, telepon, mini bar, dll.)', 'Rapid test Antigen / PCR test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-03 20:47:16', '2021-11-03 20:47:16'),
(59, 82, 'Hotel 3 malam termasuk makan pagi (Standard room)', 'Tiket WSBK 2021 (Standard GrandStand 02) termasuk pajak tiket 15 %', 'Penjemputan (Airport-Hotel-Airport)', 'Transportasi Full AC (Private)', 'Charter Speed Boat untuk Gili Trawangan Tour (Private)', 'Professional tour guide berbahasa Indonesia/Inggris (min. 10 pax)', 'Tipping untuk tour guide', 'Perjalanan sesuai itinerary', 'Makan dan minum sesuai itinerary', 'Biaya parkir', 'Soft drink + air mineral botol (600ml) selama race dan tour', 'Handuk selama race dan tour', 'Snack selama main race dan tour', 'Gratis 6 item merchandise Mandalika Circuit (hanya untuk 100 tamu pertama)', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, 'Tiket Pesawat', 'Upgrade Tiket WSBK', 'Airport Porter', 'Pengeluaran pribadi (biaya laundry, telepon, mini bar, dll.)', 'Rapid test Antigen / PCR test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-03 21:10:35', '2021-11-03 21:10:35'),
(60, 83, 'Hotel 2 malam termasuk makan pagi (Standard room)', 'Tiket WSBK 2021 (Standard GrandStand 02) -> tidak termasuk pajak tiket 15 %', 'Penjemputan (Airport-Hotel-Airport)', 'Transportasi Full AC (SIC)', 'Professional tour guide berbahasa Indonesia/Inggris', 'Tipping tour guide', 'Perjalanan sesuai itinerary', 'Makan dan minum sesuai itinerary', 'Biaya parkir', 'Air mineral botol (600ml) selama race dan tour', '1x snack selama Main Race', 'Gratis 5 item merchandise Mandalika Circuit (hanya untuk 100 tamu pertama)', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket Pesawat', 'Pajak 15% untuk tiket WSBK', 'Upgrade tiket WSBK', 'Airport porter', 'Pengeluaran pribadi (biaya laundry, telepon, mini bar, dll.)', 'Rapid test Antigen / PCR test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-03 21:28:52', '2021-11-03 21:28:52'),
(61, 84, 'Hotel 3 malam  termasuk makan pagi (Standard room)', 'Tiket WSBK 2021 (Standard GrandStand 02) -> tidak termasuk pajak tiket 15 %', 'Penjemputan (Airport-Hotel-Airport)', 'Transportasi Full AC (SIC)', 'Charter Speed Boat untuk Gili Trawangan Tour (Sharing)', 'Professional tour guide berbahasa Indonesia/Inggris', 'Tipping tour guide', 'Perjalanan sesuai itinerary', 'Makan dan minum sesuai itinerary', 'Biaya parkir', 'Air mineral botol (600ml) selama race dan tour', '1x snack selama Main Race', 'Gratis 5 item merchandise Mandalika Circuit (hanya untuk 100 tamu pertama)', 'PPN 1%', NULL, NULL, NULL, NULL, NULL, NULL, 'Tiket Pesawat', 'Pajak 15% untuk Tiket WSBK', 'Upgrade Tiket WSBK', 'Airport Porter', 'Pengeluaran pribadi (biaya laundry, telepon, mini bar, dll.)', 'Rapid test Antigen / PCR test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-03 21:33:26', '2021-11-03 21:33:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_06_150607_create_vendor_data_masters_table', 1),
(5, '2021_02_06_150649_create_type_data_masters_table', 1),
(6, '2021_02_06_150738_create_destination_data_masters_table', 1),
(7, '2021_02_06_162341_create_destination_categories_table', 1),
(8, '2021_02_07_083657_create_travel_packages_table', 1),
(9, '2021_02_07_083717_create_galleries_table', 1),
(10, '2021_02_07_083731_create_departure_dates_table', 1),
(11, '2021_02_07_083742_create_transactions_table', 1),
(12, '2021_02_07_083809_create_transaction_details_table', 1),
(13, '2021_02_11_205411_create_trip_itineraries_table', 1),
(14, '2021_02_12_224846_create_inclusives_table', 1),
(15, '2021_08_29_103651_create_staycation_igposts_table', 1),
(16, '2021_09_01_025303_create_staycations_table', 1),
(17, '2021_09_01_025944_create_hotels_table', 1),
(18, '2021_09_01_025945_create_hotel_rooms_table', 1),
(19, '2021_09_01_030045_create_hotel_room_prices_table', 1),
(20, '2021_09_08_050927_create_tour_offers_table', 1),
(21, '2021_09_17_110801_create_booking_tour_statuses_table', 1),
(22, '2021_09_18_093422_create_booking_tours_table', 1),
(23, '2021_09_18_100248_create_passenger_pics_table', 1),
(24, '2021_09_18_100507_create_passengers_table', 1),
(25, '2021_10_01_114949_create_show_travel_packages_table', 1),
(26, '2021_10_01_165106_create_show_hotels_table', 1),
(27, '2021_10_03_193511_create_flyer_image_table', 1),
(28, '2021_10_07_202155_create_slider_images_table', 1),
(29, '2021_10_07_210312_create_visa_data_masters_table', 1),
(30, '2021_10_21_120452_create_commition_request_statuses_table', 2),
(32, '2021_10_24_091303_add_down_payment_to_booking_tours_table', 3),
(33, '2021_10_24_135323_remove_room_facilities_from_hotel_room_prices_table', 4),
(34, '2021_10_24_135917_change_room_price_type_from_hotel_room_prices_table', 5),
(35, '2021_10_28_021905_create_hotel_galleries_table', 6),
(36, '2021_10_28_090102_remove_hotel_image_from_hotels_table', 6),
(37, '2021_10_28_105544_create_careers_table', 7),
(38, '2021_10_31_152101_add_description_to_visa_data_masters_table', 8),
(39, '2021_11_01_180856_create_commition_requests_table', 9),
(40, '2021_11_01_193959_add_is_tour_to_travel_packages_table', 10),
(41, '2021_11_02_162833_change_destination_data_code_to_travel_packages_table', 11),
(42, '2021_11_02_180529_create_show_tour_events_table', 12),
(43, '2021_11_10_053738_change_pdf_url_from_visa_data_masters_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

CREATE TABLE `passengers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_tour_id` bigint(20) UNSIGNED NOT NULL,
  `passenger_pic_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_birth` date NOT NULL,
  `identity_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_media` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `food_allergy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical_history` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `life_insurance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buy_insurance` tinyint(1) NOT NULL,
  `clothing_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `passenger_pics`
--

CREATE TABLE `passenger_pics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_tour_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_birth` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `show_hotels`
--

CREATE TABLE `show_hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `show_hotels`
--

INSERT INTO `show_hotels` (`id`, `hotel_id`, `created_at`, `updated_at`) VALUES
(1, 48, '2021-11-07 11:35:19', '2021-11-07 11:35:19'),
(2, 12, '2021-11-07 11:35:19', '2021-11-07 11:35:19'),
(3, 2, '2021-11-07 11:35:19', '2021-11-07 11:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `show_tour_events`
--

CREATE TABLE `show_tour_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `show_tour_events`
--

INSERT INTO `show_tour_events` (`id`, `travel_package_id`, `created_at`, `updated_at`) VALUES
(7, 81, '2021-11-07 11:37:05', '2021-11-07 11:37:05'),
(8, 82, '2021-11-07 11:37:13', '2021-11-07 11:37:13'),
(9, 83, '2021-11-07 11:37:23', '2021-11-07 11:37:23');

-- --------------------------------------------------------

--
-- Table structure for table `show_travel_packages`
--

CREATE TABLE `show_travel_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `show_travel_packages`
--

INSERT INTO `show_travel_packages` (`id`, `travel_package_id`, `created_at`, `updated_at`) VALUES
(1, 69, '2021-11-07 11:39:43', '2021-11-07 11:39:43'),
(2, 71, '2021-11-07 11:39:43', '2021-11-07 11:39:43'),
(3, 65, '2021-11-07 11:39:43', '2021-11-07 11:39:43');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`id`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 'assets/slider-image/Quarantine Package.png', '2021-10-10 12:47:48', '2021-11-10 01:29:48'),
(2, 'assets/slider-image/slider-2.jpg', '2021-10-10 12:48:47', '2021-10-10 12:48:47'),
(3, 'assets/slider-image/slider-3.jpg', '2021-10-10 12:48:47', '2021-10-10 12:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `staycations`
--

CREATE TABLE `staycations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `special_promo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stars` int(11) NOT NULL,
  `price_flat` int(11) NOT NULL,
  `price_per_night` int(11) NOT NULL,
  `booking_until` date NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_source` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fact_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fact_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fact_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fact_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fact_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staycation_igposts`
--

CREATE TABLE `staycation_igposts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staycation_igposts`
--

INSERT INTO `staycation_igposts` (`id`, `post_url`, `created_at`, `updated_at`) VALUES
(1, 'CM_6HeVMta4', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(2, 'CM7Le75sULv', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(3, 'CM4KT0vM8sM', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(4, 'CM1mwprs4YQ', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(5, 'CMzKSfKsVEW', '2021-10-18 06:53:23', '2021-10-18 06:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `tour_offers`
--

CREATE TABLE `tour_offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tour_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tour_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tour_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `additional_visa` int(11) NOT NULL,
  `transaction_total` int(11) NOT NULL,
  `transaction_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE `transaction_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transactions_id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_visa` tinyint(1) NOT NULL,
  `doe_passport` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `travel_packages`
--

CREATE TABLE `travel_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accomodation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_transport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `travel_docs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `special` tinyint(4) NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vendor_data_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_data_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_data_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_available` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_tours` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `travel_packages`
--

INSERT INTO `travel_packages` (`id`, `title`, `code`, `slug`, `region`, `country`, `about`, `duration`, `flight`, `accomodation`, `land_transport`, `meal`, `travel_docs`, `special`, `category_id`, `vendor_data_code`, `destination_data_code`, `type_data_code`, `is_available`, `created_at`, `updated_at`, `is_tours`) VALUES
(47, '7D4N Paris', 'ATF-FRA001-2021100001', '7d4n-paris', 'Europe', 'Perancis\r\n', 'Free and Easy Paris', '7D4N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 1, 'AT', 'FRA001', 'F', 1, '2021-10-13 04:45:23', '2021-10-13 23:32:32', 0),
(48, '7D4N Switzerland (Zurich - Lucerne - Mt. Titlis)', 'ATF-CHE001-2021100002', '7d4n-switzerland-zurich-lucerne-mt-titlis', 'Europe', 'Swiss\r\n', 'Zurich - Lucerne - Mt. Titlis', '7D4N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 1, 'AT', 'CHE001', 'F', 1, '2021-10-13 04:47:19', '2021-10-13 23:32:58', 0),
(50, '7D4N Spain - Madrid - Toledo', 'ATF-ESP001-2021100003', '7d4n-spain-madrid-toledo', 'Europe', 'Spanyol\r\n', 'Madrid - Toledo', '7D4N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 8, 'AT', 'ESP001', 'F', 1, '2021-10-13 23:54:06', '2021-10-24 22:21:21', 0),
(51, '7D4N Amsterdam', 'ATF-NLD001-2021100004', '7d4n-amsterdam', 'Europe', '', 'Amsterdam - Zaanse Schans - Volendam - Markem', '7D4N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 1, 'AT', '', 'F', 0, '2021-10-14 00:12:20', '2021-10-21 22:52:04', 0),
(52, '28D25N Los Angeles at Leisure + SFO + Honolulu', 'ATF-USA001-2021100005', '28d25n-los-angeles-at-leisure-sfo-honolulu', 'America', 'Amerika Serikat\r\n', 'Los Angeles - San Francisco - Honolulu', '28D25N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 7, 'AT', 'USA001', 'F', 1, '2021-10-14 00:24:08', '2021-10-14 00:24:08', 0),
(53, '9D6N Los Angeles at Leisure + Vaksin Johnson & Johnson', 'ATF-USA002-2021100006', '9d6n-los-angeles-at-leisure-vaksin-johnson-johnson', 'America', 'Amerika Serikat\r\n', 'Los Angeles - Jakarta', '9D6N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 6, 'AT', 'USA002', 'F', 1, '2021-10-14 01:00:35', '2021-10-14 01:00:35', 0),
(54, '6D3N Los Angeles at Leisure + Vaksin Johnson & Johnson', 'ATF-USA003-2021100007', '6d3n-los-angeles-at-leisure-vaksin-johnson-johnson', 'America', 'Amerika Serikat\r\n', 'Los Angeles - Jakarta', '6D3N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 6, 'AT', 'USA003', 'F', 1, '2021-10-14 01:20:10', '2021-10-14 01:20:10', 0),
(55, '27D24N Los Angeles at Leisure + Vaksin Pfizer by NH', 'ATF-USA004-2021100008', '27d24n-los-angeles-at-leisure-vaksin-pfizer-by-nh', 'America', 'Amerika Serikat\r\n', 'Los Angeles - Anaheim - Jakarta', '27D24N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 6, 'AT', 'USA004', 'F', 1, '2021-10-14 01:48:56', '2021-10-14 01:48:56', 0),
(57, '27D24N Los Angeles at Leisure + Vaksin Pfizer by SQ', 'ATF-USA005-2021100009', '27d24n-los-angeles-at-leisure-vaksin-pfizer-by-sq', 'America', 'Amerika Serikat\r\n', 'Los Angeles - Jakarta', '27D24N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 6, 'AT', 'USA005', 'F', 1, '2021-10-14 02:39:57', '2021-10-14 02:39:57', 0),
(58, '9D5N East Coast New York + Vaksin Johnson & Johnson', 'ATF-USA006-2021100010', '9d5n-east-coast-new-york-vaksin-johnson-johnson', 'America', 'Amerika Serikat\r\n', 'New York', '9D5N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA006', 'F', 1, '2021-10-14 20:46:48', '2021-10-14 20:46:48', 0),
(59, '9D5N East Coast New York + Vaksin Booster Pfizer/Moderna', 'ATF-USA007-2021100011', '9d5n-east-coast-new-york-vaksin-booster-pfizermoderna', 'America', 'Amerika Serikat\r\n', 'New York', '9D5N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA007', 'F', 1, '2021-10-14 21:03:13', '2021-10-14 21:03:13', 0),
(60, '28D24N East Coast New York + Vaksin Pfizer', 'ATF-USA008-2021100012', '28d24n-east-coast-new-york-vaksin-pfizer', 'America', 'Amerika Serikat\r\n', 'New York', '28D24N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA008', 'F', 1, '2021-10-14 21:17:45', '2021-10-14 21:17:45', 0),
(61, '7D3N USA East Coast New York + Vaksin J&J', 'ATF-USA009-2021100013', '7d3n-usa-east-coast-new-york-vaksin-jj', 'America', 'Amerika Serikat\r\n', 'New York', '7D3N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA009', 'F', 1, '2021-10-14 21:41:01', '2021-10-14 21:41:01', 0),
(62, '9D8N Private Tour USA West Coast', 'ATP-USA010-2021100014', '9d8n-private-tour-usa-west-coast', 'America', 'Amerika Serikat\r\n', 'Los Angeles – Anaheim – Las Vegas – San Francisco', '9D8N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 6, 'AT', 'USA010', 'P', 1, '2021-10-14 21:55:24', '2021-10-14 21:55:24', 0),
(63, '11D8N Private Tour USA West Coast', 'ATP-USA011-2021100015', '11d8n-private-tour-usa-west-coast', 'America', 'Amerika Serikat\r\n', 'Los Angeles – Anaheim – Las Vegas – San Francisco', '11D8N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 6, 'AT', 'USA011', 'P', 1, '2021-10-14 22:12:39', '2021-10-14 22:12:39', 0),
(64, '9D5N New York - Washington - Niagara Falls', 'ATG-USA012-2021100016', '9d5n-new-york-washington-niagara-falls', 'America', 'Amerika Serikat\r\n', 'New York - Washington - Niagara Falls', '9D5N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA012', 'G', 1, '2021-10-14 22:42:41', '2021-10-14 22:42:41', 0),
(65, '10D6N Supersale East Coast USA', 'ATG-USA013-2021100017', '10d6n-supersale-east-coast-usa', 'America', 'Amerika Serikat\r\n', 'New York - Washington - Philadelphia - Boston - Niagara Falls', '10D6N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA013', 'G', 1, '2021-10-14 23:02:05', '2021-10-14 23:02:05', 0),
(66, '10D6N USA East Coast Highlight', 'ATG-USA014-2021100018', '10d6n-usa-east-coast-highlight', 'America', 'Amerika Serikat\r\n', 'New York - Washington - Corning - Niagara Falls - Albany - Boston', '10D6N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA014', 'G', 1, '2021-10-15 02:10:58', '2021-10-15 02:10:58', 0),
(67, '10D7N Swiss Winter Wonderland', 'ATG-CHE002-2021100019', '10d7n-swiss-winter-wonderland', 'Europe', 'Swiss\r\n', 'Bern - Geneva - Schaffhausen - Zermatt - Piz Gloria - Brienz Lake - Blausee Lake', '10D7N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 1, 'AT', 'CHE002', 'G', 1, '2021-10-17 20:14:41', '2021-10-17 20:14:41', 0),
(68, '10D7N Super Sale Madrid Corbora Barcelona', 'ATG-ESP002-2021100020', '10d7n-super-sale-madrid-corbora-barcelona', 'Europe', 'Spanyol\r\n', 'Madrid - Avila - Salamanca - Cordoba - Granada - Valencia - Barcelona', '10D7N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 8, 'AT', 'ESP002', 'G', 1, '2021-10-17 20:51:17', '2021-10-24 22:21:38', 0),
(69, '10D7N Finland Magical Aurora + Santa Claus Igloo', 'ATG-FIN001-2021100021', '10d7n-finland-magical-aurora-santa-claus-igloo', 'Europe', 'Finlandia\r\n', 'Helsinki - Poorvo - Rovaniemi – Kakslauttanen - Saariselka -  Kemi - Turku', '10D7N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 9, 'AT', 'FIN001', 'G', 1, '2021-10-17 21:20:45', '2021-10-17 21:20:45', 0),
(70, '10D7N Finland Magical Aurora + Kakslauttanen Glass Igloo', 'ATG-FIN002-2021100022', '10d7n-finland-magical-aurora-kakslauttanen-glass-igloo', 'Europe', 'Finlandia\r\n', 'Helsinki - Poorvo - Rovaniemi - Kakslauttanen - Kemi - Turku', '10D7N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 9, 'AT', 'FIN002', 'G', 1, '2021-10-17 21:43:43', '2021-10-17 21:43:43', 0),
(71, '11D8N Winter Turkey + Mt. Uludag & Bosphorus Cruise', 'ATG-TUR001-2021100023', '11d8n-winter-turkey-mt-uludag-bosphorus-cruise', 'Middle East', 'Turki\r\n', 'Istanbul - Bursa - Kusadasi - Pamukkale - Konya - Cappadocia - Ankara - Bolu', '11D8N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 10, 'AT', 'TUR001', 'G', 1, '2021-10-17 22:00:52', '2021-10-17 22:00:52', 0),
(72, '10D7N Winter Turkey + Mt. Uludag & Bosphorus Cruise', 'ATG-TUR002-2021100024', '10d7n-winter-turkey-mt-uludag-bosphorus-cruise', 'Middle East', 'Turki\r\n', 'Istanbul - Bursa - Kusadasi - Pamukkale - Konya - Cappadocia - Ankara', '10D7N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 10, 'AT', 'TUR002', 'G', 1, '2021-10-17 22:29:10', '2021-10-17 22:29:10', 0),
(73, '10D7N Best of Turkey + Bosphorus Cruise', 'ATG-TUR003-2021100025', '10d7n-best-of-turkey-bosphorus-cruise', 'Middle East', 'Turki\r\n', 'Istanbul - Bursa - Kusadasi - Pamukkale - Konya - Cappadocia - Ankara - Bolu', '10D7N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 10, 'AT', 'TUR003', 'G', 1, '2021-10-18 01:58:37', '2021-10-18 01:58:37', 0),
(74, 'Private Tour East Coast USA 9D5N', 'ATP-USA015-2021100026', 'private-tour-east-coast-usa-9d5n', 'America', 'Amerika Serikat\r\n', 'New York - Philadelphia - Washington DC', '9D5N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 5, 'AT', 'USA015', 'P', 1, '2021-10-18 19:29:21', '2021-10-18 19:29:21', 0),
(75, '5D Dubai + Dessert Safari Tour', 'ATF-ARE001-2021100027', '5d-dubai-dessert-safari-tour', 'Middle East', 'Uni Emirat Arab\r\n', 'Dubai', '5D4N', NULL, NULL, NULL, NULL, 'Visa, Paspor', 0, 11, 'AT', 'ARE001', 'F', 1, '2021-10-18 20:09:23', '2021-10-18 20:09:23', 0),
(76, '6D5N Wisata Belitung', 'AGF-IDN001-2021110028', '6d5n-wisata-belitung', 'Indonesia', 'Indonesia', 'Belitung Timur, Belitung Selatan, Sijuk, Island Hopping Tour, City Tour, Culinary Tour', '6D5N', NULL, NULL, NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'AG', 'IDN001', 'F', 1, '2021-11-02 20:00:13', '2021-11-02 20:53:23', 0),
(77, '6D5N Wisata Belitung Hemat', 'AGF-IDN002-2021110029', '6d5n-wisata-belitung-hemat', 'Indonesia', 'Indonesia', 'Belitung Timur, Belitung Selatan, Sijuk, Island Hopping Tour, City Tour, Culinary Tour', '6D5N', NULL, NULL, NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'AG', 'IDN002', 'F', 1, '2021-11-02 20:31:24', '2021-11-02 20:53:50', 0),
(78, '3D2N Special Island Hopping Maratua', 'MRF-IDN003-2021110030', '3d2n-special-island-hopping-maratua', 'Indonesia', 'Indonesia', 'Maratua Tour', '3D2N', NULL, 'Pratasaba Resort', NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'MR', 'IDN003', 'F', 1, '2021-11-02 20:52:03', '2021-11-02 21:50:38', 0),
(79, '4D3N Explore Derawan Archipelago', 'MRF-IDN004-2021110031', '4d3n-explore-derawan-archipelago', 'Indonesia', 'Indonesia', 'Maratua - Talisayan - Pulau Spongebob - Labuan Cermin', '4D3N', NULL, 'Pratasaba Resort', NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'MR', 'IDN004', 'F', 1, '2021-11-02 21:10:38', '2021-11-02 21:49:25', 0),
(80, '5D4N Explore Journey Derawan Archipelago', 'MRF-IDN005-2021110032', '5d4n-explore-journey-derawan-archipelago', 'Indonesia', 'Indonesia', 'Maratua Tour - Talisayan - Pulau Spongebob - Labuan Cermin', '5D4N', NULL, 'Pratasaba Resort', NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'MR', 'IDN005', 'F', 1, '2021-11-02 21:28:31', '2021-11-02 21:49:15', 0),
(81, '3D2N Mandalika World Superbike 2021', 'PRP-IDN006-2021110033', '3d2n-mandalika-world-superbike-2021', 'Indonesia', 'Indonesia', 'World Superbike Main Race 2021 at Mandalika International Street Circuit', '3D2N', NULL, NULL, NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'PR', 'IDN006', 'P', 1, '2021-11-03 20:19:09', '2021-11-03 20:19:09', 1),
(82, '4D3N Mandalika World Superbike 2021', 'PRP-IDN007-2021110034', '4d3n-mandalika-world-superbike-2021', 'Indonesia', 'Indonesia', 'World Superbike Main Race 2021 at Mandalika International Street Circuit', '4D3N', NULL, NULL, NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'PR', 'IDN007', 'P', 1, '2021-11-03 20:57:14', '2021-11-03 20:57:14', 1),
(83, '3D2N Mandalika World Superbike 2021 (SIC)', 'PRG-IDN008-2021110035', '3d2n-mandalika-world-superbike-2021-sic', 'Indonesia', 'Indonesia', 'World Superbike Main Race 2021 at Mandalika International Street Circuit', '3D2N', NULL, NULL, NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'PR', 'IDN008', 'G', 1, '2021-11-03 21:21:50', '2021-11-03 21:21:50', 1),
(84, '4D3N Mandalika World Superbike 2021 (SIC)', 'PRG-IDN009-2021110036', '4d3n-mandalika-world-superbike-2021-sic', 'Indonesia', 'Indonesia', 'World Superbike Main Race 2021 at Mandalika International Street Circuit', '4D3N', NULL, NULL, NULL, NULL, 'KTP, Bukti Vaksinasi', 0, 4, 'PR', 'IDN009', 'G', 1, '2021-11-03 21:22:54', '2021-11-03 21:22:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trip_itineraries`
--

CREATE TABLE `trip_itineraries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `travel_package_id` bigint(20) UNSIGNED NOT NULL,
  `day1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `day2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `day3` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day4` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day5` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day6` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day7` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day8` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day9` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day10` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day11` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day12` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day13` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day14` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day15` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day16` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day17` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day18` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day19` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day20` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day21` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day22` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day23` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day24` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day25` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day26` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day27` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day28` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day29` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day30` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `highlights` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf_file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trip_itineraries`
--

INSERT INTO `trip_itineraries` (`id`, `travel_package_id`, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`, `day8`, `day9`, `day10`, `day11`, `day12`, `day13`, `day14`, `day15`, `day16`, `day17`, `day18`, `day19`, `day20`, `day21`, `day22`, `day23`, `day24`, `day25`, `day26`, `day27`, `day28`, `day29`, `day30`, `highlights`, `pdf_file`, `created_at`, `updated_at`) VALUES
(26, 47, 'Hari ini Anda berkumpul di Bandara Soekarno-Hatta untuk menuju Paris.', 'Setibanya di Paris Anda akan diajak berkeliling di pusat kota mode Paris, lalu Anda akan diaturkan transportasi menuju ke Hotel.\r\n(Check-in time 15.00 sore)\r\nAkomodasi : Hotel Holiday Inn Paris Opera – Grands Blvd atau setaraf', 'Hari ini waktu bebas berkeliling kota Paris. Anda dapat mengikuti beberapa OPTIONAL TOUR seperti Seine River Cruise (300.000/orang), Disneyland Paris (900.000/orang), menaiki Eiffel Tower (1.000.000/orang), atau berkeliling kota Paris dengan menggunakan HOP-ON HOP-OFF (700.000/orang).\r\nAkomodasi: Holiday Inn Paris Opera – Grands Blvd atau setaraf', 'Hari ini waktu bebas berkeliling kota Paris dengan berbelanja di boutique boutique di sepanjang jalan Rue Saint Honore atau berbelanja di Galeries Lafayette dengan dekorasi yang sangat megah dan indah atau shopping di La Vallee Village Outlet dengan menggunakan kereta (tanpa bus).\r\nAkomodasi:  Holiday Inn Paris Opera – Grands Blvd atau setaraf', 'Hari Acara Bebas (Hari ini Anda dapat melakukan tes PCR untuk salah satu persyaratan terbang kembali ke Indonesia.\r\nAkomodasi:  Holiday Inn Paris Opera – Grands Blvd atau setaraf', 'Hari ini anda akan diantar menuju bandara untuk kembali ke Tanah Air.', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda Bersama daysOff Tour & Travel. Semoga bertemu kembali dilain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Disneyland Paris, Eiffel Tower and many more!', 'assets/itinerary-pdf/daysOff FIT Itinerary Paris.pdf', '2021-10-13 23:36:07', '2021-11-10 02:27:08'),
(29, 50, 'Hari ini Anda berkumpul di Bandara Soekarno-Hatta untuk menuju Madrid, Spain.', 'Setibanya di Madrid Anda akan di aturkan transportasi menuju ke Hotel, Anda bebas berkeliling kota Madrid. (Check-in time 15.00 sore)\r\nAkomodasi : Hotel sesuai pilihan atau setaraf', 'Hari ini Acara bebas di Madrid dimana Anda dapat mengikuti OPTIONAL TOUR untuk berkeliling kota Paris dengan menggunakan HOP-ON HOP-OFF (450.000/orang-ticket only), atau berbelanja di daerah Calle Gran Via dengan menggunakan kereta, atau mengikuti Bernabeu Stadium Tour (300.000/orang-ticket only)  atau berbelanja ke La Rozas Outlet dengan menggunakan bus/kereta.\r\nAkomodasi:Hotel sesuai pilihan atau setaraf', 'Hari ini Anda akan mengikuti tour ke kota TOLEDO yang merupakan ibukota Spanyol pada dahulu kala. Tiba di Toledo, bus akan berhenti di beberapa tempat untuk berfoto dengan pemandangan yang sangat menakjubkan. Kemudian Anda akan diberi waktu menjelajahi kota Toleno seperti: Toledo’s Cathedral yang memiliki gaya arsitektur gothic yang menawan.\r\nAkomodasi: Hotel sesuai pilihan atau setaraf', 'Hari Acara Bebas (Hari ini Anda dapat melakukan tes PCR untuk salah satu persyaratan terbang kembali ke Indonesia - Biaya PCR EUR 75) (tanpa bus).\r\nAkomodasi:Hotel sesuai pilihan atau setaraf', 'Hari ini anda akan diantar menuju bandara untuk kembali ke Tanah Air.', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda Bersama daysOff Tour & Travel. Semoga bertemu kembali dilain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Calle Gran Via, Toledo’s Cathedral and many more!', 'assets/itinerary-pdf/daysOff FIT Itinerary Spain MADRID-TOLEDO.pdf', '2021-10-13 23:55:28', '2021-11-10 02:27:39'),
(30, 51, 'Hari ini Anda berkumpul di Bandara Soekarno-Hatta untuk menuju Amsterdam.', 'Setibanya di Amsterdam Anda akan di aturkan transportasi menuju ke Hotel. (Check-in time 15.00 sore)\r\nAkomodasi : Hotel sesuai pilihan atau setaraf', 'Hari ini waktu bebas berkeliling kota Amsterdam. Anda dapat mengikuti beberapa OPTIONAL TOUR seperti Canal Day Cruise (300.000/orang), atau menjelajahi kota Amsterdam dengan menyewa sepeda (150.000/orang/3 jam), atau berbelanja ke Roermond Designer Outlet dengan menggunakan kereta atau shuttle bus.\r\nAkomodasi: Hotel sesuai pilihan atau setaraf', 'Hari ini Anda akan mengikuti tour untuk menikmati suasana pedesaan di Belanda dimana Anda akan diajak untuk mengunjungi Desa Zaanse Schans untuk melihat kincir angin dan rumah khas belanda, kemudian dilanjutkan menuju ke Volendam yang merupakan Desa Nelayan dimana Anda bisa berfoto dengan baju Khas Belanda (biaya tambahan). Dalam perjalanan kembali ke Amsterdam, anda akan stop di Desa Markem dimana terkenal dengan tempat pembuat sepatu kayu khas Belanda.\r\nAkomodasi: Hotel sesuai pilihan atau setaraf', 'Hari Acara Bebas (Hari ini Anda dapat melakukan tes PCR untuk salah satu persyaratan terbang kembali ke Indonesia.\r\nAkomodasi: Hotel sesuai pilihan atau setaraf', 'Hari ini anda akan diantar menuju bandara untuk kembali ke Tanah Air.', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda Bersama daysOff Tour & Travel. Semoga bertemu kembali dilain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Zaanse Schans, Volendam and many more!', 'assets/itinerary-pdf/daysOff_FIT Itinerary Amsterdam - Zaanse Schans - Volendam - Markem.docx.pdf', '2021-10-14 00:16:05', '2021-10-14 01:15:30'),
(31, 52, 'Hari ini Anda akan tiba di hari yang sama di Los Angeles, Anda akan diantar menuju Hotel untuk check in. \r\n(Note : self check in & check in time around 16.00)', 'Hari ini Anda akan diantar untuk Vaksin Pfizer dosis pertama didampingi oleh Asisten kami.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitaran hotel.', 'Perjalanan hari ini anda akan diantar menuju kota San Francisco dengan melewati Kota Solvang dan ST. Barbara untuk Photoshop. (Note : self check in & check in time around 16.00)', 'Pagi ini Anda melakukan PCR Test (dilakukan sendiri dengan diberikan panduan & biaya Tidak termasuk) untuk persyaratan ke Honolulu. Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk menikmati kota San Fransisco.', 'Hari ini anda akan diantar menuju Bandara San Francisco untuk penerbangan menuju Honolulu (tidak termasuk Tiket Penerbangan Domestik – Estimasi Harga domestic mulai dari 3 Juta Rupiah). Setelah sampai , akan diantar Menuju Hotel. (Note : self check in & check in time around 16.00)', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk bersantai dan menikmati pantai Honolulu.', 'Hari ini Anda berkesempatan untuk vaksin gratis Pfizer dosis kedua (dilakukan sendiri dengan diberikan panduan) sesuai dengan ketersediaan dan kebutuhan Anda.', 'Hari ini Anda melakukan PCR Test (dilakukan sendiri dengan diberikan panduan & biaya tidak termasuk) untuk persyaratan kembali ke Tanah Air.', 'Hari ini Anda akan diantar ke Airport untuk penerbangan kembali ke Tanah Air.', 'Anda akan melalui International Date Line.', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali dilain kesempatan.', NULL, NULL, 'Vaccine Vacations ( Pfizer Free) tergantung ketersediaan dan kebutuhan Anda.', 'assets/itinerary-pdf/Itinerary_FIT LA + Honolulu (Vaksin pfizer) 28D25N2021 .docx.pdf', '2021-10-14 00:31:10', '2021-10-24 21:31:59'),
(33, 54, 'Hari ini Anda akan tiba di hari yang sama di Los Angeles, Anda akan diantar menuju hotel Hilton Woodland Hills bersama Asisten kami.\r\nAkomodasi: Anaheim Area atau sekitarnya atau setaraf', 'Hari ini Anda berkesempatan untuk vaksin gratis Johnson & Johnson sesuai dengan ketersediaan dan kebutuhan Anda. Anda juga akan diantar menuju tempat vaksin dengan didampingi oleh Asisten kami. \r\nAkomodasi: Anaheim Area atau sekitarnya atau setaraf', 'Hari ini Anda akan diantar untuk PCR Test (biaya sudah termasuk) untuk persyaratan kembali ke Indonesia. Selanjutnya Anda akan\r\nmenikmati acara bebas atau Anda juga bisa berbelanja di sekitar hotel. \r\nAkomodasi: Anaheim Area atau setaraf', 'Hari ini Anda akan diantar ke Airport untuk penerbangan kembali ke Tanah Air.', 'Anda akan melalui International Date Line', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Vaccine Vacations ( Johnson & Johnson )', 'assets/itinerary-pdf/Itinerary_FIT LA (Vaksin J&J) 6D3N2021 .docx.pdf', '2021-10-14 01:22:25', '2021-10-14 01:22:25'),
(34, 55, 'Hari ini Anda akan tiba di hari yang sama di Los Angeles, Anda akan diantar menuju hotel bersama Asisten kami.\r\nAkomodasi: Bermalam di Hotel', 'Hari ini Anda berkesempatan untuk vaksin gratis Pfizer dosis pertama sesuai dengan ketersediaan dan kebutuhan Anda. Anda juga akan diantar menuju tempat vaksin dengan didampingi oleh Asisten kami. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Vaccine Vacations ( Pfizer Free) tergantung ketersediaan dan kebutuhan Anda.', 'assets/itinerary-pdf/Itinerary_FIT LA (Vaksin Pfizer) 27D24N 2021 .docx.pdf', '2021-10-14 01:52:26', '2021-10-14 01:52:26'),
(37, 57, 'Hari ini Anda akan tiba di hari yang sama di Los Angeles, Anda akan diantar menuju hotel bersama Asisten kami.\r\nAkomodasi: Bermalam di Hotel', 'Hari ini Anda berkesempatan untuk vaksin gratis Pfizer dosis pertama sesuai dengan ketersediaan dan kebutuhan Anda. Anda juga akan diantar menuju tempat vaksin dengan didampingi oleh Asisten kami. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. \r\nAkomodasi:  Bermalam di Hotel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Vaccine Vacations ( Pfizer Free) tergantung ketersediaan dan kebutuhan Anda.', 'assets/itinerary-pdf/Itinerary_FIT LA (Vaksin Pfizer) 27D24N SQ 2021 .docx.pdf', '2021-10-14 02:41:57', '2021-10-14 02:41:57'),
(38, 58, 'Hari ini Anda berkumpul di Bandara Internasional Soekarno Hatta untuk penerbangan menuju New York.', 'Setibanya di New York, Anda akan diantar menuju hotel untuk istirahat. ( Check in sendiri & waktu check in sekitar jam 16.00)\r\nAkomodasi: Element Hotel Times Square area atau sekitarnya atau Setaraf', 'Hari ini Anda berkesempatan untuk booster untuk vaksin gratis Johnson & Johnson sesuai dengan ketersediaan dan kebutuhan Anda. Anda akan menuju tempat vaksin dengan ditemani oleh asisten kami\r\nAkomodasi: Element Hotel Times Square area atau sekitarnya atau Setaraf', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitar hotel Akomodasi: Element Hotel Times Square area atau sekitarnya atau Setaraf', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitar hotel Akomodasi: Element Hotel Times Square area atau sekitarnya atau Setaraf', 'Hari ini Anda akan ditemani oleh asisten kami untuk PCR Test (biaya PCR tidak termasuk) untuk persyaratan kembali ke Indonesia. Selanjutnya Acara bebas yang dapat Anda gunakan untuk berbelanja di sekitar hotel.\r\nAkomodasi: Element Hotel Times Square area atau sekitarnya atau Setaraf', 'Acara bebas, sampai saatnya Anda menuju Airport (tidak termasuk biaya pengantaran) untuk penerbangan kembali menuju Tanah Air', 'Melalui International Date Line', 'Hari ini Anda sampai di Tanah Air. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Stay 5 Nights Hotel in City Centre (At Times Square Area), Vaccine Johnson & Johnson (Sesuai dengan ketersedian dan kebutuhan Anda), New York', 'assets/itinerary-pdf/Itinerary_FIT East Coast USA 9D5N Vaksin Johnson & Johnson.docx.pdf', '2021-10-14 20:53:27', '2021-10-14 20:53:27'),
(39, 59, 'Hari ini Anda berkumpul di Bandara Internasional Soekarno Hatta untuk penerbangan menuju New York.', 'Setibanya di New York, Anda akan diantar menuju hotel untuk istirahat. ( Check in sendiri & waktu check in sekitar jam 16.00). Akomodasi: Hotel disekitar Times Square area / Fifth Avenue atau Setaraf', 'Hari ini Anda berkesempatan untuk booster untuk vaksin gratis Pfizer/Moderna sesuai dengan ketersediaan dan kebutuhan Anda. Anda akan menuju tempat vaksin sendiri dengan mengikuti petunjuk dari kami. Akomodasi : hotel sekitar Times Square area / Fifth Avenue atau setaraf.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitar hotel. Akomodasi:  hotel sekitar Times Square area / Fifth Avenue atau setaraf.', 'Hari ini acara bebas yang dapat Anda gunakan untuk berjalan- jalan di New York dengan menggunakan Subway dengan biaya sendiri. Hari ini Anda juga harus melakukan PCR Test sebagai syarat pulang ke Indonesia (biaya tidak termasuk & jika masih diperlukan). Akomodasi: hotel sekitar Times Square area / Fifth Avenue atau setaraf.', 'Hari ini Acara bebas yang dapat Anda gunakan untuk berbelanja di sekitar hotel. Akomodasi: hotel sekitar Times Square area / Fifth Avenue atau setaraf.', 'Acara bebas, sampai saatnya Anda menuju Airport (tidak termasuk biaya pengantaran) untuk penerbangan kembali menuju Tanah Air.', 'Melalui International Date Line.', 'Hari ini Anda sampai di Tanah Air. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Stay 5 Nights Hotel in City Centre (At Times Square Area), Vaccine Booster Pfizer/Moderna(selama ketersediaan ada), New York', 'assets/itinerary-pdf/Itinerary_FIT East Coast USA 9D5N Vaksin booster Pfizer_Moderna.docx.pdf', '2021-10-14 21:09:14', '2021-10-14 21:09:14'),
(40, 60, 'Hari ini Anda berkumpul di Bandara Internasional Soekarno Hatta untuk penerbangan menuju New York.', 'Setibanya di New York, Anda akan diantar menuju hotel untuk istirahat. ( One way transfer/max waktu tunggu 2 jam). Check in sendiri dan acara bebas. Akomodasi: Bermalam di hotel', 'Hari ini Anda berkesempatan untuk vaksin gratis Pfizer dosis pertama sesuai dengan ketersediaan dan kebutuhan Anda. Anda akan menuju tempat vaksin sendiri dengan mengikuti petunjuk dari kami. Akomodasi : Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi:  Bermalam di hotel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Vaccine Pfizer (Tergantung ketersedian dan kebutuhan Anda), New York', 'assets/itinerary-pdf/Itinerary_FIT East Coast New York USA 28D24N Vaksin Pfizer.docx.pdf', '2021-10-14 21:25:55', '2021-10-14 21:25:55'),
(41, 61, 'Hari ini Anda berkumpul di Bandara Internasional Soekarno Hatta untuk penerbangan menuju New York.', 'Setibanya di New York, Anda akan diantar menuju hotel untuk istirahat. ( Check in sendiri & waktu check in sekitar jam 16.00) Akomodasi: Hotel disekitar Times Square area atau Setaraf', 'Hari ini Anda berkesempatan untuk booster untuk vaksin gratis Johnson & Johnson sesuai dengan ketersediaan dan kebutuhan Anda. Anda akan menuju tempat vaksin sendiri dengan mengikuti petunjuk dari kami. Akomodasi : hotel sekitar Times Square area atau setaraf.', 'Hari ini Anda akan menikmati Acara Bebas atau Anda dapat menggunakan waktu bebas untuk berbelanja di sekitar hotel. Akomodasi:  hotel sekitar Times Square area  atau setaraf.', 'Acara bebas, sampai saatnya anda diantar menuju Airport.', 'Melalui International Date Line.', 'Hari ini Anda sampai di Tanah Air. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Stay 3 Nights Hotel in City Centre (At Times Square Area), Vaccine Johnson & Johnson , New York', 'assets/itinerary-pdf/Itinerary_FIT East Coast New York USA 7D3N.docx.pdf', '2021-10-14 21:43:39', '2021-10-14 21:43:39'),
(42, 62, 'Tiba di Los Angeles, Anda akan diantar menuju hotel di Anaheim Area untuk beristirahat. Akomodasi: The Fullerton Hotel Anaheim atau setaraf.', 'Hari ini acara bebas untuk mengunjungi sanak saudara, atau berjalan-jalan disekitar hotel. Anda juga dapat mengikuti tour tambahan (optional) mengunjungi Disneyland dengan transportasi dari hotel atau Anda juga dapat berkesempatan untuk Vaksin Johnson & Johnson (1x Suntik) gratis, sesuai dengan kebutuhan Anda dan selama persediaan masih ada. Akomodasi: The Fullerton Hotel Anaheim atau setaraf.', 'Hari ini Anda akan menuju Kota Los Angeles untuk city tour dengan mengunjungi Beverly Hills, Hollywood Walk of Fame, dan berfoto serta berfoto berlatar belakang TCL Chinese Theatre, Dolby Theatre kemudian melewati Sunset Boulevard dan Rodeo Drive. Siang hari akan melanjutkan perjalanan menuju Las Vegas untuk Anda menikmati keindahan kota Las Vegas dengan hiasan lampu-lampu dari hotel Casino sepanjang Las Vegas Strip. Akomodasi: Excalibur Hotel atau setaraf.', 'Hari ini, Acara bebas di Las Vegas yang dapat Anda gunakan untuk mengikuti tour tambahan (OPTIONAL) mengunjungi GRAND CANYON atau berjalan-jalan disekitar hotel atau mencoba peruntungan di atas meja casino. Akomodasi: Excalibur Hotel atau setaraf.', 'Setelah makan pagi, anda diajak berbelanja di Las Vegas Premium Outlet dimana terdapat banyak brand-brand terkenal seperti Hugo Boss, Coach, Calvin Klein, Tommy Hilfiger, etc. Kemudian perjalanan kembali menuju Anaheim untuk bermalam. Akomodasi: The Fullerton Hotel Anaheim atau setaraf.', 'Hari ini perjalanan dilanjutkan menuju San Francisco, sebelumnya Anda akan diajak untuk melewati Santa Barbara dan singgah di Solvang dan mengunjungi Little Denmark, tiba di San Francisco diajak menuju Union Square untuk berbelanja. Setelah itu Anda akan menuju hotel untuk beristirahat. Akomodasi : Hyatt Place San Francisco Downtown Hotel atau setaraf.', 'San Francisco city tour meliputi Golden Gate Bridge, Bay Bridge, Treasure Island, Twin Peaks, Civic Center Chinatown, Fisherman’s Wharf dan lain-lainnya. Kemudian Anda akan diantar menuju hotel untuk bermalam. Akomodasi :Hyatt Place San Francisco Downtown Hotel atau setaraf', 'Perjalanan dilanjutkan menuju Anaheim, dalam perjalanan Anda akan diajak singgah di Monterey Bay untuk mengunjungi 17 Miles Drive dimana terdapat perumahan orang-orang kaya di sepanjang pesisir pantai, Lonely Cyprus, Pebble Beach Golf Club & Cannery Row. Kemudian diantar menuju hotel untuk beristirahat. Akomodasi: The Fullerton Hotel Anaheim atau setaraf', 'Hari ini Anda akan diantar ke Airport untuk penerbangan kembali ke Tanah Air. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Las Vegas Premium Outlet, Solvang Little Denmark and many more!', 'assets/itinerary-pdf/Itinerary_Private Tour USA 9D8N.docx.pdf', '2021-10-14 22:02:27', '2021-10-14 22:02:27'),
(43, 63, 'Hari ini Anda akan tiba di hari yang sama di Los Angeles, Anda akan diantar menuju hotel di Anaheim Area untuk beristirahat.', 'Hari ini acara bebas untuk mengunjungi sanak saudara, atau berjalan-jalan disekitar hotel. Anda juga dapat mengikuti tour tambahan (optional) mengunjungi Disneyland dengan transportasi dari hotel atau Anda juga dapat berkesempatan untuk Vaksin Johnson & Johnson (1x Suntik) gratis, sesuai dengan kebutuhan Anda dan selama persediaan masih ada. ​​Akomodasi: The Fullerton Hotel Anaheim atau setaraf.', 'Hari ini Anda akan menuju Kota Los Angeles untuk city tour dengan mengunjungi Beverly Hills, Hollywood Walk of Fame, dan berfoto serta berfoto berlatar belakang TCL Chinese Theatre, Dolby Theatre kemudian melewati Sunset Boulevard dan Rodeo Drive. Siang hari akan melanjutkan perjalanan menuju Las Vegas untuk Anda menikmati keindahan kota Las Vegas dengan hiasan lampu-lampu dari hotel Casino sepanjang Las Vegas Strip. Akomodasi: Excalibur Hotel atau setaraf', 'Hari ini, Acara bebas di Las Vegas yang dapat Anda gunakan untuk mengikuti tour tambahan (OPTIONAL) mengunjungi GRAND CANYON atau berjalan-jalan disekitar hotel atau mencoba peruntungan di atas meja casino. Akomodasi: Excalibur Hotel atau setaraf.', 'Setelah makan pagi, anda diajak berbelanja di Las Vegas Premium Outlet dimana terdapat banyak brand-brand terkenal seperti Hugo Boss, Coach, Calvin Klein, Tommy Hilfiger, etc. Kemudian perjalanan kembali menuju Anaheim untuk bermalam. Akomodasi: The Fullerton Hotel Anaheim atau setaraf', 'Hari ini perjalanan dilanjutkan menuju San Francisco, sebelumnya Anda akan diajak untuk melewati Santa Barbara dan singgah di Solvang dan mengunjungi Little Denmark, tiba di San Francisco diajak menuju Union Square untuk berbelanja. Setelah itu Anda akan menuju hotel untuk beristirahat. Akomodasi : Hyatt Place San Francisco Downtown Hotel atau setaraf.', 'San Francisco city tour meliputi Golden Gate Bridge, Bay Bridge, Treasure Island, Twin Peaks, Civic Center, Chinatown, Fisherman’s Wharf dan lain-lainnya. Kemudian Anda akan diantar menuju hotel untuk bermalam. Akomodasi : Hyatt Place San Francisco Downtown Hotel atau setaraf.', 'Perjalanan dilanjutkan menuju Anaheim, dalam perjalanan Anda akan diajak singgah di Monterey Bay untuk mengunjungi 17 Miles Drive dimana terdapat perumahan orang-orang kaya di sepanjang pesisir pantai, Lonely Cyprus, Pebble Beach Golf Club & Cannery Row. Kemudian diantar menuju hotel untuk beristirahat. Akomodasi: The Fullerton Hotel Anaheim atau setaraf.', 'Hari ini Anda akan diantar ke Airport untuk penerbangan kembali ke Tanah Air.', 'Anda akan melalui International Date Line.', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Las Vegas Premium Outlet, Solvang Little Denmark and many more!', 'assets/itinerary-pdf/Itinerary_Private Tour USA 11D8N.docx.pdf', '2021-10-14 22:16:53', '2021-10-14 22:16:53'),
(44, 64, 'Hari ini berkumpul di Soekarno Hatta, Jakarta untuk bersama-sama berangkat menuju kota Washington via Abu Dhabi.', 'Setibanya di Washington , Anda akan diantar menuju hotel untuk istirahat. Akomodasi: Hyatt Fairfax Hotel atau setaraf', 'Hari ini Anda akan city tour kota Washington mengunjungi Washington Monument, White House, Lincoln Memorial, Jefferson Memorial, Vietnam War, Korean War, Capitol Hill, etc. Akomodasi: Hyatt Fairfax Hotel atau setaraf', 'Hari ini perjalanan menuju Niagara Falls, salah satu air terjun terbesar di dunia. Tiba di Niagara Falls, menuju hotel untuk bermalam. Akomodasi: Fourpoint Niagara Falls New York Hotel atau setaraf', 'Pagi hari mengunjungi Goat Island dimana Anda berkesempatan untuk melihat dari dekat air terjun Niagara Falls yang merupakan salah satu air terjun terbesar di dunia. Setelah itu, menuju New York untuk bermalam. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setaraf', 'Hari ini Acara bebas yang dapat Anda gunakan untuk berjalan-jalan di New York dengan menggunakan Subway dengan biaya sendiri atau Anda juga berkesempatan untuk mendapatkan vaksin Johnson & Johnson (1x suntik) tergantung ketersediaan. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setaraf', 'Hari ini Anda akan dijemput untuk city tour mengunjungi: Liberty Island Cruise dengan ferry, Wall Street, Rockefeller Center, Time Square, 5th Avenue, Ground Zero, etc. Sore hari Anda akan menuju Airport untuk kembali ke tanah air.', 'Melewati International Dateline.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi dilain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Liberty Island Cruises, stay at Times Square Area 2N, and many more!', 'assets/itinerary-pdf/NEW YORK -WASHINGTON -NIAGARA FALLS 9D_5N.pdf', '2021-10-14 22:48:35', '2021-10-14 22:48:35'),
(45, 65, 'Berkumpul di Jakarta untuk melakukan penerbangan menuju New York. Bermalam di pesawat.', 'Tiba di New York, Anda akan dijemput dan diantar untuk mengunjungi Brooklyn Park dimana Anda bisa berfoto Brooklyn Bridge dengan latar belakang kota Manhattan yang sangat Indah. Anda juga akan melewati Wall Street & Rockefeller Center. Akomodasi: Holiday Inn / Best Western/ Hampton Inn atau setaraf.', 'Hari ini Anda akan menuju Washington melalui Philadelphia dengan mengunjungi Indepence Hall, Liberty Bell. Tiba di kota Washington city tour mengunjungi White House, Lincoln Memorial, Capitol Hill dan melewati Washington Monument.\r\nAkomodasi: Holiday Inn / Best Western/ Hampton Inn atau setaraf.', 'Hari ini perjalanan menuju Niagara Falls, salah satu air terjun terbesar di dunia. Akomodasi: Holiday Inn / Best Western/ Hampton Inn atau setaraf.', 'Pagi hari mengunjungi Goat Island dimana Anda berkesempatan untuk melihat dari dekat air terjun Niagara Falls. Perjalanan dilanjutkan menuju ke Albany yang merupakan ibu kota dari negara bagian New York, dimana Anda akan diajak untuk photo stop di The Egg, New York State Capitol, & Albany City Hall. Akomodasi: Holiday Inn / Best Western/ Hampton Inn atau setaraf.', 'Menuju Boston dimana Anda akan diajak city tour kota Boston dengan berfoto di salah satu university yang sangat terkenal di dunia: Harvard University. Kemudian melewati Harvard Village, MIT University, dan Copley Square serta mengunjungi Quincy Market.\r\nAkomodasi: Holiday Inn / Best Western/ Hampton Inn atau setaraf.', 'Hari ini Anda akan diajak untuk berbelanja di Woodbury Premium Outlet dimana terdapat banyak sekali brand brand terkenal seperti Prada, Gucci, Hugo Boss, Tods dan masih banyak lagi. Akomodasi: Holiday Inn / Best Western/ Hampton Inn atau setaraf.', 'Pagi hari Anda akan diajak untuk mengunjungi Liberty Island dengan ferry (termasuk biaya ferry), Ground Zero, tidak lupa juga Anda akan diajak untuk berbelanja di Times Square dan 5th Avenue. Setelah itu, Anda akan diantar menuju airport untuk penerbangan kembali menuju Tanah Air.', 'Melewati International Dateline.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Independence Hall, Liberty Bell, White House, Liberty Island Cruises, The Egg, New York State Capitol, Albany City Hall, and many more!', 'assets/itinerary-pdf/SS EAST COAST 10D_6N.pdf', '2021-10-14 23:06:50', '2021-10-14 23:06:50'),
(46, 66, 'Hari ini berkumpul di Soekarno Hatta, Jakarta untuk bersama-sama berangkat menuju kota Washington via Abu Dhabi.', 'Setibanya di Washington , Anda akan diantar menuju hotel untuk istirahat. Akomodasi: Hyatt Fairfax Hotel atau setaraf.', 'Hari ini Anda akan city tour kota Washington mengunjungi White House, Lincoln Memorial, Jefferson Memorial, Vietnam War, Korean War, Capitol Hill dan melewati Washington Monument. Akomodasi: Fairfield Inn by Marriott Corning atau setaraf.', 'Hari ini perjalanan menuju Niagara Falls, salah satu air terjun terbesar di dunia. Tiba di Niagara Falls, Anda akan diajak untuk menikmati keindahan pemandangan air terjun Niagara dengan Maid of the Mist Cruise (USA Side) dimana Anda bisa menikmati pemandangan Niagara Falls yang sangat Indah dari dekat. Note : Untuk keberangkatan di bulan Nov – Dec (winter season), Maid of The Mist Cruise akan digantikan menjadi Goat Island. Akomodasi: Fourpoint Niagara Falls New York Hotel atau setaraf.', 'Setelah makan pagi, acara dilanjukan menuju ke Albany yang merupakan ibu kota dari Negara bagian New York, dimana Anda akan diajak untuk photo stop di New York State Capitol. Bermalam di Boston. Akomodasi: Doubletree By Hilton Westborough atau setaraf', 'Pagi hari Anda akan diajak untuk diajak berfoto di salah satu university yang sangat terkenal di dunia: Harvard University. Kemudian melewati Harvard Village, MIT University dan Copley Square serta mengunjungi Quincy Market. Setelah itu, perjalanan dilanjutkan menuju kota New York. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setaraf.', 'Hari ini Acara bebas yang dapat Anda gunakan untuk berjalan-jalan di New York dengan menggunakan Subway dengan biaya sendiri atau Anda juga berkesempatan untuk mendapatkan vaksin Johnson & Johnson (1x suntik) tergantung ketersediaan. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setaraf.', 'Hari ini Anda akan dijemput untuk city tour mengunjungi: Liberty Island Cruise dengan ferry, melewati Wall Street & Rockefeller Center. Anda juga akan diajak untuk berbelanja Times Square, 5th Avenue, dan mengunjungi Ground Zero. Sore hari Anda akan menuju Airport untuk kembali ke tanah air.', 'Melewati International Dateline.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Liberty Island Cruises, Maid of The Mist Cruise, stay at Times Square Area 2N, and many more!', 'assets/itinerary-pdf/EAST COAST HIGHLIGHT 10D_6N.pdf', '2021-10-15 02:14:33', '2021-10-15 02:14:33'),
(47, 67, 'Hari ini Anda berkumpul di Bandara Soekarno - Hatta untuk menuju Zurich.', 'Setibanya di Zurich Anda akan diantar menuju menuju ibu kota negara Swiss yaitu kota Bern dan di kota ini pula terdapat salah satu penangkaran beruang yang dilindungi oleh pemerintah Swiss, jika Anda beruntung Anda dapat melihatnya, di kota ini Anda diajak untuk berjalan kaki di sepanjang kota tua Bern sambil berbelanja souvenir khas kota ini. Setelah itu Anda diantar menuju hotel untuk beristirahat. Akomodasi : Mercure Plaza Biel hotel atau setaraf', 'Hari ini anda menuju Geneva yang terkenal dengan danau Geneva yang indah yang di tengahnya terdapat air mancur Jet D’au, berfoto di Flower Clock yang indah, serta melewati Red Cross Building dan Palais Des Nations. Kemudian perjalanan menuju Lausanne dengan melihat Charlie Chaplin Statue, Nestle Palais yang menjadikan icon kota ini. Setelah itu menuju Montreux dan Anda akan diajak untuk berfoto di depan Chillon Castle yang berada di pinggir danau yang menampilkan pemandangan yang menakjubkan. Tak lupa berfoto di depan patung Freddie Mercury yang melegenda di kota ini. Akomodasi: Hotel Vatel atau setaraf', 'Pagi hari setelah makan pagi, menuju ke Gstaad sebuah desa kecil yang masih tradisional typical Switzerland yang berada di kaki gunung, kemudian dilanjutkan ke Tasch station dan dengan menggunakan kereta menuju kota Zermatt. Di kota ini Anda dapat berjalan santai sambil menikmati pemandangan alam yang sangat indah, Zermatt dikenal sebagai kota bebas polusi, Anda dapat menghirup udara segar di kaki Gunung Matterhorn dan berjalan tanpa ada polusi. Kemudian perjalanan dilanjutkan menuju hotel untuk beristirahat. Akomodasi: -', 'Hari ini perjalanan menuju Blausee Lake yang merupakan danau yang berada di antara pegunungan Alpen dengan pemandangan yang sangat indah, kemudian dilanjutkan menuju Piz Gloria dan dengan menggunakan cable car menuju puncak Gunung Schilthorn dimana tempat ini pernah menjadi salah satu lokasi shooting film James Bond 007. Setelah itu Anda diantar menuju hotel untuk beristirahat. Akomodasi: -', 'Pagi hari Anda akan diantar kembali menuju Zurich, tapi Anda akan mampir menuju salah satu tempat shooting film Korea Crash Landing On You yang lagi hits yang berada di Lake Brienz, disini Anda dapat merasakan kesejukan udara segar sambil berfoto seperti bintang film Korea. Setelah itu Anda akan diajak untuk menuju ke Interlaken, sebuah kota yang sangat indah sebelum Anda diantar menuju hotel di Zurich. Akomodasi : Dorint hotel atau setaraf.', 'Hari ini setelah makan pagi Anda akan diantar menuju Schaffhausen untuk melihat Rhine Falls yang merupakan air terjun terbesar yang berada diantara Swiss dan Germany. Kemudian Anda akan diantar menuju Sankt Gallen dimana kota ini yang diakui oleh UNESCO karena di dalamnya terdapat Abbey Cathedral of St. Gall yang sudah ada sejak abad 18. Setelah itu Anda akan diajak untuk berbelanja souvenir khas Swiss di kota Lucerne. Akomodasi: -', 'Pagi ini Anda diberi waktu bebas untuk menikmati salah satu kota terbesar di Swiss untuk sekedar berbelanja atau berorientasi di Old Town dengan Tour Leader berpengalaman dari daysOff. Rekomendasi untuk hari ini, Anda dapat mengunjungi Lake Zurich, Grossmunster Church, Limmat River. Menikmati suasana / kuliner khas negara ini. Akomodasi: -', 'Hari ini tiba waktunya Anda akan diantar menuju bandara guna kembali ke Tanah Air.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bern, Flower Clock, Rhine Falls, Train Experience to Zermatt, Blausee Lake, Schilthorn 007, Brienz Lake,  and many more!', 'assets/itinerary-pdf/10D GRP EUR SWISS WINTER WONDERLAND.pdf', '2021-10-17 20:19:19', '2021-11-10 02:34:48'),
(48, 68, 'Hari ini Anda berkumpul di Bandara Soekarno - Hatta untuk menuju Madrid.', 'Hari ini tour dilanjutkan dengan city tour di kota Madrid di mulai dengan melewati Puerta Del Sol, Cibeles Fountain,Royal Palace (Istana Negara), Debod Temple yang merupakan temple yang diberikan oleh mesir pada Madrid, serta Plaza Espana dengan Monument Cervantes. Akomodasi : Madrid Area Hotel atau setaraf', 'Setelah sarapan pagi, Anda menuju kota Salamanca yang terkenal dengan arsitektur Baroquenya, melihat Cathedral of Salamanca yang indah dan juga alun-alun kota tua Salamanca. Setelah itu mengunjungi kota Avila, kota kecil yang memiliki banyak bangunan bersejarah seperti Avila Cathedral dan San Vicente Shrine. Kemudian kembali ke Madrid untuk istirahat. Akomodasi: Madrid Area Hotel atau setaraf', 'Hari ini, Anda akan mengunjungi 3 icon kota Cordoba yaitu The Mezquita. Ini adalah saksi kejayaan kerajaan islam yang pernah menguasai Spanyol. Bangunan yang kini menjadi sebuah Gereja, awalnya adalah Masjid Agung Cordoba saat Kerajaan Islam berkuasa disini. Berikutnya Anda akan diajak mengunjungi Alcazar De Los Reyes Cristianos, bangunan ini pun awalnya adalah istana para pemimpin kerajaan Islam di Spanyol Selatan ketika mereka berkuasa, yang kemudian dijadikan istana kerajaan Katolik ketika mereka merebut kota Cordoba. Terakhir Anda akan mengunjungi Jewish Quarter, ini adalah salah satu distrik bersejarah di kota Cordoba, daerah ini sekitar 1.000 tahun yang lalu adalah salah satu distrik Yahudi terbesar di Spanyol selatan sebelum akhirnya mereka diusir oleh kerajaan Katolik. Akomodasi: Cordoba Area Hotel atau setaraf', 'Menuju ke kota Granada. Setibanya Anda diajak untuk berfoto dengan latar belakang Alhambra dan kemudian menikmati Old Town Granada. Akomodasi: Granada Area Hotel atau setaraf', 'Setelah sarapan pagi, perjalanan menuju kota Valencia. Sesampainya Anda diajak berfoto di City of Arts and Science, Torres de Serranos salah satu gerbang yang membentuk bagian dari tembok kota kuno. Akomodasi: Valencia Area Hotel atau setaraf', 'Hari ini Anda akan diajak untuk berbelanja di La Roca Village Outlet. Anda dapat berbelanja berbagai macam brand internasional dengan harga yang menarik. Kemudian perjalanan dilanjutkan ke Barcelona yang merupakan kota kedua terbesar di Spanyol. Akomodasi: Barcelona Àrea Hotel atau setaraf', 'City tour di kota Barcelona di mulai dengan berfoto dengan latar belakang Gereja Sagrada Familia hasil karya Antonio Gaudi yang sampai sekarang masih terus dibangun, Monument Christopher Columbus, Passeig de Gracia, serta Olympic Stadium yang terletak di puncak bukit Monjuich, tak lupa Anda diajak untuk berfoto di Camp Nou Stadium yang merupakan klub sepakbola tim Barcelona. Akomodasi: Barcelona Area Hotel atau setaraf', 'Hari ini bersiap siap Anda diantar menuju airport guna kembali ke Tanah Air.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Avila, Salamanca, The Mezquita, Alhambra, La Rocca Vilage,  and many more!', 'assets/itinerary-pdf/GRP-EUR- MADRID CORDOBA BARCELONA 10D.pdf', '2021-10-17 21:00:04', '2021-10-17 21:12:05');
INSERT INTO `trip_itineraries` (`id`, `travel_package_id`, `day1`, `day2`, `day3`, `day4`, `day5`, `day6`, `day7`, `day8`, `day9`, `day10`, `day11`, `day12`, `day13`, `day14`, `day15`, `day16`, `day17`, `day18`, `day19`, `day20`, `day21`, `day22`, `day23`, `day24`, `day25`, `day26`, `day27`, `day28`, `day29`, `day30`, `highlights`, `pdf_file`, `created_at`, `updated_at`) VALUES
(49, 69, 'Hari ini Anda berkumpul di Bandara Soekarno - Hatta untuk menuju Helsinki.', 'Pagi hari Anda akan diajak menuju kota Porvo, kota tertua kedua di Finlandia setelah Turku dan sudah ada sejak abad ke-13 untuk mengunjungi Porvoo Cathedral merupakan sebuah katedral yang didedikasikan untuk Bunda Perawan Maria dan sudah dibangun dari abad ke 15, Porvoo Old Town merupakan kota tua yang cantik dan antik. Akomodasi : Scandic Park Helsinki atau setaraf', 'Setelah santap pagi, Anda akan diajak menuju diajak berkeliling kota dengan melewati Parliament House, Market Square, Sibelius Monument, dan mengunjungi Gereja Temppeliaukio. Setelah itu Anda akan diantar menuju stasiun untuk kembali menuju Rovaniemi dengan menggunakan kereta api. Anda akan bermalam di kereta. Akomodasi: Overnight Train', 'Pagi hari setelah makan pagi, Anda akan diantar ke Kakslauttanen Arctic Resort dimana terdapat Igloo house/Ice Hotel, galery serta Ice Bar dan disini pula tempat Anda mencoba pengalaman yang mengesankan. Lalu perjalanan dilanjutkan menuju Saariselka untuk bermalam. Akomodasi: Holiday Club Saariselka atau setaraf', 'Diawali dengan mengunjungi Santa Claus Village, perjalanan Anda akan seperti di dunia dongeng sebab Anda dapat melihat dari dekat Kantor Pos dan Ruang Kerja Santa Claus serta Anda dapat berbelanja souvenir. Disinilah Anda akan menyeberangi batas lingkar kutub utara dimana Anda akan mendapatkan sertifikat sebagai bukti telah menyeberangi batas dan masuk ke dalam Lingkar kutub utara (Arctic Circle). Akomodasi: Original Sokos Hotel Vaakuna Rovaniemi', 'Hari ini, Anda akan berkesempatan untuk mengeksplor kota es ini lebih lama lagi. Rovaniemi akan memberikan Anda pemandangan yang cantik dimana terdapat hamparan tundra dan hutan pinus, danau beku yang menjadi tempat wisata yang tidak tertandingi. Akomodasi: Santa Claus Glass Igloo. Note : Aurora Borealis adalah fenomena alam dimana daysOff tidak bisa menjamin Anda dapat melihat, karena itu akan tergantung dari situasi dan kondisi serta keadaan alam setempat.', 'Setelah makan pagi, Anda akan mengunjungi Kemi Church adalah gereja Lutheran Evangelical klasik Finlandia, yang menampilkan gaya arsitektur Finlandia utara di masa lalu. Lalu Anda akan bermalam di kereta untuk menuju Helsinki. Akomodasi: Overnight Train', 'Hari ini Anda akan diajak menuju Turku dimana Anda akan diajak untuk melewati Turku Castle yang merupakan salah satu bangunan tertua di Finlandia dari masa Medieval, Turku Cathedral adalah gereja Katolik tertua di Finlandia yang masih digunakan untuk beribadah, Turku Old Town Square yang merupakan alun-alun kota Turku yang sangat cantik. Akomodasi: Scandic Park Hotel atau setaraf', 'Anda akan segera diantar menuju Helsinki-Vantaa Airport untuk kembali ke Tanah Air tercinta. Akomodasi: Dalam Pesawat', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Temppeliaukio Church, Santa Claus’s House, 1 night stay in Santa Claus Igloo, Aurora Borealis, Igloo House Tour, Turku Castle, and many more!', 'assets/itinerary-pdf/GRP-EUR- FINLAND MAGICAL AURORA + SANTA CLAUS IGLOO 10D.pdf', '2021-10-17 21:32:42', '2021-10-17 21:32:42'),
(50, 70, 'Hari ini Anda berkumpul di Bandara Soekarno - Hatta untuk menuju Helsinki.', 'Pagi hari Anda akan diajak menuju kota Porvo, kota tertua kedua di Finlandia setelah Turku dan sudah ada sejak abad ke-13 untuk mengunjungi Porvoo Cathedral merupakan sebuah katedral yang didedikasikan untuk Bunda Perawan Maria dan sudah dibangun dari abad ke 15, Porvoo Old Town merupakan kota tua yang cantik dan antik. Akomodasi : Scandic Park Helsinki atau setaraf', 'Setelah santap pagi, Anda akan diajak menuju diajak berkeliling kota dengan melewati Parliament House, Market Square, Sibelius Monument, dan mengunjungi Gereja Temppeliaukio. Setelah itu Anda akan diantar menuju stasiun untuk kembali menuju Rovaniemi dengan menggunakan kereta api. Anda akan bermalam di kereta. Akomodasi: Overnight Train', 'Pagi hari setelah makan pagi, Anda akan diantar ke Kakslauttanen Arctic Resort dimana terdapat Igloo house/Ice Hotel, galery serta Ice Bar. Kemudian Anda akan diajak untuk mengunjungi peternakan Husky yang terkenal serta berkesempatan untuk menikmati Husky Sleigh Ride, berseluncur di atas salju dengan ditarik oleh anjing kutub yang menjadikan pengalaman Anda yang tak terlupakan. Pada malam harinya Anda juga berkesempatan untuk melakukan Aurora Hunting dengan mengendarai Snowmobile. Anda akan diajak untuk berburu salah satu fenomena alam yang sangat indah, The Northern Lights atau yang dikenal dengan nama Aurora Borealis** jika cuaca memungkinkan. Note : Aurora Borealis adalah fenomena alam dimana daysOff tidak bisa menjamin Anda dapat melihat, karena itu akan tergantung dari situasi dan kondisi serta keadaan alam setempat. Akomodasi: Kakslauttanen Arctic Resort (*Small Glass Igloo*)', 'Hari ini Anda akan diajak untuk mengunjungi Santa Claus Village, perjalanan Anda akan seperti di dunia dongeng sebab Anda dapat melihat dari dekat Kantor Pos dan Ruang Kerja Santa Claus serta Anda dapat berbelanja souvenir. Disinilah Anda akan menyeberangi batas lingkar kutub utara dimana Anda akan mendapatkan sertifikat sebagai bukti telah menyeberangi batas dan masuk ke dalam Lingkar kutub utara (Arctic Circle). Akomodasi: Original Sokos Hotel Vaakuna Rovaniemi', 'Hari ini, Anda akan berkesempatan untuk mengeksplor kota es ini lebih lama lagi. Rovaniemi akan memberikan Anda pemandangan yang cantik dimana terdapat hamparan tundra dan hutan pinus, danau beku yang menjadi tempat wisata yang tidak tertandingi. Akomodasi: -', 'Setelah makan pagi, Anda akan mengunjungi Kemi Church adalah gereja Lutheran Evangelical klasik Finlandia, yang menampilkan gaya arsitektur Finlandia utara di masa lalu. Lalu Anda akan bermalam di kereta untuk menuju Helsinki. Akomodasi: Overnight Train', 'Hari ini Anda akan diajak menuju Turku dimana Anda akan diajak untuk melewati Turku Castle yang merupakan salah satu bangunan tertua di Finlandia dari masa Medieval, Turku Cathedral adalah gereja Katolik tertua di Finlandia yang masih digunakan untuk beribadah, Turku Old Town Square yang merupakan alun-alun kota Turku yang sangat cantik. Akomodasi: Scandic Park Hotel atau setaraf', 'Anda akan segera diantar menuju Helsinki-Vantaa Airport untuk kembali ke Tanah Air tercinta. Akomodasi: Dalam Pesawat', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Temppeliaukio Church, Santa Claus’s House, 1 night stay in Kakslauttanen Small Glass Igloo, Igloo House Tour, Turku Castle, and many more!', 'assets/itinerary-pdf/GRP-EUR- FINLAND MAGICAL AURORA + KAKSLAUTTANEN GLASS IGLOO 10D.pdf', '2021-10-17 21:46:55', '2021-10-17 21:46:55'),
(51, 71, 'Berangkat dari Jakarta menuju Istanbul. Akomodasi: Dalam pesawat', 'Pagi hari tiba di Istanbul, Anda akan melanjutkan perjalanan menuju Mt. Uludag dimana Anda dapat menikmati salju dan aktivitas musim dingin (tidak termasuk peralatan ski). Perjalanan dilanjutkan menuju kota Bursa untuk mengunjungi Grand Mosque yang merupakan masjid terbesar yang ada di kota ini. Setelah itu Anda akan diantar ke hotel untuk makan malam dan beristirahat. Akomodasi: Tugcu Hotel atau setaraf', 'Hari ini, Anda akan melanjutkan perjalanan menuju Kusadasi untuk mengunjungi House of Virgin Mary yang dipercaya sebagai tempat tinggal Bunda Maria dan Turkish Delight Shop untuk membeli snack khas Turkey. Setelah itu Anda akan diantar ke hotel untuk makan malam dan beristirahat. Akomodasi: Grand Belish Hotel atau setaraf', 'Pagi hari Anda akan diajak mengunjungi Ephesus City, salah satu kota kuno dengan peninggalan kebudayaan Romawi & Yunani terbesar dan mengunjungi Leather Factory Outlet. Setelah itu Anda akan melanjutkan perjalanan menuju ke Pamukkale, dimana Anda akan melihat pegunungan kapur atau dikenal dengan nama The White Travertines dan sisa reruntuhan Hierapolis yang berada di kota ini. Anda juga akan diajak untuk mengunjungi Textile Outlet. Kemudian Anda akan diantar ke hotel untuk beristirahat. Akomodasi: Colossae Thermal Hotel atau setaraf', 'Setelah makan pagi perjalanan dilanjutkan menuju Cappadocia dengan melewati kota Konya dan berfoto di Caravanserai, dimana dahulu digunakan sebagai tempat perhentian para pedagang. Perjalanan dilanjutkan menuju ke Cappadocia untuk menikmati makan malam dan check in hotel. Akomodasi: Perissia Hotel atau setaraf', 'Pagi ini, Anda dapat mengikuti tur tambahan Hot Air Balloon** sambil menikmati keindahan pemandangan matahari terbit dari atas ketinggian. Setelah makan pagi, Anda akan orientasi di kota Cappadocia dengan mengunjungi Goreme Open Air Museum, Uchisar Valley, Pigeon Valley, Mushroom Headrock, Avanos village yang merupakan tempat kerajinan gerabah, Carpet Shop serta Jewelry & Turquoise Shop. Setelah itu Anda akan diantar kembali ke hotel untuk beristirahat.', 'Pagi hari Anda akan melanjutkan perjalanan menuju ke Ankara untuk mengunjungi Mausoleum of Atatürk yang merupakan makam dari Presiden pertama Turki yaitu Mustafa Kemal Ataturk. Di perjalanan Anda akan photo stop di Salt Lake. Setibanya di Bolu/Izmit Anda akan diantar ke hotel untuk bermalam. Akomodasi: Hampton by Hilton Hotel atau setaraf', 'Hari ini perjalanan dilanjutkan menuju ke Istanbul, setibanya Anda akan diantar Camlica Hill untuk photo stop dengan panoramic view kota Istanbul. Setelah itu Anda akan diajak untuk menyusuri selat Bosphorus dengan Kapal Cruise sambil menikmati keindahan dua benua sekaligus yaitu Asia dan Eropa. Lalu Anda akan diantar menuju Taksim Square dan Grand Bazaar untuk berbelanja. Setelah makan malam menuju hotel untuk beristirahat. Akomodasi: Ramada Tekstilkent Hotel atau setaraf', 'Hari ini Anda akan city tour di kota Istanbul, mengunjungi Blue Mosque yang terkenal dengan interior-nya yang bernuansa biru, Hippodrome Square, Hagia Sophia yang dahulu merupakan Gereja pada masa Byzantium dan diubah menjadi Masjid pada masa pemerintahan Ottoman dan Topkapi Palace.', 'Setelah makan Pagi Anda akan diantar ke Bandara untuk penerbangan kembali ke Tanah Air.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MT. ULUDAG, GRAND MOSQUE, HOUSE OF VIRGIN MARY,EPHESUS CITY, GOREME OPEN AIR MUSEUM, MAUSOLEUM OF ATTATURK, BLUE MOSQUE, HAGIA SOPHIA, TOKAPI PALACE, BOSPHORUS CRUISE, GRAND BAZAR, and many more!', 'assets/itinerary-pdf/GRP-TUR-WINTER TURKEY+MT. ULUDAG & BOSPHORUS CRUISE-11D.pdf', '2021-10-17 22:06:31', '2021-10-17 22:06:31'),
(52, 72, 'Berangkat dari Jakarta menuju Istanbul. Akomodasi: Dalam pesawat', 'Setibanya di Istanbul, Anda akan menuju Mt. Uludag dengan Cable Car yang terkenal dengan keindahan pemandangan alam nya dan aktivitas musim dingin. Lalu perjalanan dilanjutkan menuju Bursa untuk mengunjungi Grand Mosque yang merupakan masjid terbesar yang ada di kota ini. Lalu menuju hotel untuk bermalam. Akomodasi: Euro Park Hotel atau setaraf', 'Hari ini Anda akan melanjutkan perjalanan menuju Kusadasi untuk mengunjungi Ephesus City, salah satu kota kuno dengan peninggalan kebudayaan Romawi & Yunani terbesar dan Turkish Delight Shop untuk membeli snack khas Turkey. Setelah itu Anda akan diantar ke hotel untuk makan malam dan beristirahat. Akomodasi: Grand Belish Hotel atau setaraf', 'Pagi hari Anda akan diajak mengunjungi Leather Factory Outlet lalu melanjutkan perjalanan menuju ke Pamukkale, dimana Anda akan melihat pegunungan kapur atau dikenal dengan nama The White Travertines dan sisa reruntuhan Hierapolis yang berada di kota ini. Anda juga akan diajak untuk mengunjungi Textile Outlet. Kemudian Anda akan diantar ke hotel untuk beristirahat. Akomodasi: Adempira Hotel atau setaraf', 'Setelah makan pagi, perjalanan dilanjutkan menuju Cappadocia melalui Konya dan berfoto di Caravanserai, dimana dahulu digunakan sebagai tempat perhentian para pedagang. Perjalanan dilanjutkan menuju ke Cappadocia untuk menikmati makan malam dan check in hotel. Akomodasi: Suhan Hotel atau setaraf', 'Pagi ini, Anda dapat mengikuti tur tambahan Hot Air Balloon** sambil menikmati keindahan pemandangan matahari terbit dari atas ketinggian. Setelah makan pagi, Anda akan orientasi di kota Cappadocia dengan mengunjungi Goreme Valley, Uchisar Valley, Pigeon Valley, Mushroom Headrock, Avanos village yang merupakan tempat kerajinan gerabah, Carpet Shop serta Jewelry & Turquoise Shop. Setelah itu Anda akan diantar kembali ke hotel untuk beristirahat.', 'Pagi hari Anda akan melanjutkan perjalanan menuju ke Ankara untuk mengunjungi Mausoleum of Atatürk yang merupakan makam dari Presiden pertama Turki yaitu Mustafa Kemal Ataturk. Di perjalanan Anda akan photo stop di Salt Lake. Bermalam di Istanbul. Akomodasi: Delta Hotel by Marriot atau setaraf', 'Hari ini Anda akan city tour di kota Istanbul, mengunjungi Blue Mosque yang terkenal dengan interior-nya yang bernuansa biru, Hippodrome Square, Hagia Sophia yang dahulu merupakan Gereja pada masa Byzantium dan diubah menjadi Masjid pada masa pemerintahan Ottoman dan Topkapi Palace. Lalu menyusuri selat Bosphorus dengan Kapal Cruise sambil menikmati keindahan dua benua sekaligus yaitu Asia dan Eropa dan Grand Bazaar untuk berbelanja. Setelah makan malam menuju hotel untuk beristirahat.', 'Hari ini Anda akan diantar ke Bandara untuk penerbangan kembali ke Tanah Air.', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GRAND MOSQUE, MT. ULUDAG, EPHESUS CITY, GOREME VALLEY, MAUSOLEM OF ATTATURK, BLUE MOSQUE, HAGIA SOPHIA, TOPKAPI PALACE, BOSPHORUS CRUISE, GRAND BAZAR, and many more!', 'assets/itinerary-pdf/GRP-TUR-WINTER TURKEY+MT ULUDAG & BOSPHORUS CRUISE SQ-10D.pdf', '2021-10-17 22:34:04', '2021-10-17 22:34:04'),
(53, 73, 'Berangkat dari Jakarta menuju Istanbul. Akomodasi: Dalam pesawat', 'Pagi hari tiba di Istanbul, Anda akan melanjutkan perjalanan menuju kota Bursa untuk mengunjungi Grand Mosque yang merupakan masjid terbesar yang ada di kota ini. Setelah itu Anda akan diantar ke hotel untuk makan malam dan beristirahat. Akomodasi: Europark Hotel atau setaraf', 'Hari ini Anda akan melanjutkan perjalanan menuju Kusadasi untuk mengunjungi Ephesus City, salah satu kota kuno dengan peninggalan kebudayaan Romawi & Yunani terbesar dan Turkish Delight Shop untuk membeli snack khas Turkey. Setelah itu Anda akan diantar ke hotel untuk makan malam dan beristirahat. Akomodasi: Grand Belish Hotel atau setaraf', 'Pagi hari Anda akan diajak mengunjungi Leather Factory Outlet lalu perjalanan dilanjutkan menuju ke Pamukkale, dimana Anda akan melihat pegunungan kapur atau dikenal dengan nama The White Travertines dan sisa reruntuhan Hierapolis yang berada di kota ini. Anda juga akan diajak untuk mengunjungi Textile Outlet. Kemudian Anda akan diantar ke hotel untuk beristirahat. Akomodasi: Adempira Hotel atau setaraf', 'Setelah makan pagi, perjalanan dilanjutkan menuju Cappadocia dan berfoto di Caravanserai, dimana dahulu digunakan sebagai tempat perhentian para pedagang. Perjalanan dilanjutkan menuju ke Cappadocia untuk menikmati makan malam dan check in hotel. Akomodasi : Suhan Hotel atau setaraf', 'Pagi ini, Anda dapat mengikuti tur tambahan Hot Air Balloon** sambil menikmati keindahan pemandangan matahari terbit dari atas ketinggian. Setelah makan pagi, Anda akan orientasi di kota Cappadocia dengan mengunjungi Goreme Valley, Uchisar Valley, Pigeon Valley, Mushroom Headrock, Avanos village yang merupakan tempat kerajinan gerabah, Carpet Shop serta Jewelry & Turquoise Shop. Setelah itu Anda akan diantar kembali ke hotel untuk beristirahat. Akomodasi : Suhan Hotel atau setaraf', 'Pagi hari Anda akan melanjutkan perjalanan menuju ke Ankara untuk mengunjungi Mausoleum of Atatürk yang merupakan makam dari Presiden pertama Turki yaitu Mustafa Kemal Ataturk. Di perjalanan Anda akan photo stop di Salt Lake. Setibanya di Bolu/Izmit Anda akan diantar ke hotel untuk bermalam. Akomodasi : Buyuk Abant Hotel atau setaraf', 'Hari ini perjalanan dilanjutkan menuju ke Istanbul, setibanya Anda akan diantar Camlica Hill untuk photoshop dengan panoramic view kota Istanbul. Lalu menyusuri selat Bosphorus dengan Kapal Cruise sambil menikmati keindahan dua benua sekaligus yaitu Asia dan Eropa. Lalu menuju Grand Bazaar untuk berbelanja oleh-oleh khas Turkey. Setelah makan malam menuju hotel untuk beristirahat. Akomodasi : Delta Hotel by Marriott atau setaraf', 'Hari ini Anda akan city tour di kota Istanbul, mengunjungi Blue Mosque yang terkenal dengan interior-nya yang bernuansa biru, Hippodrome Square, Hagia Sophia yang dahulu merupakan Gereja pada masa Byzantium dan diubah menjadi Masjid pada masa pemerintahan Ottoman dan Topkapi Palace. Lalu Anda memiliki waktu bebas untuk berbelanja di Taksim Square. Setelah makan malam, Anda akan diantar ke Bandara untuk melakukan penerbangan kembali ke Tanah Air besok dini hari. Akomodasi : Delta Hotel by Marriott atau setaraf', 'Hari ini Anda tiba di Jakarta. Dengan demikian berakhirnya sudah perjalanan anda bersama daysOff Tour and Travel. Semoga bertemu lagi di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'GRAND MOSQUE, EPHESUS CITY, GOREME VALLEY, MAUSOLEUM OF ATTATURK, BLUE MOSQUE, HAGIA SOPHIA, TOPKAPI  PALACE, BOSPHORUS CRUISE, GRAND BAZAAR, and many more!', 'assets/itinerary-pdf/GRP -TUR-BEST OF TURKEY + BOSPHORUS CRUISE.pdf', '2021-10-18 02:43:25', '2021-10-18 02:43:25'),
(54, 74, 'Hari ini Anda berkumpul di Bandara Internasional Soekarno Hatta untuk penerbangan menuju New York.', 'Setibanya di New York, Anda akan diantar menuju hotel untuk istirahat. Akomodasi: DoubleTree by Hilton Newark Penn Station Hotel atau setaraf.', 'Hari ini perjalanan menuju Philadelphia untuk melihat Liberty Bell & Independence Hall. Selanjutnya dilanjutkan menuju ke Washington DC untuk beristirahat. Bermalam di Washington DC. Akomodasi: Hyatt Fairfax Hotel atau setaraf.', 'Hari ini Anda akan city tour kota Washington dengan berfoto dan melewati Washington Monument, White House, Lincoln Memorial, Jefferson Memorial, Vietnam War, Korean War, Capitol Hill, etc. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setara', 'Hari ini Acara bebas yang dapat Anda gunakan untuk berjalan- jalan di New York dengan menggunakan Subway dengan biaya sendiri atau Anda juga berkesempatan untuk mendapatkan vaksin Johnson & Johnson (1x suntik) tergantung ketersediaan. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setara', 'Hari ini Acara bebas yang dapat Anda gunakan untuk berbelanja di sekitar hotel atau Anda juga dapat mengikuti acara tambahan (Opsional) ke Liberty Island. Akomodasi: Millenium Hotel Broadway Times Square / Doubletree Times Square West atau setara', 'Acara bebas, sampai saatnya anda diantar menuju Airport', 'Melalui International Date Line', 'Hari ini Anda sampai di Tanah Air. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Stay 3 Nights Hotel in City Centre (At Times Square Area), Vaccine Johnson & Johson, New York, Washington DC', 'assets/itinerary-pdf/Itinerary_Private Tour East Coast USA 9D5N.docx.pdf', '2021-10-18 19:37:08', '2021-10-18 19:37:08'),
(55, 75, 'Setibanya dini hari/ pagi hari Anda akan melakukan PCR TEST (biaya sudah termasuk) di bandara. Setelah itu, Anda akan diantar ke hotel dan dapat langsung check in untuk beristirahat. Akomodasi: S19 Hotel Al Jaddaf / Holiday Inn Al Barsha  atau setaraf', 'Hari ini Anda akan menunggu hasil PCR Test di hotel. Lalu sore hari Anda akan diajak untuk Desert Safari Tour dengan kendaraan 4WD sambil menyaksikan tarian Belly Dance Show. (tidak tersedia transportasi). Akomodasi : S19 Hotel Al Jaddaf / Holiday Inn Al Barsha atau setaraf', 'Hari ini Anda akan diajak untuk halfday city tour kota Dubai dengan foto stop di Jumeirah Beach, Burj Al Arab, Burj Khalifa dan melewati Atlantis the Palm. Setelah itu Anda akan diajak untuk mengunjungi Gold Souk and Spice Souk serta mencoba Abra Water Taxi. Kemudian Anda juga memiliki waktu bebas untuk berbelanja di Dubai Mall. Setelah itu Anda akan diantar ke hotel untuk bermalam. Akomodasi: S19 Hotel Al Jaddaf / Holiday Inn Al Barsha atau setara', 'Hari ini acara bebas untuk menikmati keindahan kota Dubai (tidak tersedia transportasi). Malam hari Anda dapat mengikuti **optional tour Dhow Cruise Marina untuk makan malam sambil menikmati pemandangan atau mengunjungi Dubai Expo (1 October - 30 March). Akomodasi: S19 Hotel Al Jaddaf / Holiday Inn Al Barsha atau setaraf', 'Hari ini Anda akan diantar ke bandara untuk melanjutkan penerbangan. (Transfer Only)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Burj Al Arab, Burj Khalifa, Spice & Gold Souk, Desert Safari Tour, and many more!', 'assets/itinerary-pdf/FIT-TUR 5D DUBAI+DESERT SAFARI TOUR_OCT 21-MAR 22.pdf', '2021-10-18 20:13:20', '2021-10-18 20:13:20'),
(57, 53, 'Hari ini Anda akan tiba di hari yang sama di Los Angeles, Anda akan diantar menuju hotel bersama Asisten kami. Akomodasi: Anaheim Clarion Resort atau sekitarnya atau setaraf', 'Hari ini Anda berkesempatan untuk vaksin gratis Johnson & Johnson sesuai dengan ketersediaan dan kebutuhan Anda. Anda juga akan diantar menuju tempat vaksin dengan didampingi oleh Asisten kami. Akomodasi: Anaheim Clarion Resort atau sekitarnya atau setaraf', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi: Anaheim Clarion Resort atau sekitarnya atau setaraf', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi: Anaheim Clarion Resort atau sekitarnya atau setaraf', 'Hari ini Anda akan menikmati acara bebas atau Anda dapat menggunakan waktu bebas untuk berkunjung ke rumah keluarga atau kerabat. Akomodasi: Anaheim Clarion Resort atau sekitarnya atau setaraf', 'Hari ini Anda akan diantar untuk PCR Test (biaya sudah termasuk*) untuk persyaratan kembali ke Indonesia.\r\nSelanjutnya Anda akan menikmati acara bebas atau Anda juga bisa berbelanja di sekitar hotel. Akomodasi: Anaheim Clarion Resort atau sekitarnya atau setaraf', 'Hari ini Anda akan diantar ke Airport untuk penerbangan kembali ke Tanah Air', 'Anda akan melalui International Date Line', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah\r\nperjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Los Angeles - Jakarta + Vaccine Vacations Johnson & Johnson', 'assets/itinerary-pdf/Itinerary_FIT LA (Vaksin J&J) 9D6N2021.pdf', '2021-10-25 21:12:11', '2021-10-25 21:12:11'),
(58, 48, 'Hari ini Anda akan terbang menuju ke Zurich, Switzerland.', 'Tiba di Zurich, Anda akan menuju ke Hotel dengan pengaturan\r\nsendiri. NOVOTEL WEST CITY: Kereta langsung dari ZURICH AIRPORT -\r\nZÜRICH HARDBRÜCKE, kemudian menuju ke Hotel Novotel yang\r\nberjarak 0.65KM. IBIS STYLES / CENTRAL PLAZA: Kereta Langsung dari ZURICH AIRPORT - ZURICH HB kemudian menuju ke Hotel yang berjarak\r\n0.50KM. (Check in mulai pukul 15.00). Akomodasi: SESUAI PILIHAN atau setaraf', 'Hari ini Acara Bebas dimana Anda bisa mengikuti OPTIONAL TOUR seperti Tuk - Tuk Experience (1.200.000/orang) dimana Anda bisa berkeliling kota Zurich dengan menggunakan Tuk-Tuk / Bajaj modern dan berbelanja di Outlet of Switzerland (30 menit menggunakan kereta). Akomodasi: SESUAI PILIHAN atau setaraf', 'Hari ini Anda akan mengikuti tour untuk menjelajahi & menikmati Kota Lucerne dan dilanjutkan menuju ke Mount Titlis dimana Anda bisa menikmati pemandangan yang sangat indah dari pegunungan Alpen & Ice Grotto (termasuk dengan Cable Car). *Meeting point di Station Main Zurich. Akomodasi: SESUAI PILIHAN atau setaraf', 'Hari Acara Bebas (Hari ini Anda dapat melakukan PCR tes untuk salah satu persyaratan terbang kembali ke Indonesia - Biaya PCR CHF 145). Akomodasi: SESUAI PILIHAN atau setaraf', 'Pagi hari acara bebas, sampai saatnya Anda harus menuju ke Airport dengan pengaturan sendiri, dimana bisa ditempuh dengan kereta dari ZURICH HARDBRICK menuju ke ZURICH AIRPORT.', 'Anda akan tiba di Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Lucerne, Mount Tiltlis, Alpen & Ice Grotto, and many more!', 'assets/itinerary-pdf/daysOff __FIT Itinerary Zurich - Lucerne - Mt. Titlis_.pdf', '2021-10-26 23:41:19', '2021-11-10 02:36:15'),
(60, 76, 'Setibanya di Bandara Tanjung Pandan, Anda dijemput dan diantar ke Belitung Timur untuk mengunjungi Replika SD Laskar Pelangi, Dermaga Kirana (Rumah Keong), dan foto stop di Museum Kata Andrea Hirata (tidak termasuk tiket masuk). Makan siang di restoran lokal. Selanjutnya mengunjungi Wisata Batu Mentas (tidak termasuk tiket wahana). Anda akan menikmati makan malam di restoran lokal kemudian diantar ke hotel untuk check-in dan beristirahat.', 'Makan pagi di hotel, lalu Anda diantar ke Gurok Beraye sampai di batas mobil harus berhenti. Selanjutnya menggunakan mobil angkot untuk naik menuju Air Terjun Gurok Beraye. Lalu, Anda akan kembali ke parkiran mobil dengan mobil angkot dan akan dibagikan snack. Kemudian, Anda akan menikmati makan siang di restoran lokal. Setelah itu menuju Vihara Dewi Kwan Im dan Pantai Burung Mandi (jika waktu masih cukup). Makan malam di restoran lokal lalu diantar ke hotel untuk beristirahat.', 'Makan pagi di hotel. Kemudian menuju Belitung Selatan (Kec. Membalong), melewati Bukit Batu Granit Raksasa “Batu Baginde”, dan mengunjungi Pantai Penyabong. Makan siang di restoran lokal. Setelah makan siang, Anda diantar menuju Pantai Teluk Gembira dan Batu Penjuru Microcity. Setelah makan malam di restoran lokal, Anda langsung diantar ke hotel untuk beristirahat.', 'Sarapan pagi di hotel. Hari ini Anda diantar mengunjungi Bukit Peramun, Desa Bali, Klenteng Sijuk, dan Pantai Bukit Berahu. Setelah makan malam, Anda diantar ke hotel untuk beristirahat.', 'Sarapan pagi di hotel. Hari ini Anda akan menikmati Island Hopping Beach Tour. Anda diantar ke Pantai Tanjung Kelayang dan menggunakan kapal untuk Island Hopping Tour. Tour akan melewati Batu Kepala Burung Garuda, mengunjungi Pulau Pasir, Pulau Batu Berlayar, dan melakukan snorkeling di Pulau Lengkuas. Setelah makan siang, Anda melanjutkan perjalanan menuju Pulau Kelayang Besar, Goa Kelayang “Telaga Bidadari”, dan kembali ke Pantai Tanjung Kelayang untuk mandi. Kemudian, mengunjungi Pantai Tanjung Tinggi Laskar Pelangi. Anda juga berkesempatan untuk belanja oleh-oleh khas Belitung. Selesai makan malam, Anda diantar ke hotel untuk beristirahat.', 'Sarapan pagi di hotel dan melakukan check-out. Hari ini Anda akan diantar menuju Pantai Tanjung Pendam. Lalu, Anda berkesempatan untuk mencicipi kopi terkenal di Belitung, “Kopi Kong Djie Sejak Tahun 1943” (optional). Setelah itu, Anda akan mengunjungi Museum dan Mini Zoo Kota Tanjung Pandan, menikmati Mie Belitung & Es Jeruk Kunci, kemudian menuju Rumah Adat Belitung dan Danau Kaolin (Kawah Putih). Lalu, Anda akan diantar ke bandara. Dengan demikian berakhir sudah perjalanan Anda Bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Island Hopping Tour,  Replika SD Laskar Pelangi, Air Terjun Gurok Beraye, Batu Baginde, Klenteng Sijuk, Desa Bali', 'assets/itinerary-pdf/6D5N_Wisata Belitung.pdf', '2021-11-02 20:26:23', '2021-11-02 20:27:51'),
(61, 77, 'Setibanya di Bandara Tanjung Pandan, Anda dijemput dan diantar ke Belitung Timur untuk mengunjungi Replika SD Laskar Pelangi, Dermaga Kirana (Rumah Keong), dan foto stop di Museum Kata Andrea Hirata (tidak termasuk tiket masuk). Selanjutnya mengunjungi Wisata Batu Mentas (tidak termasuk tiket wahana). Kemudian, Anda langsung diantar ke hotel untuk check-in dan beristirahat.', 'Makan pagi di hotel, lalu Anda akan diantar ke Gurok Beraye sampai di batas mobil harus berhenti. Selanjutnya menggunakan mobil angkot untuk naik menuju Air Terjun Gurok Beraye. Lalu, Anda akan kembali ke parkiran mobil dengan mobil angkot dan akan dibagikan snack. Setelah itu menuju Vihara Dewi Kwan Im dan Pantai Burung Mandi (jika waktu masih cukup). Kemudian, Anda akan diantar ke hotel untuk beristirahat.', 'Makan pagi di hotel. Kemudian menuju Belitung Selatan (Kec. Membalong), melewati Bukit Batu Granit Raksasa “Batu Baginde”, dan mengunjungi Pantai Penyabong, Pantai Teluk Gembira, dan Batu Penjuru Microcity. Setelah itu, Anda langsung diantar ke hotel untuk beristirahat.', 'Sarapan pagi di hotel. Hari ini Anda diantar untuk mengunjungi Bukit Peramun, Desa Bali, Klenteng Sijuk, dan Pantai Bukit Berahu. Kemudian, Anda diantar ke hotel untuk beristirahat.', 'Sarapan pagi di hotel. Hari ini Anda akan menikmati Island Hopping Beach Tour. Anda akan diantar ke Pantai Tanjung Kelayang dan menggunakan kapal untuk Island Hopping Tour. Tour akan melewati Batu Kepala Burung Garuda, mengunjungi Pulau Pasir, Pulau Batu Berlayar, melakukan snorkeling di Pulau Lengkuas, mengunjungi  Pulau Kelayang Besar, dan Goa Kelayang “Telaga Bidadari”, kembali ke Pantai Tanjung Kelayang untuk mandi. Kemudian, mengunjungi Pantai Tanjung Tinggi Laskar Pelangi. Anda juga berkesempatan untuk belanja oleh-oleh khas Belitung. Lalu, Anda diantar ke hotel untuk beristirahat.', 'Sarapan pagi di hotel dan melakukan check-out. Hari ini Anda akan diantar menuju Pantai Tanjung Pendam. Lalu, Anda berkesempatan untuk mencicipi kopi terkenal di Belitung, “Kopi Kong Djie Sejak Tahun 1943” (optional). Setelah itu, Anda akan mengunjungi Museum dan Mini Zoo Kota Tanjung Pandan, menikmati Mie Belitung & Es Jeruk Kunci, kemudian menuju Rumah Adat Belitung dan Danau Kaolin (Kawah Putih). Lalu, Anda akan diantar ke bandara. Dengan demikian berakhir sudah perjalanan Anda Bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Island Hopping Tour, Replika SD Laskar Pelangi, Air Terjun Gurok Beraye, Batu Baginde, Klenteng Sijuk, Desa Bali', 'assets/itinerary-pdf/6D5N Wisata Belitung Hemat.pdf', '2021-11-02 20:33:35', '2021-11-02 20:33:35'),
(62, 78, 'Penjemputan di Bandara Kalimarau Berau, lalu Anda melanjutkan perjalanan menuju Pulau Derawan. Setelah makan siang, Anda akan mengunjungi Gusung Sanggalau, destinasi yang sangat instagramable, spot foto yang cantik gugusan pasir di tengah laut. Kemudian, Anda akan mengunjungi Pulau Maratua menuju Pratasaba Resort untuk check in dan snorkeling menjelajah dunia laut Pratasaba sembari menunggu sunset di ujung Saba Resto. Lalu, Anda akan menikmati dinner di Saba Resto.', 'Setelah breakfast di hotel, Anda akan mengikuti Island Hopping Tour. Pulau pertama yaitu Pulau Sangalaki, untuk mengunjungi konservasi penyu dan eksplor Pulau Sangalaki, jika beruntung Anda akan memiliki kesempatan melihat tukik menetas dari dalam pasir. Berikutnya perjalanan menuju Pulau Kakaban untuk snorkeling bersama stingless jellyfish (ubur-ubur tidak beracun) yang merupakan destinasi favorit di wisata kali ini. Anda akan menikmati makan siang di Saba Resto. Setelah makan siang, Anda diajak untuk mengunjungi Goa Halo Tabung dimana Anda dapat merasakan sensasi dinginnya air di tebing batu & sensasi melompat dari ketinggian goa. Lalu, melanjutkan perjalanan kembali ke Pratasaba Resort, Anda dapat bermain di pantai sembari menunggu sunset. Dinner hari ini akan dinikmati di Pratasaba Resort.', 'Setelah breakfast, Anda diberi waktu untuk snorkeling di area resort, lalu proses check out dan menuju Pelabuhan Tanjung Redeb. Kemudian, Anda akan diantar ke Bandara Kalimarau, Berau untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda Bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Island Hopping; Gusung Sanggalau', 'assets/itinerary-pdf/3D2N Special Island Hopping Maratua_.pdf', '2021-11-02 20:57:24', '2021-11-02 21:53:57'),
(63, 79, 'Penjemputan di Bandara Kalimarau Berau, lalu Anda melanjutkan perjalanan menuju Pulau Derawan. Setelah makan siang, Anda akan mengunjungi Gusung Sanggalau, destinasi yang sangat instagramable, spot foto yang cantik gugusan pasir di tengah laut. Kemudian, Anda akan mengunjungi Pulau Maratua menuju Pratasaba Resort untuk check in dan snorkeling menjelajah dunia laut Pratasaba sembari menunggu sunset di ujung Saba Resto. Lalu, Anda akan menikmati dinner di Saba Resto.', 'Setelah breakfast di hotel, Anda akan mengikuti Island Hopping Tour. Pulau pertama yaitu Pulau Sangalaki, untuk mengunjungi konservasi penyu dan eksplor Pulau Sangalaki, jika beruntung Anda akan memiliki kesempatan melihat tukik menetas dari dalam pasir. Berikutnya perjalanan menuju Pulau Kakaban untuk snorkeling bersama stingless jellyfish (ubur-ubur tidak beracun) yang merupakan destinasi favorit di wisata kali ini. Anda akan menikmati makan siang di Saba Resto. Setelah makan siang, Anda diajak untuk mengunjungi Goa Halo Tabung dimana Anda dapat merasakan sensasi dinginnya air di tebing batu & sensasi melompat dari ketinggian goa. Lalu, melanjutkan perjalanan kembali ke Pratasaba Resort, Anda dapat bermain di pantai sembari menunggu sunset. Dinner hari ini akan dinikmati di Pratasaba Resort.', 'Breakfast berupa mealbox. Pukul 04.30, Anda diajak menuju Talisayan, untuk berenang dengan whale shark/hiu paus yang bersahabat. Kemudian, melanjutkan perjalanan menuju Pulau Spongebob yang memiliki hamparan pantai berpasir putih menawan. Lalu, Anda akan mengunjungi Labuan Cermin, destinasi kelas dunia yang terkenal karena sebening kaca dan keunikan dua rasa di dalamnya. Makan siang akan dinikmati di lokal resto. Lalu, kembali ke resort untuk beristirahat dan makan malam.', 'Setelah breakfast, Anda diberi waktu untuk snorkeling di area resort, lalu proses check out dan menuju Pelabuhan Tanjung Redeb. Kemudian, Anda akan diantar ke Bandara Kalimarau, Berau untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Island Hopping,  Gusung Sanggalau, Talisayan, Pulau Spongebob, Labuan Cermin', 'assets/itinerary-pdf/4D3N Explore Derawan Archipelago.pdf', '2021-11-02 21:14:15', '2021-11-02 21:14:15'),
(64, 80, 'Penjemputan di Bandara Kalimarau Berau, lalu Anda melanjutkan perjalanan menuju Pulau Derawan. Setelah makan siang, Anda akan mengunjungi Gusung Sanggalau, destinasi yang sangat instagramable, spot foto yang cantik gugusan pasir di tengah laut. Kemudian, Anda akan mengunjungi Pulau Maratua menuju Pratasaba Resort untuk check in dan snorkeling menjelajah dunia laut Pratasaba sembari menunggu sunset di ujung Saba Resto. Lalu, Anda akan menikmati dinner di Saba Resto.', 'Setelah breakfast di hotel, Anda akan mengikuti Island Hopping Tour. Pulau pertama yaitu Pulau Sangalaki, untuk mengunjungi konservasi penyu dan eksplor Pulau Sangalaki, jika beruntung Anda akan memiliki kesempatan melihat tukik menetas dari dalam pasir. Berikutnya perjalanan menuju Pulau Kakaban untuk snorkeling bersama stingless jellyfish (ubur-ubur tidak beracun) yang merupakan destinasi favorit di wisata kali ini. Anda akan menikmati makan siang di Saba Resto. Selesai makan siang, Anda akan diantarkan kembali ke hotel untuk menikmati waktu bebas. Makan malam akan dinikmati di hotel.', 'Breakfast berupa mealbox. Pukul 04.30, Anda diajak menuju Talisayan, untuk berenang dengan whale shark/hiu paus yang bersahabat. Kemudian, melanjutkan perjalanan menuju Pulau Spongebob yang memiliki hamparan pantai berpasir putih menawan. Lalu, Anda akan mengunjungi Labuan Cermin, destinasi kelas dunia yang terkenal karena sebening kaca dan keunikan dua rasa di dalamnya. Makan siang akan dinikmati di lokal resto. Lalu, kembali ke resort untuk beristirahat dan makan malam.', 'Selesai breakfast, Anda akan diajak untuk keliling Pulau Maratua menggunakan mobil. Kami akan membawa Anda mengunjungi kampung di Maratua untuk melihat kehidupan masyarakat suku Bajau dan mengabadikan momen di landmark Maratua. Kemudian, melanjutkan perjalanan menuju Goa Halo Tabung dimana Anda diajak untuk merasakan sensasi dinginnya air di tebing batu & sensasi melompat dari ketinggian goa. Setelah itu, Anda akan kembali ke resort untuk beristirahat dan makan malam.', 'Setelah breakfast, Anda diberi waktu untuk snorkeling di area resort, lalu proses check out dan menuju Pelabuhan Tanjung Redeb. Kemudian, Anda akan diantar ke Bandara Kalimarau, Berau untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Island Hopping,  Gusung Sanggalau, Talisayan, Pulau Spongebob, Labuan Cermin', 'assets/itinerary-pdf/5D4N Exclusive Journey Derawan Archipelago.pdf', '2021-11-02 21:36:18', '2021-11-02 21:36:18'),
(65, 81, 'Penjemputan di Bandara Internasional Lombok, bertemu dengan tim kami. Lalu, Anda akan diantar menuju meeting point yaitu rest area atau restoran. Makan siang atau makan malam di restoran lokal. Setelah itu, melanjutkan perjalanan menuju hotel untuk check-in dan beristirahat.', 'Breakfast di hotel lalu hari ini, Anda akan mengunjungi Mandalika International Street Circuit untuk menyaksikan World Superbike Main Race (Final Race / Round 13th). Anda akan menikmati makan malam di restoran lokal, lalu kembali ke hotel.', 'Breakfast di hotel kemudian menuju Shopping Center di Mataram atau mengunjungi Islamic Center. Selesai makan siang di restoran lokal, lalu Anda diantar menuju Bandara Internasional Lombok untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mandalika International Street Circuit and Mataram City Tour', 'assets/itinerary-pdf/3D2N Mandalika World Superbike 2021 (Private).pdf', '2021-11-03 20:44:14', '2021-11-03 20:44:14'),
(66, 82, 'Penjemputan di Bandara Internasional Lombok, bertemu dengan tim kami. Lalu, Anda akan diantar menuju meeting point yaitu rest area atau restoran. Makan siang atau makan malam di restoran lokal. Setelah itu, melanjutkan perjalanan menuju hotel untuk check-in dan beristirahat.', 'Breakfast di hotel lalu hari ini, Anda akan mengunjungi Mandalika International Street Circuit untuk menyaksikan World Superbike Main Race (Final Race / Round 13th). Anda akan menikmati makan malam di restoran lokal, lalu kembali ke hotel.', 'Setelah breakfast di hotel, Anda akan mengunjungi Bukit Malimbu untuk photo stop. Kemudian, melanjutkan perjalanan ke pelabuhan untuk mengunjungi Pulau Gili Trawangan menggunakan speed boat. Setelah puas menikmati keindahan Gili Trawangan, kembali ke pelabuhan dan menikmati makan malam di restoran lokal. Lalu, Anda akan diantar kembali ke hotel.', 'Breakfast di hotel kemudian menuju Shopping Center di Mataram atau mengunjungi Islamic Center. Selesai makan siang di restoran lokal, lalu Anda diantar menuju Bandara Internasional Lombok untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mandalika International Street Circuit, Gili Trawangan Island Tour, Mataram City Tour', 'assets/itinerary-pdf/4D3N Mandalika World Superbike 2021 (Private).pdf', '2021-11-03 20:59:08', '2021-11-03 20:59:08'),
(67, 83, 'Penjemputan di Bandara Internasional Lombok, bertemu dengan tim kami. Lalu, Anda akan diantar menuju meeting point yaitu rest area atau restoran. Makan siang atau makan malam di restoran lokal. Setelah itu, melanjutkan perjalanan menuju hotel untuk check-in dan beristirahat.', 'Breakfast di hotel lalu hari ini, Anda akan mengunjungi Mandalika International Street Circuit untuk menyaksikan World Superbike Main Race (Final Race / Round 13th). Anda akan menikmati makan malam di restoran lokal, lalu kembali ke hotel.', 'Breakfast di hotel kemudian menuju Shopping Center di Mataram atau mengunjungi Islamic Center. Selesai makan siang di restoran lokal, lalu Anda diantar menuju Bandara Internasional Lombok untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mandalika International Street Circuit and Mataram City Tour', 'assets/itinerary-pdf/3D2N Mandalika World Superbike 2021 (SIC).pdf', '2021-11-03 21:25:03', '2021-11-03 21:25:03'),
(68, 84, 'Penjemputan di Bandara Internasional Lombok, bertemu dengan tim kami. Lalu, Anda akan diantar menuju meeting point yaitu rest area atau restoran. Makan siang atau makan malam di restoran lokal. Setelah itu, melanjutkan perjalanan menuju hotel untuk check-in dan beristirahat.', 'Breakfast di hotel lalu hari ini, Anda akan mengunjungi Mandalika International Street Circuit untuk menyaksikan World Superbike Main Race (Final Race / Round 13th). Anda akan menikmati makan malam di restoran lokal, lalu kembali ke hotel.', 'Setelah breakfast di hotel, Anda akan mengunjungi Bukit Malimbu untuk photo stop. Kemudian, melanjutkan perjalanan ke pelabuhan untuk mengunjungi Pulau Gili Trawangan menggunakan speed boat. Setelah puas menikmati keindahan Gili Trawangan, kembali ke pelabuhan dan menikmati makan malam di restoran lokal. Lalu, Anda akan diantar kembali ke hotel.', 'Breakfast di hotel kemudian menuju Shopping Center di Mataram atau mengunjungi Islamic Center. Selesai makan siang di restoran lokal, lalu Anda diantar menuju Bandara Internasional Lombok untuk kembali ke Jakarta. Dengan demikian berakhir sudah perjalanan Anda bersama daysOff Tour & Travel. Semoga bertemu kembali di lain kesempatan.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mandalika International Street Circuit, Gili Trawangan Island Tour, Mataram City Tour', 'assets/itinerary-pdf/4D3N Mandalika World Superbike 2021 (SIC).pdf', '2021-11-03 21:30:53', '2021-11-03 21:30:53');

-- --------------------------------------------------------

--
-- Table structure for table `type_data_masters`
--

CREATE TABLE `type_data_masters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type_data_masters`
--

INSERT INTO `type_data_masters` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'FIT', 'F', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(2, 'GROUP', 'G', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(3, 'INCENTIVE', 'I', '2021-10-18 06:53:23', '2021-10-18 06:53:23'),
(4, 'PRIVATE TOUR', 'P', '2021-10-18 06:53:23', '2021-10-18 06:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `roles`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Liliani', 'superadmin@daysweb.com', NULL, '$2y$10$ajYp4FrEjyrmaoVBmxExy.0mt1JytfhZOq28j6eEzjZkRsBm40OqW', 'SUPERADMIN', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(2, 'Technical Travel', 'emailtechnicalstartuptravel@gmail.com', NULL, '$2y$10$vcvbeIY1/jl3X5ZBTNkdPO5IbarZNOwmUyZhxIDX/IIAfZBx3Rj0q', 'ADMIN', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(3, 'Tasya Cristalis', 'touroperator2@daysweb.com', NULL, '$2y$10$ObMBoV6P95qTWgSHxoufxu7dnn0V4hFwLvTfjw5wOYlXZfdx4TCGu', 'ADMIN', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-27 15:19:22'),
(4, 'Viona Wong', 'viona.wong@daysweb.com', NULL, '$2y$10$SMJYSMlLX6DCPW/rhtf6qON0c.01z5zvMkSM45IbqESgu1dw3l9aC', 'ADMIN', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(5, 'Liliani', 'touroperator1@daysweb.com', NULL, '$2y$10$a3Er/1KLumcEmKresJ6aUuyX2hhLkYXK2mnevV1pN0JqYLUUMQhki', 'ADMIN', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-27 15:19:03'),
(6, 'Tona', 'sales1@daysweb.com', NULL, '$2y$10$oV05hwEWsWtDm3Cj3RBbsOxpjxC6IytWyPFTV/wvFZoh0iDwjKvv2', 'SALES', 'Active', '7LjzGdki5lTpMGUCqHilReWjmfNKZfV6bYofRoR2wkw5Fm7YTj5qfVS0swVk', '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(7, 'Andri', 'sales2@daysweb.com', NULL, '$2y$10$p3tOukpF8r0ZLZQgiuK43uAlg8jBO.RvQr9Vda7piFQK2HNxbb/he', 'SALES', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(8, 'Liliani', 'liliani.liliani@daysweb.com', NULL, '$2y$10$.eS5sY1QN4yfxicLbvsxM.C5JOdCsDH0r5c6qzygjJYpKweY2C5ki', 'SUPERADMIN', 'Active', 'Gaub4NsCwFuWvlM832AURlgJsbEkzPtG4L6DXdIA2Bdx8vEyGwhqAfMJfpk3', '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(9, 'Tasya', 'tasya.cristalis@daysweb.com', NULL, '$2y$10$U9c9pV1Agkc500Sli4TBguT1VWi9PQO8YGnAoOPCoW.wC/Uf783hS', 'SUPERADMIN', 'Active', 'Q2MY7hvTZHZjM58XJM1JIoZRlq1iVCSuiChPL0NFLoL2m5EIxEFAaGM8AYgg', '2021-10-18 03:01:59', '2021-10-18 03:01:59'),
(10, 'Kevin', 'kevin.candra@daysweb.com', NULL, '$2y$10$n/Nfs24CaoIL9bwuhA9G5.51wZl9wNivnhYFgdqAIkw62nqBfbld2', 'SUPERADMIN', 'Active', NULL, '2021-10-18 03:01:59', '2021-10-18 03:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_data_masters`
--

CREATE TABLE `vendor_data_masters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_data_masters`
--

INSERT INTO `vendor_data_masters` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'ANTAVAYA\r\n', 'AN', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(2, 'ATS', 'AT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(3, 'SKYHUB', 'SK', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(4, 'DAYSOFF', 'DO', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(5, 'Avia Tour\r\n', 'AV', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(6, 'Boutique JTB\r\n', 'JT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(7, 'Hamkyu Travel\r\n', 'HT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(8, 'Jaipak\r\n', 'JA', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(9, 'E-Turism\r\n', 'ET', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(10, 'VIP Travel\r\n', 'VI', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(11, 'Sedunia Travel \r\n', 'ST', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(12, 'My Taiwan Tour\r\n', 'MT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(13, 'Vietnamese Private Tour\r\n', 'VP', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(14, 'Moroc Experience\r\n', 'ME', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(15, 'Morocci Traveland\r\n', 'MO', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(16, 'RJ Travel Tour\r\n', 'RJ', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(17, 'Aba Trip Plus \r\n', 'AB', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(18, 'Dolores Travel\r\n', 'DT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(19, 'Travel China Guide Tour\r\n', 'TC', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(20, 'Agatha Tour\r\n', 'AG', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(21, 'H.I.S Travel Indo\r\n', 'HI', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(22, 'HN Travel\r\n', 'HN', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(23, 'Global Tour\r\n', 'GL', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(24, 'Matta Pariwisata Indonesia\r\n', 'MP', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(25, 'Holidaystours\r\n', 'HO', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(26, 'ALTOUR\r\n', 'AL', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(27, 'e-Philippines Adventure Travel \r\n', 'EP', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(28, 'Filipinotravel\r\n', 'FI', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(29, 'Maldives Tour\r\n', 'MA', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(30, 'Accolade Events\r\n', 'AE', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(31, 'Exploring Tourism \r\n', 'EX', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(32, 'Diethelmtravel\r\n', 'DI', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(33, 'Salom Travel Service\r\n', 'SA', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(34, 'Globalconnect.uz\r\n', 'GC', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(35, 'Uzbek-travel\r\n', 'UT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(36, 'Turan-travel\r\n', 'TU', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(37, 'Aulia Tour Travel\r\n', 'AU', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(38, 'Hore Wisata\r\n', 'HW', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(39, 'Aurora Wisata\r\n', 'AR', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(40, 'Bintang Holidays Travel\r\n', 'BN', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(41, 'PPI Dunia\r\n', 'PD', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(42, 'PPI France\r\n', 'PF', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(43, 'Explore Sumba\r\n', 'ES', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(44, 'Tripuri\r\n', 'TR', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(45, 'Best of Turkeytour\r\n', 'BT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(46, 'Advan Tour\r\n', 'AD', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(47, 'Tour-Orient\r\n', 'TO', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(48, 'Uzbektour\r\n', 'UZ', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(49, 'Tobu Top Tours\r\n', 'TT', '2021-10-06 11:09:03', '2021-10-06 11:09:03'),
(50, 'ACT Travel', 'AC', '2021-10-06 13:10:41', '2021-10-06 13:10:41'),
(51, 'Marioga Tour and Travel', 'MR', '2021-10-29 02:12:30', '2021-10-29 02:12:30'),
(52, 'Pratama Tour', 'PR', '2021-11-02 02:37:28', '2021-11-02 02:37:28');

-- --------------------------------------------------------

--
-- Table structure for table `visa_data_masters`
--

CREATE TABLE `visa_data_masters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_tours`
--
ALTER TABLE `booking_tours`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `booking_tours_invoice_code_unique` (`invoice_code`),
  ADD KEY `booking_tours_status_id_foreign` (`status_id`),
  ADD KEY `booking_tours_travel_package_code_foreign` (`travel_package_code`);

--
-- Indexes for table `booking_tour_statuses`
--
ALTER TABLE `booking_tour_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `careers_slug_unique` (`slug`);

--
-- Indexes for table `commition_requests`
--
ALTER TABLE `commition_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commition_requests_booking_tour_id_foreign` (`booking_tour_id`),
  ADD KEY `commition_requests_status_id_foreign` (`status_id`);

--
-- Indexes for table `commition_request_statuses`
--
ALTER TABLE `commition_request_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departure_dates`
--
ALTER TABLE `departure_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departure_dates_travel_package_code_foreign` (`travel_package_code`);

--
-- Indexes for table `destination_categories`
--
ALTER TABLE `destination_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destination_data_masters`
--
ALTER TABLE `destination_data_masters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `destination_data_masters_code_unique` (`code`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `flyer_image`
--
ALTER TABLE `flyer_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_travel_package_code_foreign` (`travel_package_code`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hotels_slug_unique` (`slug`);

--
-- Indexes for table `hotel_galleries`
--
ALTER TABLE `hotel_galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_galleries_hotel_id_foreign` (`hotel_id`);

--
-- Indexes for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_room_prices`
--
ALTER TABLE `hotel_room_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_room_prices_hotel_id_foreign` (`hotel_id`),
  ADD KEY `hotel_room_prices_room_id_foreign` (`room_id`);

--
-- Indexes for table `inclusives`
--
ALTER TABLE `inclusives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inclusives_travel_package_id_foreign` (`travel_package_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `passengers_booking_tour_id_foreign` (`booking_tour_id`),
  ADD KEY `passengers_passenger_pic_id_foreign` (`passenger_pic_id`);

--
-- Indexes for table `passenger_pics`
--
ALTER TABLE `passenger_pics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `passenger_pics_booking_tour_id_foreign` (`booking_tour_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `show_hotels`
--
ALTER TABLE `show_hotels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `show_hotels_hotel_id_foreign` (`hotel_id`);

--
-- Indexes for table `show_tour_events`
--
ALTER TABLE `show_tour_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `show_tour_events_travel_package_id_foreign` (`travel_package_id`);

--
-- Indexes for table `show_travel_packages`
--
ALTER TABLE `show_travel_packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `show_travel_packages_travel_package_id_foreign` (`travel_package_id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staycations`
--
ALTER TABLE `staycations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staycations_title_unique` (`title`),
  ADD UNIQUE KEY `staycations_slug_unique` (`slug`);

--
-- Indexes for table `staycation_igposts`
--
ALTER TABLE `staycation_igposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_offers`
--
ALTER TABLE `tour_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_travel_package_id_foreign` (`travel_package_id`);

--
-- Indexes for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaction_details_transactions_id_foreign` (`transactions_id`);

--
-- Indexes for table `travel_packages`
--
ALTER TABLE `travel_packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `travel_packages_title_unique` (`title`),
  ADD UNIQUE KEY `travel_packages_code_unique` (`code`),
  ADD UNIQUE KEY `travel_packages_slug_unique` (`slug`),
  ADD KEY `travel_packages_category_id_foreign` (`category_id`),
  ADD KEY `travel_packages_vendor_data_code_foreign` (`vendor_data_code`),
  ADD KEY `travel_packages_type_data_code_foreign` (`type_data_code`);

--
-- Indexes for table `trip_itineraries`
--
ALTER TABLE `trip_itineraries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trip_itineraries_travel_package_id_foreign` (`travel_package_id`);

--
-- Indexes for table `type_data_masters`
--
ALTER TABLE `type_data_masters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_data_masters_code_unique` (`code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendor_data_masters`
--
ALTER TABLE `vendor_data_masters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendor_data_masters_code_unique` (`code`);

--
-- Indexes for table `visa_data_masters`
--
ALTER TABLE `visa_data_masters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_tours`
--
ALTER TABLE `booking_tours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_tour_statuses`
--
ALTER TABLE `booking_tour_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `commition_requests`
--
ALTER TABLE `commition_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `commition_request_statuses`
--
ALTER TABLE `commition_request_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `departure_dates`
--
ALTER TABLE `departure_dates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `destination_categories`
--
ALTER TABLE `destination_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `destination_data_masters`
--
ALTER TABLE `destination_data_masters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flyer_image`
--
ALTER TABLE `flyer_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `hotel_galleries`
--
ALTER TABLE `hotel_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `hotel_room_prices`
--
ALTER TABLE `hotel_room_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=322;

--
-- AUTO_INCREMENT for table `inclusives`
--
ALTER TABLE `inclusives`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `passengers`
--
ALTER TABLE `passengers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `passenger_pics`
--
ALTER TABLE `passenger_pics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `show_hotels`
--
ALTER TABLE `show_hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `show_tour_events`
--
ALTER TABLE `show_tour_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `show_travel_packages`
--
ALTER TABLE `show_travel_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `staycations`
--
ALTER TABLE `staycations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staycation_igposts`
--
ALTER TABLE `staycation_igposts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tour_offers`
--
ALTER TABLE `tour_offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `travel_packages`
--
ALTER TABLE `travel_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `trip_itineraries`
--
ALTER TABLE `trip_itineraries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `type_data_masters`
--
ALTER TABLE `type_data_masters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vendor_data_masters`
--
ALTER TABLE `vendor_data_masters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `visa_data_masters`
--
ALTER TABLE `visa_data_masters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking_tours`
--
ALTER TABLE `booking_tours`
  ADD CONSTRAINT `booking_tours_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `booking_tour_statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `booking_tours_travel_package_code_foreign` FOREIGN KEY (`travel_package_code`) REFERENCES `travel_packages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commition_requests`
--
ALTER TABLE `commition_requests`
  ADD CONSTRAINT `commition_requests_booking_tour_id_foreign` FOREIGN KEY (`booking_tour_id`) REFERENCES `booking_tours` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `commition_requests_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `commition_request_statuses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `departure_dates`
--
ALTER TABLE `departure_dates`
  ADD CONSTRAINT `departure_dates_travel_package_code_foreign` FOREIGN KEY (`travel_package_code`) REFERENCES `travel_packages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_travel_package_code_foreign` FOREIGN KEY (`travel_package_code`) REFERENCES `travel_packages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel_galleries`
--
ALTER TABLE `hotel_galleries`
  ADD CONSTRAINT `hotel_galleries_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel_room_prices`
--
ALTER TABLE `hotel_room_prices`
  ADD CONSTRAINT `hotel_room_prices_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hotel_room_prices_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inclusives`
--
ALTER TABLE `inclusives`
  ADD CONSTRAINT `inclusives_travel_package_id_foreign` FOREIGN KEY (`travel_package_id`) REFERENCES `travel_packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `passengers`
--
ALTER TABLE `passengers`
  ADD CONSTRAINT `passengers_booking_tour_id_foreign` FOREIGN KEY (`booking_tour_id`) REFERENCES `booking_tours` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `passengers_passenger_pic_id_foreign` FOREIGN KEY (`passenger_pic_id`) REFERENCES `passenger_pics` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `passenger_pics`
--
ALTER TABLE `passenger_pics`
  ADD CONSTRAINT `passenger_pics_booking_tour_id_foreign` FOREIGN KEY (`booking_tour_id`) REFERENCES `booking_tours` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `show_hotels`
--
ALTER TABLE `show_hotels`
  ADD CONSTRAINT `show_hotels_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `show_tour_events`
--
ALTER TABLE `show_tour_events`
  ADD CONSTRAINT `show_tour_events_travel_package_id_foreign` FOREIGN KEY (`travel_package_id`) REFERENCES `travel_packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `show_travel_packages`
--
ALTER TABLE `show_travel_packages`
  ADD CONSTRAINT `show_travel_packages_travel_package_id_foreign` FOREIGN KEY (`travel_package_id`) REFERENCES `travel_packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_travel_package_id_foreign` FOREIGN KEY (`travel_package_id`) REFERENCES `travel_packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD CONSTRAINT `transaction_details_transactions_id_foreign` FOREIGN KEY (`transactions_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `travel_packages`
--
ALTER TABLE `travel_packages`
  ADD CONSTRAINT `travel_packages_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `destination_categories` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `travel_packages_type_data_code_foreign` FOREIGN KEY (`type_data_code`) REFERENCES `type_data_masters` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `travel_packages_vendor_data_code_foreign` FOREIGN KEY (`vendor_data_code`) REFERENCES `vendor_data_masters` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trip_itineraries`
--
ALTER TABLE `trip_itineraries`
  ADD CONSTRAINT `trip_itineraries_travel_package_id_foreign` FOREIGN KEY (`travel_package_id`) REFERENCES `travel_packages` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
