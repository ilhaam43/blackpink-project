// $(function () {
//     $(window).on('scroll', function () {
//         if ( $(window).scrollTop() > 10 ) {
//             $('.navbar').addClass('active');
//         } else {
//             $('.navbar').removeClass('active');
//         }
//     });
// });

(function ($) {
	$(document).ready(function(){
		$(".my-navbar-booking").hide();
		$(function () {
			$(window).scroll(function () {
				// set distance user needs to scroll before we fadeIn navbar
				if ($(this).scrollTop() < 300) {
					$('.my-navbar-booking').fadeOut();
				} else {
					$('.my-navbar-booking').fadeIn();
				}
			});
	  	});
  	});
}(jQuery));

(function ($) {
	$(document).ready(function(){
		$(".second-nav").hide();
		$(function () {
			$(window).scroll(function () {
				// set distance user needs to scroll before we fadeIn navbar
				if ($(this).scrollTop() < 300) {
					$('.second-nav').fadeOut();
				} else {
					$('.second-nav').fadeIn();
				}
			});
	  	});
  	});
}(jQuery));

$('.pulse-card').hover(
	function(){ $(this).addClass('animated pulse') },
	function(){ $(this).removeClass('animated pulse') }
);
$('.bounce-card').hover(
	function(){ $(this).addClass('animated bounce') },
	function(){ $(this).removeClass('animated bounce') }
);
$(document).ready(function(){
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 100);
			return false;
		});
});

$( document ).ready(function() {
	new WOW().init();
});