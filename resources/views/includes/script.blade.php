<script src="{{url('frontend/libraries/jquery/jquery-3.5.1.min.js')}}"></script>
<script src="{{url('frontend/libraries/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://use.fontawesome.com/b3848cc099.js"></script>
<script src="{{url('frontend/libraries/mdbootstrap/js/wow.min.js')}}"></script>
<script src="{{url('frontend/scripts/main.js')}}?ver={{ filemtime(public_path('frontend/scripts/main.js')) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>