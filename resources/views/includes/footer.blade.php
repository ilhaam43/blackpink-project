    <!-- Footer -->
    <footer class="section-footer mb-4 border-top">
        <div class="container pt-5 pb-5">
          <div class="row">
            <div class="col-12 col-lg-3">
              <div class="row">
                <div class="col">
                  <a href="index.html">
                  <img width="200px" src="{{url('frontend/images/temp/daysoff logo utama.png')}}" alt="daysoff-logo">
                  </a>
                </div>
              </div>
              <div class="row d-flex align-items-center mb-3">
                <div class="col-2">
                  <a href="https://facebook.com/daysoff.id/"><img src="{{url('frontend/images/temp/facebook.png')}}" alt="facebook-logo"></a>
                </div>
                <div class="col-2">
                  <a href="https://instagram.com/daysoff.id/"><img src="{{url('frontend/images/temp/instagram.png')}}" alt="instagram-logo"></a>
                </div>
                <div class="col-2">
                  <a href="https://vm.tiktok.com/ZSWPdFm5/"><img src="{{url('frontend/images/temp/logo-tiktok.png')}}" alt="tiktok-logo"></a>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-3">
              <div class="row">
                <div class="col">
                  <h5>Visit Us</h5>
                  <p style="font-size:14px; ">The Kensington Office Tower Lt. 15<br>
                    Kelapa Gading, Jakarta Utara<br>
                    Indonesia<br>
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <h5>Office Hours</h5>
                  <p>MON - FRI 09:00 - 17:00<br>
                  SAT - SUN 09:00 - 17:00 (CONSULTATION VIA WHATSAPP ONLY)
                  </p>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-3">
              <div class="row">
                <div class="col-12">
                  <h5>Contact Us</h5>
                  <i class="fa fa-building" aria-hidden="true"></i> 021 3114 6635<br>
                  <i class="fa fa-building" aria-hidden="true"></i> 021 3114 6636<br>
                  <i class="fa fa-whatsapp" aria-hidden="true"></i> WA : 0811 9728 228 <br>
                  <i class="fa fa-envelope" aria-hidden="true"></i> management@daysofftour.com
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-3">
              <div class="row">
                <div class="col">
                  <h5>INFORMATION</h5>
                  <ul class="list-unstyled">
                    <li><a href="{{url('/about')}}">ABOUT US</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="{{route('TermsAndConditions')}}">TERMS & CONDITIONS</a></li>
                    <li><a href="{{route('Faq')}}">F.A.Q</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row border-top justify-content-center align-items-center pt-4">
            <div class="col-auto text-gray-500 font-weight-light">
              Copyright ©2024 PT Adiwangsa Kertayasa Internasional.
            </div>
          </div>
        </div>
        <a id="back-to-top" href="#" class="btn btn-light btn-lg rounded-circle back-to-top" role="button">
          <i class="fa fa-chevron-up"></i>
        </a>
      </footer>