<!-- Navbar -->
<nav class="navbar my-navbar navbar-light bg-light navbar-expand-lg">
  <div class="container">
    <a class="navbar-brand" href="{{url('/')}}">
    <img src="{{url('frontend/images/temp/daysoff logo utama.png')}}" height="60px" alt="daysoff-logo-utama">
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item mx-2">
          <a class="nav-link" href="{{url('/')}}">HOME</a>
        </li>
        <li class="nav-item mx-2 dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DESTINATIONS
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
            <a class="dropdown-item" href="{{route('EuropeRegion')}}"><i class="fa fa-map-marker"></i> EUROPE</a>
            <a class="dropdown-item" href="{{route('AsiaRegion')}}"><i class="fa fa-map-marker"></i> ASIA</a>
            <a class="dropdown-item" href="{{route('DomesticRegion')}}"><i class="fa fa-map-marker"></i> INDONESIA</a>
            <a class="dropdown-item" href="{{route('AmericaRegion')}}"><i class="fa fa-map-marker"></i> AMERICA</a>
            <a class="dropdown-item" href="{{route('AustraliaRegion')}}"><i class="fa fa-map-marker"></i> AUSTRALIA</a>
            <a class="dropdown-item" href="{{route('AfricaRegion')}}"><i class="fa fa-map-marker"></i> AFRICA</a>
            <a class="dropdown-item" href="{{route('MiddleEastRegion')}}"><i class="fa fa-map-marker"></i> MIDDLE EAST</a>
          </div>
        </li>
        <li class="nav-item mx-2 dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          OFFERS
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
            <a class="dropdown-item" href="{{route('Transportation')}}"><i class="fa fa-plane"></i> TRANSPORTATION</a>
            <a class="dropdown-item" href="{{url('/tours')}}"><i class="fa fa-map"></i> TOURS & EVENTS</a>
            <a class="dropdown-item" href="{{route('Attractions')}}"><i class="fa fa-ticket"></i> ATTRACTIONS</a>
          </div>
        </li>
        <li class="nav-item mx-2">
          <a class="nav-link" href="{{route('Mice')}}">MICE CORPORATE SERVICE</a>
        </li>
        <li class="nav-item mx-2 dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          DOCUMENTS
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
            <a class="dropdown-item" href="{{url('/visa-and-passport')}}"><i class="fa fa-id-card"></i> VISA & PASSPORT</a>
            <a class="dropdown-item" href="#"><i class="fa fa-suitcase"></i> TRAVEL ESSENTIALS</a>
          </div>
        </li>
        <li class="nav-item mx-2 dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          ABOUT
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
            <a class="dropdown-item" href="{{url('/about')}}"><i class="fa fa-users"></i> ABOUT US</a>
            <a class="dropdown-item" href="{{url('/contact')}}"><i class="fa fa-inbox"></i> CONTACT US</a>
            <a class="dropdown-item" href="{{url('/galleries')}}"><i class="fa fa-picture-o"></i> GALLERIES</a>
            <a class="dropdown-item" href="{{route('Careers')}}"><i class="fa fa-briefcase"></i> CAREER</a>
          </div>
        </li>

        @auth
        <li class="nav-item mx-2 dropdown">
          <a href="#" class="nav-link dropdown-toggle" id="accountDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Hi, {{Auth::user()->name}}
          </a>
          <div class="dropdown-menu">
            @if(auth()->user()->roles == 'ADMIN')
            <a href="{{url('/admin')}}" class="dropdown-item">Admin Dashboard</a>
            <div class="dropdown-divider"></div>
            @endif
            @if(auth()->user()->roles == 'SUPERADMIN')
            <a href="{{url('/superadmin')}}" class="dropdown-item">Superadmin Dashboard</a>
            <div class="dropdown-divider"></div>
            @endif
            @if(auth()->user()->roles == 'SALES')
            <a href="{{url('/sales')}}" class="dropdown-item">Sales Dashboard</a>
            <div class="dropdown-divider"></div>
            @endif
            <form action="{{route('logout')}}" method="post">
            @csrf
            <button class="dropdown-item" type="submit">Log Out</button>
            </form>
          </div>
        </li>
        @endauth
      </ul>
      {{-- @guest
      <!-- Mobile button -->
      <form class="form-inline d-sm-block d-xl-none">
        @csrf
        <button class="btn btn-outline-primary btn-signin btn-my-2 mb-2" type="button"
          onclick="event.preventDefault(); location.href='{{route('login')}}';">
        Sign In
        </button>
      </form>
      <form class="form-inline d-sm-block d-xl-none">
        @csrf
        <button class="btn btn-primary btn-signup btn-my-2 my-sm-0" type="button"
          onclick="event.preventDefault(); location.href='{{url('register')}}';">
        Sign Up
        </button>
      </form>
      <!-- Desktop Button -->
      <form class="form-inline my-2 my-lg-0 d-none d-xl-block">
        @csrf
        <button class="btn btn-outline-primary btn-signin my-2 my-sm-0 ml-2 mr-2" type="button"
          onclick="event.preventDefault(); location.href='{{url('login')}}';">
        Sign In
        </button>
      </form>
      <form class="form-inline my-2 my-lg-0 d-none d-xl-block">
        @csrf
        <button class="btn btn-primary btn-signup my-2 my-sm-0 ms-2" type="button"
          onclick="event.preventDefault(); location.href='{{url('register')}}';">
        Sign Up
        </button>
      </form>
      @endguest --}}
    </div>
  </div>
</nav>