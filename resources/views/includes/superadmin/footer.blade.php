            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2020-2021 daysOfftour.com All Rights Reserved</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->