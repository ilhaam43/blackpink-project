        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: #333333" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center mt-2" href="{{ route('Home') }}">
                <img class="w-100" src="{{url('frontend\images\temp\daysoff logo utama.png')}}" alt="daysofftour-logo">
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ Route::is('superadmin.dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('superadmin.dashboard')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('superadmin.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('superadmin.index')}}">
                    <i class="fas fa-fw fa-user-shield"></i>
                    <span>Superadmin</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('admin.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('admin.index')}}">
                    <i class="fas fa-fw fa-user-tie"></i>
                    <span>Admin</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('sales.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('sales.index')}}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Sales</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ Route::is('monitor.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('monitor.index')}}">
                    <i class="fa fa-book"></i>
                    <span>Booking Tour</span></a>
            </li>

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('monitor.confirmation') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('monitor.confirmation')}}">
                    <i class="fas fa-fw fa-desktop"></i>
                    <span>Booking Tour Confirmation</span></a>
            </li>
            
            <!-- Divider -->
            @if(Auth::user()->name == 'Kevin' || Auth::user()->name == 'Tasya' || Auth::user()->name == 'Liliani')
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ Route::is('commition.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('commition.index')}}">
                    <i class="fas fa-fw fa-dollar-sign"></i>
                    <span>Sales Commission Confirmation</span></a>
            </li>
            @endif

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline mt-3">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->
