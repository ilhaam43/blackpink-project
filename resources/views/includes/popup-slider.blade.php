<!-- PopUp -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="myModal" style="display: none;">
    <div class="custom-modal">
        <div class="modal-dialog modal-dialog-centered modal-lg" style="max-width: 570px;">
            <div class="modal-content">
            <div class="xtclose">
                Close
            </div>
                <div id="flyer-carousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block img-fluid" src="{{url('storage/'.$flyerImage1->image_url)}}" alt="PRIVATE TOUR USA">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid" src="{{url('storage/'.$flyerImage2->image_url)}}" alt="QUARANTINE PACKAGE">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid" src="{{url('storage/'.$flyerImage3->image_url)}}" alt="PRIVATE TOUR SWISS">
                        </div>
                    </div>
                        <!-- Controls -->
                    <a class="carousel-control-prev" href="#flyer-carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#flyer-carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>
            </div>
        </div>
    </div>
</div>