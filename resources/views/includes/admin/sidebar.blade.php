        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: #333333" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center mt-2" href="{{ route('Home') }}">
                <img class="w-100" src="{{url('frontend\images\temp\daysoff logo utama.png')}}" alt="daysofftour-logo">
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ Route::is('dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('dashboard')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('travel-package.index') || Route::is('travel-package.create') || Route::is('travel-package.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('travel-package.index')}}">
                    <i class="fas fa-fw fa-suitcase-rolling"></i>
                    <span>Paket Perjalanan</span></a>
            </li>

            <!-- Nav Item - Package  MICE-->
            <li class="nav-item {{ Route::is('travelMice') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('travelMice')}}">
                    <i class="fas fa-fw fa-suitcase-rolling"></i>
                    <span>Paket Perjalanan - MICE</span></a>
            </li>

            <!-- Nav Item - Show Package -->
            <li class="nav-item {{ Route::is('show-travel-package.index') || Route::is('show-travel-package.create') || Route::is('show-travel-package.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('show-travel-package.index')}}">
                    <i class="fas fa-fw fa-eye"></i>
                    <span>Tampilkan Paket Perjalanan</span></a>
            </li>

            <!-- Nav Item - Show Package MICE -->
            <li class="nav-item {{ Route::is('show-travel-package-mice.index') || Route::is('show-travel-package-mice.create') || Route::is('show-travel-package-mice.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('show-travel-package-mice.index')}}">
                    <i class="fas fa-fw fa-eye"></i>
                    <span>Tampilkan Paket Perjalanan MICE</span></a>
            </li>

            <!-- Nav Item - Show Package -->
            <li class="nav-item {{ Route::is('show-tour-events.index') || Route::is('show-tour-events.create') || Route::is('show-tour-events.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('show-tour-events.index')}}">
                    <i class="fas fa-fw fa-eye"></i>
                    <span>Tampilkan Tour & Events</span></a>
            </li>
            
            <!-- Nav Item - Category Destinastion -->
            <li class="nav-item {{ Route::is('destination-category.index') || Route::is('destination-category.create') || Route::is('destination-category.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('destination-category.index')}}">
                    <i class="fas fa-list-alt"></i>
                    <span>Kategori Destinasi</span></a>
            </li>

            <!-- Nav Item - Package Date -->
            <li class="nav-item {{ Route::is('departure-date.index') || Route::is('departure-date.create') || Route::is('departure-date.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('departure-date.index')}}">
                    <i class="fas fa-fw fa-calendar"></i>
                    <span>Tanggal Keberangkatan dan Harga</span></a>
            </li>

            <!-- Nav Item - Itinerarties -->
            <li class="nav-item {{ Route::is('trip-itineraries.index') || Route::is('trip-itineraries.create') || Route::is('trip-itineraries.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('trip-itineraries.index')}}">
                    <i class="fas fa-fw fa-list"></i>
                    <span>Trip Itineraries</span></a>
            </li>

            <!-- Nav Item - Inclusive -->
            <li class="nav-item {{ Route::is('inclusives.index') || Route::is('inclusives.create') || Route::is('inclusives.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('inclusives.index')}}">
                    <i class="fas fa-fw fa-check"></i>
                    <span>Inclusive / Not Inclusive</span></a>
            </li>
            
            <!-- Nav Item - Package Gallery-->
            <li class="nav-item {{ Route::is('gallery.index') || Route::is('gallery.create') || Route::is('gallery.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('gallery.index')}}">
                    <i class="fas fa-fw fa-images"></i>
                    <span>Galeri Travel</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Hotel Karantina -->
            <li class="nav-item {{ Route::is('quarantine-hotel.index') || Route::is('quarantine-hotel.create') || Route::is('quarantine-hotel.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('quarantine-hotel.index')}}">
                    <i class="fas fa-fw fa-hotel"></i>
                    <span>Hotel Karantina</span></a>
            </li>

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('show-quarantine-hotel.index') || Route::is('show-quarantine-hotel.create') || Route::is('show-quarantine-hotel.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('show-quarantine-hotel.index')}}">
                    <i class="fas fa-fw fa-eye"></i>
                    <span>Tampilkan Hotel Karantina</span></a>
            </li>

            <li class="nav-item {{ Route::is('quarantine-hotel-gallery.index') || Route::is('quarantine-hotel-gallery.create') || Route::is('quarantine-hotel-gallery.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('quarantine-hotel-gallery.index')}}">
                    <i class="fas fa-fw fa-images"></i>
                    <span>Hotel Gallery</span>
                </a>
            </li>

            <li class="nav-item {{ Route::is('quarantine-hotel-room.index') || Route::is('quarantine-hotel-room.create') || Route::is('quarantine-hotel-room.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('quarantine-hotel-room.index')}}">
                    <i class="fas fa-fw fa-door-open"></i>
                    <span>Tipe Kamar Hotel Karantina</span>
                </a>
            </li>

            <li class="nav-item {{ Route::is('quarantine-hotel-price.index') || Route::is('quarantine-hotel-price.create') || Route::is('quarantine-hotel-price.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('quarantine-hotel-price.index')}}">
                    <i class="fas fa-fw fa-money-bill-wave"></i>
                    <span>Harga Kamar Hotel Karantina</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <li class="nav-item {{ Route::is('transportation.index') || Route::is('transportation.create') || Route::is('transportation.edit') ? 'active' : '' }}">
                <a href="{{ route('transportation.index') }}" class="nav-link">
                    <i class="fa fa-bus"></i>
                    <span>Transportation</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Booking -->
            <li class="nav-item {{ Route::is('booking.index') || Route::is('booking.create') || Route::is('booking.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('booking.index')}}">
                    <i class="fa fa-book"></i>
                    <span>Booking Tour</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            
            <!-- Nav Item - Instagram -->
            <li class="nav-item {{ Route::is('sections.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('sections.index')}}">
                    <i class="fa fa-list"></i>
                    <span>Section</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            
            <!-- Nav Item - Instagram -->
            <li class="nav-item {{ Route::is('staycation-ig-post.index') || Route::is('staycation-ig-post.create') || Route::is('staycation-ig-post.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('staycation-ig-post.index')}}">
                    <i class="fab fa-instagram"></i>
                    <span>Instagram Post</span></a>
            </li>

            <!-- Nav Item - Flyer Image -->
            <li class="nav-item {{ Route::is('flyer-image.index') || Route::is('flyer-image.create') || Route::is('flyer-image.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('flyer-image.index')}}">
                    <i class="fa fa-images"></i>
                    <span>Flyer Image</span></a>
            </li>

            <!-- Nav Item - Slider Image -->
            <li class="nav-item {{ Route::is('slider-image.index') || Route::is('slider-image.create') || Route::is('slider-image.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('slider-image.index')}}">
                    <i class="fa fa-images"></i>
                    <span>Slider Image</span></a>
            </li>

            <!-- Nav Item - Gallery Tour -->
            <li class="nav-item {{ Route::is('gallery-tour.index') || Route::is('gallery-tour.icreate') || Route::is('gallery-tour.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('gallery-tour.index')}}">
                    <i class="fa fa-images"></i>
                    <span>Gallery Tour</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <li class="nav-item {{ Route::is('career.index') || Route::is('career.create') || Route::is('career.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('career.index')}}">
                    <i class="fa fa-briefcase"></i>
                    <span>Career</span>
                </a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <li class="nav-item {{ Route::is('visa-data.index') || Route::is('visa-data.create') || Route::is('visa-data.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('visa-data.index')}}">
                    <i class="fas fa-database"></i>
                    <span>Visa Data</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Vendor Data -->
            <li class="nav-item {{ Route::is('vendor-data.index') || Route::is('vendor-data.create') || Route::is('vendor-data.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('vendor-data.index')}}">
                    <i class="fas fa-database"></i>
                    <span>Vendor Data</span></a>
            </li>

            <!-- Nav Item - Destination Data -->
            <li class="nav-item {{ Route::is('destination-data.index') || Route::is('destination-data.create') || Route::is('destination-data.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('destination-data.index')}}">
                    <i class="fas fa-database"></i>
                    <span>Destination Data</span></a>
            </li>

            <!-- Nav Item - Type Data -->
            <li class="nav-item {{ Route::is('type-data.index') || Route::is('type-data.create') || Route::is('type-data.edit') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('type-data.index')}}">
                    <i class="fas fa-database"></i>
                    <span>Tour Type Data</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline mt-3">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->
