<!-- Navbar -->
<nav class="navbar my-navbar-booking navbar-light bg-light navbar-expand-lg fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{url('frontend/images/temp/daysoff logo utama.png')}}" height="60px" alt="daysoff-logo-utama">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar1">
            <h3>{{$item->title}}</h3>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-2 mt-2">
                    <form class="navbar-form">
                    @if (($item->departureDates->count()) &&
                        (\Carbon\Carbon::parse($departureDate->departure) >= $now) && 
                        (\Carbon\Carbon::parse($departureDate->arrival) >= $now))
                        
                        <select name="" id="" style="width: 200px;">
                        @foreach($item->departureDates as $departureDate)
                            @if((\Carbon\Carbon::parse($departureDate->departure) >= $now) && 
                                (\Carbon\Carbon::parse($departureDate->arrival) >= $now))
                            <option value="{{$departureDate->id}}">
                                {{\Carbon\Carbon::parse($departureDate->departure)->format('d M Y')}} - 
                                {{\Carbon\Carbon::parse($departureDate->arrival)->format('d M Y')}}
                            </option>
                            @endif
                        @endforeach
                        </select>
                    @endif
                    </form>
                </li>
                <li class="nav-item mx-2 mt-2">
                    @if (($item->departureDates->count()) &&
                        (\Carbon\Carbon::parse($departureDate->departure) >= $now) && 
                        (\Carbon\Carbon::parse($departureDate->arrival) >= $now))
                        <p>IDR {{$item->departureDates->sortBy('price')->first()->price/1000000}} jt/PAX </p>
                    @else
                        <p>Please contact us for price!</p>
                    @endif  
                </li>
                <li class="nav-item mx-2 mt-2">
                    <a href="https://api.whatsapp.com/send?phone=628119728228&text=Hallo daysOff, saya mau booking {{$item->title}} - {{$item->code}}" class="btn btn-lg btn-outline-secondary btn-outline-black">Book Now</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<nav class="navbar navbar-expand bg-dark navbar-dark sticky-top second-nav">
    <div class="container">
        <div class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-2 mr-2">
                    @if($item->tripItinerary == NULL)
                        <a href="#">
                            <img style="width: 45px;" src="{{url('frontend/images/tour-details/pdf-icon.jpg')}}">
                        </a>
                        <a href="#">
                            <img style="width: 50px;" src="{{url('frontend/images/tour-details/mail-icon.png')}}">
                        </a>
                    @else
                        <a target="_blank" href="{{url('storage/'.$item->tripItinerary->pdf_file)}}" download>
                            <img style="width: 45px;" src="{{url('frontend/images/tour-details/pdf-icon.jpg')}}">
                        </a>
                        <a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site http://www.daysofftour.com/details/travel-packages/{{$item->slug}}">
                            <img style="width: 50px;" src="{{url('frontend/images/tour-details/mail-icon.png')}}">
                        </a>
                    @endif
                </li>   
            </ul>
        </div>
    </div>
    
</nav>