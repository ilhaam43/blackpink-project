<script>
    var url = 'https://wati-integration-service.clare.ai/ShopifyWidget/shopifyWidget.js?97254';
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url;
    if(window.location.href.indexOf("details/travel-packages") > -1) {
        var options = {
            "enabled":true,
            "chatButtonSetting":{
                "backgroundColor":"#4dc247",
                "ctaText":"",
                "borderRadius":"25",
                "marginLeft":"50",
                "marginBottom":"50",
                "marginRight":"50",
                "position":"left"
            },
            "brandSetting":{
                "brandName":"daysOffTour",
                "brandSubTitle":"Delivering Your Travel Experience!",
                "brandImg":"{{url('frontend/images/temp/android-chrome-512x512.png')}}",
                "welcomeText":"Hi there!\nHow can I help you?",
                "messageText":"Hallo daysOff, saya mau booking " + {!! json_encode($item->title) !!} + " - " + {!! json_encode($item->code) !!},
                "backgroundColor":"#0a5f54",
                "ctaText":"Start Chat",
                "borderRadius":"25",
                "autoShow":false,
                "phoneNumber":"+628119728228"
            }
        }
    }else{
        var options = {
            "enabled":true,
            "chatButtonSetting":{
                "backgroundColor":"#4dc247",
                "ctaText":"",
                "borderRadius":"25",
                "marginLeft":"50",
                "marginBottom":"50",
                "marginRight":"50",
                "position":"left"
            },
            "brandSetting":{
                "brandName":"daysOffTour",
                "brandSubTitle":"Delivering Your Travel Experience!",
                "brandImg":"{{url('frontend/images/temp/android-chrome-512x512.png')}}",
                "welcomeText":"Hi there!\nHow can I help you?",
                "messageText":"Hallo daysOff, I want to book a quarantine hotel at " + {!! json_encode($item->hotel_name) !!},
                "backgroundColor":"#0a5f54",
                "ctaText":"Start Chat",
                "borderRadius":"25",
                "autoShow":false,
                "phoneNumber":"+628119728228"
            }
        }
    }
    s.onload = function() {
        CreateWhatsappChatWidget(options);
    };
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
</script>