<link rel="icon" type="image/x-icon" href="{{url('frontend/images/temp/favicon-16x16.png')}}">
<link rel="stylesheet" href="{{url('frontend/libraries/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('frontend/styles/main.css')}}?ver={{ filemtime(public_path('frontend/styles/main.css')) }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.2.0/css/ionicons.min.css" integrity="sha256-F3Xeb7IIFr1QsWD113kV2JXaEbjhsfpgrKkwZFGIA4E=" crossorigin="anonymous" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />