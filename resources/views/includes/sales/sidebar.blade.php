        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: #333333" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center mt-2" href="{{ route('Home') }}">
                <img class="w-100" src="{{url('frontend\images\temp\daysoff logo utama.png')}}" alt="daysofftour-logo">
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ Route::is('sales.dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('sales.dashboard')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Package -->
            <li class="nav-item {{ Route::is('bookingtour.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('bookingtour.index')}}">
                    <i class="fas fa-fw fa-book"></i>
                    <span>Booking Tour</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <li class="nav-item {{ Route::is('sales.sales.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('sales.sales.index')}}">
                    <i class="fas fa-fw fa-dollar-sign"></i>
                    <span>Request Commission</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline mt-3">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->
