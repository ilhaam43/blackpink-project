            <!-- Why Us -->
            <section class="section-why-us py-5" id="why-us">
                <div class="container">
                  <div class="section-heading text-center">
                    <h2>Why Us?</h2>
                    <p class="text-muted">The Values We Offer to You</p>
                  </div>
                  <div class="row text-center">
                    <div class="col-md-4 wow bounceInUp">
                      <img class="bounce-card infinite" height=100px src="{{url('frontend/images/home/Values/value-professional.png')}}" alt="Professionalism">
                      <h4 class="my-3">Professionalism</h4>
                      <p class="text-muted">Sesuai visi dari daysOff Tour & Travel, kami fokus untuk membangun hubungan yang profesional dengan rekan bisnis dan client.</p>
                    </div>
                    <div class="col-md-4 wow tada">
                      <img class="bounce-card infinite" height=100px src="{{url('frontend/images/home/Values/value-customer_focused.png')}}" alt="Customer Focused">
                      <h4 class="my-3">Customer Focused</h4>
                      <p class="text-muted">Keinginan pelanggan adalah prioritas utama kami. Untuk memenuhi hal itu, kami menyediakan layanan konsultasi dan paket perjalanan sesuai permintaan.</p>
                    </div>
                    <div class="col-md-4 wow fadeInLeft">
                      <img class="bounce-card infinite" height=100px src="{{url('frontend/images/home/Values/value-affordable.png')}}" alt="Affordable Service">
                      <h4 class="my-3">Affordable Service</h4>
                      <p class="text-muted">Kami percaya bahwa perjalanan liburan adalah kebutuhan semua orang. Maka dari itu kami berupaya menyusun paket yang terjangkau.</p>
                    </div>
                    <div class="col-md-4 wow fadeInRight">
                      <img class="bounce-card infinite" height=100px src="{{url('frontend/images/home/Values/value-reliable.png')}}" alt="Reliable">
                      <h4 class="my-3">Reliable</h4>
                      <p class="text-muted">Kesuksesan liburan Anda menjadi tanggungjawab kami. Tugas kami lah untuk memastikan liburan Anda berjalan sesuai keinginan Anda.</p>
                    </div>
                    <div class="col-md-4 wow fadeInUp">
                      <img class="bounce-card infinite" height=100px src="{{url('frontend/images/home/Values/value-communicative.png')}}" alt="Communicative">
                      <h4 class="my-3">Communicative</h4>
                      <p class="text-muted">Anda memiliki kebutuhan khusus dalam perjalanan atau ingin memodifikasi paket perjalanan Anda? Sampaikan saja dahulu kepada kami.</p>
                    </div>
                    <div class="col-md-4 wow fadeInUp">
                     <img class="bounce-card infinite" height=100px src="{{url('frontend/images/home/Values/value-strong_drive_for_success.png')}}" alt="Excellence">
                     <h4 class="my-3">Excellence</h4>
                     <p class="text-muted">Sejak berdirinya daysOff Tour & Travel, kami selalu berusaha untuk terus mengembangkan produk kami demi kepuasan pelanggan.</p>
                   </div>
                    <!-- PENDING -->
                    <!-- <div class="col-md-4 wow fadeInUp">
                      <img height=150px src="frontend/images/home/Values/value-certified.png" alt="Certified">
                      <h4 class="my-3">Certified</h4>
                      <p class="text-muted">Per tahun 2021, daysOff Tour & Travel resmi berusaha di bawah naungan PT Adiwangsa Karya Internasional.</p>
                    </div> -->
                  </div>
                </div>
              </section>