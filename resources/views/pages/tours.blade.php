@extends('layouts.app')

@section('title')
    daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('frontend/images/tour-offers/Banner\ Header\ Section.jpg')}}')">
        <div class="container">
            <h1 class="mt-5 mb-3">Tour & Events</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Tour & Events</a></li>
                
            </ol>
        </div>
    </section>

    <section class="section-featured">
        <div class="container">
            <div class="featured-destinations row justify-content-center">
            @forelse ($tours as $tour)
                <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                    <a href="{{route('TravelPackagesDetails', $tour->slug)}}"><img loading="lazy" class="card-img-top" src="{{$tour->travelGalleries->count() ? Storage::url($tour->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                    <div class="card-body">
                    <h3 class="card-title">
                        <a href="{{route('TravelPackagesDetails', $tour->slug)}}">{{$tour->title}}</a>
                        <br>{{$tour->duration}}
                    </h3>
                    <h4 class="card-price">
                    @if ($tour->departureDates->count())
                        From IDR {{$tour->departureDates->sortBy('price')->first()->price/1000000}} jt/PAX 
                    @elseif($tour->price)
                        IDR {{ number_format($tour->price/1000000) }} jt/PAX
                    @else
                        Please contact us for price!
                    @endif  
                    </h4>
                    <p class="text-muted">{{Str::words($tour->about, 15, '...')}}</p>
                    </div>
                </div>
                </div>
            @empty
                <p>Paket Perjalanan Belum Tersedia</p>
            @endforelse
            </div>
        </div>
    </section>
@endsection