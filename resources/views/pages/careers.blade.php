@extends('layouts.app')

@section('title')
    Careers | Offers | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('frontend/images/career/Banner\ Header.jpg')}}')">
        <div class="container">
            <h1 class="mt-5 mb-3">Careers</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item active">Careers at daysOffTour</li>
            </ol>
        </div>
    </section>

    <section class="section-featured">
        <div class="container">
            <div class="featured-destinations row justify-content-center">
                @foreach ($careers as $career)
                    <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                        <div class="card pulse-card infinite shadow h-100">
                            <a><img class="card-img-top" src="{{url('storage/'.$career->image)}}" alt="" loading ="lazy"></a>
                            <div class="card-body">
                                <h3 class="card-title">
                                    <a href="{{route('CareersDetails', $career->slug)}}" >{{$career->title}}</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection