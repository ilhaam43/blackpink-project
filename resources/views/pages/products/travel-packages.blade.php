@extends('layouts.app')

@section('title')
  Travel Packages | daysOff Official Website
@endsection

@section('body-class')
  <body class="body-travel-packages">
@endsection

@section('content')
  <!-- Main Content -->
  <main>
    <!-- Page Content -->
    <!-- Page Heading/Breadcrumbs -->
    <section class="section-header" style="background-image: url('{{url('frontend/images/products/Banner\ Header\ Section.jpg')}}')">
      <div class="container">
        <h1 class="mt-5 mb-3">Travel Packages</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('Products')}}">Products</a></li>
          <li class="breadcrumb-item active">Travel Packages</li>
        </ol>
      </div>
    </section>

    {{-- Travel Packages --}}
    <!-- /.container -->
    <section class="section-featured" id="featured-destinations">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Paket Perjalanan</h2>
          <p class="text-muted">Destinasi yang Kami Tawarkan Saat Ini!</p>
        </div>
        <div class="container">
          <div class="featured-destinations row justify-content-center">
            @foreach ($travelPackages as $travelPackage)
              <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                  <a href="{{route('TravelPackagesDetails', $travelPackage->slug)}}"><img loading="lazy" class="card-img-top" src="{{$travelPackage->travelGalleries->count() ? Storage::url($travelPackage->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                  <div class="card-body">
                    <h3 class="card-title">
                      <a href="{{route('TravelPackagesDetails', $travelPackage->slug)}}">{{$travelPackage->title}}</a>
                      <br>{{$travelPackage->duration}}
                    </h3>
                    <h4 class="card-price">
                    @if ($travelPackage->departureDates->count())
                      From IDR {{ number_format($travelPackage->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                    @elseif($travelPackage->price)
                      IDR {{ number_format($travelPackage->price/1000000, 2) }} jt/PAX
                    @else
                      Please contact us for price!
                    @endif  
                    </h4>
                    <p class="text-muted">{{Str::words($travelPackage->about, 15, '...')}}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </section>

      <!-- Featured Destinations -->
    @if ($seasonals->count()!=0)
    <section class="section-featured" id="featured-destinations">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Special Offers</h2>
          <p class="text-muted">Paket Perjalanan Spesial!</p>
        </div>
        <div class="container">
          <div class="featured-destinations row justify-content-center">
            @foreach ($seasonals as $seasonal)
              <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                  <a href="{{route('TravelPackagesDetails', $seasonal->slug)}}"><img loading="lazy" class="card-img-top" src="{{$seasonal->travelGalleries->count() ? Storage::url($seasonal->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                  <div class="card-body">
                    <h3 class="card-title">
                      <a href="{{route('TravelPackagesDetails', $seasonal->slug)}}">{{$seasonal->title}}</a>
                      <br>{{$seasonal->duration}}
                    </h3>
                    <h4 class="card-price">
                    @if ($seasonal->departureDates->count())
                      From IDR {{ number_format($seasonal->departureDates->sortBy('price')->first()->price/1000000,2) }} jt/PAX 
                    @elseif($seasonal->price)
                      IDR {{ number_format($seasonal->price/1000000, 2) }} jt/PAX
                    @else
                      Please contact us for price!
                    @endif  
                    </h4>
                    <p class="text-muted">{{Str::words($seasonal->about, 15, '...')}}</p>
                  </div>
                </div>
              </div>
            @endforeach        
          </div>
        </div>
      </div>
    </section>
    @endif
  </main>
@endsection