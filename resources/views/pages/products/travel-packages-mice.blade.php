@extends('layouts.app')

@section('title')
  Travel Packages - MICE | daysOff Official Website
@endsection

@section('body-class')
  <body class="body-travel-packages">
@endsection

@section('content')
  <!-- Main Content -->
  <main>
    <!-- Page Content -->
    <!-- Page Heading/Breadcrumbs -->
    <section class="section-header" style="background-image: url('{{url('frontend/images/products/Banner\ Header\ Section.jpg')}}')">
      <div class="container">
        <h1 class="mt-5 mb-3">Travel Packages MICE</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('Products')}}">Products</a></li>
          <li class="breadcrumb-item active">Travel Packages MICE</li>
        </ol>
      </div>
    </section>

    {{-- Travel Packages MICE --}}
    <!-- /.container -->
    <section class="section-featured" id="featured-destinations">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Paket Perjalanan MICE</h2>
          <p class="text-muted">Destinasi yang Kami Tawarkan Saat Ini!</p>
        </div>
        <div class="container">
          <div class="featured-destinations row justify-content-center">
            @foreach ($travelMices as $travelMice)
              <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                  <a href="{{route('TravelPackagesDetails', $travelMice->slug)}}">
                      <img loading="lazy" class="card-img-top" src="{{$travelMice->travelGalleries->count() ? Storage::url($travelMice->travelGalleries->shuffle()->first()->image) : ''}}" alt="">
                    </a>
                  <div class="card-body">
                    <h3 class="card-title">
                      <a href="{{route('TravelPackagesDetails', $travelMice->slug)}}">{{$travelMice->title}}</a>
                      <br>{{$travelMice->duration}}
                    </h3>
                    <h4 class="card-price">
                    @if ($travelMice->departureDates->count())
                      From IDR {{ number_format($travelMice->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                    @elseif($travelMice->price)
                      IDR {{ number_format($travelMice->price/1000000, 2) }} jt/PAX
                    @else
                      Please contact us for price!
                    @endif  
                    </h4>
                    <p class="text-muted">{{Str::words($travelMice->about, 15, '...')}}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection