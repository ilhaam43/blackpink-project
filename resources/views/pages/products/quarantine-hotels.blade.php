@extends('layouts.app')

@section('custom-meta')
    <meta name="_token" content="{{ csrf_token() }}">
@endsection

@section('title')
    Quarantine Hotels | daysOff Official Website
@endsection

@section('content')
    <!-- Main Content -->
    <main>
        <!-- Page Content -->
        <!-- Page Heading/Breadcrumbs -->
        <section class="section-header" style="background-image: url('{{url('frontend/images/products/Banner\ Header\ Section.jpg')}}')">
            <div class="container">
            <h1 class="mt-5 mb-3">Quarantine Hotels
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="{{route('Products')}}">Products</a></li>
                <li class="breadcrumb-item active">Quarantine Hotels</li>
            </ol>
            </div>
        </section>
        {{-- Quarantine Hotels --}}
        <!-- /.container -->
        <section class="section-featured" id="featured-destinations">
            <div class="container">
                <div class="section-heading text-center" id="paketPerjalanan">
                    <h2>Quarantine Hotels</h2>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-sm-4">
                        <div class="searchInputWrapper">
                            <input type="text" class="searchInput" placeholder="Search Hotel Name" id="search_hotel" name="search_hotel">
                            <i class="searchInputIcon fa fa-search"></i>
                        </div>
                        
                        <div class="card card-body checkboxInputWrapper mt-2">
                            <p>Filter By City</p>
                            <hr>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="Central Jakarta">
                                <label class="form-check-label" for="hotel_city">Central Jakarta</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="North Jakarta">
                                <label class="form-check-label" for="hotel_city">North Jakarta</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="South Jakarta">
                                <label class="form-check-label" for="hotel_city">South Jakarta</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="East Jakarta">
                                <label class="form-check-label" for="hotel_city">East Jakarta</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="West Jakarta">
                                <label class="form-check-label" for="hotel_city">West Jakarta</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="Tangerang">
                                <label class="form-check-label" for="hotel_city">Tangerang</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="Bekasi">
                                <label class="form-check-label" for="hotel_city">Bekasi</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="Bali">
                                <label class="form-check-label" for="hotel_city">Bali</label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="hotel_city" value="Lainnya">
                                <label class="form-check-label" for="hotel_city">Lainnya</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <!-- <div class="card card-body ml-auto">
                            <select class="custom-select" id="sortingPrice">
                                <option disabled selected>Sort By</option>
                                <option value="sort-asc">Price (Lowest - Highest)</option>
                                <option value="sort-desc">Price (Highest - Lowest)</option>
                            </select>
                        </div> -->
                        @foreach($quarantineHotels as $hotel)
                        <div class="card card-body mt-2l">
                            <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                @if(count($hotel->hotelGalleries) > 0)
                                <div class="col-sm-4"> 
                                    <div class="card pulse-card infinite shadow h-100">
                                        <img loading="lazy" class="card-img-top hotel-thumbnail" src="{{$hotel->hotelGalleries->count() ? Storage::url($hotel->hotelGalleries->shuffle()->first()->image) : ''}}" alt="">
                                    </div>
                                </div>
                                @endif
                                <div class="media-body">
                                    <h6 class="card-title"> <a href="{{route('QuarantineHotelsDetails', $hotel->slug)}}" data-abc="true">{{$hotel->hotel_name}}</a> </h6>
                                    <p class="text-muted"><i class="fa fa-map-marker"></i> {{$hotel->hotel_address}}, {{$hotel->hotel_city}}</p>
                                    <div class="col-sm-4 chse-box">
                                        <p>CHSE</p>
                                        <img src="{{url('frontend/images/quarantinehotel-details/CHSE-Ceritificate.png')}}" style="width: 100%;">
                                        <p>{{$hotel->chse_score}}</p>
                                    </div>
                                </div>
                                <div class="mt-3 mt-lg-0 ml-lg-3 text-center">
                                    <p>Start From</p>
                                    <h4 class="card-price">
                                                @if ($hotel->hotelPrices->count())
                                    From IDR {{$hotel->hotelPrices->sortBy('room_price')->whereNotIn('room_id', [10])->first()->room_price/1000000}} jt/Person
                                    @else
                                    Please contact us for price!
                                    @endif   
                                    </h4>
                                    <div class="sold_stars ml-auto"> 
                                        @for($i=0; $i < 5; $i++)
                                            @if($i<$hotel->hotel_rate)
                                                <i class="fa fa-star"></i>
                                            @endif
                                        @endfor
                                    </div>
                                    <a href="{{route('QuarantineHotelsDetails', $hotel->slug)}}" class="btn btn-lg btn-outline-primary btn-outline-black" style="color: black; margin-top:30px;">Details</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        {{-- /.Products List Section --}}
    </main>
@endsection

@push('custom-scripts')

    <script type="text/javascript">
        $('#search_hotel').on('keyup',function(){
            hotel_cities = [];
            value = $(this).val();

            $('input[name="hotel_city"]:checked').each(function(i){
                hotel_cities[i] = $(this).val();
            });
            
            $.ajax({
                type : 'get',
                url : "{{route('QuarantineHotelSearch')}}",
                data:{'search_hotel':value, 'hotel_city':hotel_cities},
                success:function(data){
                    $('.col-sm-8').html(data);
                }
            });
        });

        $('input[name="hotel_city"]').on('change', function(){
            var hotel_cities = [];
            search_hotel = $('#search_hotel').val();

            $('input[name="hotel_city"]:checked').each(function(){
                hotel_cities.push($(this).val());
            });

            $.ajax({
                type : 'get',
                url : "{{route('QuarantineHotelSearch')}}",
                data:{hotel_city: hotel_cities, 'search_hotel':search_hotel},
                success:function(data){
                    $('.col-sm-8').html(data);
                }
            });
        });

        // $('#sortingPrice').on('change', function(){
        //     sorting_price = $('#sortingPrice option:selected').val();
        //     $.ajax({
        //         type: 'get',
        //         url : "{{route('QuarantineHotelSorting')}}",
        //         data:  {sorting_price},
        //         success:function(data){
        //             $('.col-sm-8').html(data);
        //         }
        //     });
        // });
    </script>

    <script type="text/javascript">
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>

@endpush