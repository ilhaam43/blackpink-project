@extends('layouts.app')

@section('title')
    Products | daysOff Official Website
@endsection

@section('body-class')
    <body class="body-staycation">
@endsection

@section('content')
        <!-- Main Content -->
        <main>
            <!-- Page Content -->
            <!-- Page Heading/Breadcrumbs -->
            <section class="section-header" style="background-image: url('{{url('frontend/images/products/Banner\ Header\ Section.jpg')}}')">
              <div class="container">
                <h1 class="mt-5 mb-3">Products
                </h1>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="{{url('/')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Products</li>
                </ol>
              </div>
            </section>
            {{-- Products section if Exists --}}
            <section class="section-products">
                <div class="container">
                  <div class="row">
                    @if ($travelPackages)
                      <div class="col-md-6 mb-3">
                          <div class="card h-100 shadow-lg">
                            <a href="{{route('TravelPackagesPage')}}"><img class="card-img-top" src="{{url('frontend\images\products\Products-Travel-Packages.jpg')}}" loading="lazy" alt=""></a>
                            <div class="card-body">
                              <h4 class="card-title">
                                <a href="{{route('TravelPackagesPage')}}" class="text-color-gold" >Travel Packages</a>
                              </h4>
                              <h4>{{$travelPackages}} paket tersedia</h4>
                              <p class="card-text">Booking paket perjalanan bersamma daysOff Tour & Travel, buat perjalanan Anda jadi mudah dan sesuai dengan keinginan Anda!</p>
                            </div>
                          </div>
                      </div>
                    @endif
                    @if ($staycationIgposts)
                      <div class="col-md-6 mb-3">
                          <div class="card h-100 shadow-lg">
                            <a href="{{route('StaycationsPage')}}"><img class="card-img-top" src="{{url('frontend\images\products\Products-staycations.jpg')}}" loading="lazy" alt=""></a>
                            <div class="card-body">
                              <h4 class="card-title">
                                <a class="text-color-gold" href="{{route('StaycationsPage')}}">Staycations</a>
                              </h4>
                              <h4>{{$staycationIgposts}} paket tersedia</h4>
                              <p class="card-text">Ingin bersantai di Hotel dan Resort pilihan di Indonesia? Booking bersama kami dan dapatkan penawaran paling menarik!</p>
                            </div>
                          </div>
                      </div>
                    @endif
                    @if ($travelPackages == 0 && $staycationIgposts == 0)
                    <h2 class="mt-5 text-center">
                        Kami sedang menyusun paket travel terbaik untuk Anda... STAY TUNED!
                    </h2>    
                    @endif
                  </div>
                </div>
            </section>
            <!-- /.container -->
          </main>
@endsection