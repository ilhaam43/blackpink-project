@extends('layouts.superadmin')

@section('title')
    <title>List Superadmin | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Superadmin</h1>
    <a href="{{route('superadmin.create')}}" class="btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-plus fa-sm text-white-50"></i> Add New Superadmin
    </a>
</div>

@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
@if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
@endif


<div class="row">
    <div class="card-body">
        <div class="table-responsive">
            <table id="myTable" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($superadmin as $superadmins)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$superadmins->name}}</td>
                        <td>{{$superadmins->email}}</td>
                        <td>{{$superadmins->status}}</td>
                        <td>
                            <a href="{{route('superadmin.edit', $superadmins->id)}}" class="btn btn-info" title="edit">
                                <i class="fa fa-pencil-alt"></i>
                            </a>
                            <form action="{{route('superadmin.destroy', $superadmins->id)}}" method="post" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" title="delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->
@endsection