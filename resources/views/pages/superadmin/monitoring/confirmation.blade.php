@extends('layouts.superadmin')

@section('title')
    <title>Monitoring Booking | daysOff Official Website</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
        <a class="btn btn-sm btn-secondary shadow-sm" data-toggle="modal" data-target="#myModal"><i class="far fa-question-circle"></i> Status Details</a><br><br>
            <h1 class="h3 mb-0 text-gray-800">Waiting For Your Approval</h1>
            <table id="AcceptTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tour Package</th>
                        <th>Invoice Code</th>
                        <th>Customer Name</th>
                        <th>Total Passenger</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($approves as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tourPackages->title}}</td>
                            <td>{{$item->invoice_code}}</td>
                            @foreach($item->passengerContactPersons as $pic)
                                <td>{{$pic->first_name}} {{$pic->last_name}}</td>
                            @endforeach
                            <td>{{$item->passengerLists->count()}}</td>
                            <td class="text-center">
                                <form action="{{route('monitor.approve', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-outline-success btn-sm">
                                        <i class="fa fa-check"></i> Accept
                                    </button>
                                </form><br>
                                <a href="{{route('monitor.show', $item->id)}}"><button type="button" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> Detail</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

<div class="container-fluid">
    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <h1 class="h3 mb-0 text-gray-800">Invoice To Customer</h1>
            <table id="InvoiceTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tour Package</th>
                        <th>Invoice Code</th>
                        <th>Customer Name</th>
                        <th>Total Passenger</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invoiceSent as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tourPackages->title}}</td>
                            <td>{{$item->invoice_code}}</td>
                            @foreach($item->passengerContactPersons as $pic)
                                <td>{{$pic->first_name}} {{$pic->last_name}}</td>
                            @endforeach
                            <td>{{$item->passengerLists->count()}}</td>
                            <td class="text-center">
                                <form action="{{route('monitor.invoiceSent', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-outline-success btn-sm">
                                        <i class="fa fa-check"></i> Sent
                                    </button>
                                </form><br>
                                <a href="{{route('monitor.show', $item->id)}}"><button type="button" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> Detail</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <h1 class="h3 mb-0 text-gray-800">In Process</h1>
            <table id="PaymentWaitTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tour Package</th>
                        <th>Invoice Code</th>
                        <th>Customer Name</th>
                        <th>Total Passenger</th>
                        <th>Option</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tourPackages->title}}</td>
                            <td>{{$item->invoice_code}}</td>
                            @foreach($item->passengerContactPersons as $pic)
                                <td>{{$pic->first_name}} {{$pic->last_name}}</td>
                            @endforeach
                            <td>{{$item->passengerLists->count()}}</td>
                            <td class="text-center">
                                <form method="post" action="{{route('monitor.refund', $item->id)}}">
                                    @csrf
                                    @method('PUT')
                                    <select class="form-control select2" name="status" onchange='this.form.submit()'>
                                        @foreach($statusInProcess as $statusProcess)
                                            <option value="{{$statusProcess->id}}">{{$statusProcess->name}}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </td>
                            <td class="text-center">
                                <a href="{{route('monitor.show', $item->id)}}"><button type="button" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> Detail</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

<div class="container-fluid">
    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <h1 class="h3 mb-0 text-gray-800">Complete</h1>
            <table id="CompletedTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tour Package</th>
                        <th>Invoice Code</th>
                        <th>Customer Name</th>
                        <th>Total Passenger</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($completes as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tourPackages->title}}</td>
                            <td>{{$item->invoice_code}}</td>
                            @foreach($item->passengerContactPersons as $pic)
                                <td>{{$pic->first_name}} {{$pic->last_name}}</td>
                            @endforeach
                            <td>{{$item->passengerLists->count()}}</td>
                            <td class="text-center">
                                <form action="{{route('monitor.completed', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-outline-success btn-sm">
                                        <i class="fa fa-check"></i> Completed
                                    </button>
                                </form><br>
                                <a href="{{route('monitor.show', $item->id)}}"><button type="button" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> Detail</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

<div class="container-fluid">
    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <h1 class="h3 mb-0 text-gray-800">On Hold</h1>
            <table id="CancelledTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tour Package</th>
                        <th>Invoice Code</th>
                        <th>Customer Name</th>
                        <th>Total Passenger</th>
                        <th>Option</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cancelled as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tourPackages->title}}</td>
                            <td>{{$item->invoice_code}}</td>
                            @foreach($item->passengerContactPersons as $pic)
                                <td>{{$pic->first_name}} {{$pic->last_name}}</td>
                            @endforeach
                            <td>{{$item->passengerLists->count()}}</td>
                            <td class="text-center">
                                <form method="post" action="{{route('monitor.refund', $item->id)}}">
                                    @csrf
                                    @method('PUT')
                                    <select class="form-control select2" name="status" onchange='this.form.submit()'>
                                        @foreach($statusRefund as $status)
                                            <option value="{{$status->id}}">{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </td>
                            <td><a href="{{route('monitor.show', $item->id)}}"><button type="button" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> Detail</button></a></td>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Status Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>    
                        @foreach($allStatus as $stat)
                        <tr>
                            <td>{{$stat->name}}</td>
                            <td>{{$stat->description}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('custom-script')
    <!-- DataTable -->
    <script>
        $(document).ready( function () {
            $('#AcceptTable, #InvoiceTable, #PaymentWaitTable, #CompletedTable, #CancelledTable').DataTable({
                responsive: true,
                "scrollX": true
            });
        });
    </script>
@endpush