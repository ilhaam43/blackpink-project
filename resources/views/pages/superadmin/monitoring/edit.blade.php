@extends('layouts.superadmin')

@section('title')
    <title>Booking Tour | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container body-booking mt-5 mb-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-10">
                <form id="packageBooking" action="{{route('monitor.update', $item->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <h1 id="reservation">Booking Form</h1>

                    <div>
                        <!-- Contact Person -->
                        <div>
                            <h4 class="text-center">Tour Package</h4>
                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>Tour Package</h6>
                                    <input type="text" disabled value="{{$item->tourPackages->title}}">
                                </div>
                                <div class="col-sm-6 mt-4">
                                    <h6>Booking Invoice Code</h6>
                                    <input type="text" disabled value="{{$item->invoice_code}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>Down Payment</h6>
                                    <input type="text" id="down_payment" name="down_payment" placeholder="IDR" value="{{$item->down_payment}}" required>
                                </div>
                            </div>
                            
                            @foreach($item->passengerContactPersons as $pic)
                            <h4 class="text-center mt-4">Contact Person Details</h4>
                            <div class="row">
                                <div class="col-sm-3 mt-4">
                                    <h6>Title</h6>
                                    <input type="text" disabled value="{{$pic->title}}">
                                </div>
                                <div class="col-sm-3 mt-4">
                                    <h6>Gender</h6>
                                    <input type="text" disabled value="{{$pic->gender}}">
                                </div>
                                <div class="col-sm-3 mt-4">
                                    <h6>First name</h6>
                                    <input type="text" placeholder="Enter First Name" disabled value="{{$pic->first_name}}">
                                </div>
                                <div class="col-sm-3 mt-4">
                                    <h6>Last name</h6>
                                    <input type="text" placeholder="Enter Last Name" value="{{$pic->last_name}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>E-mail</h6>
                                    <input type="email" disabled value="{{$pic->email}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>Phone Number</h6>
                                    <input type="tel" disabled value="{{$pic->phone_number}}">
                                </div>

                                <div class="col-sm-6 mt-4">
                                    <h6>Address</h6>
                                    <input type="text" name="pic_address" disabled value="{{$pic->address}}">
                                </div>
                            </div>
                            @endforeach

                        </div>
                        <button type="submit" class="btn btn-secondary btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('custom-script')
<script>
    $(document).on('keyup', '#down_payment', function(event) {
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });
</script>
@endpush