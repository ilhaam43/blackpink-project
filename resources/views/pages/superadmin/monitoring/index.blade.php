@extends('layouts.superadmin')

@section('title')
    <title>Booking Tour | daysOff Official Website</title>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Booking Tour</h1>
    </div>

    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <a class="btn btn-sm btn-secondary shadow-sm" data-toggle="modal" data-target="#myModal"><i class="far fa-question-circle"></i> Status Details</a>
            </br></br>  
            <table id="myTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No.</th>
                        <th>Tour Package</th>
                        <th>Invoice Code</th>
                        <th>Customer Name</th>
                        <th>Customer Phone</th>
                        <th>Total Passenger</th>
                        <th>Status</th>
                        <th>Submitted By</th>
                        <th>Date Submitted</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                        <td class="text-center">
                                @if($item->status_id == 6 && (\Carbon\Carbon::parse($item->created_at) < \Carbon\Carbon::now()->subDays(13)))
                                <button class="btn btn-outline-danger" id="reminder-button" data-toggle="tooltip" title="Payment Has Not Completed Within 14 Days">
                                    <i class="fa fa-bullhorn" style="color: red;" aria-hidden="true"></i>
                                </button>
                                @endif
                            </td>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->tourPackages->title}}</td>
                            <td>{{$item->invoice_code}}</td>
                            @foreach($item->passengerContactPersons as $pic)
                                <td>{{$pic->first_name}} {{$pic->last_name}}</td>
                                <td>{{$pic->phone_number}}</td>
                            @endforeach
                            <td>{{$item->passengerLists->count()}}</td>
                            <td>
                            @if($item->status_id == 6 || $item->status_id == 7 || $item->status_id == 8 || $item->status_id == 10 || $item->status_id == 11 || $item->status_id == 9 || $item->status_id == 12)
                                <form method="post" action="{{route('monitor.update.status', $item->id)}}">
                                    @csrf
                                    @method('PUT')
                                    <select name="status" class="form-control select2" onchange='this.form.submit()'>
                                    @foreach($updateStatus as $updatesStat)
                                        <option id="{{$updatesStat->id}}" value="{{$updatesStat->id}}"  {{ ( $item->status_id == $updatesStat->id) ? 'selected' : '' }}>{{$updatesStat->name}}</option>
                                    @endforeach
                                    </select>
                                </form>
                                @else
                                    @if($item->status_id <= 3)
                                    <span class="badge badge-info badge-sm">{{$item->bookingStatus->name}}</span>
                                    @elseif($item->status_id == 4 || $item->status_id == 9)
                                    <span class="badge badge-success badge-sm">{{$item->bookingStatus->name}}</span>
                                    @elseif($item->status_id == 12)
                                    <span class="badge badge-success badge-sm">{{$item->bookingStatus->name}}</span><br> 
                                    @elseif($item->status_id >= 5 && $item->status_id <= 8 || $item->status_id == 10)
                                    <span class="badge badge-warning badge-sm">{{$item->bookingStatus->name}}</span>
                                    @elseif($item->status_id == 11)
                                    <span class="badge badge-danger badge-sm">{{$item->bookingStatus->name}}</span>
                                    @else
                                    <span class="badge badge-secondary badge-sm">{{$item->bookingStatus->name}}</span>
                                    @endif
                                @endif
                            </td>
                            <td>{{$item->users->name}}</td>
                            <td>{{$item->created_at}}</td>
                            <td class="text-center">
                                <a href="{{route('monitor.show', $item->id)}}"><button type="button" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> Detail</button></a><br>
                                <a href="{{route('monitor.edit', $item->id)}}"><button type="button" class="btn btn-outline-success btn-sm"><i class="fas fa-edit"></i> Edit</button><br>
                                @if($item->status_id >= 2)
                                <a href="{{route('exportExcel', $item->id)}}"><button type="button" class="btn btn-outline-success btn-sm"><i class="fas fa-file-excel"></i> Excel</button></a><br>
                                <a target="_blank" href="{{route('exportInvoice', $item->id)}}"><button type="button" id="inv-btn" class="btn btn-outline-secondary btn-sm"><i class="fas fa-file-invoice"></i> Invoice</button></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Status Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>    
                            @foreach($allStatus as $stat)
                            <tr>
                                <td>{{$stat->name}}</td>
                                <td>{{$stat->description}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection