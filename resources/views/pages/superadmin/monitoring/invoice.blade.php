<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Booking Tour INV#{{$item->invoice_code}} {{$item->tourPackages->title}}</title>

        <style type="text/css">
            * {
                font-family: Verdana, Arial, sans-serif;
            }
            table{
                font-size: x-small;
            }
            tfoot tr td{
                font-weight: bold;
                font-size: x-small;
            }
            th{
                font-size: 14px;
                color: #272791;
            }
            .headerCol{
                background-color: #272791;
                width: 100%;
                height: 20px;
            }
            p{
                line-height: 5px;
            }
            h1,h2,h3,h4,h5,h6{
                color: #272791;
            }
            .description tr:nth-child(even) {
                background-color: #f2f2f2;
            }
        </style>
    </head>

    <body>
        <div class="headerCol"></div>
        <table width="100%">
            <tr>
                <td valign="top"><img src="{{ public_path('frontend/images/temp/daysoff-logo.jpg')}}" width="200"></td>
            </tr>
            <p>(021) 29601494</p>
            <p>13220 DKI Jakarta</p>
            <p>Rawamangun, Kec. Pulo Gadung, Kota Jakarta Timur</p>
            <p>Jl. Perserikatan No. 1, RT.002/RW.008</p>
            <p>vOffice East</p>
        </table>

        <h1>Invoice</h1>
        <h4 style="line-height:2px;">Submitted on: {{(\Carbon\Carbon::parse($item->created_at)->setTimezone('Asia/Jakarta')->format('d/m/Y'))}}</h4>

        <table width="100%">
            <tr style="font-weight: bold;">    
                <td>Invoice For</td>
                <td>Invoice #</td>
            </tr>
            <tr> 
                @foreach($item->passengerContactPersons as $pic)
                <td>{{$pic->title}} {{$pic->first_name}} {{$pic->last_name}}</td>
                @endforeach
                <td>{{$item->invoice_code}}</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td></td>
                <td style="font-weight: bold;">Due Date</td>
            </tr>
            <tr>
                <td></td>
                <td>{{(\Carbon\Carbon::parse($item->created_at)->setTimezone('Asia/Jakarta')->addDay()->format('d/m/Y'))}}</td>
            </tr>
        </table><br>

        <table width="100%" class="description">
            <thead style="border-top: 1 px solid grey;">
                <tr>
                    <th>Description</th>
                    <th></th>
                </tr>
            </thead>
            <tbody style="border-bottom: 1 px solid grey;">
                <tr><td colspan="2">{{$item->tourPackages->code}} {{$item->tourPackages->title}}</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">Includes:</td></tr>
                @if($item->tourPackages->inclusive)
                    @for($i = 0; $i <= 19; $i++)
                        @if(is_null($item->tourPackages->inclusive->{'incl'.$i}))
                            @continue
                        @endif
                        <tr><td colspan="2">{{ $item->tourPackages->inclusive->{'incl'.$i} }}</td></tr>
                    @endfor
                @endif
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">Booking Details:</td></tr>
                <tr><td colspan="2">{{ $item->booking_details ?? '' }}</td></tr>
                <tr>
                    <th>Guest</th>
                    <th style="width: 20%;">Price</th>
                </tr>
                @foreach($item->passengerLists as $passenger)
                <tr>
                    <td>{{$loop->iteration}}. {{$passenger->title}} {{$passenger->first_name}} {{$passenger->last_name}}</td>
                    
                    <td align="center">Rp{{number_format($passenger->price)}}</td>
                </tr>
                @endforeach
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <th align="right">Total Price</th>
                    <td align="center">Rp{{number_format($item->passengerLists->sum('price'))}}</td>
                </tr>
            </tbody>
        </table>

        <br><p style="font-size:12px; color: grey">Notes:</p>
        <p style="font-size: 13px;">Payment via bank transfer available, addressed to:</p>
        <p style="font-size: 13px;">Bank Name : Bank Central Asia</p>
        <p style="font-size: 13px;">Bank Code : 014</p>
        <p style="font-size: 13px;">Account Number : 0948608858</p>
        <p style="font-size: 13px;">Account Name : PT. Adiwangsa Kertayasa Internasional </p>
        <p style="font-size: 13px;">SWIFT Code : CENAIDJA </p>
        <p style="font-size: 13px;">Branch Code : 0094 </p>
        @if($item->down_payment !=NULL)
        <p style="font-size: 13px;">Kirimkan DP Sebesar: <strong>Rp{{number_format($item->down_payment)}}</strong></p>
        @endif
        
        <h3 align="right" style="color: fuchsia;">Rp{{number_format($item->passengerLists->sum('price'))}}</h3>

        <footer>
            <h5 align="center">www.daysofftour.com</h5>
        </footer>
    </body>
</html>