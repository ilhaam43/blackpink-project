@extends('layouts.superadmin')

@section('title')
    <title>Edit Sales | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Sales</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('sales.update', $sales->id)}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="sales_name">Sales Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Sales Name" value="{{$sales->name}}">
                    </div>
                    <div class="col-sm-4">
                        <label for="sales_email">Sales Email</label>
                        <input type="text" class="form-control" name="email" placeholder="Sales Email" value="{{$sales->email}}">
                    </div>
                    <div class="col-sm-4">
                    <label for="sales_status">Sales Status</label>
                    <select name="status" required class="form-control">
                        <option value="Active" @if ($sales->status == 'Active') selected @endif>Active</option>
                        <option value="Inactive" @if ($sales->status == 'Inactive') selected @endif>Inactive</option>
                    </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="category_name">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        <input type="checkbox" onclick="showPassword()"> Show Password
                    </div>
                    <div class="col-sm-6">
                        <label for="category_name">Confirm Password</label>
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
                        <input type="checkbox" onclick="showConfirmPassword()"> Show Confirm Password
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
<script>
    function showPassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
<script>
    function showConfirmPassword() {
        var x = document.getElementById("confirm_password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>