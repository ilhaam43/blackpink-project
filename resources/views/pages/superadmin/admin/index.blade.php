@extends('layouts.superadmin')

@section('title')
    <title>List Admin | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Admin</h1>
    <a href="{{route('admin.create')}}" class="btn btn-sm btn-primary shadow-sm">
        <i class="fas fa-plus fa-sm text-white-50"></i> Add New Admin
    </a>
</div>

@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
@if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
@endif


<div class="row">
    <div class="card-body">
        <div class="table-responsive">
            <table id="myTable" class="table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($admin as $admins)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$admins->name}}</td>
                        <td>{{$admins->email}}</td>
                        <td>{{$admins->status}}</td>
                        <td>
                            <a href="{{route('admin.edit', $admins->id)}}" class="btn btn-info" title="edit">
                                <i class="fa fa-pencil-alt"></i>
                            </a>
                            <form action="{{route('admin.destroy', $admins->id)}}" method="post" class="d-inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" title="delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="14" class="text-center">Belum Ada Data Admin</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->
@endsection