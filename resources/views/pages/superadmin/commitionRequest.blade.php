@extends('layouts.superadmin')


@section('title')
    <title>Commission Request | daysOff Official Website</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <h1 class="h3 mb-0 text-gray-800">Sales Commission Request</h1>
            <table id="myTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Sales Name</th>
                        <th>Tour Invoice</th>
                        <th>Submitted On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        @if(Auth::user()->id == 10 && $item->status_id == 1)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->users->name}}</td>
                            <td>{{$item->bookingTours->invoice_code}}</td>
                            <td>{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                            <td>
                                <form action="{{route('commition.approved1', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-outline-success btn-sm">
                                        <i class="fa fa-check"></i> Approve Request
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @elseif(Auth::user()->id == 9 && $item->status_id == 2)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->users->name}}</td>
                            <td>{{$item->bookingTours->invoice_code}}</td>
                            <td>{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                            <td>
                                <form action="{{route('commition.approved2', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-outline-success btn-sm">
                                        <i class="fa fa-check"></i> Approve Request
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @elseif(Auth::user()->id == 8 && $item->status_id == 3)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->users->name}}</td>
                            <td>{{$item->bookingTours->invoice_code}}</td>
                            <td>{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                            <td>
                                <form action="{{route('commition.approved3', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-outline-success btn-sm">
                                        <i class="fa fa-check"></i> Approve Request
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection