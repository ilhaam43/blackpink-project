@extends('layouts.app')

@section('title')
    Galleries | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content') 
    <section class="section-header" style="background-image: url('frontend/images/galleries/Banner\ Header\ Section.jpg');">
        <div class="container">
            <h1 class="mt-5 mb-3">Galleries
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item active">Galleries</li>
            </ol>
        </div>
    </section>

    <section class="section-featured" id="featured-destinations">
        <div class="container">
            <div class="section-heading text-center" id="galleries">
                <h2>Galleries</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 mx-auto">
                <div class="card main-card card-details shadow mb-5 py-4">
                    <div class="container">
                        @foreach($gallery->chunk(4) as $item)
                            <div class="row">
                                @foreach($item as $row)
                                    <div class="col-sm-3">
                                        <img src="{{ Storage::url($row->image) }}" alt="{{ $row->title }}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                                    </div>
                                @endforeach
                            </div>
                            <br>
                        @endforeach
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (1).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (2).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (3).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (4).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (5).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (6).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (8).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (9).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (10).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (11).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (12).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (13).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (14).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (15).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (16).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (17).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (18).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (19).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (20).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (21).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (22).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (23).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (24).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (25).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (26).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (27).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (28).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (29).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (30).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (31).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (32).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (33).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                        </br>
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (34).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (35).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (36).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                            <div class="col-sm-3">
                                <img src="{{url('frontend\images\gallery-tour\1 (5).jpg')}}" style="object-fit:cover; height:300px; width:100%" class="rounded mx-auto d-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection