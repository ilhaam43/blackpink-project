@extends('layouts.app')

@section('title')
    Tentang Kami | daysOff Official Website
@endsection

@section('body-class')
    <body class="body-about-us">
@endsection

@section('content')
        <!-- Main Content -->
        <main>
            <!-- Page Content -->
            <section class="section-header" style="background-image: url('frontend/images/about-us/Banner\ Header\ Section.jpg');">
              <div class="container">
                <h1 class="mt-5 mb-3 text-color-white">About Us
                </h1>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a class="text-color-gold" href="{{url('/')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">About</li>
                </ol>
              </div>
            </section>
            <!-- Page Heading/Breadcrumbs -->
            <!-- Intro Content -->
            <section class="section-intro">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6">
                    <img class="img-fluid rounded mb-4" src="frontend/images/about-us/about-us-intro -2.jpg" alt="">
                  </div>
                  <div class="col-lg-6">
                    <h2>Behind Story of 'daysOff'</h2>
                    <p class="justified-content">“daysOff” nama yang kerap katanya sulit untuk disebut ini sering mendapatkan
                        pertanyaan apa sih artinya? Memilih kata yang artinya adalah hari libur/cuti/
                        kosong bertujuan untuk selalu mengingatkan seluruh kalangan untuk tidak lupa
                        menyisihkan hari libur, di tengah keseharian yang padat. Tentunya banyak
                        kegiatan yang dipilih untuk mengisi hari libur, namun tidak semua orang memilih
                        mengisi waktunya untuk berpergian dan menghabiskan waktu di luar rumah.
                        Padahal dengan berlibur Anda dapat mengurangi beban pikiran, menambah
                        wawasan dan pastinya bertemu dengan hal-hal yang baru. Berlibur juga di
                        percayai dapat mengurangi Stress, meningkatkan kualitas tidur dan suasana hati.
                        Anda juga dapat mempererat hubungan Anda dengan keluarga atau kerabat.</p>
                      <p class="justified-content">daysOff tour & travel bergerak di bidang penyedia jasa pariwisata yang menyediakan
                        seluruh kebutuhan berlibur maupun rencana hiburan tanpa harus berpergian.
                        Berawal dari hobby berpergian atau traveling, tentunya sangat memotivasi para
                        pendirinya untuk berbagi pengalaman mereka sendiri selama di tinggal di negara
                        eropa, Jerman, sehingga menjadi suatu passion dalam menjalankan usaha di bidang
                        pariwisata. ”liburan adalah sebuah kebutuhan, bukan hanya tambahan”.</p>
                    <p class="justified-content">Berawal dari hobby berpergian atau traveling, tentunya sangat memotivasi para
                        pendirinya untuk berbagi pengalaman mereka sendiri selama di tinggal di negara
                        eropa, Jerman, sehingga menjadi suatu passion dalam menjalankan usaha di bidang
                        pariwisata. ”liburan adalah sebuah kebutuhan, bukan hanya tambahan”.</p>
                    
                    <h3>Most Important Value</h3>
                    <h4>Visi</h4>
                    <p class="justified-content">Menjadi perusahaan dibidang biro
                      perjalanan wisata yang terpercaya,
                      serta berupaya membangun hubungan
                      yang profesional antar rekan bisnis dan
                      client dalam bidang tour & travel.
                    </p>
                    <h4>Misi</h4>
                    <p class="justified-content">Memberikan pelayanan yang sangat
                      cepat dan memberikan informasi
                      akurat kepada pelanggan serta
                      membuat perjalanan wisata yang
                      unik dan menarik.
                    </p>
                    <h3>Our Unique Superiorities</h3>
                    <ul>
                      <li>High Priority Services</li>
                      <li>Majestic Hidden Gems</li>
                      <li>Comprehensive, Yet Relaxing Tours</li>
                      <li>Following the Trend & Improve!</li>
                      <li>High Quality Services with Affordable Values</li>
                      <li>Safety Travel - “Global Protocols”</li>
                    </ul>
                  </div>
                </div>
                <!-- /.row -->
              </div>
            </section>

            @include('includes.sections.why-us')

            <!-- Fancy Blockquote -->
            <section class="section-fancy-blockquote">
               <div class="container">
                  <div class="row">
                     <div class="col text-center">
                        <h2>Today's Wisdom</h2>
                     </div>
                  </div>
                  <div class="row mt-4">
                     <div class="col-lg-6 mx-auto">
                         <blockquote class="blockquote blockquote-custom bg-white p-5 shadow rounded">
                             <p class="mb-0 mt-2 font-italic">"IN THE END... We only regret the chances we did not take,
                                the relationships we were afraid to have, and the decisions we waited too long to make."</p>
                             <footer class="blockquote-footer pt-4 mt-4 border-top">Lewis Carroll
                             </footer>
                         </blockquote>
                     </div>
                 </div>
               </div>
            </section>
            <!-- /.container -->
          </main>
@endsection