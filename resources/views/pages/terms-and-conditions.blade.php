@extends('layouts.app')

@section('title')
    Terms and Conditions | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('frontend/images/terms-and-conditions/Banner\ Header\ Section.jpg');">
        <div class="container">
            <h1 class="mt-5 mb-3">Terms and Conditions
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item active">Terms and Conditions</li>
            </ol>
        </div>
    </section>

    <!-- General -->
    <section class="section-featured" id="featured-destinations">
        <div class="container">
            <div class="section-heading text-center" id="termsandconditions">
                <h2>Terms and Conditions</h2>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-10 mx-auto">
                    <p class="justified-content">
                        Sebelum melakukan booking melalui daysOff
                        Tour & Travel, kami menyarankan Anda
                        untuk membaca dan mengerti syarat dan
                        ketentuan jasa daysOff, karena syarat dan
                        ketentuan tersebut merupakan kontrak jasa
                        antara daysOff dengan klien setelah booking
                        ditanda tangankan oleh kedua pihak. Klien
                        yang sudah menandatangani perjanjian
                        tersebut menerima syarat dan ketentuan yang
                        berlaku atas nama semua orang di dalam grup
                        dan bertanggung jawab untuk semua batas
                        waktu pembayaran. Pembelian jasa travel
                        yang ditawarkan oleh daysOff merupakan
                        perjanjian kontraktual antara sang klien dan
                        daysOff, dan merepresentasikan persetujuan
                        sang klien dengan syarat dan ketentuan yang
                        telah tertera disini.
                    </p>

                    <p class="card-price"><b>Kondisi Pemesanan</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Klien akan menerima dokumen
                                perjalanan, antara lain: e-ticket,
                                voucher, dan itinerary melalui Email
                                atau Whatsapp. Semua dokumen
                                tersebut harus dicetak atau print,
                                karena klien akan diminta untuk
                                menunjukan dokumen-dokumen
                                tersebut selama tour berjalan.
                            </li>
                            <li>
                                Bagi peserta Tour yang berumur di
                                atas 70 tahun atau penyandang
                                disabilitas <b> wajib </b> oleh
                                keluarga, teman atau saudara yang
                                bertanggung jawab selama perjalanan
                                tour.
                            </li>
                            <li>
                                Peserta dengan permintaan khusus
                                terhadap group tour min. 20 peserta,
                                dapat meminta informasi harga
                                kepada tour operator kami.
                            </li>
                            <li>
                                Jika peserta tour tidak mencapai 15
                                orang dalam kurun waktu 1 minggu
                                sebelum keberangkatan, maka tour
                                akan di jadwalkan ulang (reschedule)
                                atau dibatalkan.
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Pihak daysOff Tour & Travel dan seluruh agen - agen tidak bertanggung jawab dan tidak bisa dituntut atas:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Kecelakaan, kehilangan koper dan
                                keterlambatan tibanya koper akibat
                                tindakan pihak maskapai
                                penerbangan, hotel dan alat angkutan
                                lainnya.
                            </li>
                            <li>
                                Pemisahan dari tour selama tour
                                berlangsung dan terjadi kecelakaan.
                            </li>
                            <li>
                                Keterlambatan atau pembatalan
                                jadwal penerbangan dan seluruh
                                kejadian yang terjadi di luar kuasa
                                pihak daysOff Tour.
                            </li>
                            <li>
                                Kehilangan benda pribadi, koper,
                                titipan barang di airport, hotel dan
                                tindakan kriminal yang menimpa
                                peserta tour selama perjalanan.
                            </li>
                            <li>
                                Perubahan atau berkurangnya acara
                                perjalanan akibat dari hal-hal yang
                                bersifat “Force Majeure” (Bencana
                                alam, Pandemic, Kerusuhan,
                                Regulasi Pemerintah di negara
                                bersangkutan, dll).
                            </li>
                            <li>
                                Pelayanan paket tour yang tidak
                                digunakan oleh para peserta
                                dikarenakan berhalangan, sakit
                                ataupun atas keinginan pribadi (tidak
                                ada potongan harga dan peserta tidak
                                dapat digantikan dengan orang lain).
                            </li>
                            <li>
                                Deportasi atau penolakan imigrasi
                                negara setempat dengan alasan
                                apapun.
                            </li>
                            <li>
                                Kesalahan dalam hal mentransfer
                                biaya ke rekening bank selain
                                rekening bank resmi daysOff Tour &
                                Travel
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>daysOff tour dan seluruh agen berhak:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Membatalkan atau menunda
                                keberangkatan tour yang telah
                                dijadwalkan, apabila jumlah peserta
                                kurang dari jumlah minimum peserta
                                sesuai produk tour masing-masing.
                            </li>
                            <li>
                                Membatalkan atau menunda
                                keberangkatan tour yang telah
                                dijadwalkan, apabila hal-hal yang
                                bersifat “Force Majeure” (Bencana
                                alam, Pandemic, Kerusuhan, dll).
                            </li>
                            <li>
                                Membatalkan perjalanan peserta tour
                                yang belum membayar uang muka
                                atau pelunasan sesuai batas waktu
                                yang telah ditentukan oleh pihak
                                daysOff tourr.
                            </li>
                            <li>
                                Membatalkan sebagian dari
                                rangkaian acara paket tour, akibat
                                dari ketentuan pihak imigrasi dalam
                                kasus deportasi atas wewenang
                                imigrasi dengan alasan apapun
                                termasuk yang disebabkan oleh
                                dokumen perjalanan yang tidak
                                lengkap atau tidak memenuhi.
                            </li>
                            <li>
                                Membatalkan booking saat
                                servis-servis tertentu tidak tersedia
                                (cth. penyewaan mobil tidak tersedia,
                                atau akomodasi tidak tersedia).
                            </li>
                            <li>
                                Merubah atau mengganti rute dan
                                acara perjalanan sesuai dengan
                                kondisi dan konfirmasi dari maskapai
                                penerbangan, transportasi atau
                                berdasarkan kondisi hotel di
                                masing-masing kota/negara.
                            </li>
                            <li>
                                Meminta peserta Tour untuk keluar
                                dari rombongan apabila peserta yang
                                bersangkutan mencoba membuat
                                kerusuhan, mengacaukan acara tour,
                                meminta dengan paksa dan
                                memberikan informasi yang tidak
                                benar mengenai acara tour, dll.
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Permintaan Khusus:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Permintaan khusus seperti menu
                                makanan (Vegetarian, No Pork, Diet,
                                dll), kamar tidur yang
                                bersebelahan/saling berhubungan
                                (Connecting Door), kursi tempat
                                duduk bersebelahan/berdekatan di
                                pesawat, dll.
                                Silahkan memberitahukan ke pihak
                                kami pada saat pemesanan tour
                                (reservasi).
                            </li>
                            <li>
                                Pihak daysOff tour tidak menjamin
                                ketersediaan permintaan tersebut,
                                karena permintaan khusus tersebut
                                membutuhkan konfirmasi dari luar
                                pihak daysOff (hotel, restoran,
                                penerbangan, dll).
                            </li>
                        </ul>
                    </p>

                    <br><p class="justified-content">
                        Dengan melanjutkan dan melakukan
                        reservasi serta pembayaran deposit,
                        maka Anda dianggap telah
                        menyetujui dan memahami syarat
                        dan kondisi yang tertera tersebut.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Payments -->
    <section class="section-featured">
        <div class="container">
            <div class="section-heading text-center" id="paymentsconditions">
                <h2>Pembayaran daysOff Tours & Travel</h2>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-10 mx-auto">
                    <p class="card-price"><b>Pendaftaran & Prosedur Pembayaran:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Uang muka pendaftaran yang
                                dibayarkan kepada badan usaha
                                daysOff sebesar Rp 2.500.000,- <b>tidak
                                dapat dikembalikan.</b>
                            </li>
                            <li>
                                Pembayaran dilakukan dengan
                                ketentuan berikut:
                                <ul><b>Down Payment (DP) </b>= Rp. 2.500.000,-</ul>
                                <ul><b>20-15 hari sebelum keberangkatan</b> = Rp. 5.300.000,-</ul>
                                <ul><b>10 hari sebelum keberangkatan/pelunasan</b> = Rp. 7.800.000,-</ul>
                                <ul><b>TOTAL</b> = Rp. 15.600.000,-</ul>
                            </li>
                            <li>
                                Airport Tax, Fuel Surcharge, Biaya
                                Visa dan asuransi perjalanan dapat
                                berubah sewaktu-waktu.
                            </li>
                            <li>
                                Peserta tour daysOff bersedia
                                memenuhi kelengkapan persyaratan
                                dokumen sesuai jadwal dan ketentuan
                                dari pihak kedutaan negara yang
                                dituju. Biaya visa tidak dapat
                                dikembalikan jika visa tidak disetujui
                                oleh Kedutaan. Demikian juga jika
                                terdapat biaya lain seperti pembatalan
                                hotel, transportasi dan atau tiket
                                pesawat yang terjadi karena adanya
                                tenggat waktu yang belum tentu sesuai
                                dengan waktu penyelesaian proses
                                visa dari kedutaan, dan juga biaya tour
                                lainnya maka akan dibebankan kepada
                                peserta tour daysOff.
                            </li>
                            <li>
                                Pelunasan biaya tour paling lambat 10
                                hari sebelum tanggal keberangkatan.
                            </li>
                            <li>
                                Sebelum pelunasan, harga tidak
                                mengikat dan dapat berubah
                                sewaktu-waktu.
                            </li>
                            <li>
                                Invoice tagihan biaya service tour
                                akan di kirim langsung oleh Pihak tour
                                kepada customer melalui email.
                            </li>
                            <li>
                                Pembayaran dan pelunasan biaya
                                service tour harus di transfer ke:
                                <ul>Rekening a/n <b>Liliani</b></ul>
                                <ul>Bank BCA cabang Harapan Indah, Bekasi</ul>
                                <ul>No. Account: <b>5210860381</b></ul><br>
                                <ul><i><b>Credit Card</b>: Master Card, Visa, American Express</i>. Pembayaran bisa dilakukan langsung di kantor kami.</ul>
                                <ul>*Pembayaran menggunakan <i>Credit Card</i> akan dikenakan biaya tambahan sesuai ketentuan yang berlaku.</ul>
                            </li>
                            <li>
                                Peserta tour wajib mencantumkan
                                bukti pembayaran dengan cara
                                mencantumkan kode booking pada
                                referensi pada saat melakukan
                                transaksi. Bukti pembayaran
                                dikirimkan melalui Whatsapp
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Deviasi</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Deviasi (penyimpangan perjalanan)
                                dapat dilakukan setelah melakukan
                                deposit dan melampirkan fotokopi
                                paspor.
                            </li>
                            <li>
                                Biaya yang timbul dibebankan kepada
                                peserta daysOff tour yang mengajukan
                                dan harus dilunaskan sebelum
                                keberangkatan tour
                            </li>
                            <li>
                                Pihak daysOff tour tidak bertanggung
                                jawab atas konfirmasi reservasi tiket
                                pesawat, hotel dan sebagainya apabila
                                peserta daysOff tour menghendaki
                                deviasi.
                            </li>
                            <li>
                                Deviasi harus diajukan min. 1 bulan
                                sebelum keberangkatan. Pihak
                                daysOff tour berhak memutuskan
                                apakah deviasi yang diajukan dapat
                                diterima atau ditolak.
                            </li>
                            <li>
                                Deviasi dapat dilakukan jika peserta
                                yang berangkat dan yang pulang
                                memenuhi kuota maskapai
                                penerbangan.
                            </li>
                            <li>
                                Deviasi dengan cara mempersingkat
                                jadwal paket tour tidak mendapat
                                pengurangan biaya.
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Pembatalan:</b></h3>
                    <p class="justified-content">
                        Jika peserta daysOff Tour & Travel melakukan
                        pembatalan sebelum tanggal keberangkatan,
                        maka akan berlaku biaya pembatalan sesuai
                        ketentuan berikut:
                        <ul>
                            <li>
                                15 hari sebelum keberangkatan = 50% dari harga tour
                            </li>
                            <li>
                                10 hari sebelum keberangkatan = 100% dari harga tour
                            </li>
                        </ul>
                        Biaya pembatalan di atas juga berlaku untuk hal-hal berikut:
                        <ul>
                            <li>
                                Peserta yang mengganti tanggal keberangkatan atau mengganti paket/jenis tour.
                            </li>
                            <li>
                                Peserta yang terlambat memberikan
                                persyaratan visa dari batas waktu yang
                                ditentukan oleh daysOff tour dan
                                mengakibatkan peserta tidak dapat
                                berangkat tepat waktu karena
                                permohonan visa masih diproses oleh
                                kedutaan.
                            </li>
                            <li>
                                Pembatalan sepihak peserta tour
                                dikarenakan alasan pribadi ataupun
                                <i>“Force Majeure”</i>.
                            </li>
                        </ul>
                        Jika pihak daysOff membatalkan atau
                        menunda keberangkatan tour yang telah
                        dijadwalkan, apabila jumlah peserta kurang dari jumlah minimum peserta sesuai produk
                        tour masing-masing, maka pihak daysOff akan
                        mengembalikan seluruh biaya tour kepada tiap
                        peserta (full refund).
                    </p>

                    <p class="card-price"><b>Biaya Tour Termasuk:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Tiket pesawat udara p.p. kelas
                                ekonomi (non-endorsable, nonrefundable & non-reroutable
                                berdasarkan harga group / promosi).
                            </li>
                            <li>
                                Airport tax International (dapat berubah sewaktu-waktu).
                            </li>
                            <li>
                                Asuransi Perjalanan GROUP (usia tertanggung maksimal 65 tahun).
                            </li>
                            <li>
                                Penginapan di hotel berdasarkan 2 (dua) orang dalam 1 kamar (twin sharing).
                            </li>
                            <li>
                                Acara tour, transportasi (Private Bus free WIFI & AC) dan makan sesuai
                                yang tercantum dalam acara perjalanan.
                                (MP – Makan Pagi; MS – Makan Siang; MM – Makan Malam)
                            </li>
                            <li>
                                Berat maksimum sebesar 30kg untuk 1 bagasi atau sesuai dengan peraturan
                                maskapai penerbangan yang digunakan; dan berat maksimum 7 kg untuk 1 handbag kecil untuk dibawa
                                ke kabin pesawat atau sesuai dengan peraturan maskapai penerbangan yang digunakan.
                            </li>
                            <li>
                                PCR Test di Hotel, Travel KIT Masker & <i>Hand Sanitizer</i>.
                            </li>
                            <li>
                                Merchandise (cth. Masker, T-shirt, dll) dari daysOff Tour.
                            </li>
                            <li>
                                Tips untuk Local Guide & Tour Leader dari daysOff Tour.
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Biaya Tour Tidak Termasuk:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Pengeluaran pribadi (Mini bar, telepon, makanan tambahan diluar dari menu yang disediakan, dll) 
                                atau uang saku peserta selama mengikuti rangkaian kegiatan acara tour.
                            </li>
                            <li>
                                Tour tambahan (Optional tour).
                            </li>
                            <li>
                                Biaya bea masuk bagi barang-barang.
                            </li>
                            <li>
                                Excess baggage (Biaya kelebihan bagasi).
                            </li>
                            <li>
                                Biaya single supplement bagi peserta yang ingin menempati satu kamar sendiri.
                            </li>
                            <li>
                                PCR/Swab Test dari Jakarta menuju Istanbul (tidak disarankan melakukan Test di Bandara pada hari-H)
                            </li>
                            <li>
                                Tips jika menggunakan porter di bandara.
                            </li>
                        </ul>
                        <br>
                            Dengan melanjutkan dan melakukan reservasi
                            serta pembayaran deposit, maka Anda
                            dianggap telah menyetujui dan memahami
                            syarat dan kondisi tersebut.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Covid-19 -->
    <section class="section-featured">
        <div class="container">
            <div class="section-heading text-center" id="paymentsconditions">
                <h2>Traveling di masa pandemi COVID-19</h2>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-10 mx-auto">
                    <p class="justified-content">
                        Sebelum Anda menjalankan tour bersama
                        daysOff Tour & Travel di saat masa pandemi
                        ini, Anda disarankan untuk membaca dan
                        memahami beberapa peraturan dan imbauan
                        tambahan di masa ini, agar semua berjalan
                        lancar dan aman bagi Anda dan grup tour
                        Anda. Pada akhir syarat dan ketentuan ini
                        Anda diharuskan untuk menandatangani dan
                        menyetujui surat ini bagi kelancaran tour yang
                        akan datang.
                    </p>
                    <p class="card-price"><b>Saran Sebelum Keberangkatan:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Sebelum Anda melakukan perjalanan ke kota / negara tujuan, Anda
                                disarankan untuk selalu menyimak situasi dan peraturan terbaru mengenai COVID-19 di negara tujuan.
                            </li>
                            <li>
                                2 (dua) minggu sebelum keberangkatan, Anda disarankan untuk
                                meminimalisir rencana untuk bepergian.
                            </li>
                            <li>
                                Anda wajib menyertakan bukti hasil PCR Test negatif kepada salah satu travel consultant daysOff Tour & Travel.
                            </li>
                            <li>
                                Anda disarankan memiliki polis
                                asuransi jiwa <i>(life insurance)</i> yang
                                dapat mencakup kebutuhan biaya medis Anda dan penanganan
                                COVID-19 di negara bersangkutan.
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Aturan Tambahan Selama Masa Pandemi:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Anda diharuskan untuk melakukan
                                PCR Test sebelum keberangkatan dan
                                sebelum penerbangan balik dari
                                negara tujuan.
                            </li>
                            <li>
                                Peserta Tour diwajibkan untuk
                                memberikan bukti hasil test negatif
                                COVID-19 kepada pihak tur, paling lambat 2 hari sebelum keberangkatan dari Indonesia.
                            </li>
                            <li>
                                Selama di kota / negara tujuan, Anda
                                diwajibkan untuk selalu memakai
                                masker selain di kamar hotel.
                            </li>
                            <li>
                                Setiap kali sebelum masuk ke dalam
                                bus, Anda diharuskan untuk selalu
                                menggunakan desinfektan yang akan
                                disediakan oleh tim daysOff Tour and
                                Travel.
                            </li>
                            <li>
                                Jika hasil PCR Test menunjukan hasil
                                positif, Anda tidak dapat melanjutkan
                                penerbangan dari negara
                                keberangkatan.
                            </li>
                            <li>
                                Jika pihak bandara dari negara
                                keberangkatan menyatakan bahwa ada
                                gejala COVID-19, maka Anda
                                diwajibkan untuk melakukan
                                karantina, mengikuti aturan yang
                                berlaku di negara bersangkutan.
                            </li>
                        </ul>
                    </p>

                    <p class="card-price"><b>Pembatalan Pemesanan:</b></h3>
                    <p class="justified-content">
                        <ul>
                            <li>
                                Jika dari negara tujuan sudah ada
                                keputusan untuk menutup perbatasan,
                                reservasi tour yang sudah Anda
                                lakukan akan dibatalkan, dan Anda
                                akan mendapatkan uang kembali
                                (refund) sesuai dengan ketentuan
                                pihak yang bertanggung jawab.
                            </li>
                            <li>
                                Jika Anda mendapatkan reaksi positif
                                pada PCR Test sebelum
                                keberangkatan, Anda tidak dapat
                                mengikuti perjalanan dan akan
                                dikembalikan uang sesuai dengan
                                ketentuan pihak yang bertanggung
                                jawab.
                            </li>
                            <li>
                                Peserta tour wajib mengisi refund form
                                untuk mengajukan pengembalian
                                uang, jika hasil PCR Test menunjukan
                                positif.
                            </li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection