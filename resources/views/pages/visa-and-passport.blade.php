@extends('layouts.app')

@section('title')
    Visa dan Passport | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
<style>
  li {
    text-align: justify;
  }
</style>
    <!-- Page Heading/Breadcrumbs -->
    <section class="section-header" style="background-image: url('frontend/images/visa-and-passport/Banner\ Header\ Section.jpg');">
        <div class="container">
        <h1 class="mt-5 mb-3">Visa dan Passport
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Visa & Passport</li>
        </ol>
        </div>
    </section>

    <div class="row">
    <div class="col-md-10 ml-auto col-xl-8 mr-auto">
      <!-- Nav tabs -->
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs vp-tabs justify-content-center" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#visa" role="tab">
                <i class="now-ui-icons objects_umbrella-13"></i> Visa
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#passport" role="tab">
                <i class="now-ui-icons shopping_cart-simple"></i> Passport
              </a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <!-- Tab panes -->
          <div class="tab-content text-center">
            <div class="tab-pane active" id="visa" role="tabpanel">
              <p>Select country for visa information : </p>
                <center><div class="col-4">
                <select name="country_id" id="visa_country" class="form-control">
                  <option value="country">- Country -</option>
                  @foreach($visa as $visas)
                  <option value="{{$visas->id}}">{{$visas->country}}</option>
                  @endforeach
                </select>
                </div></center>
                <center><div class="col-8"></div></center>
            </div>
            <div class="tab-pane" id="passport" role="tabpanel">
              <p class="justified-content"> Paspor adalah salah satu tanda pengenal yang wajib dibawa saat bepergian ke luar negeri. Sebagai identitas resmi, paspor dikeluarkan oleh pihak berwenang. Paspor berisi biodata yang meliputi foto
              pemegang, tanda tangan, tempat dan tanggal kelahiran, informasi kebangsaan dan kadang-kadang juga beberapa informasi lain mengenai identifikasi individual. Paspor biasa memiliki masa berlaku paling lama lima tahun sejak tanggal diterbitkan. </p><p class="justified-content">
              Persyaratan Pembuatan Paspor
              Berikut ini syarat cara membuat paspor berdasarkan pada Peraturan Menteri Hukum dan HAM RI
              Nomor 8 Tahun 2014 tentang Paspor Biasa dan Surat Perjalanan Laksana Paspor: </p><p class="justified-content">
              1. KTP, asli dan fotocopy
              </p><p class="justified-content">
              2. Kartu Keluarga, asli dan fotocopy
              </p><p class="justified-content">
              3. Akta kelahiran, asli dan fotocopy
              </p><p class="justified-content">
              4. Ijazah terakhir atau akta perkawinan atau buku nikah
              </p><p class="justified-content">
              5. Paspor lama bagi yang ingin memperpanjang
              </p><p class="justified-content">
              Informasi lebih lanjut harap menghubungi Customer Service daysOff Tour & Travel </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
@endsection
@push('custom-scripts')
<script type="text/javascript">
        $('#visa_country').on('change',function(){
            id = $(this).val();
            
            $.ajax({
                type : 'get',
                url : "{{route('VisaAndPassportSearch')}}",
                data:{'id': id},
                success:function(data){
                    $('.col-8').html(data);
                }
            });
        });
</script>
<script type="text/javascript">
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
@endpush