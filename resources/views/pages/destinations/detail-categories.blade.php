@extends('layouts.app')

@section('title')
    daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('frontend/images/tour-offers/Banner\ Header\ Section.jpg')}}')">
        <div class="container">
            <h1 class="mt-5 mb-3">Destinations</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Destinations</a></li>
                <li class="breadcrumb-item">{{$categoryName->category_name}}</li>
            </ol>
        </div>
    </section>

    <section class="section-featured">
        <div class="container">
        @if(count($fitTour) > 0)
        <p style="color: #ba9a37; font-size:50px">FIT</p>
        @endif
            <div class="featured-destinations row">
            @forelse ($fitTour as $fit)
                <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                    <div class="card pulse-card infinite shadow h-100">
                        <a href="{{route('TravelPackagesDetails', $fit->slug)}}"><img loading="lazy" class="card-img-top" src="{{$fit->travelGalleries->count() ? Storage::url($fit->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                        <div class="card-body">
                        <h3 class="card-title">
                            <a href="{{route('TravelPackagesDetails', $fit->slug)}}">{{$fit->title}}</a>
                            <br>{{$fit->duration}}
                        </h3>
                        <h4 class="card-price">
                        @if ($fit->departureDates->count())
                            From IDR {{ number_format($fit->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                        @elseif(isset($fit->price))
                            IDR {{ number_format($fit->price/1000000, 2) }} jt/PAX
                        @else
                            Please contact us for price!
                        @endif  
                        </h4>
                        <p class="text-muted">{{Str::words($fit->about, 15, '...')}}</p>
                        </div>
                    </div>
                </div>
            @empty
            @endforelse
            </div>
            @if(count($groupTour) > 0)
            <p style="color: #ba9a37; font-size:50px">GROUP</p>
            @endif
                <div class="featured-destinations row">
                @forelse ($groupTour as $group)
                    <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                        <div class="card pulse-card infinite shadow h-100">
                            <a href="{{route('TravelPackagesDetails', $group->slug)}}"><img loading="lazy" class="card-img-top" src="{{$group->travelGalleries->count() ? Storage::url($group->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                            <div class="card-body">
                            <h3 class="card-title">
                                <a href="{{route('TravelPackagesDetails', $group->slug)}}">{{$group->title}}</a>
                                <br>{{$group->duration}}
                            </h3>
                            <h4 class="card-price">
                            @if ($group->departureDates->count())
                                From IDR {{ number_format($group->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                            @elseif(isset($group->price))
                                IDR {{ number_format($group->price/1000000, 2) }} jt/PAX
                            @else
                                Please contact us for price!
                            @endif  
                            </h4>
                            <p class="text-muted">{{Str::words($group->about, 15, '...')}}</p>
                            </div>
                        </div>
                    </div>
                @empty
                @endforelse
                </div>
                @if(count($incentiveTour) > 0)
                <p style="color: #ba9a37; font-size:50px">INCENTIVE</p>
                @endif
                    <div class="featured-destinations row">
                    @forelse ($incentiveTour as $incentive)
                        <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                            <div class="card pulse-card infinite shadow h-100">
                                <a href="{{route('TravelPackagesDetails', $incentive->slug)}}"><img loading="lazy" class="card-img-top" src="{{$incentive->travelGalleries->count() ? Storage::url($incentive->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                                <div class="card-body">
                                <h3 class="card-title">
                                    <a href="{{route('TravelPackagesDetails', $incentive->slug)}}">{{$incentive->title}}</a>
                                    <br>{{$incentive->duration}}
                                </h3>
                                <h4 class="card-price">
                                @if ($incentive->departureDates->count())
                                    From IDR {{ number_format($incentive->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                                @elseif(isset($incentive->price))
                                    IDR {{ number_format($incentive->price/1000000, 2) }} jt/PAX
                                @else
                                    Please contact us for price!
                                @endif  
                                </h4>
                                <p class="text-muted">{{Str::words($incentive->about, 15, '...')}}</p>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                    </div>
                    @if(count($privateTour) > 0)
                    <p style="color: #ba9a37; font-size:50px">PRIVATE</p>
                    @endif
                        <div class="featured-destinations row">
                        @forelse ($privateTour as $private)
                            <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                                <div class="card pulse-card infinite shadow h-100">
                                    <a href="{{route('TravelPackagesDetails', $private->slug)}}"><img loading="lazy" class="card-img-top" src="{{$private->travelGalleries->count() ? Storage::url($private->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                                    <div class="card-body">
                                    <h3 class="card-title">
                                        <a href="{{route('TravelPackagesDetails', $private->slug)}}">{{$private->title}}</a>
                                        <br>{{$private->duration}}
                                    </h3>
                                    <h4 class="card-price">
                                    @if ($private->departureDates->count())
                                        From IDR {{ number_format($private->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                                    @elseif(isset($private->price))
                                        IDR {{ number_format($private->price/1000000, 2) }} jt/PAX
                                    @else
                                        Please contact us for price!
                                    @endif  
                                    </h4>
                                    <p class="text-muted">{{Str::words($private->about, 15, '...')}}</p>
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                        </div>
                        @if(count($attractionTour) > 0)
                    <p style="color: #ba9a37; font-size:50px">ATTRACTION</p>
                    @endif
                        <div class="featured-destinations row">
                        @forelse ($attractionTour as $attraction)
                            <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                                <div class="card pulse-card infinite shadow h-100">
                                    <a href="{{route('TravelPackagesDetails', $attraction->slug)}}"><img loading="lazy" class="card-img-top" src="{{$attraction->travelGalleries->count() ? Storage::url($attraction->travelGalleries->shuffle()->first()->image) : ''}}" alt=""></a>
                                    <div class="card-body">
                                    <h3 class="card-title">
                                        <a href="{{route('TravelPackagesDetails', $attraction->slug)}}">{{$attraction->title}}</a>
                                        <br>{{$attraction->duration}}
                                    </h3>
                                    <h4 class="card-price">
                                    @if ($attraction->departureDates->count())
                                        From IDR {{ number_format($attraction->departureDates->sortBy('price')->first()->price/1000000, 2) }} jt/PAX 
                                    @elseif(isset($attraction->price))
                                        IDR {{ number_format($attraction->price/1000000, 2) }} jt/PAX
                                    @else
                                        Please contact us for price!
                                    @endif  
                                    </h4>
                                    <p class="text-muted">{{Str::words($attraction->about, 15, '...')}}</p>
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                        </div>
        </div>
    </section>
@endsection