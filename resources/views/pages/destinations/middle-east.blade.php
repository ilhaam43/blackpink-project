@extends('layouts.app')

@section('title')
    Middle East | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('frontend/images/tour-offers/Mid-East.jpg')}}')">
        <div class="container">
            <h1 class="mt-5 mb-3">Middle East</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Destinations</a></li>
                <li class="breadcrumb-item active">Middle East</li>
            </ol>
        </div>
    </section>

    <section class="section-featured">
        <div class="container">
            <div class="featured-destinations row justify-content-center">
                @forelse($categories as $category)
                <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                    <div class="card pulse-card infinite shadow h-100">
                        <a><img class="card-img-top" src="{{url('storage/'.$category->category_image)}}" width="100" height="200" alt="" loading ="lazy"></a>
                        <div class="card-body">
                            <h3 class="card-title">
                                <a href="{{route('DestinationCategory', $category->id)}}">{{$category->category_name}}</a>
                            </h3>
                            <p class="text-muted">
                            {{$category->category_details}}
                            </p>
                        </div>
                    </div>
                    </div>
                @empty
                    <p>Paket Perjalanan Belum Tersedia</p>
                @endforelse
            </div>
        </div>
    </section>
@endsection