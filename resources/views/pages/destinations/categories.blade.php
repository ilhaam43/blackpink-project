@extends('layouts.app')

@section('title')
    daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('frontend/images/tour-offers/Banner\ Header\ Section.jpg')}}')">
        <div class="container">
            <h1 class="mt-5 mb-3">Destinations</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Destinations</a></li>
                <li class="breadcrumb-item">{{$categoryName->category_name}}</li>
            </ol>
        </div>
    </section>

    <section class="section-featured">
        <div class="container">
            <div class="featured-destinations row justify-content-center">
            @forelse ($tourType as $tourTypes)
                <div class="col-lg-6 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                    <div class="card-body">
                    <h3 class="card-title">
                        <center><a href="{{route('DestinationDetailCategory',['categoryDestination' => $category, 'tourType' => $tourTypes->code])}}">{{$tourTypes->name}}</a></center>
                    </h3>
                    </div>
                </div>
                </div>
            @empty
                <p>Paket Perjalanan Belum Tersedia</p>
            @endforelse
            </div>
        </div>
    </section>
@endsection