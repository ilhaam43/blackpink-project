@extends('layouts.app')

@section('title')
  Beranda | daysOff Official Website
@endsection

@section('body-class')
  <body class="body-home">
@endsection

@section('content')


  <!-- Hero Title -->
  <header class="text-center">
    @if (session('status'))
        <div class="mb-4 font-medium text-sm text-green-600">
            {{ session('status') }}
        </div>
    @endif
    <div id="headerCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        @foreach ($sliderImage as $image)
          <div class="carousel-item @if ($loop->first) active @endif">
              <img src="{{$image!=NULL ? Storage::url($image->image_url) : ''}}" class="d-block w-100">
          </div>
        @endforeach
        <a class="carousel-control-prev" href="#headerCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#headerCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </header>

  <!-- Main Contents -->
  <main>
    <section class="section-featured" id="featured-destinations">
      <!-- Paket Perjalanan -->
      <div class="homeSection container" id="paketPerjalanan">
        <div class="section-heading text-center">
          <h2>Paket Perjalanan</h2>
          @if(count($travelPackages) > 0)
          <p class="text-muted">Destinasi yang Kami Tawarkan Saat Ini!</p>
        </div>
        <div class="container">
          <div class="featured-destinations row">
            @foreach ($travelPackages as $travelPackage)
              <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                  <a href="{{route('TravelPackagesDetails', $travelPackage->slug)}}"><img class="card-img-top" src="{{$travelPackage->travelGalleries->count() ? Storage::url($travelPackage->travelGalleries->shuffle()->first()->image) : ''}}" alt="" loading ="lazy" height="280"></a>
                  <div class="card-body">
                    <h3 class="card-title">
                      <a href="{{route('TravelPackagesDetails', $travelPackage->slug)}}" >{{$travelPackage->title}}</a>
                      <br>{{$travelPackage->duration}}
                    </h3>
                    <h4 class="card-price">
                    @if ($travelPackage->departureDates->count())
                      From IDR {{ number_format($travelPackage->departureDates->sortBy('price')->first()->price/1000000) }} JT/PAX 
                    @elseif($travelPackage->price)
                      Start From IDR {{ number_format($travelPackage->price/1000000) }} JT/PAX
                    @else
                      Please contact us for price!
                    @endif  
                    </h4>
                    <p class="text-muted">{{Str::words($travelPackage->about, 15, '...')}}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <center><a href="{{route('TravelPackagesPage')}}" class="btn btn-lg btn-outline-primary btn-outline-black">Lihat Semua</a></center>
        </div>
        @else
        <p class="text-muted">Paket Perjalanan Sedang Tidak Tersedia</p>
        @endif
      </div>
      
      <!-- Tour Domestic -->
      <div class="homeSection container" id="tourEvents"><hr>
        <div class="section-heading text-center" >
          <h2>Tour Domestik</h2>
          @if(count($showTourEvents) > 0)
          <p class="text-muted">Tour Domestik Tour yang Kami Tawarkan Saat Ini!</p>
        </div>
        <div class="container">
          <div class="featured-destinations row">
            @foreach ($showTourEvents as $events)
              <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                  <a href="{{route('TravelPackagesDetails', $events->slug)}}"><img class="card-img-top" src="{{$events->travelGalleries->count() ? Storage::url($events->travelGalleries->shuffle()->first()->image) : ''}}" alt="" loading ="lazy"  height="280"></a>
                  <div class="card-body">
                    <h3 class="card-title">
                      <a href="{{route('TravelPackagesDetails', $events->slug)}}" >{{$events->title}}</a>
                    </h3>
                    <h4 class="card-price">
                    @if ($events->departureDates->count())
                      Start From IDR {{ number_format($events->departureDates->sortBy('price')->first()->price/1000000) }} JT/DAY 
                    @elseif($events->price)
                      Start From IDR {{ number_format($events->price/1000000) }} JT/DAY
                    @else
                      Please contact us for price!
                    @endif  
                    </h4>
                    <p class="text-muted">{{Str::words($events->about, 15, '...')}}</p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <center><a href="{{route('Tours')}}" class="btn btn-lg btn-outline-primary btn-outline-black">Lihat Semua</a></center>
        </div>
        @else
        <p class="text-muted">Tour & Events Sedang Tidak Tersedia</p>
        @endif
      </div>

      <!-- Quarantine Hotel -->
      <div class="homeSection container" id="hotelKarantina"><hr>
        <div class="section-heading text-center">
          <h2>Quarantine Hotels</h2>
        </div>
        <div class="container">
          <div class="featured-destinations row">
            @foreach($quarantineHotels as $hotel)
              <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                <div class="card pulse-card infinite shadow h-100">
                  <a href="{{route('QuarantineHotelsDetails', $hotel->slug)}}">
                    <img class="card-img-top hotel-thumbnail" src="{{url('storage/'.$hotel['hotelGalleries'][0]['image'])}}" loading ="lazy">
                  </a>
                  <div class="card-body">
                    <p class="card-title">
                      <a href="{{route('QuarantineHotelsDetails', $hotel->slug)}}">{{$hotel->hotel_name}}</a>
                    </p>
                    <p class="card-price">
                      @if ($hotel->hotelPrices->count())
                        From IDR {{$hotel->hotelPrices->sortBy('room_price')->whereNotIn('room_id', [10])->first()->room_price/1000000}} jt/Person
                      @else
                        Please contact us for price!
                      @endif  
                    </p>
                    <p class="text-muted"><i class="fa fa-map-marker"></i> {{$hotel->hotel_city}}</p>
                    <div class="sold_stars ml-auto"> 
                      @for($i=0; $i < 5; $i++)
                        @if($i<$hotel->hotel_rate)
                          <i class="fa fa-star"></i>
                        @endif
                      @endfor
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <center><a href="{{route('QuarantineHotelsPage')}}" class="btn btn-lg btn-outline-primary btn-outline-black">Lihat Semua</a></center>
        </div>
      </div>
      
      {{-- Staycations --}}
      @if ($staycationIGPosts->count()!=0)
      <div class="homeSection container products-list mt-5" id="staycations"><hr>
        <div class="section-heading text-center">
          <h2>Staycations</h2>
          <p class="text-muted">Liburan Sambil Tetap Social Distancing? Bisa!</p>
        </div>
        <div class="container">
          <div class="row">
            @foreach ($staycationIGPosts as $staycationIGPost)
            <div class="col-lg-4 mb-4">
              <blockquote class="instagram-media shadow" data-instgrm-permalink="https://www.instagram.com/p/{{ $staycationIGPost->post_url }}/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                <div style="padding:16px;"> 
                  <a href="https://www.instagram.com/p/{{ $staycationIGPost->post_url }}/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> 
                    <div style=" display: flex; flex-direction: row; align-items: center;"> 
                      <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> 
                      <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> 
                        <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> 
                        <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div>
                      </div>
                    </div>
                    <div style="padding: 19% 0;"></div> 
                    <div style="display:block; height:50px; margin:0 auto 12px; width:50px;">
                      <svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g transform="translate(-511.000000, -20.000000)" fill="#000000">
                            <g>
                              <path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631">
                                </path>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </div>
                    <div style="padding-top: 8px;"> 
                      <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> Sieh dir diesen Beitrag auf Instagram an</div>
                    </div>
                    <div style="padding: 12.5% 0;"></div> 
                    <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
                      <div> 
                        <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> 
                        <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> 
                        <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
                      </div>
                      <div style="margin-left: 8px;"> 
                        <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> 
                        <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div>
                      </div>
                      <div style="margin-left: auto;"> 
                        <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> 
                        <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> 
                        <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
                      </div>
                    </div> 
                    <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> 
                      <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> 
                      <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
                    </div>
                  </a>
                  <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">
                    <a href="https://www.instagram.com/p/{{ $staycationIGPost->post_url }}/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Ein Beitrag geteilt von LIBURAN HEMAT &amp; BERKUALITAS (@daysoff.id)</a>
                  </p>
                </div>
              </blockquote> 
            <script async src="//www.instagram.com/embed.js"></script>
            </div>
            @endforeach
          </div>
          <center><a href="{{route('StaycationsPage')}}" class="btn btn-lg btn-outline-primary btn-outline-black">Lihat Semua</a></center>
        </div>
      </div> 
      @endif
      <hr>
    </section>  

    <!-- Coming-Soon Banner -->
    <section class="homeSection section-midpage-banner" id="comming-soon">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col text-center">
            <h2>JOIN OUR INSTAGRAM FOR UPDATE STORY</h2>
            <b><p style="font-size:24px;">@daysoff.id</p></b>
          </div>
        </div>
      </div>
    </section>
    
    @include('includes.sections.why-us')

    <!-- MICE Section -->
    <section class="homeSection container" id="mice"><hr>
      <div class="section-heading text-center">
        <h2>Paket Perjalanan MICE</h2>
        @if(count($travelMice) > 0)
        <p class="text-muted">Destinasi yang Kami Tawarkan Saat Ini!</p>
      </div>
      <div class="container">
        <div class="featured-destinations row justify-content-center">
          @foreach ($travelMice as $travel)
            <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
              <div class="card pulse-card infinite shadow h-100">
                <a href="{{route('TravelPackagesDetails', $travel->slug)}}">
                  <img class="card-img-top" src="{{$travel->travelGalleries->count() ? Storage::url($travel->travelGalleries->shuffle()->first()->image) : ''}}" alt="" loading ="lazy" height="280">
                </a>
                <div class="card-body">
                  <h3 class="card-title">
                    <a href="{{route('TravelPackagesDetails', $travel->slug)}}" >{{$travel->title}}</a>
                    <br>{{$travel->duration}}
                  </h3>
                  <h4 class="card-price">
                  @if ($travel->departureDates->count())
                    From IDR {{ number_format($travel->departureDates->sortBy('price')->first()->price/1000000) }} jt/PAX 
                  @elseif($travel->price)
                    IDR {{ number_format($travel->price/1000000) }} jt/PAX
                  @else
                    Please contact us for price!
                  @endif  
                  </h4>
                  <p class="text-muted">{{Str::words($travel->about, 15, '...')}}</p>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <center><a href="{{route('TravelMicePage')}}" class="btn btn-lg btn-outline-primary btn-outline-black">Lihat Semua</a></center>
      </div>
      @else
      <p class="text-muted">Paket Perjalanan Sedang Tidak Tersedia</p>
      @endif
    </section>

    <!-- Youtube -->
    <section class="homeSection section-videos" id="youtube">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h2>Check Out Our <br>
                YouTube Channel!</h2>
            <p>Updating You with Our Adventures</p>
            <ul>
              <li>
                Tour Packages
              </li>
              <li>
                Travel Stories
              </li>
              <li>
                Travel Knowledges
              </li>
              <li>
                Special Offers
              </li>
            </ul>
          </div>
          <div class="col-lg-6">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/s3VFXKVDblQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" loading ="lazy" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </section>

    <!-- Call to Action Section -->
    {{-- @guest
    <section class="section-cta">
      <div class="container">
        <div class="row mb-4">
          <div class="col-md-8">
            <h3>Belum Punya Akun? Daftar di sini untuk dapat banyak penawaran menarik!</h3>
          </div>
          <div class="col-md-4">
            <a class="btn btn-lg btn-primary btn-signup" href="{{route('register')}}">Sign Up</a>
          </div>
        </div>
      </div>
    </section>
    @endguest --}}  
  </main>
@endsection

@push('custom-scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      // Open modal on page load
      $("#myModal").modal('show');

      // Close modal on button click
      $(".xtclose").click(function(){
        $("#myModal").modal('hide');
      });
    });
  </script>

  <script>
    $(document).ready(function(){
      $.ajax({
        url: "section",
        type: 'GET',
        success: function(data) {
          $.each(data, function(i, data){
            if(data.is_show != 1){
              $('#' + data.section_id).hide();
            }else{
              $('#' + data.section_id).show();
            }
          });
        }
      });
    });
  </script>
@endpush