@extends('layouts.app')

@section('title')
    Transportation | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <!-- Page Heading/Breadcrumbs -->
    <section class="section-header" style="background-image: url('frontend/images/mice/Banner\ Header\ Section.jpg');">
        <div class="container">
        <h1 class="mt-5 mb-3">Transportation
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Transportation</li>
        </ol>
        </div>
    </section>
    
    <section class="section-featured" id="featured-destinations">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 mx-auto">
                    <br><br>
                    <span class="miceIcon">
                        <i class="fa fa-bus"></i>
                    </span>
                    <span class="miceIcon">
                        <i class="fa fa-car"></i>
                    </span>

                    <div class="section-heading text-center">
                        <br> <h3 style="font-family: assistant; font-weight: 600;">Transportation</h3><br>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="featured-destinations row justify-content-center">
                    @foreach ($transportation as $item)
                        <div class="col-lg-4 col-sm-6 portfolio-item mb-4">
                            <div class="card pulse-card infinite shadow h-100">
                                <img loading="lazy" class="card-img-top" src="{{ Storage::url($item->image) }}" alt="{{ $item->title }}" style="object-fit:cover; height:280px; width:100%">
                                <div class="card-body">
                                    <h3 class="card-title">
                                        <a href="#" >{{ $item->title }}</a>
                                    </h3>
                                    <h4 class="card-price">
                                        Up to {{ $item->capacity }} people <br><br>
                                        @if(isset($item->price))
                                            IDR {{ round($item->price/1000000) }} jt 
                                        @else
                                            Please contact us for price!
                                        @endif  
                                    </h4>
                                    <p class="text-muted">
                                        {{ $item->type }} <br>
                                        {{ Str::words($item->note, 15, '...') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection