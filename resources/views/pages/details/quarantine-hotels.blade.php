@extends('layouts.app')

@section('title')
    Quarantine Hotel Details | daysOff Official Website
@endsection

@section('body-class')
    <body class="body-details">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('../frontend/images/quarantinehotel-details/Banner\ Header\ Section.jpg')}}');">
        <div class="container">
            <h1 class="mt-5 mb-3">Tour Details
            </h1>
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('Home')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('Home')}}#hotelKarantina">Quarantine Hotel</a>
            </li>
            <li class="breadcrumb-item active">{{$item->hotel_name}}</li>
            </ol>
        </div>
    </section>

    <section class="section-details-content">
        <div class="container">
            <div class="section-heading text-center">
                <h2>{{$item->hotel_name}}</h2>
            </div>

            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="card main-card card-details shadow mb-5 py-4">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div id="carouselhotelKarantina" class="carousel slide shadow" data-ride="carousel">
                                        <div class="carousel-inner">
                                            @foreach ($item->hotelGalleries as $hotelGallery)
                                            <div class="carousel-item @if ($loop->first) active @endif">
                                                <img src="{{$hotelGallery!=NULL ? Storage::url($hotelGallery->image) : ''}}" class="d-block w-100" alt="{{$item->hotel_name}}.jpg">
                                            </div>
                                            @endforeach
                                        </div>
                                        @if(count($item->hotelGalleries) > 1)
                                        <a class="carousel-control-prev" href="#carouselhotelKarantina" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselhotelKarantina" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                        @endif
                                    </div><br>

                                    <p class="card-title-hoteldetails">{{$item->hotel_address}} ({{$item->hotel_city}})</p>
                                    <div class="sold_stars"> 
                                        @for($i=0; $i < 5; $i++)
                                            @if($i<$item->hotel_rate)
                                                <i class="fa fa-star"></i>
                                            @endif
                                        @endfor
                                    </div>
                                    <p>{!! $item->description !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="section-heading text-center">
                        <p class="card-title-hoteldetails">Room Types </p>
                    </div>
                        
                    @foreach($rooms as $room)
                    <div class="card main-card card-details shadow mb-5 py-4">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="col">
                                        @foreach($room->unique('room_name') as $roomname)
                                        <p class="card-title-hoteldetails">{{$roomname->rooms->room_name}}</p>
                                        @endforeach
                                    </div>
                                    <div class="col">
                                        <p><b>Price:</b></p>
                                        @foreach($room as $roomdetail)
                                            @if($roomdetail->room_occupant == '1')
                                            <p>Start From <b>IDR {{number_format($roomdetail->room_price)}}</b> for {{$roomdetail->room_occupant}} person</p>
                                            @else
                                            <p>Start From <b>IDR {{number_format($roomdetail->room_price)}}</b> for {{$roomdetail->room_occupant}} persons</p>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="text-center">
                                        <a href="https://api.whatsapp.com/send?phone=628119728228&text=Hallo%20daysOff%20Tour%20%26%20Travel%2C%20I%20want%20to%20book%20a%20quarantine%20hotel%20at%20{{ $item->hotel_name }}" class="btn btn-lg btn-outline-primary btn-outline-black">Book Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
      </section>
@endsection