@extends('layouts.app')

@section('title')
    Career Details | daysOff Official Website
@endsection

@section('body-class')
    <body class="body-details">
@endsection

@section('content')
    <section class="section-header" style="background-image: url('{{url('../frontend/images/career/Banner\ Header.jpg')}}');">
        <div class="container">
            <h1 class="mt-5 mb-3">Careers Details
            </h1>
            <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('Home')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('Careers')}}">Careers</a>
            </li>
            <li class="breadcrumb-item active">{{$item->title}}</li>
            </ol>
        </div>
    </section>

    <section class="section-details-content">
        <div class="container">
            <div class="section-heading text-center mt-3">
                <h2>{{$item->title}}</h2>
            </div>

            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <div class="card main-card card-details shadow mb-5 py-4">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div id="carouselhotelKarantina" class="carousel slide shadow" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src="{{Storage::url($item->image)}}" class="d-block w-100" alt="{{$item->title}}.jpg">
                                            </div>
                                        </div>
                                    </div><br>
                                    <h3 class="card-title">Deskripsi: </h3>
                                    <p>{!! $item->description !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection