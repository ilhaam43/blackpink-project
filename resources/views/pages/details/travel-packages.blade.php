@extends('layouts.booking')

@section('title')
  Tour Details | daysOff Official Website
@endsection

@section('body-class')
  <body class="body-details">
@endsection

@section('content')
  <!-- Page Heading/Breadcrumbs -->
  <main>
    <section class="section-header" style="background-image: url('{{url('../frontend/images/tour-details/Banner\ Header\ Section.jpg')}}');">
      <div class="container">
        <h1 class="mt-5 mb-3">Tour Details
        </h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{route('Home')}}">Home</a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{route('Home')}}#paketPerjalanan">Tour Packages</a>
          </li>
          <li class="breadcrumb-item active">{{$item->title}}</li>
        </ol>
      </div>
    </section>
    <section class="section-details-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 pl-lg-0">
            <div class="card main-card card-details shadow mb-5 py-4">
              <div class="container">
                <div class="row">
                  <div class="col">
                    <h1>
                      {{$item->title}}
                    </h1>
                    <h4>
                      {{$item->duration}}
                    </h4>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div id="carouselPaketPerjalanan" class="carousel slide shadow" data-ride="carousel">
                      <ol class="carousel-indicators">
                        @foreach ($item->travelGalleries as $travelGallery)
                          <li data-target="#carouselPaketPerjalanan" data-slide-to="{{$loop->iteration}}" @if ($loop->first) class="active" @endif></li>
                        @endforeach
                      </ol>
                      <div class="carousel-inner">
                        @foreach ($item->travelGalleries as $travelGallery)
                        <div class="carousel-item @if ($loop->first) active @endif">
                          <img src="{{$travelGallery!=NULL ? Storage::url($travelGallery->image) : ''}}" class="d-block w-100" alt="{{$item->title}}.jpg">
                        </div>
                        @endforeach
                      </div>
                      <a class="carousel-control-prev" href="#carouselPaketPerjalanan" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselPaketPerjalanan" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <br>
                    <h2>
                      Tentang Paket Tour
                    </h2>
                    <p>
                      {{$item->about}}
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="travel-details">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="highlights-tab" data-toggle="tab" href="#highlights" role="tab" aria-controls="highlights" aria-selected="true">Highlights</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="itinerary-tab" data-toggle="tab" href="#itinerary" role="tab" aria-controls="itinerary" aria-selected="false">Itinerary</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="includes-tab" data-toggle="tab" href="#includes" role="tab" aria-controls="includes" aria-selected="false">What's Included?</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="tnc-tab" data-toggle="tab" href="#tnc" role="tab" aria-controls="tnc" aria-selected="false">Terms & Conditions</a>
                        </li>
                      </ul>
                      <div class="container">
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="highlights" role="tabpanel" aria-labelledby="highlights-tab">
                            <br>
                            <p><b>{{$item->tripItinerary->highlights ?? ""}}</b></p>
                          </div>
                          <div class="tab-pane fade" id="itinerary" role="tabpanel" aria-labelledby="itinerary-tab">
                            <br>
                              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                @foreach ($days as $day)
                                @if ($day == NULL)
                                  @break
                                @endif
                                <li class="nav-item">
                                  <a class="nav-link @if ($loop->first) active @endif" id="pills-day{{$loop->iteration}}-tab" data-toggle="pill" href="#pills-day{{$loop->iteration}}" role="tab" aria-controls="pills-day{{$loop->iteration}}" aria-selected="true">Day {{$loop->iteration}}</a>
                                </li>
                                @endforeach
                              </ul>
                            <div class="tab-content" id="pills-tabContent">
                              @foreach ($days as $day)
                              @if ($day == NULL)
                                @break
                              @endif
                              <div class="tab-pane fade show @if ($loop->first) active @endif" id="pills-day{{$loop->iteration}}" role="tabpane{{$loop->iteration}}" aria-labelledby="pills-day{{$loop->iteration}}-tab">
                                {{$day}}
                              </div>
                              @endforeach

                              {{-- Coba Pake Accordion --}}
                              {{-- @foreach ($days as $day)
                              @if ($day == NULL)
                                @break
                              @endif
                              <div class="accordion" id="accordionItinerary">
                                <div class="card">
                                  <div class="card-header" id="heading{{$loop->iteration}}">
                                    <h5 class="mb-0">
                                      <button class="btn btn-light @if (!$loop->first) collapsed @endif " type="button" data-toggle="collapse" data-target="#collapse{{$loop->iteration}}" aria-expanded="true" aria-controls="collapse{{$loop->iteration}}">
                                        Itinerary Day {{$loop->iteration}}
                                      </button>
                                    </h5>
                                  </div>
                                  <div id="collapse{{$loop->iteration}}" class="collapse @if ($loop->first) show @endif" aria-labelledby="heading{{$loop->iteration}}" data-parent="#accordionItinerary">
                                    <div class="card-body">
                                      {{$day}}
                                    </div>
                                  </div>
                                </div>
                              @endforeach --}}
                            </div>
                          </div>
                            <div class="tab-pane fade" id="includes" role="tabpanel" aria-labelledby="includes-tab">
                              <br>
                              <div class="row">
                                <div class="col">
                                  <ul class="list-group">
                                    <li class="list-group-item list-group-item-success" aria-current="true"><strong>Biaya Sudah Termasuk</strong></li>
                                    @if($incs)
                                    @for ($i = 0; $i <= 19; $i ++)
                                    @if ($incs[$i] == NULL)
                                      @break
                                    @endif
                                    <li class="list-group-item">{{$incs[$i]}}</li>
                                    @endfor
                                    @endif
                                  </ul>
                                </div>
                                <div class="col">
                                  <ul class="list-group">
                                    <li class="list-group-item list-group-item-danger" aria-current="true"><strong>Biaya Tidak Termasuk</strong></li>
                                    @if($incs)
                                    @for ($i = 20; $i <= 39; $i ++)
                                    @if ($incs[$i] == NULL)
                                    @break
                                    @endif
                                    <li class="list-group-item">{{$incs[$i]}}</li>
                                    @endfor
                                    @endif
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane fade" id="tnc" role="tabpanel" aria-labelledby="tnc-tab">
                              <br>Kontak kami untuk info lebih lanjut!
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col text-center mt-3">
                    <p>Selengkapnya:</p>
                    <a href="{{$item->tripItinerary ? Storage::url($item->tripItinerary->pdf_file) : "" }}" class="btn btn-outline-primary btn-general">
                      Download Itinerary
                      </a>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-4 pl-lg-0">
              <div class="card card-details shadow py-4">
                <div class="row justify-content-center">
                  <div class="col-10 text-center">
                    <h4>Informasi Tour</h4>
                    <hr>
                  </div>
                </div>
                <div class="row justify-content-center">
                  <div class="col-11">
                    <table class="table table-borderless">
                      @if($item->duration != null)
                      <tr>
                        <th>Durasi Perjalanan</th>
                        <td>{{$item->duration}}</td>
                      </tr>
                      @endif
                      @if($item->flight != null)
                      <tr>
                        <th>Penerbangan</th>
                        <td>{{$item->flight}}</td>
                      </tr>
                      @endif
                      @if($item->accomodation != null)
                      <tr>
                        <th>Akomodasi</th>
                        <td>{{$item->accomodation}}</td>
                      </tr>
                      @endif
                      @if($item->land_transport != null)
                      <tr>
                        <th>Transportasi Darat</th>
                        <td>{{$item->land_transport}}</td>
                      </tr>
                      @endif
                      @if($item->meal != null)
                      <tr>
                        <th>Konsumsi</th>
                        <td>{{$item->meal}}</td>
                      </tr>
                      @endif
                      @if($item->travel_docs !=null)
                      <tr>
                        <th>Dokumen Perjalanan</th>
                        <td>{{$item->travel_docs}}</td>
                      </tr>
                      @endif
                      @if($item->price != null)
                      <tr>
                        <th>Harga</th>
                        <td>IDR {{ number_format($item->price) }} jt/PAX</td>
                      </tr>
                      @endif
                    </table>
                  </div>
                  <div class="col-9 text-center">
                    <a href="{{$item->tripItinerary ? Storage::url($item->tripItinerary->pdf_file) : "" }}" class="btn btn-outline-primary btn-general">
                    Full Info
                    </a>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          
          <div class="col-lg-8 pl-lg-0">
            <div class="card tanggal-keberangkatan card-details shadow py-4 my-4">
                <div class="row justify-content-center">
                  <div class="col-10 text-center">
                    <h4>Tanggal Perjalanan</h4>
                  </div>
                </div>
                <div class="row justify-content-center">
                  <div class="col-10">
                    @forelse ($item->departureDates as $departureDate)
                      @if((\Carbon\Carbon::parse($departureDate->departure) >= $now) && 
                        (\Carbon\Carbon::parse($departureDate->arrival) >= $now))
                        <div class="card mb-2 pt-3 px-3">
                          <div class="row">
                            <div class="col-sm-9">
                              <p><strong>{{\Carbon\Carbon::parse($departureDate->departure)->format('d F Y')}} - {{\Carbon\Carbon::parse($departureDate->arrival)->format('d F Y')}}</strong></p>
                              <p class="p-harga"><strong>IDR {{$departureDate->price/1000000}} jt/PAX</strong></p>
                              <hr>
                            </div>
                            
                          </div>
                        </div>
                      @endif
                    @empty   
                      <div class="col text-center mt-4">
                        <b>Belum ada tanggal perjalanan. Silakan hubungi customer service kami.</b>
                      </div>
                    @endforelse
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection