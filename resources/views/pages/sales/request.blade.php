@extends('layouts.sales')

@section('title')
    <title>Request Commission | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Make Commission Request</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('sales.sales.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="booking_tour_id"> Invoice Code </label>
                    <select name="booking_tour_id" required class="form-control select2">
                        <option value="">Pilih Invoice Code</option>
                        @foreach ($completed as $complete)
                        <option value="{{$complete->id}}">{{$complete->tourPackages->title}} ({{$complete->invoice_code}})</option>
                        @endforeach
                    </select>
                    <input type="hidden" name="status_id" value="1">
                </div>
                <button type="submit" class="btn btn-primary btn-block">Make Request</button>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-4">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">My Commission Request</h1>
        </div>
        <div class="card row">
            <div class="card-body col-sm-11 mx-auto">
                <table id="myTable" class="table" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Invoice Code</th>
                            <th>Submitted On</th>
                            <th>Request Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comm as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->bookingTours->invoice_code}}</td>
                                <td>{{\Carbon\Carbon::parse($item->created_at)->format('d F Y')}}</td>
                                <td>
                                    @if($item->status_id == 1)
                                    <span class="badge badge-info badge-sm">{{$item->commitionStatus->name}}</span></td>
                                    @elseif($item->status_id == 2 || $item->status_id == 3)
                                    <span class="badge badge-warning badge-sm">Waiting For Confirmation</span></td>
                                    @elseif($item->status_id == 4)
                                    <span class="badge badge-success badge-sm">Request Approved</span></td>
                                    @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection