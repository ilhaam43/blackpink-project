@extends('layouts.sales')

@section('title')
    <title>Booking | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card col-sm-11 mx-auto">
            <div class="row">
                <h4 class="mt-4 text-center">Booking Tour {{$item->invoice_code}} {{$item->tourPackages->title}} Details</h4>
                <div class="card-body col-sm-5 ml-3 mr-3">
                    <div class="fieldset mt-4">
                        <h1>Booking Info</h1>
                        <div class="mt-4 mb-2 ml-3">
                            <div class="h6">daysOff Info</div>
                            <p>{{$item->info_daysoff}}</p>
                        </div>
                    </div>
                </div>

                <div class="card-body col-sm-6 ml-3 mr-3">
                    <div class="fieldset mt-4">
                        <h1>Passenger Contact Person</h1>
                        @foreach($item->passengerContactPersons as $pic)
                            <div class="mt-4 mb-2 ml-3">
                                <div class="h6">Name</div>
                                <p>{{$pic->first_name}} {{$pic->last_name}}</p>
                            
                                <div class="h6">Gender</div>
                                <p>{{$pic->gender}}</p>

                                <div class="h6">Date of Birth</div>
                                <p>{{(\Carbon\Carbon::parse($pic->date_birth)->format('d F Y'))}}</p>
                            
                                <div class="h6">Email</div>
                                <p>{{$pic->email}}</p>
                            
                                <div class="h6">Address</div>
                                <p>{{$pic->address}}</p>

                                <div class="h6">Phone Number</div>
                                <p>{{$pic->phone_number}}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            
            <div class="card-body col-sm-12 ml-3 mr-3">
                <div class="fieldset mt-4">
                    <h1>Passenger Lists</h1>
                    <div class="mt-4 mb-2 ml-3 mr-3">
                        @foreach($item->passengerLists as $passenger)
                        <h5>Passenger {{$loop->iteration}}</h5>
                        <div class="row">
                            <div class="col-sm-5 ml-3">
                                <div class="h6 mt-2">Title</div>
                                <p>{{$passenger->title}}</p>

                                <div class="h6">Name</div>
                                <p>{{$passenger->first_name}} {{$passenger->last_name}}</p>

                                <div class="h6">Jenis Kelamin</div>
                                <p>{{$passenger->gender}}</p>

                                <div class="h6">Date of Birth</div>
                                <p>{{(\Carbon\Carbon::parse($passenger->date_birth)->format('d F Y'))}}</p>

                                <div class="h6">Email</div>
                                <p>{{$passenger->email}}</p>

                                <div class="h6">Address</div>
                                <p>{{$passenger->address}}</p>
                            </div>
                            
                            <div class="col-sm-5 ml-3">
                                <div class="h6 mt-2">Occupation</div>
                                <p>{{$passenger->occupation}}</p>

                                <div class="h6">Phone Number</div>
                                <p>{{$passenger->phone_number}}</p>

                                <div class="h6">Identity Number</div>
                                <p>{{$passenger->identity_number}}</p>

                                <div class="h6">Passport Number</div>
                                <p>{{$passenger->passport_number}}</p>

                                <div class="h6">Social Media</div>
                                <p>{{$passenger->social_media}}</p>
                                
                                <div class="h6">Price</div>
                                <p>Rp{{number_format($passenger->price)}}</p>
                            </div>
                        </div>

                        <div class="ml-3">
                            <h4>Additional Info</h4>
                            
                            <div class="h6 mt-2">Clothes Size</div>
                            <p>{{$passenger->clothing_size}}</p>
                            @if($passenger->food_allergy !=NULL)
                                <div class="h6">Food Allergy</div>
                                <p>{{$passenger->food_allergy}}</p>
                            @endif

                            @if($passenger->booking_details !=NULL)
                                <div class="h6">Booking Details</div>
                                <p>{{$passenger->booking_details}}</p>
                            @endif

                            @if($passenger->booking_notes !=NULL)
                                <div class="h6">Booking Notes</div>
                                <p>{{$passenger->booking_notes}}</p>
                            @endif

                            @if($passenger->medical_history !=NULL)
                                <div class="h6">Medical History</div>
                                <p>{{$passenger->medical_history}}</p>
                            @endif

                            @if($passenger->life_insurance !=NULL)
                                <div class="h6">life Insurance</div>
                                <p>{{$passenger->life_insurance}}</p>
                            @endif

                            <div class="h6">Buy Allianz Insurance?</div>
                            <p>{{!!$passenger->buy_insurance ? "Yes" : "No"}}</p>

                            <div class="h6">Buy Travel Insurance?</div>
                            <p>{{!!$passenger->travel_insurance ? "Yes" : "No"}}</p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection