@extends('layouts.sales')

@section('title')
    <title>Booking Tour | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container body-booking mt-5 mb-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-10">
                <form id="packageBooking" action="{{route('bookingtour.store')}}" method="post">
                    @csrf
                    <h1 id="reservation">Booking Form</h1>
                    <div class="all-steps" id="all-steps"> 
                        <span class="step"><i class="fa fa-user"></i></span> 
                        <span class="step"><i class="fa fa-suitcase"></i></span> 
                        <span class="step" id="passengerIcon"><i class="fa fa-users"></i></span>
                    </div>

                    <div id="bookingDetails">
                        <!-- Contact Person -->
                        <div class="tab">
                            <h4 class="text-center">Tour Package</h4>
                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>Tour Package</h6>
                                    <select name="tour_package" required>
                                        @foreach($tourPackage as $item)
                                        <option value="{{$item->code}}">{{$item->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <h4 class="text-center mt-4">Contact Person Details</h4>
                            <div class="row">
                                <div class="col-sm-3 mt-4">
                                    <h6>Title</h6>
                                    <select name="pic_title" required>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Ms.">Ms.</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mt-4">
                                    <h6>Gender</h6>
                                    <select name="pic_gender" required>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 mt-4">
                                    <h6>First name</h6>
                                    <input type="text" placeholder="Enter First Name" name="pic_first_name" required>
                                </div>
                                <div class="col-sm-3 mt-4">
                                    <h6>Last name</h6>
                                    <input type="text" placeholder="Enter Last Name" name="pic_last_name" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>E-mail</h6>
                                    <input type="email" placeholder="email@email.com" name="pic_email" required>
                                </div>
                                <div class="col-sm-6 mt-4">
                                    <h6>Date of Birth</h6>
                                    <input type="date" name="pic_dob" max="2009-12-31" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>Phone Number</h6>
                                    <input type="tel" placeholder="Phone Number" name="pic_phone" required>
                                </div>

                                <div class="col-sm-6 mt-4">
                                    <h6>Address</h6>
                                    <input type="text" name="pic_address" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 mt-4">
                                    <h6>How Do You Know 'DaysOff Tour & Travel?</h6>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="info_daysoff" value="Website" required>
                                                <label class="form-check-label">
                                                    Website
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="info_daysoff" value="Facebook">
                                                <label class="form-check-label">
                                                    Facebook
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="info_daysoff" value="Instagram">
                                                <label class="form-check-label">
                                                    Instagram
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="info_daysoff" value="Event">
                                                <label class="form-check-label">
                                                    Event/Webinar
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="info_daysoff" value="Rekomendasi Teman">
                                                <label class="form-check-label">
                                                    Rekomendasi Teman
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="info_daysoff" value="Lainnya">
                                                <label class="form-check-label">
                                                    Lainnya
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Reservation For -->
                        <div class="tab" id="passengerTotal">
                            <h4 class="text-center">Reservation</h4>
                            <div class="row">
                                <div class="col-sm-12 mt-4">
                                    <h6>Total Passenger</h6>
                                    <input type="number" name="total_passenger" id="total_passenger" value="1" min="1">
                                </div>
                            </div>  
                        </div>

                        <!-- Passengers -->
                        <div class="tab" id="passengerTab">
                            <div class="passenger" id="passenger_tab[]">
                                <h4 class="text-center">Passenger Details</h4>
                                <p style="color: red;">*Data diisi sesuai dengan yang tertera di paspor</p>
                                <div class="row">
                                    <div class="col-sm-3 mt-4">
                                        <h6>Title</h6>
                                        <select name="passenger_title[]" required>
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 mt-4">
                                        <h6>Gender</h6>
                                        <select name="passenger_gender[]" required>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 mt-4">
                                        <h6>First name</h6>
                                        <input type="text" placeholder="Enter First Name" name="passenger_first_name[]" required>
                                    </div>
                                    <div class="col-sm-3 mt-4">
                                        <h6>Last name</h6>
                                        <input type="text" placeholder="Enter Last Name" name="passenger_last_name[]" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 mt-4">
                                        <h6>Email</h6>
                                        <input type="email" name="passenger_email[]">
                                    </div>
                                    <div class="col-sm-6 mt-4">
                                        <h6>Phone Number</h6>
                                        <input type="tel" name="passenger_phone[]">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6 mt-4">
                                        <h6>Date of Birth</h6>
                                        <input type="date" name="passenger_dob[]" required>
                                    </div>
                                    <div class="col-sm-6 mt-4">
                                        <h6>Address</h6>
                                        <input type="text" name="passenger_address[]" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 mt-4">
                                        <h6>Passport Number</h6>
                                        <input type="text" name="passenger_passport[]" required>
                                    </div>
                                    <div class="col-sm-4 mt-4">
                                        <h6>KTP Number</h6>
                                        <input type="text" name="passenger_identity[]" required>
                                    </div>
                                    <div class="col-sm-4 mt-4">
                                        <h6>Occupation</h6>
                                        <select name="passenger_occupation[]" required>
                                            <option value=""> Choose One </option>
                                            <option value="Pelajar">Pelajar</option>
                                            <option value="Pegawai Negeri">Pegawai Negeri</option>
                                            <option value="Pegawai Swasta">Pegawai Swasta</option>
                                            <option value="Buruh">Buruh</option>
                                            <option value="Wiraswasta">Wiraswasta</option>
                                            <option value="Tidak Bekerja">Tidak Bekerja</option>
                                            <option value="Lainnya">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6 mt-4">
                                        <h6>Social Media (Instagram / Facebook)</h6>
                                        <input type="text" name="passenger_socmed[]">
                                    </div>
                                    <div class="col-sm-6 mt-4">
                                        <h6>Clothes Size (For Merchandise)</h6>
                                        <select name="clothing_size[]" required>
                                            <option value=""> Choose One </option>
                                            <option value="XS">XS</option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                            <option value="L">L</option>
                                            <option value="XL">XL</option>
                                            <option value="XXL">XXL</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 mt-4">
                                        <h6>Price (IDR)</h6>
                                        <input type="text" id="price" class="passenger-price" name="passenger_price[]" min="10000" required>
                                    </div>
                                </div>

                                <h4 class="text-center mt-5">Data Tambahan</h4>
                                <div class="row">

                                <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <h6>Booking Details</h6>
                                    <textarea name="booking_details[]"></textarea>
                                </div>

                                    <div class="col-sm-6 mt-4">
                                        <h6>Booking Request</h6>
                                        <textarea name="booking_notes[]"></textarea>
                                    </div>
                                </div>

                                    <div class="col-sm-6 mt-4">
                                        <h6>Do You Have Any Food Allergies? <p style="color: red;">*Sebutkan</p></h6>
                                        <input type="text" name="food_allergy[]">
                                    </div>

                                    <div class="col-sm-6 mt-4">
                                        <h6>Do You Have Any Medical Conditions? <p style="color: red;">*Sebutkan</p></h6>
                                        <input type="text" name="medical_history[]">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 mt-4">
                                        <h6>Do You Have Life Insurance? <p style="color: red;">*Sebutkan</p></h6>
                                        <input type="text" name="life_insurance[]">
                                        <div class="col-sm-12 mt-4">
                                            <label class="checkbox-inline"> 
                                                <input type="checkbox" value="1" name="buy_insurance[]"> I Want to Buy Life Insurance from Allianz Life Insurance?
                                            </label>
                                            </br><input type="checkbox" value="1" name="travel_insurance[]"> I Want to Buy Tour Insurance
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Next Prev Button -->
                    <div style="overflow:auto;" id="nextprevious">
                        <div style="float:right;"> 
                            <button type="button" id="prevBtn" onclick="nextPrev(-1)">
                                <i class="fa fa-angle-double-left"></i>
                            </button> 
                            <button type="button" id="nextBtn" onclick="nextPrev(1)">
                                <i class="fa fa-angle-double-right"></i>
                            </button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('custom-script')
<script>
    var currentTab = 0;
    document.addEventListener("DOMContentLoaded", function(event) {
        showTab(currentTab);
    });

    function showTab(n) {
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }

        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = '<i class="fa fa-arrow-circle-right"></i>';
        } else {
            document.getElementById("nextBtn").innerHTML = '<i class="fa fa-angle-double-right"></i>';
        }
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        var x = document.getElementsByClassName("tab");
        if (n == 1 && !validateForm()) return false;
        x[currentTab].style.display = "none";
        currentTab = currentTab + n;
        if (currentTab >= x.length) { 
            document.getElementById("nextprevious").style.display = "none";
            document.getElementById("all-steps").style.display = "none";
            document.getElementById("reservation").style.display = "none";
            document.getElementById("prevBtn").style.display = "none";
            document.getElementById("nextBtn").style.display = "none";
            document.getElementById("packageBooking").submit();
            return false;
        }
        showTab(currentTab);
    }

    function validateForm() {
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].querySelectorAll("[required]");
        for (i = 0; i < y.length; i++) { 
            if (y[i].value=="" ) { 
                y[i].className +=" invalid" ; valid=false; 
            } 
        } 
        if (valid) { 
            document.getElementsByClassName("step")[currentTab].className +=" finish" ; 
        } return valid; 
    } 
    
    function fixStepIndicator(n) { 
        var i, x=document.getElementsByClassName("step"); 
        for (i=0; i < x.length; i++) { 
            x[i].className=x[i].className.replace(" active", "" ); 
        } x[n].className +=" active" ; 
    }

    $("#total_passenger").bind('input', function () {
        $('#passengerTab').nextAll('#passengerTab').remove();
        $('#passengerIcon').nextAll('#passengerIcon').remove();
        var total_passenger = $(this).val() - 1; 
        var passenger = document.getElementById("passengerTab"),
            passengerIcon = document.getElementById("passengerIcon"),
            i;
            
        for(i=0; i<total_passenger; i++){
            var clone = passenger.cloneNode(true);
            var cloneIcon = passengerIcon.cloneNode(true);
            document.getElementById("bookingDetails").appendChild(clone);
            document.getElementById("all-steps").appendChild(cloneIcon);
        }         
    });
</script>

<script>
    $(document).on('keyup', '.passenger-price', function(event) {
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });
</script>
@endpush