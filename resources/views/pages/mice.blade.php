@extends('layouts.app')

@section('title')
    MICE | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content')
    <!-- Page Heading/Breadcrumbs -->
    <section class="section-header" style="background-image: url('frontend/images/mice/Banner\ Header\ Section.jpg');">
        <div class="container">
        <h1 class="mt-5 mb-3">MICE CORPORATE SERVICE
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">MICE CORPORATE SERVICE</li>
        </ol>
        </div>
    </section>

    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 mx-auto">
                <br><br>
                <span class="miceIcon">
                    <i class="fa fa-flag"></i>
                </span>

                <div class="section-heading text-center">
                    <br> <h3 style="font-family: assistant; font-weight: 600;">daysOffTour MICE Corporate Service</h3><br>
                </div>
                <p>Meeting Incentive Conference Exhibition atau yang lebih dikenal dengan sebutan MICE adalah salah satu bentuk pelayanan kami untuk kepentingan Anda, seperti acara gathering kantor, special reward holiday for employee dan lainnya. Rencanakan perjalanan sesuai dengan keinginan Anda mulai dari pilihan destinasi wisata, tiket pesawat, akomodasi, dsb dan kami akan memberikan solusi terbaik sesuai kebutuhan MICE Anda.</p>
                <p>Sebagai salah satu travel agent di Indonesia, kami menyediakan pelayanan untuk kebutuhan MICE Anda seperti:</p>
                <div class="col-lg-4 mx-auto">
                    <ul>
                        <li style="list-style-type:square;">Tiket Pesawat untuk Perjalanan Dinas</li>
                        <li style="list-style-type:square;">Akomodasi (Hotel, Villa, Apartment, dll)</li>
                        <li style="list-style-type:square;">Transportasi lainnya (Private Car / Bus)</li>
                        <li style="list-style-type:square;">Penawaran harga spesial untuk perjalanan bisnis, perjalanan karyawan dll</li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="section-heading text-center">
                    </br>
                    <h4>Contact Us By</h4></br>
                </div>
                <p><i class="fa fa-whatsapp"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="https://api.whatsapp.com/send?phone=628119728228">+62 811 9728 228</a> (Call)
                </p>
                <p><i class="fa fa-envelope-o"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="mailto:management@daysofftour.com">management@daysofftour.com</a>
                </p>
            </div>
        </div>
    </div>      
@endsection