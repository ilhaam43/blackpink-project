@extends('layouts.app')

@section('title')
    Attractions| daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content') 
    <section class="section-header" style="background-image: url('frontend/images/attractions/Banner\ Header\ Section.jpg');">
        <div class="container">
            <h1 class="mt-5 mb-3">Attractions
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
                </li>
                <li class="breadcrumb-item active">Attractions</li>
            </ol>
        </div>
    </section>

    <section class="section-featured" id="featured-destinations">
        <div class="container">
            <div class="section-heading text-center" id="attractions">
                <h2>Attractions</h2>
            </div>
        </div>
            <div class="container">
            <div class="featured-destinations row">
                <div class="col-lg-3 col-sm-3 portfolio-item mb-3">
                    <div class="card pulse-card infinite shadow h-100">
                    <a href=><img class="card-img-top" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/A8_Tokyo_Disneyland.jpg/640px-A8_Tokyo_Disneyland.jpg" alt="" loading ="lazy" height="280"></a>
                    <div class="card-body">
                        <h3 class="card-title">
                        <center><a href="#">DISNEYLAND TOKYO</a></center>
                        </h3>
                        <center><h4 class="card-price">
                        Start From 1 JT/Ticket
                        </h4></center>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 portfolio-item mb-3">
                    <div class="card pulse-card infinite shadow h-100">
                    <a href=><img class="card-img-top" src="https://www.korinatour.co.id/wp-content/uploads/2017/09/4-2.jpg" alt="" loading ="lazy" height="280"></a>
                    <div class="card-body">
                        <h3 class="card-title">
                        <center><a href="#">DISNEYLAND HONGKONG</a></center>
                        </h3>
                        <center><h4 class="card-price">
                        Start From 1 JT/Ticket
                        </h4></center>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 portfolio-item mb-3">
                    <div class="card pulse-card infinite shadow h-100">
                    <a href=><img class="card-img-top" src="https://www.japan-guide.com/g18/4021_top.jpg" alt="" loading ="lazy" height="280"></a>
                    <div class="card-body">
                        <h3 class="card-title">
                        <center><a href="#">UNIVERSAL STUDIO OSAKA</a></center>
                        </h3>
                        <center><h4 class="card-price">
                        Start From 1 JT/Ticket
                        </h4></center>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 portfolio-item mb-3">
                    <div class="card pulse-card infinite shadow h-100">
                    <a href=><img class="card-img-top" src="https://awsimages.detik.net.id/community/media/visual/2019/10/28/c87df998-b8b6-4b29-a77f-7f169826c1dd.jpeg?w=1200" alt="" loading ="lazy" height="280"></a>
                    <div class="card-body">
                        <h3 class="card-title">
                        <center><a href="#">UNIVERSAL STUDIO SINGAPORE</a></center>
                        </h3>
                        <center><h4 class="card-price">
                        Start From 1 JT/Ticket
                        </h4></center>
                    </div>
                    </div>
                </div>
        </div>
    </section>
@endsection