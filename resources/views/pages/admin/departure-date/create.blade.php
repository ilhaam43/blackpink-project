@extends('layouts.admin')

@section('title')
    <title>Departure Date | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Tanggal Keberangkatan</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('departure-date.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="travel_package_code"> Paket Perjalanan </label>
                    <select name="travel_package_code" required class="form-control select2">
                        <option value="">Pilih Paket Perjalanan</option>
                        @foreach ($travelPackages as $travelPackage)
                        <option value="{{$travelPackage->code}}">{{$travelPackage->title}} ({{$travelPackage->code}})</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="departure">Departure Date</label>
                    <input type="date" class="form-control" name="departure" placeholder="DD/MM/YYYY" value="{{old('departure')}}">
                    <small class="form-text text-muted">Masukkan tanggal perjalanan dalam format DD/MM/YYYY (PENTING)</small>
                </div>
                <div class="form-group">
                    <label for="arrival">Arrival Date</label>
                    <input type="date" class="form-control" name="arrival" placeholder="DD/MM/YYYY" value="{{old('arrival')}}">
                    <small class="form-text text-muted">Masukkan tanggal ketibaan kembali dalam format DD/MM/YYYY (PENTING)</small>
                </div>
                <div class="form-group">
                    <label for="price">Price/PAX (IDR)</label>
                    <input type="text" class="form-control" name="price" placeholder="Harga Paket" value="{{old('price')}}">
                    <small class="form-text text-muted">Masukkan harga paket (DALAM IDR, ANGKA SAJA)</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection