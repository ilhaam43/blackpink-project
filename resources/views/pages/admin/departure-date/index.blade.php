@extends('layouts.admin')

@section('title')
    <title>Departure Date | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tanggal Keberangkatan</h1>
            <a href="{{route('departure-date.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Travel Package Name</th>
                                <th>Travel Package Code</th>
                                <th>Departure Dates</th>
                                <th>Arrival Dates</th>
                                <th>Price/PAX (IDR)</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->travelPackage->title}}</td>
                                <td>{{$item->travel_package_code}}</td>
                                <td>{{ \Carbon\Carbon::parse($item->departure)->format('d/m/Y')}}</td>
                                <td>{{ \Carbon\Carbon::parse($item->arrival)->format('d/m/Y')}}</td>
                                <td>{{number_format($item->price, 0, ",", ".")}}</td>
                                <td class="text-center">
                                    <a href="{{route('departure-date.edit', $item->id)}}" class="btn btn-outline-success btn-sm" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('departure-date.destroy', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{ $item->travelPackage->title }} departure date?');" title="delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="7" class="text-center">Belum Ada Tanggal Keberangkatan</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection