@extends('layouts.admin')

@section('title')
    <title>Departure Date | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Tanggal Keberangkatan {{$item->travelPackage->title}}</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="{{route('departure-date.update', $item->id)}}" method="post">
                @method('PUT')
                @csrf
                <label for="travel_package_code"> Paket Perjalanan </label>
                <select name="travel_package_code" required class="form-control select2">
                    @foreach ($travelPackages as $travelPackage)
                        @if($travelPackage->code == $item->travel_package_code)
                            <option value="{{$item->travel_package_code}}" selected>{{$travelPackage->title}}</option>
                        @else
                            <option value="{{$travelPackage->code}}">{{$travelPackage->title}}</option>
                        @endif
                    @endforeach
                </select>
                <div class="form-group">
                    <label for="departure">Departure Date</label>
                    <input type="date" class="form-control" name="departure" placeholder="DD/MM/YYYY" value="{{$item->departure}}" required>
                    <small class="form-text text-muted">Masukkan tanggal perjalanan dalam format DD/MM/YYYY (PENTING)</small>
                </div>
                <div class="form-group">
                    <label for="arrival">Arrival Date</label>
                    <input type="date" class="form-control" name="arrival" placeholder="DD/MM/YYYY" value="{{$item->arrival}}" required>
                    <small class="form-text text-muted">Masukkan tanggal ketibaan kembali dalam format DD/MM/YYYY (PENTING)</small>
                </div>
                <div class="form-group">
                    <label for="price">Price/PAX (IDR)</label>
                    <input type="text" class="form-control" name="price" placeholder="Harga Paket" value="{{$item->price}}" required>
                    <small class="form-text text-muted">Masukkan harga paket (DALAM IDR, ANGKA SAJA)</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection