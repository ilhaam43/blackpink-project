@extends('layouts.admin')

@section('title')
    <title>Slider Image | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Slider Image</h1>
            <a href="{{route('slider-image.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
        @endif
        @if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($sliderImage as $sliderImages)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    <img src="{{url('storage/'.$sliderImages->image_url)}}" style="width: 200px" class="img-thumbnail" alt="{{$sliderImages->image_url}}" />
                                </td>
                                <td class="text-center">
                                    <a href="{{route('slider-image.edit', $sliderImages->id)}}" class="btn btn-outline-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('slider-image.destroy', $sliderImages->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete this?');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" class="text-center">Belum Ada Slider Image</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
    <!-- /.container-fluid -->

@endsection