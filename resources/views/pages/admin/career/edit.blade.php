@extends('layouts.admin')

@section('title')
    <title>Career | daysOff Official Website</title>
@endsection

@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Career {{$item->title}}</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="{{route('career.update', $item->id)}}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" value="{{$item->title}}">
                            <small class="form-text text-muted">Judul Karir Tidak Boleh Sama</small>
                        </div>
                        <div class="col-sm-6">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" name="image" placeholder="Image">
                            <small class="form-text text-muted">Upload file .jpg</small>
                        </div>
                        
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <label for="name">Description :</label>
                            <textarea class="ckeditor form-control" name="description" value="description">{{$item->description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="is_available">Status Karir</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available0" value="0" @if ($item->is_available == '0') checked @endif>
                            <label class="form-check-label" for="is_available0">
                                Not Available
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available1" value="1" @if ($item->is_available == '1') checked @endif>
                            <label class="form-check-label" for="is_available1">
                                Available
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection