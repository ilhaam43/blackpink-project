@extends('layouts.admin')

@section('title')
    <title>Career | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Career</h1>
            <a href="{{route('career.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->title}}</td>
                                <td>
                                    <img src="{{Storage::url($item->image)}}" style="width: 150px" class="img-thumbnail" alt="{{$item->image}}" />
                                </td>
                                @if($item->is_available !=0)
                                    <td> <span class="badge badge-info badge-md">Available</span> </td>
                                @else
                                    <td> <span class="badge badge-danger badge-md">Not Available</span> </td>
                                @endif
                                <td class="text-center">
                                    <a href="{{route('CareersDetails', $item->slug)}}" class="btn btn-outline-info" title="view">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{route('career.edit', $item->id)}}" class="btn btn-outline-success" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('career.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete {{ $item->title }}?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection