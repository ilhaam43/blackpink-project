@extends('layouts.admin')

@section('title')
    <title>Career | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Career</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('career.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" placeholder="Masukkan Title Career">
                            <small class="form-text text-muted">Judul Karir Tidak Boleh Sama</small>
                        </div>
                        <div class="col-sm-6">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" name="image" placeholder="Image" required>
                            <small class="form-text text-muted">Upload file .jpg dengan size ca. 806px x 436px (Edit ukuran pake canva atau sejenisnya)</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <label for="name">Description :</label>
                            <textarea class="ckeditor form-control" name="description" value="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="is_available">Status Karir</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available0" value="0">
                            <label class="form-check-label" for="is_available1">
                            Not Available
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available1" value="1">
                            <label class="form-check-label" for="is_available1">
                            Available
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection
