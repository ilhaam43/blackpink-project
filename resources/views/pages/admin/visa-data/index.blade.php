@extends('layouts.admin')

@section('title')
    <title>Visa Data Master | daysOff Official Website</title>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Visa Data Master</h1>
    </div>

    @if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
    @endif

    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <a href="{{route('visa-data.create')}}" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add Visa Data</a>
            </br></br>  
            <table id="myTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Country</th>
                        <th>PDF</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($visa as $visas)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$visas->country}}</td>
                            <td class="text-center">@if($visas->pdf_url != NULL)
                                <a href="{{Storage::url($visas->pdf_url)}}"><i class="fas fa-file-pdf" style="color: red;"></i></a>
                                @else
                                    No Data
                                @endif
                            </td>
                            <td>{!! $visas->description !!}</td>
                            <td class="text-center">
                                <a href="{{route('visa-data.edit', $visas->id)}}"><button type="button" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></button></a>
                                <form action="{{route('visa-data.destroy', $visas->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{ $visas->country }} visa?');" title="delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
</div>
@endsection