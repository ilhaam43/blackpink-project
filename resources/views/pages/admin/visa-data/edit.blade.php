@extends('layouts.admin')

@section('title')
    <title>Visa Data Master | daysOff Official Website</title>
@endsection

@section('content')
    
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Data Visa</h1>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card shadow">
                        <div class="card-body">
                            <form action="{{route('visa-data.update', $visa->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label>Country </label>
                                    <input type="text" name="country" class="form-control" value="{{$visa->country}}">
                                </div>
                                <div class="col-sm-6">
                                    <label for="visa_pdf">PDF File</label>
                                    <input type="file" class="form-control" name="visa_pdf" placeholder="PDF File">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <textarea class="ckeditor form-control" name="description" value="description">{{$visa->description}}</textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
@endsection