@extends('layouts.admin')

@section('title')
    <title>Edit Flyer Image | daysOff Official Website</title>
@endsection

@section('content')
    
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Flyer Image</h1>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <form action="{{route('flyer-image.update', $flyerImage->id)}}" method="post"
                                enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="image">Flyer Image</label></br>
                                <input type="file" class="form-control" name="flyer_image" id="image" placeholder="Image">
                                <small class="form-text text-muted">Upload file .jpg sesuaikan ukuran dengan ukuran gambar flyer (Edit ukuran pake canva atau sejenisnya)</small>
                                <img src="{{ Storage::url($flyerImage->image_url) }}" width="200px" class="rounded mx-auto d-block" id="preview-img">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script> 
    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        previewImage(this);
    });
</script>
@endpush