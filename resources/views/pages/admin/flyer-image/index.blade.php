@extends('layouts.admin')

@section('title')
    <title>Flyer Image | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Flyer Image</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('success'))
        <div class="alert alert-success">
            <ul>
                <li>{{ session('success') }}</li>
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($flyerImage as $flyerImages)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    <img src="{{url('storage/'.$flyerImages->image_url)}}" style="width: 200px" class="img-thumbnail" alt="{{$flyerImages->image_url}}" />
                                </td>
                                <td class="text-center">
                                    <a href="{{route('flyer-image.edit', $flyerImages->id)}}" class="btn btn-outline-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" class="text-center">Belum Ada Flyer Image</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
    <!-- /.container-fluid -->

@endsection