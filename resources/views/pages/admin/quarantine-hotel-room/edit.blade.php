@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Tipe Kamar {{$item->room_name}}</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('quarantine-hotel-room.update', $item->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="hotel_name">Room Type Name</label>
                    <input type="text" class="form-control" name="room_name" placeholder="Hotel Name" value="{{$item->room_name}}" required>
                    <small class="form-text text-muted">Nama Tipe Kamar Hotel Karantina</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection