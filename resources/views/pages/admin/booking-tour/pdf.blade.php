<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Booking Tour {{$item->booking_code}} {{$item->tourPackages->title}} Details</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

    <table width="100%">
        <tr>
            <td valign="top"><img src="{{ public_path('frontend/images/temp/daysoff-logo.jpg')}}" width="150"></td>
            <td align="right">
                <h3>daysOffTour Tour & Travel</h3>
                <pre>
                    vOffice East
                    Jl. Perserikatan No. 1, RT.002/RW.008
                    Rawamangun, Kec. Pulo Gadung, Kota Jakarta Timur
                    13220 DKI Jakarta
                    0811 9728 228
                </pre>
            </td>
        </tr>
    </table>

    <h4>{{$item->tourPackages->title}} {{$item->tourPackages->code}}</h4>
    <h4>Date of Issue: {{(\Carbon\Carbon::parse($item->created_at)->setTimezone('Asia/Jakarta')->format('d-M-y H:i').' (GMT+7)')}}</h4>

    @foreach($item->passengerContactPersons as $pic)
    <h4 style="text-transform: uppercase;">Contact Person</h4>
    <table width="100%">
        <tr>    
            <td><strong>Name: </strong>{{$pic->first_name}} {{$pic->last_name}}</td>
        </tr>
        <tr>
            <td><strong>Gender: </strong>{{$pic->gender}}</td>
        </tr>
        <tr>
            <td><strong>Date of Birth: </strong>{{(\Carbon\Carbon::parse($pic->date_birth)->format('d F Y'))}}</td>
        </tr>
        <tr>
            <td><strong>Email: </strong>{{$pic->email}}</td>
        </tr>
        <tr>
            <td><strong>Address: </strong>{{$pic->address}}</td>
        </tr>
        <tr>
            <td><strong>Phone Number: </strong>{{$pic->phone_number}}</td>
        </tr>
        
    </table>
    @endforeach

    <br/>

    <h4 style="text-transform: uppercase;">Passenger Lists</h4>
    <table width="100%">
        <thead style="background-color: lightgray;">
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Name</th>
            <th>Gender</th>
            <th>DOB</th>
            <th>Identity Number</th>
            <th>Passport Number</th>
            <th>Email</th>
            <th>Address</th>
            <th>Phone Number</th>
        </tr>
        </thead>
        <tbody>
        @foreach($item->passengerLists as $passenger)
        <tr>
            <th scope="row"> {{$loop->iteration}}</th>
            <td>{{$passenger->title}}</td>
            <td>{{$passenger->first_name}} {{$passenger->last_name}}</td>
            <td>@if($passenger->gender == "Female") F @else M @endif</td>
            <td>{{(\Carbon\Carbon::parse($passenger->date_birth)->format('d-m-Y'))}}</td>
            <td>{{$passenger->identity_number}}</td>
            <td>{{$passenger->passport_number}}</td>
            <td>{{$passenger->email}}</td>
            <td>{{$passenger->address}}</td>
            <td>{{$passenger->phone_number}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <h4 style="text-transform: uppercase;">Passenger Details</h4>
    <table width="100%">
        <thead style="background-color: lightgray;">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Occupation</th>
            <th>Social Media</th>
            <th>Clothing Size</th>
            <th>Booking Details</th>
            <th>Booking Notes</th>
            <th>Food Allergy</th>
            <th>Medical History</th>
            <th>Life Insurance</th>
            <th>Travel Insurance</th>
        </tr>
        </thead>
        <tbody>
        @foreach($item->passengerLists as $passenger)
        <tr>
            <th scope="row"> {{$loop->iteration}}</th>
            <td>{{$passenger->first_name}} {{$passenger->last_name}}</td>
            <td>{{$passenger->occupation}}</td>
            <td>@if($passenger->social_media !=NULL) {{$passenger->social_media}} @else None @endif</td>
            <td>{{$passenger->clothing_size}}</td>
            <td>@if($passenger->booking_details !=NULL) {{$passenger->booking_details}} @else None @endif</td>
            <td>@if($passenger->booking_notes !=NULL) {{$passenger->booking_notes}} @else None @endif</td>
            <td>@if($passenger->food_allergy !=NULL) {{$passenger->food_allergy}} @else None @endif</td>
            <td>@if($passenger->medical_history !=NULL) {{$passenger->medical_history}} @else None @endif</td>
            <td>@if($passenger->buy_insurance == 1) Yes @else No @endif</td>
            <td>@if($passenger->travel_insurance == 1) Yes @else No @endif</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>