@foreach($item->passengerContactPersons as $pic)
<table>
    <thead>
        <tr>
            <th colspan="2">{{$item->tourPackages->title}} {{$item->tourPackages->code}} | {{$item->invoice_code}}</th>
        </tr>
        <tr>
            <th colspan="2">Contact Person</th>
        </tr>
        <tr>
            <th>Name</th>
            <td>{{$pic->first_name}} {{$pic->last_name}}</td>
        </tr>
        <tr>
            <th>Gender</th>
            <td>{{$pic->gender}}</td>
        </tr>
        <tr>
            <th>DOB</th>
            <td>{{(\Carbon\Carbon::parse($pic->date_birth)->format('d F Y'))}}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{$pic->email}}</td>
        </tr>
        <tr>
            <th>Address</th>
            <td>{{$pic->address}}</td>
        </tr>
        <tr>
            <th>Phone Number</th>
            <td>{{$pic->phone_number}}</td>
        </tr>
        
    </thead>
    
</table>
@endforeach


<table>
    <thead>
        <tr>
            <th colspan="15">Booking ID: {{$item->booking_code}} | Date of Issue: {{(\Carbon\Carbon::parse($item->created_at)->setTimezone('Asia/Jakarta')->format('d-M-y H:i').' (GMT+7)')}}</th>
        </tr>
        <tr>
            <th colspan="15">Passengers</th>
        </tr>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Title</th>
            <th>Gender</th>
            <th>DOB</th>
            <th>Identity Number</th>
            <th>Passport Number</th>
            <th>Email</th>
            <th>Address</th>
            <th>Phone Number</th>
            <th>Occupation</th>
            <th>Social Media</th>
            <th>Clothing Size</th>
            <th>Food Allergy</th>
            <th>Medical History</th>
            <th>Life Insurance</th>
        </tr>
    </thead>

    <tbody>
    @foreach($item->passengerLists as $passenger)
    <tr>
        <th scope="row"> {{$loop->iteration}}</th>
        <td>{{$passenger->first_name}} {{$passenger->last_name}}</td>
        <td>{{$passenger->title}}</td>
        <td>@if($passenger->gender == "Female") F @else M @endif</td>
        <td>{{(\Carbon\Carbon::parse($passenger->date_birth)->format('d-m-Y'))}}</td>
        <td>'{{$passenger->identity_number}}'</td>
        <td>{{$passenger->passport_number}}</td>
        <td>{{$passenger->email}}</td>
        <td>{{$passenger->address}}</td>
        <td>{{$passenger->phone_number}}</td>
        <td>{{$passenger->occupation}}</td>
        <td>@if($passenger->social_media !=NULL) {{$passenger->social_media}} @else None @endif</td>
        <td>{{$passenger->clothing_size}}</td>
        <td>@if($passenger->food_allergy !=NULL) {{$passenger->food_allergy}} @else None @endif</td>
        <td>@if($passenger->medical_history !=NULL) {{$passenger->medical_history}} @else None @endif</td>
        <td>@if($passenger->life_insurance !=NULL) {{$passenger->life_insurance}} @else None @endif</td>
    </tr>
    @endforeach
    </tbody>
</table>