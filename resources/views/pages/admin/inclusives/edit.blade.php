@extends('layouts.admin')

@section('title')
    <title>Inclusive | daysOff Official Website</title>
@endsection

@section('content')
    
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Biaya Termasuk / Tidak Termasuk Perjalanan {{$item->travelPackage->title}}</h1>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <form action="{{route('inclusives.update', $item->id)}}" method="post">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <label for="travel_package_id"> Paket Perjalanan </label><br>
                                    <span class="badge badge-dark">{{$item->travelPackage->title}} ({{$item->travelPackage->code}})</span>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="incl1">Included Service 1</label>
                                                <textarea name="incl1" rows="2" class="d-block w-100 form-control">{{$item->incl1}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 1</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl2">Included Service 2</label>
                                                <textarea name="incl2" rows="2" class="d-block w-100 form-control">{{$item->incl2}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 2</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl3">Included Service 3</label>
                                                <textarea name="incl3" rows="2" class="d-block w-100 form-control">{{$item->incl3}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 3</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl4">Included Service 4</label>
                                                <textarea name="incl4" rows="2" class="d-block w-100 form-control">{{$item->incl4}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 4</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl5">Included Service 5</label>
                                                <textarea name="incl5" rows="2" class="d-block w-100 form-control">{{$item->incl5}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 5</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl6">Included Service 6</label>
                                                <textarea name="incl6" rows="2" class="d-block w-100 form-control">{{$item->incl6}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 6</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl7">Included Service 7</label>
                                                <textarea name="incl7" rows="2" class="d-block w-100 form-control">{{$item->incl7}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 7</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl8">Included Service 8</label>
                                                <textarea name="incl8" rows="2" class="d-block w-100 form-control">{{$item->incl8}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 8</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl9">Included Service 9</label>
                                                <textarea name="incl9" rows="2" class="d-block w-100 form-control">{{$item->incl9}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 9</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl10">Included Service 10</label>
                                                <textarea name="incl10" rows="2" class="d-block w-100 form-control">{{$item->incl10}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 10</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl11">Included Service 11</label>
                                                <textarea name="incl11" rows="2" class="d-block w-100 form-control">{{$item->incl11}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 11</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl12">Included Service 12</label>
                                                <textarea name="incl12" rows="2" class="d-block w-100 form-control">{{$item->incl12}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 12</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl13">Included Service 13</label>
                                                <textarea name="incl13" rows="2" class="d-block w-100 form-control">{{$item->incl13}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 13</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl14">Included Service 14</label>
                                                <textarea name="incl14" rows="2" class="d-block w-100 form-control">{{$item->incl14}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 14</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl15">Included Service 15</label>
                                                <textarea name="incl15" rows="2" class="d-block w-100 form-control">{{$item->incl15}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 15</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl16">Included Service 16</label>
                                                <textarea name="incl16" rows="2" class="d-block w-100 form-control">{{$item->incl16}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 16</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl17">Included Service 17</label>
                                                <textarea name="incl17" rows="2" class="d-block w-100 form-control">{{$item->incl17}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 17</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl18">Included Service 18</label>
                                                <textarea name="incl18" rows="2" class="d-block w-100 form-control">{{$item->incl18}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 18</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl19">Included Service 19</label>
                                                <textarea name="incl19" rows="2" class="d-block w-100 form-control">{{$item->incl19}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 19</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="incl20">Included Service 20</label>
                                                <textarea name="incl20" rows="2" class="d-block w-100 form-control">{{$item->incl20}}</textarea>
                                                <small class="form-text text-muted">Masukkan data included service 20</small>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="uincl1">Unincluded Service 1</label>
                                                <textarea name="uincl1" rows="2" class="d-block w-100 form-control">{{$item->uincl1}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 1</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl2">Unincluded Service 2</label>
                                                <textarea name="uincl2" rows="2" class="d-block w-100 form-control">{{$item->uincl2}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 2</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl3">Unincluded Service 3</label>
                                                <textarea name="uincl3" rows="2" class="d-block w-100 form-control">{{$item->uincl3}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 3</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl4">Unincluded Service 4</label>
                                                <textarea name="uincl4" rows="2" class="d-block w-100 form-control">{{$item->uincl4}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 4</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl5">Unincluded Service 5</label>
                                                <textarea name="uincl5" rows="2" class="d-block w-100 form-control">{{$item->uincl5}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 5</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl6">Unincluded Service 6</label>
                                                <textarea name="uincl6" rows="2" class="d-block w-100 form-control">{{$item->uincl6}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 6</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl7">Unincluded Service 7</label>
                                                <textarea name="uincl7" rows="2" class="d-block w-100 form-control">{{$item->uincl7}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 7</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl8">Unincluded Service 8</label>
                                                <textarea name="uincl8" rows="2" class="d-block w-100 form-control">{{$item->uincl8}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 8</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl9">Unincluded Service 9</label>
                                                <textarea name="uincl9" rows="2" class="d-block w-100 form-control">{{$item->uincl9}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 9</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl10">Unincluded Service 10</label>
                                                <textarea name="uincl10" rows="2" class="d-block w-100 form-control">{{$item->uincl10}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 10</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl11">Unincluded Service 11</label>
                                                <textarea name="uincl11" rows="2" class="d-block w-100 form-control">{{$item->uincl11}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 11</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl12">Unincluded Service 12</label>
                                                <textarea name="uincl12" rows="2" class="d-block w-100 form-control">{{$item->uincl12}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 12</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl13">Unincluded Service 13</label>
                                                <textarea name="uincl13" rows="2" class="d-block w-100 form-control">{{$item->uincl13}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 13</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl14">Unincluded Service 14</label>
                                                <textarea name="uincl14" rows="2" class="d-block w-100 form-control">{{$item->uincl14}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 14</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl15">Unincluded Service 15</label>
                                                <textarea name="uincl15" rows="2" class="d-block w-100 form-control">{{$item->uincl15}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 15</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl16">Unincluded Service 16</label>
                                                <textarea name="uincl16" rows="2" class="d-block w-100 form-control">{{$item->uincl16}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 16</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl17">Unincluded Service 17</label>
                                                <textarea name="uincl17" rows="2" class="d-block w-100 form-control">{{$item->uincl17}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 17</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl18">Unincluded Service 18</label>
                                                <textarea name="uincl18" rows="2" class="d-block w-100 form-control">{{$item->uincl18}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 18</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl19">Unincluded Service 19</label>
                                                <textarea name="uincl19" rows="2" class="d-block w-100 form-control">{{$item->uincl19}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 19</small>
                                            </div>
                                            <div class="form-group">
                                                <label for="uincl20">Unincluded Service 20</label>
                                                <textarea name="uincl20" rows="2" class="d-block w-100 form-control">{{$item->uincl20}}</textarea>
                                                <small class="form-text text-muted">Masukkan data unincluded service 20</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                </form>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
@endsection

