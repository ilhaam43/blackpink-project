@extends('layouts.admin')

@section('title')
    <title>Inclusive | daysOff Official Website</title>
@endsection

@section('content')
    
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Biaya Termasuk / Tidak Termasuk</h1>
                        <a href="{{route('inclusives.create')}}" class="btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-plus fa-sm text-white-50"></i> Add New
                        </a>
                    </div>

                    <div class="row">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="myTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Travel Package Name</th>
                                            <th>Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($items as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->travelPackage->title}}</td>
                                            <td>{{$item->travelPackage->code}}</td>      
                                            <td class="text-center">
                                                <a href="{{route('inclusives.edit', $item->id)}}" class="btn btn-outline-success" title="edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <form action="{{route('inclusives.destroy', $item->id)}}" method="post" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete {{ $item->travelPackage->title }} inclusives?');" title="delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="4" class="text-center">Belum Ada Data</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                </div>
                <!-- /.container-fluid -->


@endsection