@extends('layouts.admin')

@section('title')
    <title>Destination Category | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Kategori Destinasi {{$item->category_name}}</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('destination-category.update', $item->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="category_name">Category Name</label>
                        <input type="text" class="form-control" name="category_name" placeholder="Category Name" value="{{$item->category_name}}">
                        <small class="form-text text-muted">Nama Kategori Destinasi</small>
                    </div>
                    
                    <div class="col-sm-6">
                        <label for="category_region">Category Region</label>
                        <select name="category_region" required class="form-control select2">
                        <option value="" disabled>Pilih Region</option>
                            <option value="Asia" @if ($item->category_region=='Asia') selected @endif>Asia</option>
                            <option value="America"  @if ($item->category_region=='America') selected @endif>America</option>
                            <option value="Australia"  @if ($item->category_region=='Australia') selected @endif>Australia</option>
                            <option value="Africa"  @if ($item->category_region=='Africa') selected @endif>Africa</option>
                            <option value="Europe"  @if ($item->category_region=='Europe') selected @endif>Europe</option>
                            <option value="Indonesia" @if ($item->category_region=='Indonesia') selected @endif>Indonesia</option>
                            <option value="Middle East"  @if ($item->category_region=='Middle East') selected @endif>Middle East</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="category_details">Category Details</label>
                    <input type="text" class="form-control" name="category_details" value="{{$item->category_details}}">
                </div>
                <div class="form-group">
                    <label for="category_image">Image</label>
                    <input type="file" class="form-control" name="category_image" placeholder="Image">
                    <small class="form-text text-muted">Upload file .jpg dengan size ca. 806px x 436px (Edit ukuran pake canva atau sejenisnya)</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection