@extends('layouts.admin')

@section('title')
    <title>Destination Category | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Kategori Destinasi Paket Travel</h1>
            <a href="{{route('destination-category.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Kategori</th>
                                <th>Region</th>
                                <th>Details</th>
                                <th>Gambar</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->category_name}}</td>
                                <td>{{$item->category_region}}</td>
                                <td>{{$item->category_details}}</td>
                                <td><img src="{{url('storage/'.$item->category_image)}}" style="width: 150px" class="img-thumbnail"></td>
                                <td class="text-center">
                                    <a href="{{route('destination-category.edit', $item->id)}}" class="btn btn-outline-success btn-sm" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('destination-category.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{ $item->category_name }}?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="14" class="text-center">Belum Ada Data Kategori Destinasi</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection