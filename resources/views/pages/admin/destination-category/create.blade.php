@extends('layouts.admin')

@section('title')
    <title>Destination Category | daysOff Official Website</title>
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Kategori Destinasi</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('destination-category.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="category_name">Category Name</label>
                        <input type="text" class="form-control" name="category_name" placeholder="Category Name" value="{{old('category_name')}}">
                        <small class="form-text text-muted">Nama Kategori Destinasi</small>
                    </div>
                    <div class="col-sm-6">
                        <label for="category_region">Category Region</label>
                        <select name="category_region" required class="form-control select2">
                            <option value="" disabled>Pilih Region</option>
                            <option value="Asia">Asia</option>
                            <option value="America">America</option>
                            <option value="Australia">Australia</option>
                            <option value="Africa">Africa</option>
                            <option value="Europe">Europe</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="Middle East">Middle East</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="category_details">Category Details</label>
                    <input type="text" class="form-control" name="category_details">
                </div>
                <div class="form-group">
                    <label for="category_image">Category Image</label>
                    <input type="file" class="form-control" name="category_image" required>
                    <small class="form-text text-muted">Upload file .jpg dengan size ca. 806px x 436px (Edit ukuran pake canva atau sejenisnya)</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection