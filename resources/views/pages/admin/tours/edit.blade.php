@extends('layouts.admin')

@section('title')
    <title>Tour Offer | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Tour Offer</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('tour-offers.update', $item->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf

                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="tour_title"> Judul </label>
                        <input type="text" class="form-control" name="tour_title" placeholder="Judul Tour Offer" value="{{$item->tour_title}}">
                    </div>

                    <div class="col-sm-6">
                        <label for="tour_description"> Deskripsi </label>
                        <input type="text" class="form-control" name="tour_description" placeholder="Deskripsi Tour Offer" value="{{$item->tour_description}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tour_image">Image</label>
                    <input type="file" class="form-control" name="tour_image" placeholder="Image">
                    <small class="form-text text-muted">Upload file .jpg dengan size ca. 806px x 436px (Edit ukuran pake canva atau sejenisnya)</small>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection