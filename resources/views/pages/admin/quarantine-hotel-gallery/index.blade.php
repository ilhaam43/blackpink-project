@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel Gallery | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Galeri Hotel Karantina</h1>
            <a href="{{route('quarantine-hotel-gallery.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add News
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Hotel name</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->hotels->hotel_name}}</td>
                                <td>
                                    <img src="{{Storage::url($item->image)}}" style="width: 150px" class="img-thumbnail" alt="{{$item->image}}" />
                                </td>
                                <td class="text-center">
                                    <a href="{{route('quarantine-hotel-gallery.edit', $item->id)}}" class="btn btn-outline-success">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <form action="{{route('quarantine-hotel-gallery.destroy', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete {{ $item->hotels->hotel_name }} Image?');">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

@endsection