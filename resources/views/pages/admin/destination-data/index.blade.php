@extends('layouts.admin')

@section('title')
    <title>Destination Data Master | daysOff Official Website</title>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Destination Data Master</h1>
    </div>

    <div class="card row">
        <div class="card-body col-sm-11 mx-auto">
            <a href="{{route('destination-data.create')}}" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add Destination Data</a>
            </br></br>  
            <table id="myTable" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Destination</th>
                        <th>Code</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->code}}</td>
                            <td class="text-center">
                                <a href="{{route('destination-data.edit', $item->id)}}"><button type="button" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></button></a>
                                <form action="{{route('destination-data.destroy', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete {{ $item->name }}?');" title="delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection