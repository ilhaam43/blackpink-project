@extends('layouts.admin')

@section('title')
    <title>Trip Itineraries | daysOff Official Website</title>
@endsection

@section('content')
    
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Tambah Itinerary</h1>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card shadow">
                        <div class="card-body">
                            <form action="{{route('trip-itineraries.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="travel_package_id"> Paket Perjalanan </label>
                                <select name="travel_package_id" required class="form-control select2">
                                    <option value="">Pilih Paket Perjalanan</option>
                                    @foreach ($items as $item)
                                    <option value="{{$item->id}}">{{$item->title}} ({{$item->code}})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pdf_file">PDF File</label>
                                <input type="file" class="form-control" name="pdf_file" placeholder="PDF File">
                            </div>
                            <div class="form-group">
                                <label for="highlights">Trip Highlights</label>
                                <input type="text" class="form-control" name="highlights" value="{{old('highlights')}}">
                                <small class="form-text text-muted">List trip highlights seperti atraksi, experience, etc. (e.g. Disneyland; Balon Udara; Ski; etc.)</small>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day1">Day 1</label>
                                    <textarea name="day1" rows="10" class="d-block w-100 form-control">{{old('day1')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 1</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day2">Day 2</label>
                                    <textarea name="day2" rows="10" class="d-block w-100 form-control">{{old('day2')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 2</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day3">Day 3</label>
                                    <textarea name="day3" rows="10" class="d-block w-100 form-control">{{old('day3')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 3</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day4">Day 4</label>
                                    <textarea name="day4" rows="10" class="d-block w-100 form-control">{{old('day4')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 4</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day5">Day 5</label>
                                    <textarea name="day5" rows="10" class="d-block w-100 form-control">{{old('day5')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 5</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day6">Day 6</label>
                                    <textarea name="day6" rows="10" class="d-block w-100 form-control">{{old('day6')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 6</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day7">Day 7</label>
                                    <textarea name="day7" rows="10" class="d-block w-100 form-control">{{old('day7')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 7</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day8">Day 8</label>
                                    <textarea name="day8" rows="10" class="d-block w-100 form-control">{{old('day8')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 8</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day9">Day 9</label>
                                    <textarea name="day9" rows="10" class="d-block w-100 form-control">{{old('day9')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 9</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day10">Day 10</label>
                                    <textarea name="day10" rows="10" class="d-block w-100 form-control">{{old('day10')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 10</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day11">Day 11</label>
                                    <textarea name="day11" rows="10" class="d-block w-100 form-control">{{old('day11')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 11</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day12">Day 12</label>
                                    <textarea name="day12" rows="10" class="d-block w-100 form-control">{{old('day12')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 12</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day13">Day 13</label>
                                    <textarea name="day13" rows="10" class="d-block w-100 form-control">{{old('day13')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 13</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day14">Day 14</label>
                                    <textarea name="day14" rows="10" class="d-block w-100 form-control">{{old('day14')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 14</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day15">Day 15</label>
                                    <textarea name="day15" rows="10" class="d-block w-100 form-control">{{old('day15')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 15</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day16">Day 16</label>
                                    <textarea name="day16" rows="10" class="d-block w-100 form-control">{{old('day16')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 16</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day17">Day 17</label>
                                    <textarea name="day17" rows="10" class="d-block w-100 form-control">{{old('day17')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 17</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day18">Day 18</label>
                                    <textarea name="day18" rows="10" class="d-block w-100 form-control">{{old('day18')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 18</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day19">Day 19</label>
                                    <textarea name="day19" rows="10" class="d-block w-100 form-control">{{old('day19')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 19</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day20">Day 20</label>
                                    <textarea name="day20" rows="10" class="d-block w-100 form-control">{{old('day20')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 20</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day21">Day 21</label>
                                    <textarea name="day21" rows="10" class="d-block w-100 form-control">{{old('day21')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 21</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day22">Day 22</label>
                                    <textarea name="day22" rows="10" class="d-block w-100 form-control">{{old('day22')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 22</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day23">Day 23</label>
                                    <textarea name="day23" rows="10" class="d-block w-100 form-control">{{old('day23')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 23</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day24">Day 24</label>
                                    <textarea name="day24" rows="10" class="d-block w-100 form-control">{{old('day24')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 24</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day25">Day 25</label>
                                    <textarea name="day25" rows="10" class="d-block w-100 form-control">{{old('day25')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 25</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day26">Day 26</label>
                                    <textarea name="day26" rows="10" class="d-block w-100 form-control">{{old('day26')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 26</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day27">Day 27</label>
                                    <textarea name="day27" rows="10" class="d-block w-100 form-control">{{old('day27')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 27</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day28">Day 28</label>
                                    <textarea name="day28" rows="10" class="d-block w-100 form-control">{{old('day28')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 28</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label for="day29">Day 29</label>
                                    <textarea name="day29" rows="10" class="d-block w-100 form-control">{{old('day29')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 29</small>
                                </div>
                                <div class="col-sm-6">
                                    <label for="day30">Day 30</label>
                                    <textarea name="day30" rows="10" class="d-block w-100 form-control">{{old('day30')}}</textarea>
                                    <small class="form-text text-muted">Penjelasan Itinerary Day 30</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
@endsection