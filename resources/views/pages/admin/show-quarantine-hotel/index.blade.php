@extends('layouts.admin')

@section('title')
    <title>Travel Package | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Show Quarantine Hotels</h1>
            @if(count($items) <= 5)
            <a href="{{route('show-quarantine-hotel.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
            @endif
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Hotel Name</th>
                                <th>City</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->hotel->hotel_name}}</td>
                                <td>{{$item->hotel->hotel_city}}</td>
                                <td class="text-center">
                                    <a href="{{route('show-quarantine-hotel.edit', $item->id)}}" title="edit" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></a>
                                    <form action="{{route('show-quarantine-hotel.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" class="text-center">Belum Ada Tour Events Di Section</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection