@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel Show | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pilih Hotel Karantina</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('show-quarantine-hotel.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row form-group">
                    <div class="col-sm-12">
                    <label for="category_id">Quarantine Hotels</label>
                        <select name="hotel_id" required class="form-control select2">
                            <option selected disabled>Choose One</option>
                            @foreach ($quarantineHotels as $hotel)
                                <option value="{{$hotel->id}}">{{$hotel->hotel_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection