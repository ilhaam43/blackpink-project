@extends('layouts.admin')

@section('title')
    <title>Transportation | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Transportation {{$item->title}}</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="{{route('transportation.update', $item->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Title" value="{{$item->title}}" required>
                    <small class="form-text text-muted">(TIDAK BOLEH DOUBLE!)</small>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label>Type</label>
                        <select class="form-control select2" name="type">
                            <option disabled selected>Choose One</option>
                            <option value="Car" @if($item->type == 'Car') selected @endif>Car</option>
                            <option value="Bus" @if($item->type == 'Bus') selected @endif>Bus</option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <label for="capacity">Capacity</label>
                        <input type="number" class="form-control" min="0" name="capacity" id="capacity" placeholder="Capacity" value="{{ $item->capacity }}">
                    </div>
                    
                    <div class="col-sm-3">
                        <label for="region">Region</label>
                        <input type="text" class="form-control" name="region" placeholder="Region" value="{{ $item->region }}">
                        <small class="form-text text-muted">Lokasi dalam bahasa inggris (Sebutkan 1 saja daerah, e.g. Europe; Middle-East; Indonesia; etc.)</small>
                    </div>

                    <div class="col-sm-3">
                        <label for="price">Price (IDR)</label>
                        <input type="text" class="form-control" name="price" id="price" placeholder="Transportation Price" value="{{ $item->price }}">
                        <small class="form-text text-muted">Masukkan harga (DALAM IDR, ANGKA SAJA)</small>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="note">Note</label>
                        <input type="text" class="form-control" name="note" placeholder="note" value="{{ $item->note }}">
                        <small class="form-text text-muted">Ex: Toll & Parkir Included</small>
                    </div>

                    <div class="col-sm-6">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control">
                        <small class="form-text text-muted">Upload file .jpg atau .png</small>
                        <img src="{{ Storage::url($item->image) }}" width="200px" class="rounded mx-auto d-block" id="preview-img">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="status">Transportation Status</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available0" value="0" @if($item->is_available == 0) checked @endif>
                            <label class="form-check-label" for="is_available0">Not Available</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available1" value="1" @if($item->is_available == 1) checked @endif>
                            <label class="form-check-label" for="is_available1">Available</label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script>
    $(document).on('keyup', '#price', function(event) {
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });
</script>
<script>
    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        previewImage(this);
    });
</script>
@endpush