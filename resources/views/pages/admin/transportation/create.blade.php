@extends('layouts.admin')

@section('title')
    <title>Transportation | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add New Transportation</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('transportation.store')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Title" value="{{old('title')}}" required>
                    <small class="form-text text-muted">(TIDAK BOLEH DOUBLE!)</small>
                </div>

                <div class="row form-group">
                    <div class="col-sm-3">
                        <label>Type</label>
                        <select class="form-control select2" name="type">
                            <option disabled selected>Choose One</option>
                            <option value="Car">Car</option>
                            <option value="Bus">Bus</option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <label for="capacity">Capacity</label>
                        <input type="number" class="form-control" min="0" name="capacity" id="capacity" placeholder="Capacity" value="{{ old('capacity') }}">
                    </div>
                    
                    <div class="col-sm-3">
                        <label for="region">Region</label>
                        <input type="text" class="form-control" name="region" placeholder="Region" value="{{old('region')}}">
                        <small class="form-text text-muted">Lokasi dalam bahasa inggris (Sebutkan 1 saja daerah, e.g. Europe; Middle-East; Indonesia; etc.)</small>
                    </div>

                    <div class="col-sm-3">
                        <label for="price">Price (IDR)</label>
                        <input type="text" class="form-control" name="price" id="price" placeholder="Transportation Price" value="{{old('price')}}">
                        <small class="form-text text-muted">Masukkan harga (DALAM IDR, ANGKA SAJA)</small>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="note">Note</label>
                        <input type="text" class="form-control" name="note" placeholder="note" value="{{old('note')}}">
                        <small class="form-text text-muted">Ex: Toll & Parkir Included</small>
                    </div>

                    <div class="col-sm-6">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image" id="image" placeholder="Image">
                        <small class="form-text text-muted">Upload file .jpg atau .png</small>
                        <img src="#" width="200px" class="rounded mx-auto d-block" id="preview-img" style="visibility: hidden;">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="status">Transportation Status</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available0" value="0">
                            <label class="form-check-label" for="is_available0">Not Available</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available1" value="1">
                            <label class="form-check-label" for="is_available1">Available</label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script>
    $(document).on('keyup', '#price', function(event) {
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });
</script>
<script>
    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        $('#preview-img').css('visibility', 'visible');
        previewImage(this);
    });
</script>
@endpush