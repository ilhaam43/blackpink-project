@extends('layouts.admin')

@section('title')
    <title>Transportation | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Transportation</h1>
            <a href="{{route('transportation.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                @if ($message = session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Transportation Name</th>
                                <th>Type</th>
                                <th>Region</th>
                                <th>Price</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transportation as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->type }}</td>
                                <td>{{ $item->region }}</td>
                                <td>Rp{{ number_format($item->price) }}</td>
                                <td>{{ $item->note }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#detail{{ $item->id }}"><i class="fa fa-eye"></i></button>
                                    <a href="{{route('transportation.edit', $item->id)}}" class="btn btn-outline-success" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('transportation.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete {{ $item->title }}?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @foreach($transportation as $item)
        <!-- Detail Modal -->
        <div class="modal fade" id="detail{{ $item->id }}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Title</th>
                                    <td>{{ $item->title }}</td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <td>{{ $item->type }}</td>
                                </tr>
                                <tr>
                                    <th>Capacity</th>
                                    <td>{{ $item->capacity }}</td>
                                </tr>
                                <tr>
                                    <th>Region</th>
                                    <td>{{ $item->region }}</td>
                                </tr>
                                <tr>
                                    <th>Price</th>
                                    <td>Rp{{ number_format($item->price) }}</td>
                                </tr>
                                <tr>
                                    <th>Note</th>
                                    <td>{{ $item->note }}</td>
                                </tr>
                                <tr>
                                    <th>Image</th>
                                    <td><img src="{{ Storage::url($item->image) }}" width="200px" class="rounded mx-auto d-block"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        @if($item->is_available == 0)
                                            <p class="text-danger">Not Available</p>
                                        @else
                                            <p class="text-success">Available</p>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- /.container-fluid -->
@endsection