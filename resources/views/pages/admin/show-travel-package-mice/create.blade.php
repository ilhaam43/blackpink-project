@extends('layouts.admin')

@section('title')
    <title>Show Travel Package - MICE | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pilih Tour Package MICE</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('show-travel-package-mice.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row form-group">
                    <div class="col-sm-12">
                    <label for="travel_package_id">Travel Package</label>
                        <select name="travel_package_id" required class="form-control select2">
                            <option selected disabled>Choose One</option>
                            @foreach ($travelPackages as $travel)
                                <option value="{{$travel->id}}">{{$travel->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection