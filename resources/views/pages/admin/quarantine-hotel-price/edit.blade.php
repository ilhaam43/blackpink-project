@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Ubah Harga Kamar Ruangan Hotel </h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('quarantine-hotel-price.update', $item->id)}}" method="post">
                @method('PUT')
                @csrf
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="hotel_id"> Nama Hotel </label>
                        <select name="hotel_id" required class="form-control select2">
                            <option value="">Pilih Hotel</option>
                            @foreach ($hotels as $hotel)
                                @if($item->hotel_id == $hotel->id)
                                    <option value="{{$item->hotel_id}}" selected>{{$hotel->hotel_name}}</option>
                                @else
                                    <option value="{{$hotel->id}}">{{$hotel->hotel_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="room_id">Nama Tipe Ruangan Hotel</label>
                        <select name="room_id" required class="form-control select2">
                            <option value="">Pilih Tipe Ruangan Hotel</option>
                            @foreach($hotelrooms as $room)
                                @if($item->room_id == $room->id)
                                    <option value="{{$item->room_id}}" selected>{{$room->room_name}}</option>
                                @else
                                    <option value="{{$room->id}}">{{$room->room_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="room_occupant">Jumlah Penghuni Ruangan Hotel (Per Person)</label>
                        <input type="number" min="1" required name="room_occupant" class="form-control" value="{{$item->room_occupant}}">
                    </div>
                    <div class="col-sm-6">
                        <label for="room_price">Harga Ruangan Hotel</label>
                        <input type="text"class="form-control" id="room_price" name="room_price" placeholder="IDR" value="{{number_format($item->room_price)}}">
                        <small class="form-text text-muted">(Harga Dalam RUPIAH)</small>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script>
    $(document).on('keyup', '#room_price', function(event) {
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });
</script>
@endpush