@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Harga Hotel Karantina</h1>
            <a href="{{route('quarantine-hotel-price.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="table" width="100%" cellspacing="0">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                <label class="form-label text-dark">Filter By Hotel Name</label>
                                    <select class="form-control select2 form-select" name="hotelName" id="hotelName">
                                        <option selected></option>
                                        @foreach($hotels as $hotel)
                                            <option value="{{ $hotel->hotel_name }}">{{ $hotel->hotel_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                <label class="form-label text-dark">Filter By Room Type</label>
                                    <select class="form-control select2 form-select" name="roomType" id="roomType">
                                        <option selected></option>
                                        @foreach($hotelrooms as $room)
                                            <option value="{{ $room->room_name }}">{{ $room->room_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Hotel</th>
                                <th>Tipe Kamar Karantina</th>
                                <th>Jumlah Pengunjung</th>
                                <th>Harga</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->hotels->hotel_name}}</td>
                                <td>{{$item->rooms->room_name}}</td>
                                <td>{{$item->room_occupant}}</td>
                                <td>Rp{{number_format($item->room_price)}}</td>
                                <td class="text-center">
                                    <a href="{{route('quarantine-hotel-price.edit', $item->id)}}" class="btn btn-outline-success" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('quarantine-hotel-price.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete {{ $item->hotels->hotel_name }} {{ $item->rooms->room_name }} Price?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="14" class="text-center">Belum Ada Data Harga Hotel Karantina</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
    <script>
        $(document).ready( function () {
            var table = $('#table').DataTable({
                responsive: true
            });
            $("#hotelName").on("change", function() {
                table
                .columns(1)
                .search(this.value)
                .draw();
            });
            $("#roomType").on("change", function() {
                table
                .columns(2)
                .search(this.value)
                .draw();
            });
        });
    </script>
@endpush