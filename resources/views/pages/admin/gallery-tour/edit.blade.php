@extends('layouts.admin')

@section('title')
    <title>Gallery Tour | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Gallery Tour</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="{{route('gallery-tour.update', $gallery->id)}}" method="post"
                    enctype="multipart/form-data">
                @method('PUT')
                @csrf

                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="title"> Image Title </label>
                        <input type="text" class="form-control" name="title" id="title" required value="{{ $gallery->title }}">
                    </div>
                    <div class="col-sm-6">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image" id="image" placeholder="Image">
                        <img src="{{ Storage::url($gallery->image) }}" width="200px" class="rounded mx-auto d-block" id="preview-img">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script>
    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        previewImage(this);
    });
</script>
@endpush