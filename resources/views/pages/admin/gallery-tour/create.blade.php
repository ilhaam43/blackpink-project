@extends('layouts.admin')

@section('title')
    <title>Gallery Tour | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Gallery Tour</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('gallery-tour.store')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="title"> Image Title </label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Image Title" required>
                    </div>
                    <div class="col-sm-6">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image" id="image" placeholder="Image" required>
                        <img src="#" width="200px" class="rounded mx-auto d-block" id="preview-img" style="visibility: hidden;">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script>
    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        $('#preview-img').css('visibility', 'visible');
        previewImage(this);
    });
</script>
@endpush