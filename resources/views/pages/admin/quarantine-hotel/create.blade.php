@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tambah Hotel Karantina</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow">
            <div class="card-body">
                <form action="{{route('quarantine-hotel.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="hotel_name">Hotel Name</label>
                    <input type="text" class="form-control" name="hotel_name" placeholder="Hotel Name" value="{{old('hotel_name')}}">
                    <small class="form-text text-muted">Nama Hotel Karantina</small>
                </div>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label for="hotel_address">Address</label>
                        <input type="text" class="form-control" name="hotel_address" placeholder="Address" value="{{old('hotel_address')}}">
                        <small class="form-text text-muted">Alamat Hotel Karantina (e.g. Jl. Kebayoran Lama)</small>
                    </div>

                    <div class="col-sm-6">
                        <label for="hotel_city">City</label>
                        <input type="text" class="form-control" name="hotel_city" placeholder="City" value="{{old('hotel_city')}}">
                        <small class="form-text text-muted">Lokasi Hotel Karantina dalam Bahasa Inggris (Sebutkan 1 saja daerah, e.g. Tangerang, Bekasi.)</small>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label for="hotel_rate">Rate</label>
                        <input type="number" class="form-control" name="hotel_rate" min="1" max="5" value="{{old('hotel_rate')}}">
                        <small class="form-text text-muted">Rating Hotel Karantina (Minimal 1 dan Maksimal 5)</small>
                    </div>
                    <div class="col-sm-3">
                        <label for="chse_score">CHSE Score</label>
                        <input type="number" class="form-control" name="chse_score" min="1" max="100" value="{{old('chse_score')}}">
                        <small class="form-text text-muted">(Minimal 1 dan Maksimal 100)</small>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <label for="name">Description :</label>
                        <textarea class="ckeditor form-control" name="description" value="description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="hotel_status">Status Hotel</label><br>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="hotel_status" id="hotel_status0" value="0">
                        <label class="form-check-label" for="hotel_status1">
                        Not Available
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="hotel_status" id="hotel_status1" value="1">
                        <label class="form-check-label" for="hotel_status1">
                        Available
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection
