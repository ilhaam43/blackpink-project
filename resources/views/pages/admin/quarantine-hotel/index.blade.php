@extends('layouts.admin')

@section('title')
    <title>Quarantine Hotel | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Hotel Karantina</h1>
            <a href="{{route('quarantine-hotel.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Hotel Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Image Total</th>
                                <th>Rate</th>
                                <th>CHSE</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->hotel_name}}</td>
                                <td>{{$item->hotel_address}}</td>
                                <td>{{$item->hotel_city}}</td>
                                <td>
                                    @if($item->hotelGalleries->count()>0)
                                        {{$item->hotelGalleries->count()}}<br>
                                    @else
                                        0
                                    @endif
                                </td>
                                <td>{{$item->hotel_rate}}</td>
                                <td>{{$item->chse_score}}</td>

                                @if($item->hotel_status!=0)
                                    <td> <h6><span class="badge badge-info">Available</span></h6> </td>
                                @else
                                    <td> <h6><span class="badge badge-danger">Not Available</span></h6></td>
                                @endif

                                <td class="text-center">
                                    <a href="{{route('QuarantineHotelsDetails', $item->slug)}}" class="btn btn-outline-info btn-sm" title="view">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{route('quarantine-hotel.edit', $item->id)}}" class="btn btn-outline-success btn-sm" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('quarantine-hotel.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{ $item->hotel_name }}?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="14" class="text-center">Belum Ada Data Hotel Karantina</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection