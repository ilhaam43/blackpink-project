@extends('layouts.admin')

@section('title')
    <title>Travel Package | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Paket Perjalanan {{$item->title}}</h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="{{route('travel-package.update', $item->id)}}" method="post">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="title">Travel Package Name</label>
                    <input type="text" class="form-control" name="title" placeholder="Nama paket perjalanan" value="{{$item->title}}">
                    <small class="form-text text-muted">Masukkan nama paket perjalanan (TIDAK BOLEH DOUBLE!)</small>
                </div>
                <div class="row form-group">
                    <label>Travel Package Code</label>
                    <div class="col-sm-4">
                        <label>Vendor Code</label>
                        <select class="form-control select2" name="vendor_code">
                            <option value="">Choose One</option>
                            @foreach($vendors as $vendor)
                                @if($item->vendor_data_code == $vendor->code)
                                    <option value="{{$item->vendor_data_code}}" selected>{{$vendor->name}} ({{$vendor->code}})</option>
                                @else
                                    <option value="{{$vendor->code}}">{{$vendor->name}} ({{$vendor->code}})</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label>Type</label>
                        <select class="form-control select2" name="type_code">
                            <option value="">Choose One</option>
                            @foreach($types as $type)
                                @if($item->type_data_code == $type->code)
                                    <option value="{{$item->type_data_code}}" selected>{{$type->name}} ({{$type->code}})</option>
                                @else 
                                    <option value="{{$type->code}}">{{$type->name}} ({{$type->code}})</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label>Destination</label>
                        <select class="form-control select2" name="destination_code">
                            <option value="">Choose One</option>
                            @foreach($destinations as $destination)
                                @php
                                    $dest = substr($item->destination_data_code, 0,3);
                                @endphp
                                @if($dest == $destination->code)
                                    <option value="{{$dest}}" selected>{{$destination->name}} ({{$destination->code}})</option>
                                @else
                                    <option value="{{$destination->code}}">{{$destination->name}} ({{$destination->code}})</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="region">Region</label>
                        <input type="text" class="form-control" name="region" placeholder="Region"  value="{{$item->region}}">
                        <small class="form-text text-muted">Lokasi dalam bahasa inggris (Sebutkan 1 saja daerah, e.g. Europe; Middle-East; Indonesia; etc.)</small>
                    </div>
                    
                    <div class="col-sm-4">
                        <label for="category_id">Nama Kategori Destinasi</label>
                        <select name="category_id" required class="form-control select2">
                            <option value="">Pilih Kategori Destinasi</option>
                            @foreach($categories as $category)
                                @if($item->category_id == $category->id)
                                    <option value="{{$item->category_id}}" selected>{{$category->category_name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label for="price">Price (IDR)</label>
                        <input type="text" class="form-control" name="price" id="price" value="{{ $item->price }}">
                        <small class="form-text text-muted">Dalam Rupiah (IDR/Rp)</small>
                    </div>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="special" name="special" value="1" {{ $item->special == 1 ? 'checked="checked"' : '' }}/>
                    <label class="form-check-label" for="special">Is This A Special Offer?</label>
                    <small class="form-text text-muted">Pilih ini untuk memunculkan paket perjalanan di bagian "Special Offers"</small>
                </div>
                <div class="form-group">
                    <label for="about">About</label>
                    <textarea name="about" rows="10" class="d-block w-100 form-control">{{$item->about}}</textarea>
                    <small class="form-text text-muted">Penjelasan paket perjalanan (Jika panjang, beri spasi (ENTER) antar paragraf)</small>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="duration">Duration</label>
                        <input type="text" class="form-control" name="duration" placeholder="Duration" value="{{$item->duration}}">
                        <small class="form-text text-muted">Durasi Perjalanan (e.g. 10D07N; 10 Hari 7 Malam, etc.)</small>
                    </div>
                    <div class="col-sm-4">
                        <label for="flight">Flight</label>
                        <input type="text" class="form-control" name="flight" placeholder="Flight" value="{{$item->flight}}">
                        <small class="form-text text-muted">Masukkan penerbangan atau nomor penerbangan jika ada (e.g. Turkish airlines; SQ 08XXX; etc.)</small>
                    </div>
                    <div class="col-sm-4">
                        <label for="accomodation">Accomodation</label>
                        <input type="text" class="form-control" name="accomodation" placeholder="Accomodation" value="{{$item->accomodation}}">
                        <small class="form-text text-muted">Masukkan detail akomodasi (e.g. Hotel berbintang 4; etc.)</small>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="land_transport">Land Transport</label>
                        <input type="text" class="form-control" name="land_transport" placeholder="Land Transport" value="{{$item->land_transport}}">
                        <small class="form-text text-muted">Masukkan detail transportasi di tempat tujuan (e.g. Bus Wisata; Toyota Alphard; etc.)</small>
                    </div>
                    <div class="col-sm-4">
                        <label for="meal">Meal</label>
                        <input type="text" class="form-control" name="meal" placeholder="Meal" value="{{$item->meal}}">
                        <small class="form-text text-muted">Masukkan detail konsumsi yang disediakan (e.g. Breakfast, Lunch, Dinner; "Lihat full info"; Full Meal etc.)</small>
                    </div>
                    <div class="col-sm-4">
                        <label for="travel_docs">Travel Documents</label>
                        <input type="text" class="form-control" name="travel_docs" placeholder="Travel Documents" value="{{$item->travel_docs}}">
                        <small class="form-text text-muted">Masukkan dokumen perjalanan yang dibutuhkan (e.g. Visa, Paspor, Bukti Vaksinasi, etc.)</small>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4">
                        <label for="travel_status">Status Travel</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available0" value="0" @if ($item->is_available == '0') checked @endif>
                            <label class="form-check-label" for="is_available0">
                            Not Available
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_available" id="is_available1" value="1" @if ($item->is_available == '1') checked @endif>
                            <label class="form-check-label" for="is_available1">
                            Available
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <label for="travel_status">Is Tour Offers</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_tours" id="is_tours0" value="1" @if ($item->is_tours == '1') checked @endif>
                            <label class="form-check-label" for="is_tours0">
                            Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_tours" id="is_tours1" value="0" @if ($item->is_tours == '0') checked @endif>
                            <label class="form-check-label" for="is_tours1">
                            No
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-4">
                            <label for="travel_status">MICE</label><br>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_mice" id="is_mice0" value="1" @if ($item->is_mice == '1') checked @endif>
                            <label class="form-check-label" for="is_mice0">
                            Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_mice" id="is_mice1" value="0" @if ($item->is_mice == '0') checked @endif>
                            <label class="form-check-label" for="is_mice1">
                            No
                            </label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@push('custom-script')
<script>
    $(document).on('keyup', '#price', function(event) {
        if(event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        });
    });
</script>
@endpush