@extends('layouts.admin')

@section('title')
    <title>Travel Package | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Paket Perjalanan</h1>
            <a href="{{route('travel-package.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Country</th>
                                <th>Status</th>
                                <th>Departure Dates</th>
                                <th>Trip Itinerary</th>
                                <th>Price</th>
                                <th>Tour & Events</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td style="width:38px;">{{$item->title}}</td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->country}}</td>
                                @if($item->is_available!=0)
                                    <td> <h6><span class="badge badge-info">Available</span></h6> </td>
                                @else
                                    <td> <h6><span class="badge badge-danger">Not Available</span></h6> </td>
                                @endif
                                <td class="text-center">
                                    @forelse ($item->departureDates as $departureDate)
                                        {{$departureDate->departure}}<br>
                                    @empty
                                        -
                                    @endforelse
                                </td>
                                <td>
                                    @if (isset($item->tripItinerary))
                                        Ada
                                    @else
                                        Belum Ada
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($item->price)
                                        Rp{{ number_format($item->price, 0, '.', ',')  }} 
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($item->is_tours == 1)
                                        Iya
                                    @else
                                        Tidak
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{route('TravelPackagesDetails', $item->slug)}}" title="view" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('travel-package.edit', $item->id)}}" title="edit" class="btn btn-outline-success btn-sm"><i class="fa fa-edit"></i></a>
                                    <form action="{{route('travel-package.destroy', $item->id)}}" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure you want to delete {{ $item->title }}?');" title="delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="14" class="text-center">Belum Ada Paket Perjalanan</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection