@extends('layouts.admin')

@section('title')
    <title>Arrange Section | daysOff Official Website</title>
@endsection

@section('content')
    
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Arrange Section</h1>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Section Name</th>
                                <th>Show/Hide</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->section_name}}</td>
                                <td text-center>
                                    @if($item->is_show == true)
                                        <input type="checkbox" class="is_show" name="is_show" id="{{$item->id}}" checked>
                                    @else
                                        <input type="checkbox" class="is_show" name="is_show" id="{{$item->id}}">
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom-script')
<script>
    $('.is_show').change(function(){
		if($(this).is(':checked')){
			$.ajax({
				url: 'section/' + $(this).attr("id"),
				type: 'PUT',
				context: this,
				data: {
                    "_token": "{{ csrf_token() }}",
					'is_show' : 1,
					'id' : $(this).attr("id")
				},
				success: function(){
					window.location.reload();
				}
			})
		}else{
			$.ajax({
				url: 'section/' + $(this).attr("id"),
				type: 'PUT',
				context: this,
				data: {
                    "_token": "{{ csrf_token() }}",
					'is_show' : 0,
					'id' : $(this).attr("id")
				},
				success: function(){
					window.location.reload();
				}
			})
		}
	})
</script>
@endpush