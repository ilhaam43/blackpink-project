@extends('layouts.admin')

@section('title')
    <title>IG Posts | daysOff Official Website</title>
@endsection

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Staycation IG Post</h1>
            <a href="{{route('staycation-ig-post.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Add New
            </a>
        </div>

        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="myTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Post URL</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->post_url}}</td>
                                <td class="text-center">
                                    <a href="{{route('staycation-ig-post.edit', $item->id)}}" class="btn btn-outline-success" title="edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="{{route('staycation-ig-post.destroy', $item->id)}}" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger" onclick="return confirm('Are you sure you want to delete this?');" title="delete">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="14" class="text-center">Belum Ada Paket Perjalanan</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection