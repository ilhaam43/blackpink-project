@extends('layouts.app')

@section('title')
    Kontak | daysOff Official Website
@endsection

@section('body-class')
    <body class="body-contact-us">
@endsection

@section('content')
    <!-- Main Content -->
    <main>
      <!-- Page Content --> 
      <!-- Page Heading/Breadcrumbs -->
      <section class="section-header" style="background-image: url('frontend/images/contact-us/Banner\ Header\ Section.jpg');">
        <div class="container">
          <h1 class="mt-5 mb-3">Contact Us!
          </h1>
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{url('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Contact</li>
          </ol>
        </div>
      </section>
      <!-- Contact Details -->
      <section class="section-contact-details">
        <div class="container">
          <div class="row">
            <!-- Map Column -->
            <div class="col-lg-8 mb-4">
              <!-- Embedded Google Map -->
              <iframe style="width: 100%; height: 400px; border: 0;" src="https://maps.google.com/maps?q=voffice%20jalan%20perserikatan%20rawamangun&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
            <!-- Contact Details Column -->
            <div class="col-lg-4 mb-4">
              <h3>Konsultasi dan Booking Langsung dengan Kami:</h3>
              <p>
                vOffice East
                <br>Jl. Perserikatan No. 1, RT.002/RW.008
                <br>Rawamangun, Kec. Pulo Gadung, Kota Jakarta Timur
                <br>13220 DKI Jakarta
              </p>
              <p>
                <i class="fa fa-building" aria-hidden="true"></i>  021 2960 1494
              </p>
              <p>
                <i class="fa fa-whatsapp" aria-hidden="true"></i> <a href="https://api.whatsapp.com/send?phone=628119728228">0811 9728 228</a> 
              </p>
              <p>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                  <a href="management@daysofftour.com">management@daysofftour.com</a> <br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="marketing@daysofftour.com">marketing@daysofftour.com</a>
              </p>
              <p>
                <i class="fa fa-clock-o"></i> MON - FRI 09:00 - 17:00 WIB
              </p>
              <h4>Social Medias:</h4>
              <div class="row d-flex align-items-center mb-3">
                <div class="col-2">
                  <a href="https://facebook.com/daysoff.id/"><img src="frontend/images/temp/facebook.png" alt="facebook-logo"></a>
                </div>
                <div class="col-2">
                  <a href="https://instagram.com/daysoff.id/"><img src="frontend/images/temp/instagram.png" alt="instagram-logo"></a>
                </div>
                <div class="col-2">
                  <a href="https://vm.tiktok.com/ZSWPdFm5/"><img src="frontend/images/temp/logo-tiktok.png" alt="tiktok-logo"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>        
@endsection