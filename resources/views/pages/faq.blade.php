@extends('layouts.app')

@section('title')
    Frequently Asked Questions | daysOff Official Website
@endsection

@section('body-class')
    <body class="body">
@endsection

@section('content') 
    <!-- Page Heading/Breadcrumbs -->
    <section class="section-header" style="background-image: url('frontend/images/contact-us/Banner\ Header\ Section.jpg');">
        <div class="container">
        <h1 class="mt-5 mb-3">Frequently Asked Questions
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
            <a href="{{url('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Frequently Asked Questions</li>
        </ol>
        </div>
    </section>
    <body>
        <div id="faq" class="faq-body">
            <div class="faq-header">
                <h3 class="faq-title">General</h3>
                <div class="seperator"></div>
                <div class="faq-list">
                    <div>
                        <details open>
                            <summary>Which style of trip is right for me?</summary>
                            <p class="faq-content">
                                Our three styles of travel – Backpacker Style, Dare to take daysOff (Group
                                Tour) or Private Tour cover a whole travel experience. To find out which one,
                                visit our <a href="{{route('TravelPackagesPage')}}">tour package </a>page.
                            </p>
                        </details>
                    </div>
                    <div>
                        <details>
                            <summary>How can I cancel my trip?</summary>
                            <p class="faq-content">
                                To cancel your trip please read our <a href="{{route('TermsAndConditions')}}">terms & conditions</a> for payment and service.
                                Please contact our team.
                            </p>
                        </details>
                    </div>
                    <div>
                        <details>
                            <summary>Can I get a full refund?</summary>
                            <p class="faq-content">
                                Yes, for several cases we do a full refund. In order to get a full refund please read
                                our <a href="{{route('TermsAndConditions')}}">terms & conditions</a> for payment and service. Please contact our team.
                            </p>
                        </details>
                    </div>
                </div>
            </div>

            <div class="faq-header">
                <h3 class="faq-title">Tours</h3>
                <div class="seperator"></div>
                <div class="faq-list">
                    <div>
                        <details>
                            <summary>How many people will be on my trip?</summary>
                            <p class="faq-content">
                                Good things come in small packages, which is why we keep our group sizes
                                down. This means we’re small enough to remain flexible as we thread our way
                                through communities without intimidating the locals. On most of our trips you’ll 
                                be part of an intimate group of maximum 10 People, though our group sizes are on
                                average 15 until 25 people.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>Who will be my tour leader?</summary>
                            <p class="faq-content">
                                Your tour leader normally consists of 2 people. This will be announced by our
                                team to you, minimum 1 week before departure. You can also choose your tour
                                leaders as long as our tour leaders are available on your trips. If your tour
                                leaders are not available on your day trip, our team will replace and choose it for
                                you.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>What will the food be like on my trip?</summary>
                            <p class="faq-content">
                                Mainly the cuisine will be around asian cuisine, unless you picked the Culinary
                                Tour, which has the overview about the culinary on the package.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>What type of ground transportation is used?</summary>
                            <p class="faq-content">
                                In most cases bus transportation will be used to go from one point to the other.
                                But in some cases when the distance of one point to another is a walking
                                distance, we will walk for an extra sport session. For long distances you will
                                sometimes use a train to travel.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>How do I obtain Essential Trip Information with full details of my trip?</summary>
                            <p class="faq-content">
                                Your tour leader normally consists of 2 people. This will be announced by our
                                team to you, minimum 1 week before departure. You can also choose your tour
                                leaders as long as our tour leaders are available on your trips. If your tour
                                leaders are not available on your day trip, our team will replace and choose it for
                                you.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>When do I need to be at the airport to check in for my flight?</summary>
                            <p class="faq-content">
                                3 hours before your departure flight.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>Do I need travel insurance?</summary>
                            <p class="faq-content">
                                Yes, for every trip you will need travel insurance. The travel insurance will be
                                ensured in our tour package, you will be required to provide us with some data
                                for the travel insurance.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>How do I organise my Visa?</summary>
                            <p class="faq-content">
                                Visa requirements are different for every country, the hyperlink will help you in
                                finding the requirements for the designated trip.
                            </p>
                        </details>
                    </div>

                    <div>
                        <details>
                            <summary>Is travelling with daysOff trustable?</summary>
                            <p class="faq-content">
                                Yes, you can count on us. We will provide the best solution for your safe travels
                                and full of experience.
                            </p>
                        </details>
                    </div>
                </div>
            </div>

            <div class="faq-header">
                <h3 class="faq-title">Covid-19</h3>
                <div class="seperator"></div>
                <div class="faq-list">
                    <div>
                        <details>
                            <summary>Should I wear a mask all the time?</summary>
                            <p class="faq-content">
                                Yes, most of the time it will be required to wear a mask during the tour, but in
                                some cases, it is allowed to not use a mask on the open. The requirement differs
                                from one country to another, be sure to see the requirements in each country.
                            </p>
                        </details>
                    </div>
                    
                    <div>
                        <details>
                            <summary>When can I return to work (or to daily life) after an international trip?</summary>
                            <p class="faq-content">
                                After international travel, sometimes it is required to do a self quarantine, and
                                some only needs to be tested (PCR Test), and when the result is negative, you
                                can return to your daily life. The specific rules are different from every country, be
                                sure to check the rules in the country you’re flying back into (Where your daily life
                                takes place).
                            </p>
                        </details>
                    </div>
                </div>
            </div>
        </div>
    </body>
@endsection