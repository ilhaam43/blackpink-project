<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- By Christofanu D Winoto -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-PRD113RKT7"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-PRD113RKT7');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    @include('includes.style')
  </head>

    @yield('body-class')

    @include('includes.alternate.navbar')

    @yield('content')

    @include('includes.footer')

    @include('includes.script')

  </body>
</html>